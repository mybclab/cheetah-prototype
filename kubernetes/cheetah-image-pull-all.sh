docker pull registry.n3ncloud:5000/cheetah-jupyter:ubuntu16.04-cuda10.0-jupyter5.7.5
docker pull registry.n3ncloud:5000/cheetah-jupyter-tensorflow:ubuntu16.04-cuda10.0-jupyter5.7.5-tensorflow1.12
docker pull registry.n3ncloud:5000/cheetah-jupyter-tensorflow-keras:ubuntu16.04-cuda10.0-jupyter5.7.5-tensorflow1.12-keras2.2
docker pull registry.n3ncloud:5000/cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-cuda10.0-jupyter5.7.5-tensorflow1.12-pytorch1.0
docker pull registry.n3ncloud:5000/cheetah-jupyter-tensorflow-r:ubuntu16.04-jupyter5.7.5-tensorflow1.12-r3.6.1