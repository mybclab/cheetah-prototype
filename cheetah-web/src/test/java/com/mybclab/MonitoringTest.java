package com.mybclab;

import com.google.gson.Gson;
import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.monitoring.web.MonitoringRestController;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class MonitoringTest {

    @Autowired
    private ContainerService containerService;
    @Autowired
    private NodeService nodeService;

    private String GPU_MATRIX_URL = "http://222.231.59.54:9090/api/v1/query_range";

    @Value("${cheetah.kubernetes.prefix}")
    private String prefix;

    @Value("${cheetah.kubernetes.delimiter}")
    private String delimiter;

    private DateTimeFormatter prometheusTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    @Test
    public void cccc() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter prometheusTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        log.debug(">>>>>> now : " + now.format(prometheusTimeFormat));
    }



    @Test
    public void rest() {

        String time = "1554948682.540";
        time = time.replaceAll("[[.]]", "");
//        log.debug(">>>>>> parse double : " + Double.parseDouble(time));
        log.debug(">>>>>>>>> time : " + time);
        long timeLong = Long.parseLong(time);
        log.debug(">>>>>>> parse : " + timeLong);


//        LocalDateTime date = LocalDateTime.from(Instant.ofEpochSecond(Long.parseLong(time)));
        LocalDateTime triggerTime =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(timeLong),
                        TimeZone.getDefault().toZoneId());
//        Date date = new Date(timeLong);
        log.debug(">>>>>>>>> triggerTime : " + triggerTime);
//        log.debug(">>>>>>>>> date : " + date);

//        CollectorRegistry
//        OkHttpClient client = new OkHttpClient();
//        Node node = nodeService.findById(2L);
//        Request request = new Request.Builder().url(GPU_MATRIX_URL_PREFIX + node.getExternalIp() + ":" + GPU_MATRIX_URL_PORT + "/api/").post().build();
//
//        Response response = client.newCall(request).execute();
    }

    @Test
    public void matrix() {
        try {

            String username = "ghkdtkwkd1";
//            username = "goworld1";


            OkHttpClient client = new OkHttpClient();



            String query = "dcgm_gpu_temp{container_name=\"cheetah-ghkdtkwkd1-ypjx7m\"}";
            String start = "2019-04-08T10:10:30.781Z";
            String end = "2019-04-08T10:11:30.781Z";
            String step = "15s";

            query = "dcgm_gpu_temp{container_name=~\"cheetah-latte-.*\"}";
            query = "dcgm_gpu_temp";
            start = "2019-04-11T01:00:30.781Z";
            end = "2019-04-11T10:10:30.781Z";
            step = "60s";

            LocalDateTime now = LocalDateTime.now(Clock.systemUTC());
            start = now.minusMinutes(5).format(prometheusTimeFormat);
            end = LocalDateTime.now().format(prometheusTimeFormat);

            String url = GPU_MATRIX_URL + "?query=" + query + "&start=" + start + "&end=" + end + "&step=" + step;
            log.debug(">>> url : " + url);

//            MediaType mediaType = MediaType.parse("application/json");
//            RequestBody requestBody = RequestBody.create(mediaType, "");

            Request request = new Request.Builder().url(url)
                    .get()
//                    .post(requestBody)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache").build();
            Response response = client.newCall(request).execute();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(response.body().byteStream()));
            String inputLine;
            StringBuffer repStr = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                repStr.append(inputLine);
            }
            in.close();

            log.debug(">>>>>> response : {}", repStr);
//
//            Gson gson = new Gson();
//            MonitoringRestController.PrometheusResultDto resultDto = gson.fromJson(repStr.toString(), MonitoringRestController.PrometheusResultDto.class);
//            log.debug(">>>>>> resultDto : {}", resultDto);
//            log.debug(">>>>>> resultDto : {}", resultDto.getData());
//
//            List<MonitoringRestController.PrometheusResultDto.DataRetulsDto> gpuList = resultDto.getData().getResult();
//            log.debug(">>>>>> gpuList : {}, {} ", gpuList.size() + ", " + gpuList);
//
//            String[][] series = null;
//            String[] labels = null;
//            for (int i = 0; i < gpuList.size(); i++) {
//                MonitoringRestController.PrometheusResultDto.DataRetulsDto dataRetulsDto = gpuList.get(i);
////                dataRetulsDto.getMetric();
//                List<List<String>> values = dataRetulsDto.getValues();
//                if (i == 0) {
//                    series = new String[gpuList.size()][values.size() + 1];
//                    labels = new String[values.size() + 1];
//                }
//                log.debug(">>>>>>> values {} {}", values.size(), values );
//                for (int j = 0; j < values.size(); j++) {
//                    if (i == 0) {
//                        String timestamp = values.get(j).get(0);
//                        timestamp = timestamp.replaceAll("[[.]]", "");
//                        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(timestamp)), TimeZone.getDefault().toZoneId());
//                        DateTimeFormatter timeFormmater = DateTimeFormatter.ofPattern("hh:mm");
//                        labels[j] = date.format(timeFormmater);
//                        log.debug(" set label : {} {}", j, date.format(timeFormmater));
//                    }
//                    series[i][j] = values.get(j).get(1);
//                }
//            }
//
//            log.debug(">>>>>>>>>> labels {}", labels);
//            log.debug(">>>>>>>>>> series {}", series);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
