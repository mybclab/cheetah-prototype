package com.mybclab.management.cloud;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;
import com.mybclab.cheetah.management.cloud.rabbitmq.properties.CloudPropertiesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@TestPropertySource(properties = {"spring.config.location=classpath:application.yml"})
public class CloudPropertiesTest {
	
	@Autowired
	AutowireCapableBeanFactory autowireCapableBeanFactory;
	
    private CloudPropertiesService cloudProperties;
    private CLOUD_VENDOR cloud;

	@Before
	public void before() {
		cloud = CLOUD_VENDOR.TENCENT;
	}
    
	@Test
	public void getCloudProperties() {

        try {
			cloudProperties = (CloudPropertiesService) Class.forName(cloud.getClassName()).newInstance();
            autowireCapableBeanFactory.autowireBean(cloudProperties);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.debug("cloudProperties.getCloud : {}",								cloudProperties.getCloud()); 
        log.debug("cloudProperties.getFileUploadPath : {}", 				cloudProperties.getFileUploadPath()); 
        
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.debug("cloudProperties.getUserName : {}", 						cloudProperties.getUserName()); 
        log.debug("cloudProperties.getPrivateKey : {}", 						cloudProperties.getPrivateKey()); 
        log.debug("cloudProperties.getPrivateKeyFilePath : {}", 			cloudProperties.getPrivateKeyFilePath());
        
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"); 
        log.debug("cloudProperties.getNodeInstallFilePath : {}", 			cloudProperties.getNodeInstallFilePath());   
        log.debug("cloudProperties.getNodeInstallFileName : {}", 		cloudProperties.getNodeInstallFileName()); 
        log.debug("cloudProperties.getNodeInstallShellFile : {}", 			cloudProperties.getNodeInstallShellFile()); 
        
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.debug("cloudProperties.getMountShellFile : {}", 				cloudProperties.getMountShellFile());         
        log.debug("cloudProperties.getVolumeName : {}", 					cloudProperties.getVolumeName()); 
        log.debug("cloudProperties.getVolumeMountFileName : {}", 	cloudProperties.getVolumeMountFileName()); 
        log.debug("cloudProperties.getVolumeMountFilePath : {}", 		cloudProperties.getVolumeMountFilePath());  
	}
	
	@After
	public void after() {
	}
}
