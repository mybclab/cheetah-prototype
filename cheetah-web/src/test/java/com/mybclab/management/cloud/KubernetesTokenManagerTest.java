package com.mybclab.management.cloud;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.management.cloud.rabbitmq.kubernetes.KubernetesTokenManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles({"local", "cloud"})
public class KubernetesTokenManagerTest {
	
	@Autowired 
	KubernetesTokenManager kubernetesTokenManager;
	
	private long startTime = 0;
	private long endTime = 0;

	@Before
	public void before() {
		startTime = System.currentTimeMillis();
	}

	
	@Test
	public void getTokenTest() {
		kubernetesTokenManager.getToken();	
	}
	
	@Test
	public void getSha256Test() {	
		kubernetesTokenManager.getSha256();		
	}
	
	@After
	public void after() {
		endTime = System.currentTimeMillis();
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.SSS", Locale.KOREA);

		log.debug(">> test start datetime : {}", simpleDateFormat.format(new Date(startTime)));
		log.debug(">> test  end datetime : {}", simpleDateFormat.format(new Date(endTime)));
	}
}
