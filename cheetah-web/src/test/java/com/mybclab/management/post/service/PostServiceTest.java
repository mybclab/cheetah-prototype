package com.mybclab.management.post.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.management.post.domain.Post;
import com.mybclab.cheetah.management.post.domain.PostComment;
import com.mybclab.cheetah.management.post.domain.PostCommentPK;
import com.mybclab.cheetah.management.post.dto.PostCommentForm;
import com.mybclab.cheetah.management.post.dto.PostCriteria;
import com.mybclab.cheetah.management.post.dto.PostForm;
import com.mybclab.cheetah.management.post.service.PostCommentService;
import com.mybclab.cheetah.management.post.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class PostServiceTest {

    @Autowired
    private PostService postService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PostCommentService postCommentService;

    @Test
    public void post() {
        assertNotNull(postService);


        PostForm postForm = new PostForm();
        postForm.setTitle("한국대학교 공지사항 제목");
        postForm.setContents("한국대학교 공지사항 내용");
        postForm.setUseYn(true);

        Post post = modelMapper.map(postForm, Post.class);
        post.setBoardTypeCode(Code.BOARD_TYPE.QNA);
        Post savedPost = postService.save(post);

        log.debug("savedPost : {}", savedPost);


        Post findedPost = postService.findById(post.getPostSn());
        log.debug("findedPost : {}", findedPost);
        log.debug("getPostCommentList : {}", findedPost.getPostCommentList());


        //postService.delete(post.getPostSn());

        PostCriteria postCri = PostCriteria.builder().boardTypeCode(Code.BOARD_TYPE.QNA).title("제").useYn(true).build();
        Pageable pageRequest = PageRequest.of(0, 3, Sort.Direction.DESC, "createdAt");

        Page<Post> page = postService.findAll(postCri, pageRequest);
        log.debug("page content : {} " + page.getContent());
        log.debug("page size : {} " + page.getContent().size());


    }

    @Test
    public void comment() {

        assertNotNull(postCommentService);
        Long postSn = 4L;

        String username = "latte";

        PostCommentForm postCommentForm = new PostCommentForm();
        postCommentForm.setPostSn(postSn);
        postCommentForm.setContents("우와 댓글을 달아본다 정말 화면 그리기 쉽지 ㅇ낳다.");
        postCommentForm.setUseYn(true);

        PostCommentPK postCommentPK = modelMapper.map(postCommentForm, PostCommentPK.class);
        PostComment postComment = modelMapper.map(postCommentForm, PostComment.class);
        postComment.setPostCommentPK(postCommentPK);
        postComment.setCreatedBy(username);
        log.debug(">>>>>>>> postComment: {}", postComment);
        postCommentService.save(postComment);

        List<PostComment> postCommentList = postCommentService.findAllByPostSn(postSn);
        assertNotNull(postCommentList);

        log.debug(">>>>>>>> postCommentList: {}", postCommentList);




    }
}
