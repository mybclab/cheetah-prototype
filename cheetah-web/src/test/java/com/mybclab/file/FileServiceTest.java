package com.mybclab.file;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.common.file.domain.DbFile;
import com.mybclab.cheetah.common.file.service.FileService;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class FileServiceTest {

    @Value("${spring.servlet.multipart.location}")
    private String BASE_PATH;
    @Autowired
    private FileService fileService;

    @Test
    public void filePath() {
        String path = BASE_PATH + File.separator + LocalDate.now().getYear() + File.separator + StringUtils.leftPad(Integer.toString(LocalDate.now().getMonthValue()), 2, "0");
        log.debug("path : " + path);
    }

    @Test
    public void fileExtension() {
        String fileName = "filename.txt";
        log.debug("> extension : " + (fileName.contains(".") ? fileName.split("[.]")[1] : ""));
        fileName = "filename";
        log.debug("> extension : " + (fileName.contains(".") ? fileName.split("[.]")[1] : ""));
    }


    @Test
    public void upload() {


            MultipartFile multipartFile = new MockMultipartFile("files", "filename.txt", "text/plain", "hello world".getBytes(StandardCharsets.UTF_8));



            DbFile dbFile = fileService.uploadFile(multipartFile, "admin");

            log.debug(">>>>>> dbFile {}", dbFile);

    }
}
