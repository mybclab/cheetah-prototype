package com.mybclab;

import com.mybclab.common.utils.DateUtils;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.TimeZone;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
public class CommonTest {

    @Test
    public void memorySize() {
        int systemMemory = 128;
        int groupSize = 30;

        double memorySize = Math.floor((systemMemory * 1000.0) / groupSize);
        log.debug(">>>>> memorySize : " + memorySize);

        systemMemory = 64;
        int cpuCore = 12;
        memorySize = Math.floor((systemMemory * 1000.0) / (cpuCore-2));
        log.debug(">>>>> memorySize : " + memorySize);

        int shareSize = 4;
        systemMemory = 64;
        cpuCore = 24;

        memorySize = (int) Math.floor((((systemMemory * 1000.0) - 50) / (cpuCore - 2)) * shareSize);
        log.debug(">>>>> memorySize : " + memorySize);

    }

    @Test
    public void container() {

        String username = "coffee";
        String[] targetUsername = {"coffee", "ngkim0721", "mybclab"};
        Assert.assertTrue(Arrays.asList(targetUsername).contains(username));
        username = "latte";
        Assert.assertFalse(Arrays.asList(targetUsername).contains(username));
        username = "ngkim0721";
        Assert.assertTrue(Arrays.asList(targetUsername).contains(username));
    }

    @Test
    public void limit() {
        int cpuCore = 32;
        int systemMemory = 244;
        int countOfGroupContainers = 33;
        int cpuLimit = (cpuCore * 1000) / countOfGroupContainers;
        int memoryLimit = systemMemory / countOfGroupContainers;

        Assert.assertEquals(7, memoryLimit);
        Assert.assertEquals(969, cpuLimit);
    }

    @Test
    public void testss() {

        String time = "2019-03-22T15:14:13.000+09:00";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        LocalDateTime dateTime = LocalDateTime.parse(time, dateTimeFormatter);

        log.debug("> dateTime : " + dateTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        DateTime dateTime1 = new DateTime(time);
        LocalDateTime parse = LocalDateTime.parse(dateTime1.toString(), dateTimeFormatter);


        log.debug("> LocalDateTime.parse(dateTime1.toString(), dateTimeFormatter) : " + parse);
    }

    @Test
    public void howlong() {
        String time = "2019-06-25T18:14:13.000+09:00";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        LocalDateTime dateTime = LocalDateTime.parse(time, dateTimeFormatter);

        LocalDateTime now = LocalDateTime.now();

        long diff = ChronoUnit.NANOS.between(dateTime, now);

        long betweenDays = ChronoUnit.DAYS.between(dateTime.toLocalDate(), now.toLocalDate());
        log.debug(">>>>>>>> betweenDays : " + betweenDays);

        Duration duration = Duration.between(dateTime, now);
        Period period = Period.between(dateTime.toLocalDate(),now.toLocalDate());



        log.debug(">>>> duration: " + duration);
        log.debug(">>>> duration: " + duration.getUnits());
        log.debug(">>>> period: " + period);
        log.debug(">>>> period: " + period.getYears());
        log.debug(">>>> period: " + period.getMonths());
        log.debug(">>>> period: " + period.getDays());
        log.debug(">>>> period: " + period.getUnits());
//        log.debug(">>>> duration: year:  " + duration.get(ChronoUnit.YEARS));
//        log.debug(">>>> duration: month:  " + duration.get(ChronoUnit.MONTHS));

    }


    @Test
    public void random() {
        String str = StringUtils.randomAlphanumeric(20);
        log.debug(">>>>>>>>>> str : " + str);

        for (int i = 0; i < 100; i++){
            String random = StringUtils.randomAlphanumeric(20);
            log.debug(random);
            Assert.assertTrue(!random.contains("i"));
            Assert.assertTrue(!random.contains("I"));
            Assert.assertTrue(!random.contains("l"));
            Assert.assertTrue(!random.contains("L"));
            Assert.assertTrue(!random.contains("0"));
            Assert.assertTrue(!random.contains("o"));
            Assert.assertTrue(!random.contains("O"));
        }


    }

}
