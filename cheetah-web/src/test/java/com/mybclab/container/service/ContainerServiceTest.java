package com.mybclab.container.service;


import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.dto.ContainerCriteria;
import com.mybclab.cheetah.container.dto.ContainerForm;
import com.mybclab.cheetah.container.repository.ContainerHistoryRepository;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.container.service.ContainerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class ContainerServiceTest {

    @Autowired
    private ContainerRepository containerRepository;
    @Autowired
    private ContainerHistoryRepository containerHistoryRepository;
    @Autowired
    private ContainerService containerService;
    @Autowired
    private ContainerHistoryService containerHistoryService;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void 남은그룹사용자() {
        String containerId = "hagtsz";

        Container container = containerService.findByContainerId(containerId);
        containerService.setExtraInfo(container);

        List<User> userList = container.getUser().getGroupUsers();
        log.debug(">>>>>>>> userList.size() : " + userList.size());

        container.getGroupTargetUserList().forEach(c -> {
            log.debug(" getGroupTargetUserList : " + c.getUsername());
            userList.removeIf(u -> u.getUsername().equals(c.getUsername()));
        });

//        userList.removeAll(container.getGroupTargetUserList());
//        List<User> subtractUserList = (List<User>) CollectionUtils.subtract(userList, container.getGroupTargetUserList());
        userList.forEach(u -> {
            log.debug("subtractUserList : " + u.getUsername());

        });
        log.debug(">>>>>>>> subtractUserList.size() : " + userList.size());


        log.debug(">>>>>>>> container.getRemainContainerSize() : " + container.getRemainContainerSize());
    }

    @Test
    public void 계산() {
        String containerId = "jdmwcq";

        Container container = containerService.findByContainerId(containerId);


        log.debug("cpu core : " + container.getGroupSharedSize());
        log.debug("memory size : " + container.getGroupSharedSize() * 4 * 600);


        Container container2 = containerService.findByInstanceId("instanceid");

        log.debug("group size : " + container2.getGroupContainerListSize());

    }

    @Test
    public void 그룹사용자_컨테이너_일괄생성() {
        String username = "coffee";
        String containerId = "mbeb6p";

        Container container = containerService.findByContainerId(containerId);

        containerService.copyContainerToGroupUsers(container);

        List<Container> groupContainerList = containerService.findAllByParentContainerId(container.getContainerPK().getContainerId());
        for (Container groupContainer : groupContainerList) {
            log.debug("################### groupContainer : " + groupContainer.getContainerPK() + "," + groupContainer.getGroupSharedYn() + "," + groupContainer.getGroupSharedSize() + "," + groupContainer.getGpu());
        }
    }

    @Test
    public void 월별_컨테이너_사용량() {
        String groupId = "korea_university";
        String targetYearMonth = "201901";
        int term = 6;

        YearMonth yearMonth = YearMonth.parse(targetYearMonth, CheetahFormatter.FOR_YEAR_MONTH);
        yearMonth = yearMonth.minusMonths(term - 1);

        Map<String, Integer> monthlyContainerMap = new HashMap<>();
        for (int i = 0; i < term ; i++) {
            log.debug(">>>>>>>> yearMonth: {}", yearMonth);

            List<ContainerHistory> containerList = containerHistoryRepository.findAllByMonthlyForGroup(groupId, yearMonth.toString());
            log.debug(">>>>>>>> containerList: {}", containerList);
            monthlyContainerMap.put(yearMonth.toString(), containerList.size());
            yearMonth = yearMonth.plusMonths(1);
        }
        log.debug(">>>>>>>> monthlyContainerMap: {}", monthlyContainerMap);

        // to json

    }

    @Test
    public void 결제대상_컨테이너_목록_금액계산() {
        String groupId = "korea_university";
        String targetYearMonth = "201901";
        List<ContainerHistory> containerList = containerHistoryService.findAllByMonthly(groupId, targetYearMonth, true);
        log.debug("containerList : {} " + containerList);
        log.debug("containerList size : {} " + containerList.size());

//        yyyyMM = "2018-12";
        Double totalPaymentAmount = containerList.stream().mapToDouble(ContainerHistory::getMonthlyAmount).sum();
        int totalUseQuantity = containerList.stream().mapToInt(ContainerHistory::getGpuQuantity).sum();
        log.debug(" > > totalPaymentAmount : " + totalPaymentAmount);
        log.debug(" > > totalUseQuantity : " + totalUseQuantity);
    }

    @Test
    public void 컨테이너테스트() {
        assertNotNull(containerService);
        ContainerCriteria criteria = ContainerCriteria.builder()
                .username("korea_professor")
                .containerId("server")
                .build();

        Pageable pageeRequest = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        Page<Container> containerPage = containerService.findAll(criteria, pageeRequest);
        log.debug("containerPage content : {} " + containerPage.getContent());
        log.debug("containerPage size : {} " + containerPage.getContent().size());

        ContainerForm containerForm = new ContainerForm();
        containerForm.setContainerId("gpu-server-pro");
        String username = "korea_student";
        containerForm.setUsername(username);

        containerForm.setGpuSn(1L);
        containerForm.setDescription("");
        containerForm.setGpuQuantity(5);


        ContainerPK containerPK = modelMapper.map(containerForm, ContainerPK.class);
        Container container = modelMapper.map(containerForm, Container.class);
        container.setContainerPK(containerPK);

        Container savedContainer = containerService.save(container);

        assertNotNull(savedContainer);

        log.debug("savedContainer: {}", savedContainer);
        log.debug("savedContainer gpu: {}", savedContainer.getGpu());
        log.debug("savedContainer node: {}", savedContainer.getNode());
        log.debug("savedContainer user: {}", savedContainer.getUser());


        //containerService.delete(savedContainer.getContainerPK());


        List<Container> myContainerList = containerService.findAllByUsername(username);
        log.debug(" my list : {}", myContainerList);



    }

    @Test
    public void PersistentVolumeClaim() {
    }
}
