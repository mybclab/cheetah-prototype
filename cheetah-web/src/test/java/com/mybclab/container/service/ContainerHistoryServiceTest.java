package com.mybclab.container.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.domain.ContainerHistoryPK;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.repository.ContainerHistoryRepository;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.container.service.ContainerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class ContainerHistoryServiceTest {

    @Autowired
    private ContainerService containerService;
    @Autowired
    private ContainerHistoryService containerHistoryService;

    @Test
    public void save() {
        Container container = containerService.findById(new ContainerPK("latte", "mh3agf"));
        ContainerHistory containerHistory = new ContainerHistory();
        containerHistory.setContainerHistoryPK(new ContainerHistoryPK(container.getContainerPK()));
        containerHistory.setGpuSn(container.getGpuSn());
        containerHistory.setGpuQuantity(container.getGpuQuantity());
        containerHistory.setStartDate(LocalDateTime.now());
        containerHistory.setReturnDate(container.getReturnDate());
        containerHistoryService.save(containerHistory);

        log.debug(">>>>>>>> containerHistory : {}", containerHistory);

    }

}
