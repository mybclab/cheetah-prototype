package com.mybclab.batch;

import com.mybclab.cheetah.CheetahApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CheetahApplication.class)
@ActiveProfiles({"local", "batch"})
@Slf4j
public class BatchTest {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job paymentMonthlyBatchJob;


    @Autowired
    private Job retryFailedPaymentBatchJob;


    @Test
    public void paymentMonthlyBatch() throws Exception {
        JobParameters jobParameters = new JobParameters();

        jobLauncher.run(paymentMonthlyBatchJob, jobParameters);
    }


    @Test
    public void retryFailedPaymentBatch() throws Exception {
        JobParameters jobParameters = new JobParameters();

        jobLauncher.run(retryFailedPaymentBatchJob, jobParameters);
    }
}
