package com.mybclab.payment.service;


import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.payment.service.IamportService;
import com.siot.IamportRestClient.IamportClient;
import com.siot.IamportRestClient.exception.IamportResponseException;
import com.siot.IamportRestClient.request.AgainPaymentData;
import com.siot.IamportRestClient.response.AccessToken;
import com.siot.IamportRestClient.response.IamportResponse;
import com.siot.IamportRestClient.response.Payment;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@AutoConfigureMockMvc
@Slf4j
public class IamportTest {

    IamportClient client;

    @Autowired
    IamportService iamportService;

    @Autowired
    UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private IamportClient getNaverTestClient() {
        String test_api_key = "5978210787555892";
        String test_api_secret = "9e75ulp4f9Wwj0i8MSHlKFA9PCTcuMYE15Kvr9AHixeCxwKkpsFa7fkWSd9m0711dLxEV7leEAQc6Bxv";

        return new IamportClient(test_api_key, test_api_secret);
    }

    @Before
    public void setup() {
        String test_api_key = "8375378768244577";
        String test_api_secret = "u8czMJNC3Aaon264RnukXg30AMp7DsQ8A3HmMuZQ59esclw3I1nGirHTe71RzSZ9VHvHbvxhEMJQ8gUQ";
        client = new IamportClient(test_api_key, test_api_secret);
    }

    @Test
    public void testGetToken() {
        try {
            IamportResponse<AccessToken> auth_response = client.getAuth();
            log.debug("res : " + auth_response.getResponse().getToken());
            assertNotNull(auth_response.getResponse());
            assertNotNull(auth_response.getResponse().getToken());

//            AgainPaymentData againPaymentData = new AgainPaymentData();

//            client.againPayment();
        } catch (IamportResponseException e) {
            log.debug(e.getMessage());

            switch(e.getHttpStatusCode()) {
                case 401 :
                    break;
                case 500 :
                    break;
            }
        } catch (IOException e) {
            //서버 연결 실패
            e.printStackTrace();
        }
    }

    @Test
    public void customer_uid() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("cardNo", "4364-2006-6934-8110");
        params.add("cardExpiry", "0120");
        params.add("cardBirth", "850131");
        params.add("cardPassword", "60");

        MockHttpServletRequestBuilder request = post("/iamport/customer-uid").params(params);

        mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testSubstring() {

        String cardNo = "1111-2222-3333-1234";
        log.debug(">> substr : " + cardNo.substring(cardNo.length() - 4));
    }


	@Test
	public void testAgainPayment() {
        String username = "korea_university";
        User user = userService.findById(username);

		String customer_uid = "HGUDIEUB-8110";
        String paymentId = "HGUDIEUB1";
        paymentId = "11111111";
        BigDecimal amount = BigDecimal.valueOf(100);

        AgainPaymentData again_data = new AgainPaymentData(customer_uid, paymentId, amount);
        again_data.setName("2019-02 gpu cloud 정기결제");
        again_data.setBuyerName(user.getName());
        again_data.setBuyerEmail(user.getEmail());
        again_data.setBuyerTel(user.getPhoneNo());
        IamportResponse<Payment> response = iamportService.postSubscribePaymentsAgain(again_data);

        log.debug("response {}", response);
        log.debug("response.getCode() {}", response.getCode());
        log.debug("response.getMessage() {}", response.getMessage());
        log.debug("response.getResponse() {}", response.getResponse());
	}

    //	@Test
//	public void testOnetimePayment() {
//		CardInfo card = new CardInfo("1234123412341234", "201901", "801231", "00");
//		OnetimePaymentData onetime_data = new OnetimePaymentData(getRandomMerchantUid(), BigDecimal.valueOf(1004), card);
//		onetime_data.setName("ActiveX없는결제테스트");
//		onetime_data.setBuyerName("구매자");
//		onetime_data.setBuyerEmail("iamport@siot.do");
//		onetime_data.setBuyerTel("16705176");
//
//		IamportResponse<Payment> payment_response = client.onetimePayment(onetime_data);
//		assertEquals(payment_response.getResponse().getStatus(), "paid");
//	}
//
//
//	@Test
//	public void testSubscribeScheduleAndUnschedule() {
//		String test_customer_uid = "customer_123456";
//		ScheduleData schedule_data = new ScheduleData(test_customer_uid);
//
//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.OCTOBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d1 = cal.getTime();
//
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.NOVEMBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d2 = cal.getTime();
//
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.DECEMBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d3 = cal.getTime();
//
//		schedule_data.addSchedule(new ScheduleEntry(getRandomMerchantUid(), d1, BigDecimal.valueOf(1004)));
//		schedule_data.addSchedule(new ScheduleEntry(getRandomMerchantUid(), d2, BigDecimal.valueOf(1005)));
//		schedule_data.addSchedule(new ScheduleEntry(getRandomMerchantUid(), d3, BigDecimal.valueOf(1006)));
//
//		System.out.println("예약 요청");
//		IamportResponse<List<Schedule>> schedule_response = client.subscribeSchedule(schedule_data);
//
//		List<Schedule> schedules = schedule_response.getResponse();
//		List<ScheduleEntry> req_schedules = schedule_data.getSchedules();
//
//		for (int i = 0; i < 3; i++) {
//			assertEquals(schedules.get(i).getCustomerUid(), test_customer_uid);
//			assertEquals(schedules.get(i).getMerchantUid(), req_schedules.get(i).getMerchantUid());
//			assertDateEquals(schedules.get(i).getScheduleAt(), req_schedules.get(i).getScheduleAt());
//			assertEquals(schedules.get(i).getAmount(), req_schedules.get(i).getAmount());
//		}
//
//		try {
//			//1초 후 등록된 예약 unschedule by multiple merchant_uid
//			Thread.sleep(1000);
//			System.out.println("복수 merchant_uid 예약 취소 요청");
//			UnscheduleData unschedule_data = new UnscheduleData(test_customer_uid);
//			unschedule_data.addMerchantUid( req_schedules.get(0).getMerchantUid() );
//			unschedule_data.addMerchantUid( req_schedules.get(2).getMerchantUid() );
//
//			IamportResponse<List<Schedule>> unschedule_response = client.unsubscribeSchedule(unschedule_data);
//			List<Schedule> cancelled_schedule = unschedule_response.getResponse();
//
//			assertNotNull(cancelled_schedule);
//			assertEquals(cancelled_schedule.get(0).getMerchantUid(), req_schedules.get(0).getMerchantUid());
//			assertEquals(cancelled_schedule.get(1).getMerchantUid(), req_schedules.get(2).getMerchantUid());
//
//			//1초 후 등록된 예약 unschedule by single multiple_uid
//			Thread.sleep(1000);
//			System.out.println("단일 merchant_uid 예약 취소 요청");
//			unschedule_data = new UnscheduleData(test_customer_uid);
//			unschedule_data.addMerchantUid( req_schedules.get(1).getMerchantUid());
//
//			unschedule_response = client.unsubscribeSchedule(unschedule_data);
//			cancelled_schedule = unschedule_response.getResponse();
//
//			assertNotNull(cancelled_schedule);
//			assertEquals(cancelled_schedule.get(0).getMerchantUid(), req_schedules.get(1).getMerchantUid());
//
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Test
//	public void testSubscribeDuplicatedSchedule() {
//		String test_customer_uid = "iamportjangbora";
//		ScheduleData schedule_data = new ScheduleData(test_customer_uid);
//
//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.OCTOBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d1 = cal.getTime();
//
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.NOVEMBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d2 = cal.getTime();
//
//		cal.set(Calendar.YEAR, 2018);
//		cal.set(Calendar.MONTH, Calendar.DECEMBER);
//		cal.set(Calendar.DAY_OF_MONTH, 25);
//		Date d3 = cal.getTime();
//
//		schedule_data.addSchedule(new ScheduleEntry("scheduled_merchant_1$$$", d1, BigDecimal.valueOf(1004)));
//		schedule_data.addSchedule(new ScheduleEntry("scheduled_merchant_2$$$", d2, BigDecimal.valueOf(1005)));
//		schedule_data.addSchedule(new ScheduleEntry("scheduled_merchant_3$$$", d3, BigDecimal.valueOf(1006)));
//
//		IamportResponse<List<Schedule>> schedule_response = client.subscribeSchedule(schedule_data);
//
//		assertEquals(1, schedule_response.getCode()); //중복된 merchant_uid이므로 schedule에 실패함
//	}

}
