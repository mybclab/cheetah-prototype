package com.mybclab.payment.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.repository.PaymentRepository;
import com.mybclab.cheetah.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.YearMonth;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class PaymentServiceTest {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private UserCardService userCardService;

    @Autowired
    private UserCreditService userCreditService;

    @Autowired
    private ContainerService containerService;

    @Test
    public void 목록테스트() {
        String groupId = "korea_university";
        Pageable pageeRequest = PageRequest.of(0, 10, Sort.Direction.DESC, "paymentDate");
        Page<Payment> containerPage = paymentService.findAll(groupId, pageeRequest);
        log.debug(" containerPage : {}", containerPage);
        log.debug(" getContent : {}", containerPage.getContent());
    }

    @Test
    public void 결제_테스트() {
        assertNotNull(paymentService);

        // params
        String groupId = "coffee";
        YearMonth yearMonth = YearMonth.now().minusMonths(1);
        String targetYearMonth = CheetahFormatter.FOR_YEAR_MONTH.format(yearMonth);
        targetYearMonth =  "201906";

        Payment savedPayment = paymentService.paymentMonthly(groupId, targetYearMonth, true);
        log.debug(" saved Payment : {}", savedPayment);
        if (savedPayment != null) {
            log.debug(" saved Payment getTargetYearMonth : {}", savedPayment.getTargetYearMonth());
            log.debug(" saved Payment creditPayment : {}", savedPayment.getUserCreditPaymentList());
            log.debug(" saved Payment cardPayment : {}", savedPayment.getUserCardPaymentList());
        }

    }


    @Test
    public void retryPayment() {
        List<Payment> paymentList = paymentRepository.findAllForRetryPayment(Code.PAYMENT_STATUS.FAILED);
        log.debug(">>>>>>>>> size "+ paymentList.size());
        log.debug(">>>>>>>>> {}", paymentList);

        paymentService.retryFailedPayment();
    }
}
