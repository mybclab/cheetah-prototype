package com.mybclab.kubernetes;


import com.google.gson.reflect.TypeToken;
import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.pod.service.PodFormatService;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.exception.KubernetesException;
import com.mybclab.cheetah.kubernetes.service.KubernetesAPIService;
import com.mybclab.cheetah.kubernetes.service.KubernetesYamlService;
import io.kubernetes.client.*;
import io.kubernetes.client.apis.AppsV1Api;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.models.*;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;
import io.kubernetes.client.util.Yaml;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
//@AutoConfigureMockMvc
@Slf4j
public class KubernetesTest {

    @Autowired
    KubernetesAPIService kubernetesAPIService;

    @Autowired
    ContainerService containerService;

    @Autowired
    PodFormatService podFormatService;

    @Autowired
    KubernetesYamlService kubernetesYamlService;

    @Value("${cheetah.kubernetes.DELIMITER}")
    private static final String DELIMITER = "-";

    @Value("${cheetah.kubernetes.master-node-ip}")
    private String MASTER_IP;
    @Value("${cheetah.kubernetes.bearer-token}")
    private String BEARER_TOKEN;

//    @Value("${cheetah.default.domain}")
    private String domain;

    @Value("${cheetah.kubernetes.namespace}")
    private String namespace;

    ApiClient client = null;
    CoreV1Api api= null;
    AppsV1Api appsV1Api = null;

    @Before
    public void before() throws IOException, URISyntaxException {
//        client = Config.defaultClient();
        client = Config.fromToken("https://" + MASTER_IP + ":6443", BEARER_TOKEN, false);

        JSON json = new JSON();
        json.setLenientOnJson(true);
//        client.setJSON(json);
//        log.debug("client : " + client);
//        client.setBasePath()
//        client.setUsername("cheetah-admin");
//        client.setAccessToken();
//        client.setDebugging(true);
        Configuration.setDefaultApiClient(client);

        api = new CoreV1Api();
        appsV1Api = new AppsV1Api();


    }



    @Test
    public void patchNode() throws ApiException {

        String name = "cpu-node";
//        String body = "{\"metadata\":{\"labels\":{\"test\":\"good\"}}}";

        V1Node v1Node = new V1Node();
        V1ObjectMeta v1ObjectMeta = new V1ObjectMeta();
        Map<String, String> labels = new HashMap<String, String>() {{
            put("app", "good");
        }};
        v1ObjectMeta.setLabels(labels);
        v1Node.setMetadata(v1ObjectMeta);

        Map<String, String> beforeLabels = v1Node.getMetadata().getLabels();
        log.debug(">>>beforeLabels : " + beforeLabels);

        V1Node patchedNode = api.patchNode(name, new ArrayList<V1Node>() {{
            add(v1Node);
        }}, null, null);

        Map<String, String> patchedLabels = patchedNode.getMetadata().getLabels();
       log.debug(">>>patchedLabels : " + patchedLabels);
       log.debug(">>>patchedLabels.app : " + patchedLabels.get("app"));
    }

    @Test
    public void none() {

    }

    @Test
    public void toyaml() {
        String token = "aaaa";
        String label = "lalalalala";
        String containerId = "containerId";

        PodFormat podFormat = podFormatService.findById(1L);

        String deploymentYaml = kubernetesYamlService.findById(Kubernetes.KIND.DEPLOYMENT).getYaml();
        log.debug(">>>>>>>>>>> before deploymentYaml : " + deploymentYaml);
        deploymentYaml = deploymentYaml.replaceAll("\\{token\\}", token);
        deploymentYaml = deploymentYaml.replaceAll("\\{label\\}", label);
        deploymentYaml = deploymentYaml.replaceAll("\\{gpuQuantity\\}", Integer.toString(1));
        deploymentYaml = deploymentYaml.replaceAll("\\{jupyterPath\\}", "/cheetah/jupyter/" + containerId);

        log.debug(">>>>>>>>>>> after deploymentYaml : " + deploymentYaml);
        Yaml yaml = new Yaml();
        V1Deployment deployment = yaml.loadAs(new StringReader(deploymentYaml), V1Deployment.class);
        log.debug(">>>>>>>>> deploymentYaml to deployment: " + deployment);


        String serviceYaml = kubernetesYamlService.findById(Kubernetes.KIND.SERVICE).getYaml();
        log.debug(">>>>>>>>>>> before serviceYaml : " + serviceYaml);
        serviceYaml = serviceYaml.replaceAll("\\{serviceName\\}", label);
        serviceYaml = serviceYaml.replaceAll("\\{label\\}", label);


        log.debug(">>>>>>>>>>> after serviceYaml : " + serviceYaml);
        V1Service service = yaml.loadAs(new StringReader(serviceYaml), V1Service.class);
        log.debug(">>>>>>>>> serviceYaml to service: " + service);
    }


    @Test
    public void k8s() throws ApiException {
        log.debug("config : " + Configuration.getDefaultApiClient().getBasePath());
        log.debug("config : " + Configuration.getDefaultApiClient().toString());
        log.debug("config : " + Configuration.getDefaultApiClient().getJSON());
        log.debug("api : " + api);
        V1NodeList nodeList = api.listNode(null, null, null, null, null, null, null, null, null);
//        V1NodeList nodeList = api.listNode(true, null, null, null, null, 100, null, 100, false);
        log.debug("nodeList : " + nodeList);
//        List<V1Node> items = nodeList.getItems();
//        for (V1Node node : items) {
////            V1ObjectMeta metadata = node.getMetadata();
////                    node.getStatus()
//        }

//        String podLog = api.readNamespacedPodLog("cheetah-ghkdtkwkd1-ypjx7m-644c9987b8-bckfh", namespace, null, null, null, null, null, null, null, null);
//        log.debug("podLog : " + podLog);


//        String podCommandLogs = api.connectPostNamespacedPodExec("cheetah-ghkdtkwkd1-ypjx7m-644c9987b8-bckfh", namespace, "nvidia-smi", null, true, true, true, null);
//        log.debug("podLog : " + podCommandLogs);
//        String summary = api.connectGetNodeProxyWithPath("ubuntu4", "stats", "summary");
//        log.debug(">>>>>>>> summary : {}", summary);

//        V1Node status = api.readNodeStatus("ubuntu4", "true");
//        log.debug(">>>>>>>> status : {}", status);


//        V1Node ubuntu4 = kubernetesAPIService.getNode("ubuntu4");
//        log.debug(">>>>>>>> ubuntu4 : {}", ubuntu4);

//        V1PodList list = api.listPodForAllNamespaces(
//
// null, null, null, null, null, null, null, null, null);
//        for (V1Pod item : list.getItems()) {
//            System.out.println(item.getMetadata().getName());
//        }
//        AppsV1Api appsV1Api = new AppsV1Api();
        // String namespace, Boolean includeUninitialized, String pretty, String _continue, String fieldSelector, String labelSelector, Integer limit, String resourceVersion, Integer timeoutSeconds, Boolean watch
//        V1ReplicaSetList replicaSetList = appsV1Api.listNamespacedReplicaSet("cheetah", null, null, null, null, "app in (cheetah-latte-4ef8f8)", null, null, null, null);
//        V1PodList podList = api.listNamespacedPod("cheetah", null, null, null, null, "app in (cheetah-latte-4ef8f8)", null, null, null, null);
//        log.debug(">>>>>>>> podList: {}:" + podList);

    }

    @Test
    public void deletePOd() throws ApiException {
        String podName = "cheetah-mocha-ne91tj-6cdddc75cf-55j9h";
        V1DeleteOptions options = new V1DeleteOptions();
        CoreV1Api api = new CoreV1Api();

//        podName = "cheetah-sunghee-again-deployment";
        ApiResponse<V1Status> v1StatusApiResponse = api.deleteNamespacedPodWithHttpInfo(podName, namespace, options, null, null, null, null, null);


        log.debug(">>>>>>>> v1StatusApiResponse : " + v1StatusApiResponse);
    }

    @Test
    public void deleteDeployment() throws ApiException {

        String label = "cheetah-sunghee-zhghdt";

        AppsV1Api appsV1Api = new AppsV1Api();
        V1DeleteOptions options = new V1DeleteOptions();
        V1Status status = appsV1Api.deleteNamespacedDeployment(label, namespace, options, null, null, null, null, null);
        log.debug(">>>>>>> deleteDeployment : status : " +  status);

    }

    @Test
    public void deleteService() throws ApiException {

        String label = "cheetah-sunghee-zhghdt";

        AppsV1Api appsV1Api = new AppsV1Api();
        V1DeleteOptions options = new V1DeleteOptions();

        CoreV1Api api = new CoreV1Api();
        V1Status statusForService = api.deleteNamespacedService(label, namespace, options, null, null, null, null, null);
        log.debug(">>>>>>> deleteNamespacedService : status : " +  statusForService);

    }

    @Test
    public void getNodePort() throws KubernetesException, ApiException {

        String field = "sunghee-multi";
        CoreV1Api api = new CoreV1Api();


        V1Service service1 = api.readNamespacedService(field, namespace, null, null, null);

        log.debug(">>>>>>>>>>>>> service : " + service1);
        for(V1ServicePort port: service1.getSpec().getPorts()) {
            if ("jupyter".equals(port.getName())) {
                log.debug("jupyter:" + port.getNodePort());
            } else if ("websocket".equals(port.getName())) {
                log.debug("websocket:" + port.getNodePort());

            }
        }

    }

    @Test
    public void deployment() throws ApiException {

        String username = "want813";
        String containerId = "sample";
        String label = username + "-" + containerId;

        String image = "nginx";





        V1Deployment deployment = new V1Deployment();
        deployment.setApiVersion("apps/v1");
        deployment.setKind("Deployment");

        V1ObjectMeta meta = new V1ObjectMeta();
        meta.setName(label + "-deployment");
        meta.setLabels(new HashMap<String, String>() {{
            put("app", label);
        }});
        deployment.setMetadata(meta);

        V1DeploymentSpec deploymentSpec = new V1DeploymentSpec();
        V1LabelSelector selector = new V1LabelSelector();
        selector.setMatchLabels(new HashMap<String, String>() {{
                put("app", label);
            }});
        deploymentSpec.setSelector(selector);


        deploymentSpec.setReplicas(1);

        V1PodTemplateSpec podTemplateSpec = new V1PodTemplateSpec();


        V1ObjectMeta podMeta = new V1ObjectMeta();
        podMeta.setLabels(new HashMap<String, String>() {{
            put("app", label);
        }});
        podTemplateSpec.setMetadata(podMeta);

        V1PodSpec podSpec = new V1PodSpec();

        V1PodSecurityContext v1PodSecurityContext = new V1PodSecurityContext();
        v1PodSecurityContext.setFsGroup(1000L);
//        v1PodSecurityContext.set
        podSpec.setSecurityContext(v1PodSecurityContext);


        V1Container container = new V1Container();
        container.setName(label + "-" + image);
        container.setImage(image);

        V1ContainerPort port = new V1ContainerPort();
        port.setContainerPort(80);
        container.setPorts(new ArrayList<V1ContainerPort>() {{
            add(port);
        }});


        podSpec.setContainers(new ArrayList<V1Container>() {{
            add(container);
        }});


        podTemplateSpec.setSpec(podSpec);

        deploymentSpec.setTemplate(podTemplateSpec);

        deployment.setSpec(deploymentSpec);

        log.debug(">>>>>>> deployment : " + deployment.toString());
        log.debug(">>>>>>>>>>> deploymentSpec : " + deploymentSpec);


        V1VolumeMount v1VolumeMount4 = new V1VolumeMount();
        v1VolumeMount4.setMountPath("/home/jovyan");
        v1VolumeMount4.setName("jupyter");

//        v1VolumeMount4.set


        V1Deployment createdDeployment = appsV1Api.createNamespacedDeployment(namespace, deployment, null, null, null);
        log.debug(">>>>>>>>>>>>> createdDeployment" + createdDeployment);

        V1Service service = new V1Service();
        service.setApiVersion("v1");
        service.setKind("Service");
        V1ObjectMeta metaForService = new V1ObjectMeta();
        metaForService.setName(label + "-svc");
        metaForService.setLabels(new HashMap<String, String>() {{
            put("app", label);
        }});
        service.setMetadata(metaForService);

        V1ServiceSpec specForService = new V1ServiceSpec();
        specForService.setSelector(new HashMap<String, String> () {{
            put("app", label);
        }});
        V1ServicePort servicePort = new V1ServicePort();
        servicePort.setPort(80);
        specForService.setPorts(new ArrayList<V1ServicePort>() {{
            add(servicePort);
        }});

        specForService.setType("NodePort");
        service.setSpec(specForService);

        V1Service createdService = api.createNamespacedService(namespace, service, null, null, null);
        log.debug(">>>>>>>>>>>>> createdService" + createdService);
    }

    public void pv() {

        String label = "sunghee";
        /*
        kind: PersistentVolume
        apiVersion: v1
        metadata:
         name: {label}
         namespace: {namespace}
         annotations:
           pv.beta.kubernetes.io/gid: 1000
           volume.beta.kubernetes.io/mount-options: "uid=1000,gid=1000"
           volume.beta.kubernetes.io/storage-class: default
        spec:
         storageClassName: hostpath
         persistentVolumeReclaimPolicy: Retain | Recycle | Delete
         capacity:
          storage: {capacity}
         accessModes:
          - ReadWriteOnce
         hostPath:
          path: {jupyterPath}
         */
        V1PersistentVolume persistentVolume = new V1PersistentVolume();
//        volume.

        /*
        kind: PersistentVolumeClaim
        apiVersion: v1
        metadata:
          name: myclaim
        spec:
          accessModes:
            - ReadWriteOnce
          volumeMode: Filesystem
          resources:
            requests:
              storage: 8Gi
          storageClassName: slow
          selector:
            matchLabels:
              release: "stable"
            matchExpressions:
              - {key: environment, operator: In, values: [dev]}
         */

        V1PersistentVolumeClaim persistentVolumeClaim = new V1PersistentVolumeClaim();
        V1PersistentVolumeClaimSpec spec = new V1PersistentVolumeClaimSpec();
        V1LabelSelector labelSelector = new V1LabelSelector();
        labelSelector.setMatchLabels(new HashMap<String, String>() {{
            put("app", label);
        }});
        spec.setSelector(labelSelector);
        V1TypedLocalObjectReference reference = new V1TypedLocalObjectReference();
//        reference.set
//        persistentVolumeClaim.setSpec();
    }


    @Test
    public void pod() throws ApiException {



        V1Pod v1Pod = new V1Pod();
        v1Pod.setApiVersion("v1");
        v1Pod.setKind("PodFormat");
        V1ObjectMeta meta = new V1ObjectMeta();
        meta.setName("myapp-pod-format-web-6");
        meta.setLabels(new HashMap<String, String>() {{
            put("app", "myapp");
        }});
        v1Pod.setMetadata(meta);
        V1PodSpec spec = new V1PodSpec();
        spec.setServiceAccountName("cheetah-admin");

        V1Container container = new V1Container();
        container.setName("busybox");
        container.setImage("busybox");
//        container.setCommand(new ArrayList<String>() {{
//            add("'sh','-c','echo Hello Kubernetes! && sleep 3600'");
//        }});
        spec.setContainers(new ArrayList<V1Container>() {{
            add(container);
        }});
        v1Pod.setSpec(spec);

        client.getHttpClient().setReadTimeout(0, TimeUnit.MILLISECONDS);

        ExecutorService executorService = Executors.newFixedThreadPool(1);

        executorService.execute(() -> {
            try {
                watchPods();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        });

    }

    private void watchPods() throws ApiException {
        while (true) {
            Watch<V1Pod> watch = Watch.createWatch(client, api.listPodForAllNamespacesCall(null, null, null, null, null,
                    null, null, null, Boolean.TRUE, null, null), new TypeToken<Watch.Response<V1Pod>>() {
            }.getType());

            for (Watch.Response<V1Pod> item : watch) {
                V1PodStatus podStatus = item.object.getStatus();
                String name = item.object.getMetadata().getName();
                String status = podStatus.getPhase();
                String kind = item.object.getKind();
                String details = podStatus.toString();

                System.out.printf("NAME: %s | KIND: %s | STATUS: %s | DETAILS: %n%s%n====================%n", name, kind, status, details);
            }

        }
    }

}


