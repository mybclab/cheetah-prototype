package com.mybclab.admin.pod.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.pod.service.PodFormatService;
import com.mybclab.common.utils.MapUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class PodFormatServiceTest {

    @Autowired
    private PodFormatService podFormatService;

    @Test
    public void resetHint() {

        List<PodFormat> podFormatList = podFormatService.findAll();

        podFormatList.forEach(f -> {
            log.debug(">>>>>>>>>> f.getPodOptionMap().toString(): " + MapUtils.toStringSortByKey(f.getPodOptionSnMap()));
//            f.setPodOptionHint(f.getPodOptionSnMap().toString());
        });

//        podFormatService.saveAll(podFormatList);
    }
}
