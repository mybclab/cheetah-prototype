package com.mybclab.admin.gpu.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.dto.GpuForm;
import com.mybclab.cheetah.admin.gpu.service.GpuService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class GpuServiceTest {

    @Autowired
    private GpuService gpuService;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void GPU_테스트() {
        assertNotNull(gpuService);

        GpuForm gpuForm = new GpuForm();
        gpuForm.setGpuName("gpu-name");
        gpuForm.setManufacturerName("amd");
        gpuForm.setSpec("spec");
        gpuForm.setMonthlyAmount(10000D);
        gpuForm.setUseYn(false);

        Gpu gpu = modelMapper.map(gpuForm, Gpu.class);
        Gpu savedGpu = gpuService.save(gpu);
        log.debug(">> savedGpu : {}", savedGpu);



        List<Gpu> gpuList = gpuService.findAll();
        log.debug(">> list : {}", gpuList);

        List<Gpu> useGpuList = gpuService.findAll(true);
        log.debug(">> use list : {}", useGpuList);

    }
}
