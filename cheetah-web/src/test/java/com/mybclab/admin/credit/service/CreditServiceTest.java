package com.mybclab.admin.credit.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.dto.CreditForm;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.common.constant.Code;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class CreditServiceTest {

    @Autowired
    private CreditService creditService;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void 크레딧_테스트() {
        assertNotNull(creditService);


        CreditForm creditForm = new CreditForm();
        creditForm.setCreditId("credit-bonus-random");
        creditForm.setCreditName("크레딧이름");
        creditForm.setCreditAmount(10000D);
        creditForm.setCreditTypeCode(Code.CREDIT_TYPE.BONUS);
        creditForm.setCreditExpiryTypeCode(Code.CREDIT_EXPIRY_TYPE.MONTH);
        creditForm.setCreditExpiryValue(3);

        Credit credit = modelMapper.map(creditForm, Credit.class);
        Credit savedCredit = creditService.save(credit);

        log.debug(">>> savedCredit : {}", savedCredit);


        boolean result = creditService.existsById(credit.getCreditId());
        log.debug(">>> existsById : {}", result);

    }
}
