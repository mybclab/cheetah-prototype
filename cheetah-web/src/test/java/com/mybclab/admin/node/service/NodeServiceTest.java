package com.mybclab.admin.node.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.dto.NodeCriteria;
import com.mybclab.cheetah.admin.node.dto.NodeForm;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.common.constant.Code;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class NodeServiceTest {

    @Autowired
    private NodeService nodeService;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void 노드_테스트() {
        assertNotNull(nodeService);
        // 등록
        NodeForm nodeForm = new NodeForm();
        nodeForm.setNodeName("test-node");
        nodeForm.setInternalDomain("test");

        Node node = modelMapper.map(nodeForm, Node.class);

        node.setStatusCode(Code.NODE_STATUS.READY);
        Node savedNode = nodeService.save(node);
        log.debug("savedNode : {}", savedNode);

        // 노드 조회 테스트
        Node findedNode = nodeService.findById(savedNode.getNodeSn());
        log.debug("findedNode : {}", findedNode);
        // 삭제
        nodeService.delete(node.getNodeSn());


        // 목록조회
        NodeCriteria criteria = NodeCriteria.builder()
                .nodeName("test")
                .statusCode(Code.NODE_STATUS.READY)
                .build();

        Pageable pageeRequest = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");
        Page<Node> page = nodeService.findAll(criteria, pageeRequest);
        log.debug("page content : {} " + page.getContent());
        log.debug("page size : {} " + page.getContent().size());
    }
}
