package com.mybclab.admin.user.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import com.mybclab.cheetah.admin.user.domain.UserRole;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.admin.user.dto.UserCriteria;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.repository.UserCardRepository;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.admin.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {CheetahApplication.class})
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserCardRepository userCardRepository;
    @Autowired
    private UserCreditService userCreditService;

    @Autowired
    private CreditService creditService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserCardService userCardService;


    @Test
    public void 그룹관리자로변경() {
        User user = userService.findById("milk");

        user.setUserRoles(new ArrayList<UserRole>() {{
            add(new UserRole(user.getUsername(), Code.ROLE.GROUP_ADMIN));
        }});
        user.setGroupName("groupName");
        user.setGroupTypeCode(Code.GROUP_TYPE.SCHOOL);
        user.setGroupInviteKey(userService.generateGroupInviteKey());
        user.setUpdatedBy("admin");
        user.setUpdatedAt(LocalDateTime.now());

        userService.save(user);
    }

    @Test
    public void 사용자삭제() {
        String username = "geggege";
        User user = userService.findById(username);

        userService.delete(user);

    }

    @Test
    public void 사용자조회테스트() {
        User user = userService.findById("korea_university");

        List<User> userList = userService.findAllForPayment();

        log.debug(">>>>>>>>>>>>>> userListForPayment {}", userList);
    }

    @Test
    public void 권한으로사용자찾기() {

        List<User> usernameList = new ArrayList<>(userService.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN));
        log.debug("usernameList : {}, " + usernameList);
    }


    @Test
    public void 목록_승인_크레딧주기_테스트() {
        String groupId = "korea_university";

        UserCriteria criteria = UserCriteria.builder()
                //.parentUsername(groupId)
                .name("한국대학")
//                .confirmYn(false)
                .build();

        Pageable pageeRequest = PageRequest.of(0, 10, Sort.Direction.DESC, "createdAt");

        Page<User> userPage = userService.findAll(criteria, pageeRequest);
        log.debug("allUserPage content : {} " + userPage.getContent());
        log.debug("allUserPage size : {} " + userPage.getContent().size());

//        List<User> groupList = userService.findAllByGroupId(groupId);
//        log.debug("groupList size : " + groupList.size());


        User user = userPage.getContent().get(0);
        log.debug("user : {}", user);
        log.debug("user cards : {}", user.getUserCardList());
        log.debug("user credits : {}", user.getUserCreditList());
        // 승인
        user.setConfirmYn(true);
        user.setUpdatedBy("admin");


        User confirmedUser = userService.save(user);


        User findUser = userService.findById(confirmedUser.getUsername());

        assertEquals(user.getUserCardList().size(), findUser.getUserCardList().size());
        assertEquals(user.getUserCreditList().size(), findUser.getUserCreditList().size());
        log.debug("findUser : {}", findUser);
        log.debug("findUser cards : {}", findUser.getUserCardList());
        log.debug("findUser credits : {}", findUser.getUserCreditList());
    }



    @Test
    public void 그룹재귀테스트() {
        String groupId = "korea_university";
        User user = userService.findById(groupId);
        List<User> groupUsers = user.getGroupUsers();
        log.debug("user groupUsers size : " + groupUsers.size());
        log.debug("user groupUsers : {}" + groupUsers);


        if (groupUsers != null) {
            User groupUser = groupUsers.get(0);
            log.debug("parentUsername : " + groupUser.getParentUsername());
            User group = groupUser.getGroup();
            log.debug("GROUP : {}", group);
        }

    }

    @Test
    public void 수정() {
        User user = userService.findById("admin");
        user.setPassword(bCryptPasswordEncoder.encode("1231"));
        userService.save(user);
    }


    @Test
    public void 사용자등록용() {


        assertNotNull(userService);

        UserForm userForm = new UserForm();
        String username = "korea_test";
        userForm.setUsername(username);
        userForm.setPassword("1231");
        userForm.setConfirmPassword("1231");
        userForm.setName("김테스");
        userForm.setPhoneNo("010-8888-9999");
        userForm.setEmail("test@korea.com");

        userForm.setParentUsername("korea_university");
//        userForm.setGroupName("한국대학교");
//        userForm.setGroupTypeCode(Code.GROUP_TYPE.SCHOOL);


        userForm.setCardNo("1111222233334444");
        userForm.setCustomerUid("billing-key");
        userForm.setCardExpiry("0121");

        User user = modelMapper.map(userForm, User.class);

        // controller
        user.setCreatedBy("admin");
        user.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        user.setUserRoles(new ArrayList<UserRole>() {{
            add(new UserRole(user.getUsername(), Code.ROLE.GROUP_USER));
        }});

        UserCardPK userCardPK = modelMapper.map(userForm, UserCardPK.class);
        UserCard userCard = modelMapper.map(userForm, UserCard.class);
        userCard.setUserCardPK(userCardPK);
        userCard.setUseYn(true);

        user.setUserCardList(new ArrayList<UserCard>() {{
            add(userCard);
        }});
        User savedUser = userService.save(user);



        assertNotNull(savedUser);
        log.debug("savedUser : {}", savedUser);

        User findUser = userService.findById(savedUser.getUsername());
        log.debug("findUser : {}", findUser);
        log.debug("findUser group : {}", findUser.getGroup());
        log.debug("findUser roles : {}", findUser.getUserRoles());
        log.debug("findUser containers : {}", findUser.getContainerList());
        log.debug("findUser credits : {}", findUser.getUserCreditList());
        log.debug("findUser cards : {}", findUser.getUserCardList());

    }


}
