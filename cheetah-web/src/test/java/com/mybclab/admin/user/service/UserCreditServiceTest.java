package com.mybclab.admin.user.service;

import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.admin.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
//@Transactional
@Slf4j
public class UserCreditServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserCreditService userCreditService;

    @Test
    public void 목록_승인_크레딧주기_테스트() {
        String username = "korea_university";

        User user = userService.findById(username);


        // 크레딧
        String creditId = "WELCOME_CREDIT_FOR_GROUP";
//        creditId = "WELCOME_CREDIT_FOR_USER";
        creditId = "BONUS_CREDIT_1_YEAR";

        UserCredit savedUserCredit = userCreditService.addCreditToUser(user.getUsername(), creditId);
        log.debug("savedUserCredit : {}", savedUserCredit);
        log.debug("savedUserCredit isValidCredit : {}", savedUserCredit.isValidCredit());

    }
}
