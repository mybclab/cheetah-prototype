package com.mybclab.admin.user.service;


import com.mybclab.cheetah.CheetahApplication;
import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import com.mybclab.cheetah.admin.user.dto.UserCardForm;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CheetahApplication.class})
@ActiveProfiles("local")
@Transactional
@Slf4j
public class UserCardServiceTest {

    @Autowired
    private UserCardService userCardService;

    @Autowired
    private ModelMapper modelMapper;
    
    @Test
    public void 카드_추가등록_테스트() {
        String username = "korea_university";
//        User user = userService.findById(groupId);

        UserCardForm userCardForm = new UserCardForm();
        userCardForm.setCardNo("1111-2222-3333-4444");
        userCardForm.setCustomerUid("bibibibibibibibi");
        userCardForm.setCardExpiry("5454");
        userCardForm.setUseYn(true);

        UserCardPK userCardPK = new UserCardPK();
        UserCard userCard = modelMapper.map(userCardForm, UserCard.class);
        userCardPK.setUsername(username);
        userCard.setUserCardPK(userCardPK);
        userCard.setCreatedBy(username);
        UserCard savedUserCard = userCardService.addCard(userCard);
        log.debug("savedUserCard : {}", savedUserCard);

        List<UserCard> userCardList = userCardService.findAllByUsername(username);
        log.debug("userCardList : {}", userCardList);
        log.debug("userCardList size : {}", userCardList.size());


    }
}
