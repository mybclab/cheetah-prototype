#! /bin/bash

echo "마운트 프로그램 설치..."
#마운트 프로그램설치
sudo apt-get install nfs-common -y

echo "마운트 디렉토리 생성..."
#디렉토리 생성
sudo mkdir -p $2

#에코
echo "efs 주소:"$1
echo "mount path :"$2
echo "efs 마운트 중 ..."

#NFS 클라이언트 사용
#sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $1:/ /home/ubuntu/efs
#sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $1:/ $2

i=1
while [ $i -lt 30 ]
do
        i=$(($i+1))
        nfs_count=`dpkg -s nfs-common | grep -i ok | wc -l`
        echo $nfs_count

        if [ $nfs_count -ne 0 ];

        then
                sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $1:/ $2
                break;
        else
                sudo apt-get install nfs-common -y
        fi;
        sleep 5;
done


echo "마운트 확인"
sudo df -h | grep $2


