#! /bin/bash
echo "$1"

#swap 끄기
swapoff -a

#join Parameter output
echo "master IP: $3"
echo "token: $4"
echo "sha256: $5"

sudo kubeadm reset -f

#Hosts File Modified
echo '183.111.96.110   registry.n3ncloud'  | sudo tee -a /etc/hosts
echo $3'    n3ncloud-dev'  | sudo tee -a /etc/hosts
echo '183.111.96.108   GFS1'  | sudo tee -a /etc/hosts

sleep 10

#쿠버네티스 join
sudo kubeadm join $3:6443 --token $4 --discovery-token-ca-cert-hash sha256:$5 --node-name $7

echo "sudo kubeadm join "$3":6443 --token "$4" --discovery-token-ca-cert-hash sha256:"$5 --node-name $7

sleep 10

#node label create
echo "인스턴스ID: $7"
echo "GPU Share ? : $1"

# label attach
echo curl -k -v -H "Accept: application/json" -XPATCH -d '{"metadata":{"labels":{"accelerator":"'$7'","gpushare":"'$1'"}}}' -H "Content-Type: application/merge-patch+json" -H "Authorization: Bearer $2" https://$3:6443/api/v1/nodes/$7

curl -k -v -H "Accept: application/json" -XPATCH -d '{"metadata":{"labels":{"accelerator":"'$7'","gpushare":"'$1'"}}}' -H "Content-Type: application/merge-patch+json" -H "Authorization: Bearer $2" https://$3:6443/api/v1/nodes/$7


#externalIp check
echo "외부 IP : $6"
curl -X POST http://$8/kube/node/$7?externalIp=$6

