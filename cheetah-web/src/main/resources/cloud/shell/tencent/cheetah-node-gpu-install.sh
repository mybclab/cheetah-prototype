#! /bin/bash
echo "$1" 

#swap off
sudo swapoff -a

#join Parameter output
echo "master IP: $3"
echo "token: $4"
echo "sha256: $5"

sudo kubeadm reset -f

#Hosts File Modified
echo '183.111.96.110   registry.n3ncloud'  | sudo tee -a /etc/hosts
echo $3'    n3ncloud-dev'  | sudo tee -a /etc/hosts
echo '183.111.96.108   GFS1'  | sudo tee -a /etc/hosts

sleep 10

#쿠버네티스 join
sudo kubeadm join $3:6443 --token $4 --discovery-token-ca-cert-hash sha256:$5 --node-name $7

echo "sudo kubeadm join "$3":6443 --token "$4" --discovery-token-ca-cert-hash sha256:"$5 --node-name $7

sleep 30

#GFS connect
sudo apt install glusterfs-client -y
sudo mkdir -p $9
sudo mount -t glusterfs GFS1:/vol $9

sudo df -h | grep GFS1:/vol

sleep 10


#node label create
echo "instance id: $7"

# label attach


sleep 10

echo "curl -k -v -H \"Accept: application/json\" -XPATCH -d '{\"metadata\":{\"labels\":{\"accelerator\":'$7'}}}' -H \"Content-Type: application/merge-patch+json\" -H \"Authorization: Bearer $2\" https://$3:6443/api/v1/nodes/$7"

#labelCommand="{\"metadata\":{\"labels\":{\"accelerator\":\""$7"\"}}}"
#echo "$labelCommand"
#curl -k -v -H "Accept: application/json" -XPATCH -d $labelCommand -H "Content-Type: application/merge-patch+json" -H "Authorization: Bearer $2" https://$3:6443/api/v1/nodes/$7
                                                    #'{"metadata":{"labels":{"accelerator":"5ab88519-7149-41d6-aa91-6e0b37aafa61"}}}'
#curl -k -v -H "Accept: application/json" -XPATCH -d '{"metadata":{"labels":{"accelerator":'"$7"'}}}' -H "Content-Type: application/merge-patch+json" -H "Authorization: Bearer $2" https://$3:6443/api/v1/nodes/$7

curl -k -v -H "Accept: application/json" -XPATCH -d '{"metadata":{"labels":{"accelerator":"'$7'"}}}' -H "Content-Type: application/merge-patch+json" -H "Authorization: Bearer $2" https://$3:6443/api/v1/nodes/$7


#externalIp check
echo "외부 IP : $6"
curl -X POST http://$8/kube/node/$7?externalIp=$6