package com.mybclab.common.repository;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;

@SuppressWarnings("unchecked")
public class CheetahRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CheetahRepository<T, ID> {
    private final EntityManager entityManager;

    public CheetahRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void refresh(T t) {
        entityManager.refresh(t);
    }
}
