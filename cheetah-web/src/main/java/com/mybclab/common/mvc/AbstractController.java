package com.mybclab.common.mvc;

import com.mybclab.common.result.JsonResult;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {
    private static final String REDIRECT_PREFIX = "redirect:";
    private static final String FORWARD_PREFIX = "forward:";
    private static final String ROOT = "/";

    protected String redirect(String path) {
        return REDIRECT_PREFIX + path;
    }

    protected String foward(String path) {
        return FORWARD_PREFIX + path;
    }

    protected String goRoot() {
        return REDIRECT_PREFIX + ROOT;
    }

    protected ResponseEntity<Object> buildResponseEntity(JsonResult restResult) {
        return new ResponseEntity<>(restResult, restResult.getHttpStatus());
    }
}
