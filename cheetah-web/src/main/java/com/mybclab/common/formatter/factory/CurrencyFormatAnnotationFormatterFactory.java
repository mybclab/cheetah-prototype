package com.mybclab.common.formatter.factory;

import com.mybclab.common.formatter.annotation.CurrencyFormat;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Currency format annotation formatter factory
 *
 * @author caost
 */
public class CurrencyFormatAnnotationFormatterFactory implements AnnotationFormatterFactory<CurrencyFormat> {
    /* (non-Javadoc)
     * @see org.springframework.format.AnnotationFormatterFactory#getFieldTypes()
     */
    @Override
    public Set<Class<?>> getFieldTypes() {
        Set<Class<?>> types = new HashSet<Class<?>>(1);
        types.add(Integer.class);
        types.add(Long.class);
        types.add(Double.class);
        types.add(Float.class);
        return types;
    }

    /* (non-Javadoc)
     * @see org.springframework.format.AnnotationFormatterFactory#getPrinter(java.lang.annotation.Annotation, java.lang.Class)
     */
    @Override
    public Printer<?> getPrinter(CurrencyFormat annotation, Class<?> fieldType) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMaximumFractionDigits(0);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("₩");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);

        return (Printer<Object>) (object, locale) -> nf.format(object);
    }

    /* (non-Javadoc)
     * @see org.springframework.format.AnnotationFormatterFactory#getParser(java.lang.annotation.Annotation, java.lang.Class)
     */
    @Override
    public Parser<?> getParser(CurrencyFormat annotation, Class<?> fieldType) {
        return (Parser<Object>) (text, locale) -> {
            String number = Optional.of(text).map(o -> o.replaceAll("[^\\d.]", "")).orElse(null);

            if (fieldType.equals(Integer.class)) {
                return Integer.parseInt(number);
            } else if (fieldType.equals(Long.class)) {
                return Long.parseLong(number);
            } else if (fieldType.equals(Double.class)) {
                return Double.parseDouble(number);
            } else if (fieldType.equals(Float.class)) {
                return Float.parseFloat(number);
            }

            return null;
        };
    }
}
