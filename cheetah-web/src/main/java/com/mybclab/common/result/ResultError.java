package com.mybclab.common.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * JsonResult Error
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultError {

    /**
     * Error code
     */
    private int code;

    /**
     * Error message
     */
    private String message;

    /**
     * Errors List
     */
    private List<ResultErrors> errors = new ArrayList<>();

    /**
     * JsonResult Errors
     */
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    class ResultErrors {
        /**
         * domain
         */
        private String domain;

        /**
         * reason
         */
        private String reason;

        /**
         * message
         */
        private String message;

        /**
         * location
         */
        private String location;

        /**
         * locationType
         */
        private String locationType;

        /**
         * extendedHelp
         */
        private String extendedHelp;

        /**
         * send report
         */
        private String sendReport;
    }
}
