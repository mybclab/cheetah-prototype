package com.mybclab.common.result;

import lombok.Getter;

/**
 * JsonResult Status
 */
@Getter
public enum ResultStatus {

    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    /**
     * Status
     */
    private String status;

    ResultStatus(String status) {
        this.status = status;
    }
}
