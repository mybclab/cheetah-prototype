package com.mybclab.common.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JsonResult Json Object
 *
 * Google Json Styleguide, JSend 참고하여 생성
 *
 * Google Json Styleguide(https://google.github.io/styleguide/jsoncstyleguide.xml?showone=data#data)
 * JSend(https://labs.omniti.com/labs/jsend) - Simple and probably what you are already doing.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResult {
    /**
     * status
     */
    private ResultStatus status;

    /**
     * http status
     */
    private HttpStatus httpStatus;

    /**
     * data
     */
    private Map<String, Object> data = new HashMap<>();

    /**
     * resutl error
     */
    private ResultError error;

    /**
     * 서브 오류(validation erros)
     */
    private List<SubResultError> subResultErrors;

    /**
     * 임시 저장 Data
     */
    @JsonIgnore
    private Map<String, Object> tmpData = new HashMap<>();

    /**
     * set data
     * @param dataName key
     * @param data data object
     */
    public void setData(String dataName, Object data) {
        this.data.put(dataName, data);
    }

    /**
     * set temporary data
     *
     * @param dataName key
     * @param data     data object
     */
    public void setTmpData(String dataName, Object data) {
        this.tmpData.put(dataName, data);
    }

    /**
     * set error
     * @param errorCode error code
     * @param errorMessage error message
     */
    public void setError(int errorCode, String errorMessage) {
        ResultError error = new ResultError();

        error.setCode(errorCode);
        error.setMessage(errorMessage);

        this.setError(error);
    }

    /**
     * set error
     * @param errorCode error code
     * @param ex Exception
     */
    public void setError(int errorCode, Exception ex) {
        ResultError error = new ResultError();

        error.setCode(errorCode);
        error.setMessage(ex.getMessage());

        this.setError(error);
    }

    private void addSubError(SubResultError subResultError) {
        if (subResultErrors == null) {
            subResultErrors = new ArrayList<>();
        }

        subResultErrors.add(subResultError);
    }

    private void addValidationError(String object, String field, Object rejectedValue, String message) {
        addSubError(new ValidationError(object, field, rejectedValue, message));
    }

    private void addValidationError(String object, String message) {
        addSubError(new ValidationError(object, message));
    }

    private void addValidationError(FieldError fieldError) {
        this.addValidationError(
                fieldError.getObjectName(),
                fieldError.getField(),
                fieldError.getRejectedValue(),
                fieldError.getDefaultMessage());
    }

    public void addValidationErrors(List<FieldError> fieldErrors) {
        fieldErrors.forEach(this::addValidationError);
    }

    private void addValidationError(ObjectError objectError) {
        this.addValidationError(
                objectError.getObjectName(),
                objectError.getDefaultMessage());
    }

    public void addValidationError(List<ObjectError> globalErrors) {
        globalErrors.forEach(this::addValidationError);
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @AllArgsConstructor
    class ValidationError extends SubResultError {
        private String object;
        private String field;
        private Object rejectedValue;
        private String message;

        ValidationError(String object, String message) {
            this.object = object;
            this.message = message;
        }
    }
}
