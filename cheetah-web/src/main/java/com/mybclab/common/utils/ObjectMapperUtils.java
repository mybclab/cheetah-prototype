package com.mybclab.common.utils;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObjectMapperUtils {
    public static String toString(Object value) {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(value);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            log.debug("toString error", e);
        }

        return null;
    }
    public static < T > T stringToObject(String jsonString, Class < T > valueType) {
        try {
            return new ObjectMapper().readValue(jsonString, valueType);
        } catch (JsonParseException e) {
            log.debug("stringToObject error", e);
        } catch (JsonMappingException e) {
            log.debug("stringToObject error", e);
        } catch (IOException e) {
            log.debug("stringToObject error", e);
        }
        return null;
    } 
}
