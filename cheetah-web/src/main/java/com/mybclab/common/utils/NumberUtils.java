package com.mybclab.common.utils;

import java.math.BigDecimal;

public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils {
    /**
     * <p>Convert a <code>Long</code> to a <code>timestamp</code>, returning a
     * default currrent timestamp if the conversion fails.</p>
     *
     * <pre>
     *   NumberUtils.toTimestamp(1536314190430) = 1536314190
     *   NumberUtils.toTimestamp(1536314190) = 1536314190
     * </pre>
     *
     * @param timestamp the long value to convert timestamp
     * @return 10 digit timestamp
     */
    public static long toTimestampSeconds(final long timestamp) {
        if (timestamp > 9999999999L) {
            return timestamp/1000;
        } else {
            return timestamp;
        }
    }

    /**
     * <p>Convert a <code>Long</code> to a <code>timestamp</code>, returning a
     * default currrent timestamp if the conversion fails.</p>
     *
     * <pre>
     *   NumberUtils.toTimestamp(1536314190430) = 1536314190
     *   NumberUtils.toTimestamp(1536314190) = 1536314190
     * </pre>
     *
     * @param timestamp the long value to convert timestamp
     * @return 10 digit timestamp
     */
    public static long toTimestampSeconds(final String timestamp) {
        try {
            return toTimestampSeconds(Long.parseLong(timestamp));
        } catch (final NumberFormatException nfe) {
            return System.currentTimeMillis() / 1000;
        }
    }

    private static boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static double round(double x, int scale) {
        return round(x, scale, BigDecimal.ROUND_HALF_UP);
    }

    public static double round(double x, int scale, int roundingMethod) {
        try {
            return (new BigDecimal
                    (Double.toString(x))
                    .setScale(scale, roundingMethod))
                    .doubleValue();
        } catch (NumberFormatException ex) {
            if (Double.isInfinite(x)) {
                return x;
            } else {
                return Double.NaN;
            }
        }
    }
}
