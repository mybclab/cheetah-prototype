package com.mybclab.common.utils;

import java.util.Map;
import java.util.TreeMap;

public class MapUtils {

    public static String toStringSortByKey(Map<String, Long> srcMap) {
        TreeMap<String, Long> treeMap = new TreeMap<>(srcMap);
        return treeMap.toString();
    }
}
