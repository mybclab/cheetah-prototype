package com.mybclab.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
    /**
     * yyyy-mm-dd 포맷 localDate 로 변경
     * @param date  yyyy-mm-dd
     * @return LocalDate
     */
    public static LocalDate getLocalDateFromString(String date) {
        return LocalDate.parse(date);
    }

    public enum DATE_TYPE {
        TODAY,
        YESTERDAY,
        WEEK_AGO,
        MONTH_AGO,
        CURRENT_MONTH_FIRST_DAY,
        PREV_WEEK_FIRST_DAY,
        PREV_WEEK_END_DAY,
        PREV_MONTH_FIRST_DAY,
        PREV_MONTH_END_DAY
    }

    public enum DAY_TYPE {
        START_OF_DAY,
        CURRENT_OF_DAY,
        END_OF_DAY
    }

    public static long getBetweenDays(LocalDate from, LocalDate to) {
        return ChronoUnit.DAYS.between(from, to);
    }

    /**
     * 현재 날짜의 UTC format string 반환
     * @param dateType  date type
     * @return  utc formatted date string
     */
    public static String getUTCFormatString(DATE_TYPE dateType, DAY_TYPE dayType) {
        return getUTCFormatString(dateType, dayType, LocalDateTime.now());
    }

    /**
     * 기준 날짜의 UTC format string 반환
     *
     * @param dateType  date type
     * @param date  date
     * @return  utc formatted date string
     */
    public static String getUTCFormatString(DATE_TYPE dateType, DAY_TYPE dayType, Date date) {
        LocalDateTime localDateTime;
        Instant instant;

        if (date == null) {
            localDateTime = LocalDateTime.now();
        } else {
            instant = date.toInstant();
            localDateTime = instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
        }

        return getUTCFormatString(dateType, dayType, localDateTime);
    }

    /**
     * 기준 날짜의 UTC format string 반환
     *
     * @param dateType  date type
     * @param localDateTime  기준 localDateTime
     * @return  utc formatted date string
     */
    public static String getUTCFormatString(DATE_TYPE dateType, DAY_TYPE dayType, LocalDateTime localDateTime) {
        String formatString;

        switch (dateType) {
            case TODAY:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.toLocalDate().atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime, ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case YESTERDAY:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.minus(1, ChronoUnit.DAYS).toLocalDate().atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.DAYS), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.DAYS).toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case WEEK_AGO:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.minus(1, ChronoUnit.WEEKS).toLocalDate().atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.WEEKS), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.WEEKS).toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case MONTH_AGO:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.minus(1, ChronoUnit.MONTHS).toLocalDate().atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.MONTHS), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.MONTHS).toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case CURRENT_MONTH_FIRST_DAY:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.toLocalDate().withDayOfMonth(1).atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.withDayOfMonth(1), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.withDayOfMonth(1).toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case PREV_MONTH_FIRST_DAY:
                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDateTime.minus(1, ChronoUnit.MONTHS).withDayOfMonth(1).toLocalDate().atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.MONTHS).withDayOfMonth(1), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.MONTHS).withDayOfMonth(1).toLocalDate().atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            case PREV_MONTH_END_DAY:
                LocalDate localDate = localDateTime.minus(1, ChronoUnit.MONTHS).toLocalDate();

                if (dayType == DAY_TYPE.START_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(localDate.withDayOfMonth(localDate.lengthOfMonth()).atStartOfDay(ZoneId.of("UTC")));
                } else if (dayType == DAY_TYPE.CURRENT_OF_DAY) {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDateTime.minus(1, ChronoUnit.MONTHS).withDayOfMonth(localDate.lengthOfMonth()), ZoneId.of("UTC")));
                } else {
                    formatString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.of(localDate.withDayOfMonth(localDate.lengthOfMonth()).atTime(LocalTime.MAX), ZoneId.of("UTC")));
                }
                break;
            default:
                return null;
        }

        log.debug("convert {} : {} : {} -> {}", localDateTime, dateType.toString(), dayType.toString(), formatString);

        return formatString;
    }

    public static LocalDateTime getLocalDateTimeFromString(JsonNode node) {
        String dateInString = node.asText(); //"2016-08-16T15:23:01Z";

        if (dateInString.length() > 22) {
            dateInString = dateInString.substring(0, 22);
        }

        dateInString += "Z";

        Instant instant = Instant.parse(dateInString);

        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public static LocalDateTime getLocalDateTime(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(NumberUtils.toTimestampSeconds(timestamp)), ZoneOffset.UTC);
    }

    public static LocalDateTime toLocalDateTime(Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        return LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.of("UTC"));
    }
}
