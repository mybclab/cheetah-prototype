package com.mybclab.common.utils;

import com.mybclab.cheetah.common.exception.UnauthorizedException;
import com.mybclab.cheetah.admin.user.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Spring Security 관련 utility class
 */
@Slf4j
public class SecurityUtils {
    public static Authentication getCurrentAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static boolean hasRole(String Role) {
        Authentication authentication = getCurrentAuthentication();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(Role);
        return authentication.getAuthorities().contains(grantedAuthority);
    }

    public static User getPrincipal() {
        User user;

        try {
            user = (User) getCurrentAuthentication().getPrincipal();
        } catch (ClassCastException e) {
            log.error("로그인 되지 않은 상태에서 세션 요청", e);
            throw new UnauthorizedException("Error.unauthorized");
        }

        return user;
    }
}
