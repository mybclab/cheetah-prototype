package com.mybclab.common.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * 문자열 관련 유틸리티 클래스
 *
 * <p><b>NOTE:</b> {@link org.apache.commons.lang3.StringUtils} 를 상속받아 사용한다.
 *
 * @author hkwon
 * @since 2017.08.28
 */

public class StringUtils extends org.apache.commons.lang3.StringUtils {
    /**
     * <p>Replaces each substring of the text String that matches the given regular expression
     * with the given replacement.</p>

     * <pre>
     * StringUtils.replace("(abc)", new String[]{"(", ")"}, "")       = abc
     *
     * @param text  text to search and replace in, may be null
     * @param searchList  list
     * @param replacement  the string to be substituted for each match
     * @return  the text with any replacements processed,
     *              {@code null} if null String input

     * @since 3.5
     */
    public static String replace(final String text, final String[] searchList, final String replacement) {
        if (text == null || searchList == null|| replacement == null ) {
            return text;
        }
        String result = text;

        for (CharSequence search : searchList) {
            result = result.replace(search, replacement);
        }

        return result;
    }


    public static String randomAlphanumeric(int count) {
        return RandomStringUtils.random(count, "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789");
    }

    public static String randomLowerAlphanumeric(int count) {
        return RandomStringUtils.random(count, "abcdefghjkmnpqrstuvwxyz23456789");
    }
}
