package com.mybclab.common.notifier;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Slack Notifier *
 * @author hkwon *
 */
@Slf4j
@Component
public class SlackNotifier {
    private final RestTemplate restTemplate;

    @Value("${spring.profiles}")
    private String profile;

    @Autowired
    public SlackNotifier(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public enum SlackTarget {
        CONTAINER_REQUEST("cheetah"), REGISTER_QNA("cheetah"), REGISTER_USER("cheetah");
        String username;

        SlackTarget(String username) {
            this.username = username;
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class SlackMessageAttachement {
        private String fallback;
        private String color;
        private String pretext;
        private String author_name;
        private String author_link;
        private String author_icon;
        private String title;
        private String title_link;
        private String text;
        private String image_url;
        private String thumb_url;
        private String footer;
        private String footer_icon;
        private Long ts;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class SlackMessage {
        private String text;
        private String channel;
        private String username;
        private List<SlackMessageAttachement> attachments;

        void addAttachment(SlackMessageAttachement attachement) {
            if (this.attachments == null) {
                this.attachments = new ArrayList<>();
            }

            this.attachments.add(attachement);
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static class SlackHook {
        private String hookUrl;
        private String channel;
    }

//    public boolean notify(SlackTarget target, SlackMessageAttachement message) {
//        return this.notify(target, message, SLACK_URL, SLACK_CHANNEL);
//    }
//
//    public boolean notify(SlackTarget target, SlackMessageAttachement message, String hookUrl, String channel) {
//        log.info(">> slack hookUrl : " + hookUrl);
//        if (StringUtils.isNotEmpty(hookUrl) && StringUtils.isNotEmpty(channel)) {
//            if ("local".equals(profile)) {
//                message.setTitle("[local test] " + message.getTitle());
//            }
//
//            SlackMessage slackMessage = SlackMessage.builder()
//                    .channel("#" + channel)
//                    .username(target.username)
//                    .attachments(Collections.singletonList(message)).build();
//
//            try {
//                restTemplate.postForEntity(hookUrl, slackMessage, String.class);
//                return true;
//            } catch (Exception e) {
//                log.error("Occur Exception: {}", e);
//                return false;
//            }
//        }
//        return false;
//    }

    public void notify(SlackTarget target, SlackMessageAttachement message, List<SlackHook> slackList) {
        if ("local".equals(profile)) {
            message.setTitle("[local test] " + message.getTitle());
        }
        for (SlackHook slack : slackList) {
            if (StringUtils.isNotEmpty(slack.getHookUrl()) && StringUtils.isNotEmpty(slack.getChannel())) {
                SlackMessage slackMessage = SlackMessage.builder()
                        .channel("#" + slack.getChannel())
                        .username(target.username)
                        .attachments(Collections.singletonList(message)).build();

                try {
                    restTemplate.postForEntity(slack.getHookUrl(), slackMessage, String.class);
                } catch (Exception e) {
                    log.error("Occur Exception: {}", e);
                }
            }
        }
    }
}

