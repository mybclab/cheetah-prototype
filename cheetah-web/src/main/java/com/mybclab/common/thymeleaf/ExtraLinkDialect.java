package com.mybclab.common.thymeleaf;

import com.mybclab.common.thymeleaf.processor.LinkAttrProcessor;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 클래스를 대표할 수 있는 설명 문단을 작성한다.
 *
 * @author hkwon
 * @since 2018.11.01
 */
public class ExtraLinkDialect extends AbstractProcessorDialect {
    public static final String NAME = "ExtraLink";
    public static final String DEFAULT_PREFIX = "th";
    public static final int PROCESSOR_PRECEDENCE = 800;
    private String charset;

    public ExtraLinkDialect(String charset) {
        super(NAME, DEFAULT_PREFIX, PROCESSOR_PRECEDENCE);

        this.charset = charset;
    }

    protected ExtraLinkDialect(String name, String prefix, int processorPrecedence) {
        super(NAME, DEFAULT_PREFIX, PROCESSOR_PRECEDENCE);
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        final Set<IProcessor> processors = new LinkedHashSet<IProcessor>();

        processors.add(new LinkAttrProcessor(TemplateMode.HTML, dialectPrefix, charset));

        return processors;
    }
}