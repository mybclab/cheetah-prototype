package com.mybclab.cheetah.container.domain;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "containerHistory")
@Getter
@Setter
@ToString(exclude = {"gpu", "node"})
@NoArgsConstructor
public class ContainerHistory implements Serializable {


    @EmbeddedId
    private ContainerHistoryPK containerHistoryPK;

    public ContainerHistory(Container container) {
        this.setContainerHistoryPK(new ContainerHistoryPK(container.getContainerPK()));
        this.setGpuSn(container.getGpuSn());
        this.setNodeSn(container.getNodeSn());
        this.setGpuQuantity(container.getGpuQuantity());
        this.setGpuAmount(container.getGpu().getAmountByChargingMethod(container.getChargingMethodCode()));
        this.setChargingMethodCode(container.getChargingMethodCode());
        this.setStartDate(container.getStartDate());
    }
    /**
     * GPU 일련번호
     */
    private Long gpuSn;
    /**
     * GPU 수량
     */
    private int gpuQuantity;
    /**
     * GPU 금액
     */
    @CurrencyFormat
    private Double gpuAmount;
    /**
     * 노드 일련번호
     */
    private Long nodeSn;
    /**
     * 과금 방법 코드
     * {@link Code.CHARGING_METHOD}
     */
    @Enumerated(EnumType.STRING)
    private Code.CHARGING_METHOD chargingMethodCode;
    /**
     * 시작 일시
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;

    /**
     * 반납 일시
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime returnDate;
    /**
     * 결제 적용 시작 일시
     */
    @Transient
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime useStartDate;
    /**
     * 결제 적용 종료 일시
     */
    @Transient
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime useEndDate;
    /**
     * 결제 적용 시간/일/월 수
     */
    @Transient
    private long useMonths;
    @Transient
    private long useDays;
    @Transient
    private long useHours;
    @Transient
    private long useMinutes;

    /**
     * {@link Container}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "containerId", referencedColumnName = "containerId", insertable = false, updatable = false)
    })
    private Container container;
    /**
     * {@link Gpu}
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gpuSn", insertable = false, updatable = false)
    private Gpu gpu;
    /**
     * {@link Node}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nodeSn", referencedColumnName = "nodeSn",  insertable = false, updatable = false)
    private Node node;

    @CurrencyFormat
    @Transient
    private Double monthlyAmount;
}
