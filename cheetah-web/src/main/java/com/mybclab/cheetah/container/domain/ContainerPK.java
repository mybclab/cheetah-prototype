package com.mybclab.cheetah.container.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 컨테이너 PK
 */
@Getter
@Setter
@ToString
@Embeddable
public class ContainerPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 컨테이너 아이디
     */
    private String containerId;

    /**
     * default constructor
     */
    public ContainerPK() {
    }

    /**
     * constuctor
     * @param username 사용자아이디
     * @param containerId 컨테이너 아이디
     */
    public ContainerPK(String username, String containerId) {
        this.username = username;
        this.containerId = containerId;
    }

}
