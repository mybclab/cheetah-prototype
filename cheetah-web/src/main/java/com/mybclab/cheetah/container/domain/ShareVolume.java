package com.mybclab.cheetah.container.domain;

import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
public class ShareVolume extends HistoryBase implements Serializable {


    @EmbeddedId
    private ShareVolumePK shareVolumePK;


    private String volumeName;

    private int volumeSize;

    private String description;
}
