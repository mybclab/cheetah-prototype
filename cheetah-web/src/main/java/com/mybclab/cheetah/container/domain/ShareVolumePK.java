package com.mybclab.cheetah.container.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ShareVolumePK implements Serializable {
    private String username;

    private String volumeId;

    public ShareVolumePK(String username, String volumeId) {
        this.username = username;
        this.volumeId = volumeId;
    }
}
