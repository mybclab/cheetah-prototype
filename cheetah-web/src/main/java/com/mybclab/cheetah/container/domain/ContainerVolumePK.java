package com.mybclab.cheetah.container.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Embeddable
@NoArgsConstructor
public class ContainerVolumePK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 컨테이너 아이디
     */
    private String containerId;

    /**
     * 컨테이너 볼륨 번호
     */
    private int containerVolumeNo;

    public ContainerVolumePK(String username, String containerId, int containerVolumeNo) {
        this.username = username;
        this.containerId = containerId;
        this.containerVolumeNo = containerVolumeNo;
    }
}
