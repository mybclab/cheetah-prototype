package com.mybclab.cheetah.container.domain;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.domain.HistoryBase;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 컨테이너
 */
@Entity
@Table(name = "container")
@Getter
@Setter
@Slf4j
@ToString(exclude = {"containerHistoryList", "containerVolumeList", "groupContainerList", "parentContainer", "groupTargetUserList"})
public class Container extends HistoryBase implements Serializable {


    /**
     * {@link ContainerPK}
     */
    @EmbeddedId
    private ContainerPK containerPK;

    /**
     * 컨테이너 명
     */
    private String containerName;

    /**
     * GPU 일련번호
     */
    private Long gpuSn;

    /**
     * 노드 일련번호
     */
    private Long nodeSn;

    /**
     * 파드 포맷 일련번호
     */
    private Long podFormatSn;

    /**
     * GPU 수량
     */
    private int gpuQuantity;

    /**
     * 내부 아이피
     */
    private String internalIp;

    /**
     * 외부 아이피
     */
    private String externalIp;

    /**
     * 노트북 포트
     */
    private int notebookPort;

    /**
     * 노트북 토큰
     */
    private String notebookToken;

    /**
     * SSH 포트
     */
    private int sshPort;

    /**
     * SSH 비밀번호
     */
    private String sshPassword;

    /**
     * 웹소켓 포트
     */
    private int websocketPort;

    /**
     * 텐서보드 포트
     */
    private int tensorboardPort;

    /**
     * 설명
     */
    private String description;

    /**
     * 상태 코드
     * {@link Code.CONTAINER_STATUS}
     */
    @Enumerated(EnumType.STRING)
    private Code.CONTAINER_STATUS statusCode;

    /**
     * 파드 명
     */
    private String podName;
    /**
     * 파드 상태 코드
     * {@link Code.POD_STATUS}
     */
    @Enumerated(EnumType.STRING)
    private Code.POD_STATUS podStatusCode;
    /**
     * 과금 방법 코드
     * {@link Code.CHARGING_METHOD}
     */
    @Enumerated(EnumType.STRING)
    private Code.CHARGING_METHOD chargingMethodCode;

    /**
     * 시작 일시
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime startDate;

    /**
     * 반납 일시
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime returnDate;
    /**
     * {@link User}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private User user;


    /**
     * {@link Gpu}
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gpuSn", insertable = false, updatable = false)
    private Gpu gpu;

    /**
     * {@link Node}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nodeSn", referencedColumnName = "nodeSn",  insertable = false, updatable = false)
    private Node node;


    /**
     * {@link PodFormat}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "podFormatSn", referencedColumnName = "podFormatSn",  insertable = false, updatable = false)
    private PodFormat podFormat;


    /**
     * 컨테이너 이력 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "containerId", referencedColumnName = "containerId", insertable = false, updatable = false)
    })
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("startDate DESC")
    private List<ContainerHistory> containerHistoryList = new ArrayList<>();

    /**
     * 컨테이너 볼륨 목록
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "containerId", referencedColumnName = "containerId", insertable = false, updatable = false)
    })
    @Fetch(FetchMode.SUBSELECT)
    @OrderBy("containerVolumePK.containerVolumeNo DESC")
    private List<ContainerVolume> containerVolumeList = new ArrayList<>();

    @Transient
    private List<Container> groupContainerList = new ArrayList<>();

    @Transient
    private Container parentContainer = null;

    @CurrencyFormat
    @Transient
    private Double monthlyAmount;


    private String instanceId;

    private Boolean listenYn = false;

    private Boolean groupSharedYn = false;

    private String parentContainerId;

    private Double groupSharedSize = 0D;

    private String groupTargetUser;

    private int cpuLimit;

    private int memoryLimit;

    private Boolean tokenYn = false;

    @Transient
    public List<User> groupTargetUserList = new ArrayList<>();

    public Container() {}

    public Container(ContainerPK containerPK) {
        setContainerPK(containerPK);
    }

    public boolean isContract() {
        switch (getChargingMethodCode()) {
            case MINUTELY:
            case HOURLY:
            case DAILY:
            case MONTHLY: return false;
            default: return true;
        }
    }

    @CurrencyFormat
    public Double getAmount() {
        return gpu != null ? gpu.getAmountByChargingMethod(getChargingMethodCode()) : 0D;
    }

    public LocalDateTime getContractEndDate() {
        LocalDateTime contractEndDate = getStartDate();
        switch (getChargingMethodCode()) {
            case MONTHLY_CONTRACT_1: contractEndDate= LocalDateTime.from(contractEndDate).plusMonths(1); break;
            case MONTHLY_CONTRACT_3: contractEndDate= LocalDateTime.from(contractEndDate).plusMonths(3); break;
            case MONTHLY_CONTRACT_6: contractEndDate= LocalDateTime.from(contractEndDate).plusMonths(6); break;
            case MONTHLY_CONTRACT_12: contractEndDate= LocalDateTime.from(contractEndDate).plusMonths(12); break;
            case MONTHLY_CONTRACT_24: contractEndDate= LocalDateTime.from(contractEndDate).plusMonths(24); break;
        }
        return contractEndDate;
    }

    public Container(Container container) {
        this.setContainerName(container.getContainerName());
        this.setDescription(container.getDescription());
        this.setGpuSn(container.getGpuSn());
        this.setPodFormatSn(container.getPodFormatSn());
        this.setGpuQuantity(container.getGpuQuantity());
        this.setStatusCode(container.getStatusCode());
        this.setChargingMethodCode(container.getChargingMethodCode());
        this.setGroupSharedYn(container.getGroupSharedYn());
        this.setGroupSharedSize(container.getGroupSharedSize());
        this.setCpuLimit(container.getCpuLimit());
        this.setMemoryLimit(container.getMemoryLimit());
        this.setTokenYn(container.getTokenYn());
        this.setCreatedAt(LocalDateTime.now());
        this.setCreatedBy(container.getContainerPK().getUsername());


    }

    public Boolean isContainerWithInstance() {
        return Code.CLOUD_TYPE.ON_PREMISE.equals(gpu.getCloudType()) || (!Code.CLOUD_TYPE.ON_PREMISE.equals(gpu.getCloudType()) && getParentContainerId() != null);
    }

    public Boolean isContainerWithCloud() {
        return !Code.CLOUD_TYPE.ON_PREMISE.equals(gpu.getCloudType());
    }


    // FIXME tencent volume 하드코딩
    public Boolean isTargetToMakeTencentVolume() {
        String username = getContainerPK().getUsername();
        String[] targetUsername = {"coffee", "ngkim0721", "mybclab", "james"};
        return Code.CLOUD_TYPE.TENCENT.equals(gpu.getCloudType())
                && Arrays.asList(targetUsername).contains(username);
    }

    public int getGroupContainerListSize() {
//        log.debug(">>>>>>> getGroupContainerListSize : getGroupContainerList : " + getGroupContainerList());
//        log.debug(">>>>>>> getGroupContainerListSize : getGroupTargetUserList : " + getGroupTargetUserList());
//        log.debug(">>>>>>> getGroupContainerListSize : getGroupTargetUser : " + getGroupTargetUser());
        if (getGroupContainerList() != null && !getGroupContainerList().isEmpty()) {
            return getGroupContainerList().size();
        } else if (getGroupTargetUserList() != null && !getGroupTargetUserList().isEmpty()) {
            return getGroupTargetUserList().size();
        } else {
            return getGroupTargetUser().split(",").length;
        }
    }

    public int getRemainContainerSize() {
        if (getGroupSharedYn()) {
            int groupContainerSize = getGroupContainerListSize() + 1;
            int maxContainerSize = 0;
            if (Code.RESOURCE_TYPE.CPU.equals(getGpu().getResourceType())) {
                maxContainerSize = (int) ((getGpu().getCpuCore() - 2) / getGroupSharedSize());
            } else if (Code.RESOURCE_TYPE.GPU.equals(getGpu().getResourceType())) {
                maxContainerSize = (int) ((getGpu().getGpuMemory() - 1) / getGroupSharedSize());
            }
            log.debug(">>>>> maxContainerSize : " + maxContainerSize + ", groupContainerSize: " + groupContainerSize + ", cpuCore: " + (getGpu().getCpuCore()));
            return maxContainerSize - groupContainerSize;
        }
        return 0;
    }

}
