package com.mybclab.cheetah.container.domain;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString(exclude = {"container"})
@NoArgsConstructor
public class ContainerVolume implements Serializable {


    @EmbeddedId
    private ContainerVolumePK containerVolumePK;

    /**
     * 볼륨 아이디
     */
    private String volumeId;
    /**
     * 볼륨 크기
     */
    private int volumeSize;

    /**
     * 볼륨 마운트 경로
     */
    private String volumeMountPath;


    /**
     * 클라우드 구분
     */
    @Enumerated(EnumType.STRING)
    private Code.CLOUD_TYPE cloudType;

    /**
     * {@link Container}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "containerId", referencedColumnName = "containerId", insertable = false, updatable = false)
    })
    private Container container;

    public ContainerVolume(ContainerVolume containerVolume) {
        this.setVolumeId(containerVolume.getVolumeId());
        this.setVolumeSize(containerVolume.getVolumeSize());
        this.setVolumeMountPath(containerVolume.getVolumeMountPath());
        this.setCloudType(containerVolume.getCloudType());
    }

    public boolean isOnPremise() {
        return Code.CLOUD_TYPE.ON_PREMISE.equals(this.cloudType);
    }

}
