package com.mybclab.cheetah.container.service;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.repository.ContainerHistoryRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.TimeZone;

@Slf4j
@Service
@NoArgsConstructor
public class ContainerHistoryService {


    private ContainerHistoryRepository containerHistoryRepository;

    @Autowired
    public ContainerHistoryService(ContainerHistoryRepository containerHistoryRepository) {
        this.containerHistoryRepository = containerHistoryRepository;
    }

    @Transactional
    public ContainerHistory save(ContainerHistory containerHistory) {
        if (containerHistory.getContainerHistoryPK().getContainerHistoryNo() == 0) {
            int containerHistoryNo = containerHistoryRepository.findMaxContainerHistoryNo(containerHistory.getContainerHistoryPK().getUsername(), containerHistory.getContainerHistoryPK().getContainerId()) + 1;
            containerHistory.getContainerHistoryPK().setContainerHistoryNo(containerHistoryNo);
        }
        return containerHistoryRepository.saveAndFlush(containerHistory);
    }

    public void delete(ContainerPK containerPK) {
        containerHistoryRepository.deleteAllByContainerHistoryPKUsernameAndContainerHistoryPKContainerId(containerPK.getUsername(), containerPK.getContainerId());
        containerHistoryRepository.flush();
    }

    /**
     * 월별 결제 금액 계산
     * @param username
     * @param yyyyMM
     * @return
     */
    public double calculatePaymentAmount(String username, String yyyyMM, boolean isGroup) {
        List<ContainerHistory> containerHistoryList = this.findAllByMonthly(username, yyyyMM, isGroup);
        if (containerHistoryList != null) {
            return containerHistoryList.stream().mapToDouble(ContainerHistory::getMonthlyAmount).sum();
        }
        return 0;
    }

    /**
     * 결제대상 목록조회 (그룹, 컨테이너별 금액 설정)
     * @param username 그룹아이디(parentUsername)
     * @param targetYearMonth 대상년월(yyyy-MM)
     * @return 결제대상 목록
     */
    public List<ContainerHistory> findAllByMonthly(String username, String targetYearMonth, boolean isGroup) {
        List<ContainerHistory> containerHistoryList = isGroup ? containerHistoryRepository.findAllByMonthlyForGroup(username, targetYearMonth) : containerHistoryRepository.findAllByMonthlyForUser(username, targetYearMonth);
        containerHistoryList.forEach(c -> c.setMonthlyAmount(calculateMonthlyAmountByContainer(c, targetYearMonth)));
        return containerHistoryList;
    }

    /**
     * 컨테이너 월별 금액 계산
     * @param containerHistory {@link ContainerHistory}
     * @param targetYearMonth 대상년월(yyyy-MM)
     * @return 금액
     */
    private Double calculateMonthlyAmountByContainer(ContainerHistory containerHistory, String targetYearMonth) {


        ZoneId utc = ZoneId.of("UTC");
        ZoneId asiaSeoul = TimeZone.getTimeZone("Asia/Seoul").toZoneId();
        YearMonth yearMonth = YearMonth.parse(targetYearMonth, CheetahFormatter.FOR_YEAR_MONTH);
        if (containerHistory.getChargingMethodCode() == Code.CHARGING_METHOD.MINUTELY || containerHistory.getChargingMethodCode() == Code.CHARGING_METHOD.HOURLY) { // minutely, hourly
            long useHours = 1;
            long useMinutes = 0;

            LocalDateTime startDateTime = containerHistory.getStartDate().withSecond(0).atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDateTime();
            LocalDateTime returnDateTime = containerHistory.getReturnDate() == null ? null : containerHistory.getReturnDate().withSecond(59).atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDateTime();

            LocalDateTime startDateTimeOfMonth = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonthValue(), 1, 0, 0, 0).atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDateTime();
            LocalDateTime endDateTimeOfMonth = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonthValue(), yearMonth.atEndOfMonth().getDayOfMonth(), 23, 59, 59).atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDateTime();

            if (startDateTime.compareTo(startDateTimeOfMonth) < 0) {
                startDateTime = startDateTimeOfMonth;
            }
            if (returnDateTime == null || returnDateTime.compareTo(endDateTimeOfMonth) > 0) {
                returnDateTime = endDateTimeOfMonth;
            }
            containerHistory.setUseStartDate(startDateTime);
            containerHistory.setUseEndDate(returnDateTime);

            useMinutes = ChronoUnit.MINUTES.between(startDateTime, returnDateTime) + 1;
            if (containerHistory.getChargingMethodCode() == Code.CHARGING_METHOD.MINUTELY) {
                containerHistory.setUseMinutes(useMinutes);
                return containerHistory.getGpuAmount() * useMinutes * containerHistory.getGpuQuantity();
            } else {
                int minutesOfHour = 60;
                useHours = useMinutes / minutesOfHour;
                useMinutes = useMinutes % minutesOfHour;

                containerHistory.setUseHours(useHours);
                containerHistory.setUseMinutes(useMinutes);

                Double gpuHourly = containerHistory.getGpuAmount();
                double totalAmount = gpuHourly * useHours * containerHistory.getGpuQuantity();
                if (useMinutes != 0) {
                    double gpuMinutely = Math.floor(gpuHourly / minutesOfHour);
                    totalAmount += gpuMinutely * useMinutes * containerHistory.getGpuQuantity();
                }
                return totalAmount;

            }

        } else { // daily, monthly
            long useMonths = 1;
            long useDays = 0;

            LocalDate startDate = containerHistory.getStartDate().atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDate();
            LocalDate returnDate = containerHistory.getReturnDate() == null ? null : containerHistory.getReturnDate().atZone(utc).withZoneSameInstant(asiaSeoul).toLocalDate();

            LocalDate startDateOfMonth = LocalDate.of(yearMonth.getYear(), yearMonth.getMonthValue(), 1);
            LocalDate endDateOfMonth = LocalDate.of(yearMonth.getYear(), yearMonth.getMonthValue(), yearMonth.atEndOfMonth().getDayOfMonth());
            if (startDate.compareTo(startDateOfMonth) < 0) {
                startDate = startDateOfMonth;
            }
            if (returnDate == null || returnDate.compareTo(endDateOfMonth) > 0) {
                returnDate = endDateOfMonth;
            }

            useDays = ChronoUnit.DAYS.between(startDate, returnDate) + 1;
            containerHistory.setUseDays(useDays);
            containerHistory.setUseStartDate(startDate.atTime(0, 0, 0));
            containerHistory.setUseEndDate(returnDate.atTime(23, 59, 59));

            if (containerHistory.getChargingMethodCode() == Code.CHARGING_METHOD.DAILY) { // daily
                Double gpuDaily = containerHistory.getGpuAmount();
                return gpuDaily * useDays;
            } else { // monthly
                // 컨테이너별 월별/일별 금액계산
                Double gpuMonthly = containerHistory.getGpuAmount();
                Double amount = gpuMonthly;
                if (useDays < endDateOfMonth.getDayOfMonth()) {
                    useMonths = 0;
                } else {
                    containerHistory.setUseDays(0);
                }
                containerHistory.setUseMonths(useMonths);
                // log.debug(">>>>>>>>>>> useMonths : "+ useMonths + ", useDays : "+ useDays + ", gpuMonthly : "+ gpuMonthly);
                if (useMonths == 0 && useDays != 0) {
                    Double gpuDaily = gpuMonthly / yearMonth.atEndOfMonth().getDayOfMonth();
                    // log.debug(">>>>>>>>>>> before gpuDaily : "+ gpuDaily);
                    gpuDaily = Math.round(gpuDaily * 100) / 100.0;
                    // log.debug(">>>>>>>>>>> after gpuDaily : "+ gpuDaily);
                    amount = Double.valueOf(Math.round(gpuDaily * useDays));
                }
//                log.debug(">>>>>> calculate : original : " + containerHistory.getStartDate() + "," + containerHistory.getReturnDate() + ", cal : " + startDate + "," + returnDate + ", useMonth : " + useMonths + ", useDays : " + useDays + " :::::: amount: " + amount);
                return amount * containerHistory.getGpuQuantity();
            }



        }
    }
}
