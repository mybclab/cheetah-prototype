package com.mybclab.cheetah.container.service;

import com.mybclab.cheetah.container.domain.ContainerVolume;
import com.mybclab.cheetah.container.repository.ContainerVolumeRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
@NoArgsConstructor
public class ContainerVolumeService {

    private ContainerVolumeRepository containerVolumeRepository;

    @Autowired
    public ContainerVolumeService(ContainerVolumeRepository containerVolumeRepository) {
        this.containerVolumeRepository = containerVolumeRepository;
    }

    @Transactional
    public ContainerVolume save(ContainerVolume containerVolume) {
        if (containerVolume.getContainerVolumePK().getContainerVolumeNo() == 0) {
            int containerHistoryNo = containerVolumeRepository.findMaxContainerVolumeNo(containerVolume.getContainerVolumePK().getUsername(), containerVolume.getContainerVolumePK().getContainerId()) + 1;
            containerVolume.getContainerVolumePK().setContainerVolumeNo(containerHistoryNo);
        }
        return containerVolumeRepository.saveAndFlush(containerVolume);
    }

}
