package com.mybclab.cheetah.container.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 컨테이너 Criteria
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class ContainerCriteria implements Serializable {
    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 상위 사용자아이디
     */
    private String parentUsername;

    /**
     * 컨테이너 아이디
     */
    private String containerId;

    /**
     * GPU 일련번호
     */
    private Long gpuSn;

    /**
     * 노드 일련번호
     */
    private Long nodeSn;

    private Boolean onlyGroupUser;

    /**
     * 상태 코드
     * {@link Code.CONTAINER_STATUS}
     */
    private Code.CONTAINER_STATUS statusCode;

    private List<Code.CONTAINER_STATUS> statusCodeList;
}
