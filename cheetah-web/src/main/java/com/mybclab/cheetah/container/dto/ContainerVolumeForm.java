package com.mybclab.cheetah.container.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ToString
@Getter
@Setter
public class ContainerVolumeForm implements Serializable {

    private String volumeId;

    private String volumeMountPath;

    private int volumeSize;

    private int containerVolumeNo;

    /**
     * 클라우드 구분
     */
    @NotEmpty
    private Code.CLOUD_TYPE cloudType;
}
