package com.mybclab.cheetah.container.repository;

import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.domain.ContainerHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContainerHistoryRepository extends JpaRepository<ContainerHistory, ContainerHistoryPK> {

    @Query(" select coalesce(max(h.containerHistoryPK.containerHistoryNo), 0) " +
            "  from ContainerHistory h" +
            " where h.containerHistoryPK.username = :username" +
            "   and h.containerHistoryPK.containerId = :containerId")
    int findMaxContainerHistoryNo(@Param("username") String username, @Param("containerId") String containerId);


    void deleteAllByContainerHistoryPKUsernameAndContainerHistoryPKContainerId(String username, String containerId);

    /**
     * 결제대상 목록조회 (그룹)
     * @param groupId 그룹아이디(parentUsername)
     * @param targetYearMonth 대상년월(yyyy-MM)
     * @return 결제대상 목록
     */
    @Query("select c " +
            " From User u, ContainerHistory c " +
            "where u.username = c.containerHistoryPK.username " +
            "  and (u.parentUsername = :groupId or u.username = :groupId)" +
            "  and function('date_format', c.startDate, '%Y%m') <= :targetYearMonth " +
            "  and (c.returnDate is null or function('date_format', c.returnDate, '%Y%m') >= :targetYearMonth) " +
            "order by c.startDate asc")
    List<ContainerHistory> findAllByMonthlyForGroup(@Param("groupId") String groupId, @Param("targetYearMonth") String targetYearMonth);

    /**
     * 결제대상 목록조회 (사용자)
     * @param username 사용자아이디
     * @param targetYearMonth 대상년월(yyyy-MM)
     * @return 결제대상 목록
     */
    @Query("select c " +
            " From User u, ContainerHistory c " +
            "where u.username = c.containerHistoryPK.username " +
            "  and u.username = :username " +
            "  and function('date_format', c.startDate, '%Y%m') <= :targetYearMonth " +
            "  and (c.returnDate is null or function('date_format', c.returnDate, '%Y%m') >= :targetYearMonth) " +
            "order by c.startDate asc")
    List<ContainerHistory> findAllByMonthlyForUser(@Param("username") String username, @Param("targetYearMonth") String targetYearMonth);
}
