package com.mybclab.cheetah.container.repository;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 컨테이너 Repository
 */
@Repository
public interface ContainerRepository extends JpaRepository<Container, ContainerPK>, JpaSpecificationExecutor<Container> {

    /**
     * 컨테이너 목록조회 페이징
     * @param specification {@link Specification < Container >}
     * @param pageable {@link Pageable}
     * @return 컨테이너 목록
     */
    Page<Container> findAll(Specification<Container> specification, Pageable pageable);

    List<Container> findAll();

    List<Container> findAllByStatusCode(Code.CONTAINER_STATUS statusCode);

    /**
     * 사용자아이디로 목록조회
     * @param username 사용자아이디
     * @return 컨테이너 목록
     */
    List<Container> findAllByContainerPKUsername(String username);

    @Query("select c " +
            " From User u, Container c " +
            "where u.username = c.containerPK.username " +
            "  and (u.parentUsername = :groupId or u.username = :groupId)" +
            "order by c.createdAt asc")
    List<Container> findAllByGroup(@Param("groupId") String groupId);


    @Query("select c " +
            " From User u, Container c " +
            "where u.username = c.containerPK.username " +
            "  and (u.parentUsername = :groupId or u.username = :groupId)" +
            "  and c.statusCode = :statusCode " +
            "order by c.createdAt asc")
    List<Container> findAllByGroupAndStatusCode(@Param("groupId") String groupId, @Param("statusCode") Code.CONTAINER_STATUS statusCode);

    List<Container> findAllByContainerPKUsernameAndStatusCode(String username, Code.CONTAINER_STATUS statusCode);

    Container findByContainerPKContainerId(String containerId);

    Container findByPodName(String podName);

    List<Container> findAllByNodeSnAndStatusCode(long nodeSn, Code.CONTAINER_STATUS statusCode);
    List<Container> findAllByGpuSnAndStatusCode(long gpuSn, Code.CONTAINER_STATUS statusCode);

    List<Container> findAllByNodeSn(Long nodeSn);


    Container findByInstanceIdAndParentContainerIdIsNull(String instanceId);

    List<Container> findAllByParentContainerId(String parentContainerId);
    Container findOneByParentContainerIdAndContainerPKUsername(String parentContainerId, String username);

}
