package com.mybclab.cheetah.container.repository;

import com.mybclab.cheetah.container.domain.ShareVolume;
import com.mybclab.cheetah.container.domain.ShareVolumePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShareVolumeRepository extends JpaRepository<ShareVolume, ShareVolumePK> {

    List<ShareVolume> findAllByShareVolumePKUsername(String username);
}
