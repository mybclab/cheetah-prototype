package com.mybclab.cheetah.container.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContainerHistoryPK implements Serializable {
    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 컨테이너 아이디
     */
    private String containerId;
    /**
     * 컨테이너 이력 번호
     */
    private int containerHistoryNo;

    public ContainerHistoryPK(ContainerPK containerPK) {
        this.username = containerPK.getUsername();
        this.containerId = containerPK.getContainerId();
    }

}
