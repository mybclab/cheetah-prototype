package com.mybclab.cheetah.container.service;

import com.mybclab.cheetah.container.domain.ShareVolume;
import com.mybclab.cheetah.container.repository.ShareVolumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public class ShareVolumeService implements Serializable {

    private ShareVolumeRepository shareVolumeRepository;

    @Autowired
    public ShareVolumeService(ShareVolumeRepository shareVolumeRepository) {
        this.shareVolumeRepository = shareVolumeRepository;
    }

    public List<ShareVolume> findAllByUsername(String username) {
        return shareVolumeRepository.findAllByShareVolumePKUsername(username);
    }

}
