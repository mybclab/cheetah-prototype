package com.mybclab.cheetah.container.web;

import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.pod.domain.PodFormatOption;
import com.mybclab.cheetah.admin.pod.dto.PodFormatCriteria;
import com.mybclab.cheetah.admin.pod.service.PodFormatService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.domain.ShareVolume;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.container.service.ShareVolumeService;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@NoArgsConstructor
@RestController
@RequestMapping
public class ContainerRestController {


    private PodFormatService podFormatService;
    private GpuService gpuService;
    private CodeGroupService codeGroupService;

    private ContainerService containerService;
    private ShareVolumeService shareVolumeService;

    @Autowired
    public ContainerRestController(PodFormatService podFormatService, GpuService gpuService, CodeGroupService codeGroupService, ContainerService containerService, ShareVolumeService shareVolumeService) {
        this.podFormatService = podFormatService;
        this.gpuService = gpuService;
        this.codeGroupService = codeGroupService;
        this.containerService = containerService;
        this.shareVolumeService = shareVolumeService;
    }

    @PostMapping("/container/validate/podOptions")
    public ResponseEntity<JsonResult> findPodOptions(PodFormatCriteria podFormatCriteria) {
        JsonResult result = new JsonResult();

        List<PodFormatOption> podFormatOptionList = new ArrayList<>();
        List<PodFormat> podFormatList = podFormatService.findAll(podFormatCriteria);
        for (PodFormat podFormat : podFormatList) {
            podFormatOptionList.addAll(podFormat.getPodFormatOptionList());
        }
        long[] possiblePodOptions = podFormatOptionList.stream().mapToLong(o -> o.getPodFormatOptionPK().getPodOptionSn()).distinct().toArray();
        result.setStatus(ResultStatus.SUCCESS);
        result.setData("podOptionSns", possiblePodOptions);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/gpu/{gpuSn}")
    public ResponseEntity<JsonResult> gpu(@PathVariable long gpuSn) {
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);
        result.setData("gpu", gpuService.findById(gpuSn));
        result.setData("chargingMethodList", codeGroupService.getCodeList(CodeGroupID.CHARGING_METHOD));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping("/container/{username}/{containerId}/status")
    public ResponseEntity<JsonResult> checkStatus(ContainerPK containerPK) {
        JsonResult result = new JsonResult();

        Container container = containerService.findById(containerPK);

        result.setStatus(ResultStatus.SUCCESS);
        result.setData("statusCode", container.getStatusCode());
        result.setData("podStatusCode", container.getPodStatusCode());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @GetMapping("/container/{username}/shareVolumes")
    public ResponseEntity<JsonResult> checkStatus(@PathVariable String username) {
        JsonResult result = new JsonResult();

        List<ShareVolume> shareVolumeList = shareVolumeService.findAllByUsername(username);

        result.setData("shareVolumeList", shareVolumeList);
        result.setStatus(ResultStatus.SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

}
