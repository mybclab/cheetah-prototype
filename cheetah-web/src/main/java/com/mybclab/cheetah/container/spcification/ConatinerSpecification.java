package com.mybclab.cheetah.container.spcification;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerPK;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

/**
 * 컨테이너 Specification
 */
public class ConatinerSpecification {

    /**
     * equal 사용자아이디
     * @param username 사용자아이디
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsUsername(final String username) {
        if (null == username || "".equals(username)) {
            return null;
        }
        return Optional.ofNullable(username)
                .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<ContainerPK>get("containerPK").<String>get("username"), o))
                .orElse(null);
    }

    /**
     * equal 상위 사용자아이디
     * @param parentUsername 상위 사용자아이디
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsParentUsername(final String parentUsername, final Boolean onlyGroupUser) {
        if ("ALL".equals(parentUsername)) {
            return null;
        }
        if ("".equals(parentUsername)) {
            return Optional.ofNullable(parentUsername)
                    .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.and(
                            criteriaBuilder.isNull(root.<User>get("user").<String>get("parentUsername")), criteriaBuilder.isNull(root.<User>get("user").<String>get("groupName"))))
                    .orElse(null);
        }
        return onlyGroupUser != null && onlyGroupUser ?
                Optional.ofNullable(parentUsername).map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<User>get("user").<String>get("parentUsername"), o)).orElse(null)
                : Optional.ofNullable(parentUsername).map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.or(
                        criteriaBuilder.equal(root.<User>get("user").<String>get("parentUsername"), o), criteriaBuilder.equal(root.<User>get("user").<String>get("username"), o))).orElse(null);
    }

    /**
     * equal 상태 구분 코드
     * @param statusCode 상태 구분 코드
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsStatusCode(final Code.CONTAINER_STATUS statusCode) {
        return Optional.ofNullable(statusCode)
                .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Code.CONTAINER_STATUS>get("statusCode"), o))
                .orElse(null);
    }

    public static Specification<Container> inStatusCode(final List<Code.CONTAINER_STATUS> statusCodeList) {
        if (null == statusCodeList || statusCodeList.size() == 0) {
            return null;
        }
        return (root, query, criteriaBuilder) -> root.<Code.CONTAINER_STATUS>get("statusCode").in(statusCodeList);
    }



    /**
     * equal 컨테이너 아이디
     * @param containerId 컨테이너 아이디
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsContainerId(final String containerId) {
        return Optional.ofNullable(containerId)
                .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<ContainerPK>get("containerPK").<String>get("containerId"), o))
                .orElse(null);
    }


    /**
     * equal GPU 일련번호
     * @param gpuSn GPU 일련번호
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsGpuSn(final Long gpuSn) {
        return Optional.ofNullable(gpuSn)
                .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Long>get("gpuSn"), o))
                .orElse(null);
    }

    /**
     * equal node 일련번호
     * @param nodeSn node 일련번호
     * @return {@link Specification < Container >}
     */
    public static Specification<Container> equalsNodeSn(final Long nodeSn) {
        return Optional.ofNullable(nodeSn)
                .map(o -> (Specification<Container>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Long>get("nodeSn"), o))
                .orElse(null);
    }
}
