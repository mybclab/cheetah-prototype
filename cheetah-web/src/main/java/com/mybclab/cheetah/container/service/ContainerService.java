package com.mybclab.cheetah.container.service;

import com.mybclab.cheetah.admin.pod.repository.DeploymentConditionRepository;
import com.mybclab.cheetah.admin.pod.repository.PodConditionRepository;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.service.MailService;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.cheetah.container.domain.*;
import com.mybclab.cheetah.container.dto.ContainerCriteria;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.exception.KubernetesException;
import com.mybclab.cheetah.kubernetes.service.KubernetesAPIService;
import com.mybclab.cheetah.kubernetes.watch.KubernetesWatch;
import com.mybclab.cheetah.management.cloud.service.InstanceService;
import com.mybclab.common.utils.StringUtils;
import io.kubernetes.client.models.V1Deployment;
import io.kubernetes.client.models.V1PersistentVolume;
import io.kubernetes.client.models.V1PersistentVolumeClaim;
import io.kubernetes.client.models.V1Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.mybclab.cheetah.container.spcification.ConatinerSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

/**
 * 컨테이너 Service
 */
@Service
@Slf4j
public class ContainerService implements Serializable {

    /**
     * {@link ContainerRepository}
     */
    private ContainerRepository containerRepository;

    private KubernetesAPIService kubernetesAPIService;

    private PodConditionRepository podConditionRepository;

    private DeploymentConditionRepository deploymentConditionRepository;

    private UserService userService;
    /**
     * {@link MailService}
     */
    private MailService mailService;

    private KubernetesWatch kubernetesWatch;

    /**
     * {@link CheetahMessageSource}
     */
    private CheetahMessageSource messageSource;

    @Autowired
    private HttpServletRequest request;

    private ContainerHistoryService containerHistoryService;

    private InstanceService instanceService;

    /**
     * default constructor
     */
    public ContainerService() {}

    /**
     * constructor
     * @param containerRepository {@link ContainerRepository}
     */
    @Autowired
    public ContainerService(ContainerRepository containerRepository, KubernetesAPIService kubernetesAPIService, UserService userService
                            , PodConditionRepository podConditionRepository, DeploymentConditionRepository deploymentConditionRepository, ContainerHistoryService containerHistoryService, InstanceService instanceService
            , MailService mailService, KubernetesWatch kubernetesWatch, CheetahMessageSource messageSource) {
        this.containerRepository = containerRepository;
        this.kubernetesAPIService = kubernetesAPIService;
        this.userService = userService;
        this.podConditionRepository = podConditionRepository;
        this.deploymentConditionRepository = deploymentConditionRepository;
        this.containerHistoryService = containerHistoryService;
        this.instanceService = instanceService;
        this.mailService = mailService;
        this.kubernetesWatch = kubernetesWatch;
        this.messageSource = messageSource;
    }

    /**
     * 컨테이너 목록조회(페이징)
     * @param containerCriteria {@link ContainerCriteria}
     * @param pageable {@link Pageable}
     * @return 컨테이너 목록
     */
    public Page<Container> findAll(ContainerCriteria containerCriteria, Pageable pageable) {
        return containerRepository.findAll(getContainerSpecification(containerCriteria), pageable);
    }


    /**
     * 컨테이너 목록조회
     * @return 컨테이너 목록
     */
    public List<Container> findAll() {
        return containerRepository.findAll();
    }


    /**
     * 컨테이너 목록조회
     * @param username 사용자아이디
     * @return 컨테이너 목록
     */
    public List<Container> findAllByUsername(String username) {
        return containerRepository.findAllByContainerPKUsername(username);
    }

    public List<Container> findAllByGroup(String parentUsername) {
        return containerRepository.findAllByGroup(parentUsername);
    }

    /**
     * 저장
     * @param container {@link Container}
     * @return {@link Container}
     */
    @Transactional
    public Container save(Container container) {
        return containerRepository.saveAndFlush(container);
    }

    /**
     * 컨테이너 등록 시 승인담당자에게 이메일 보내기
     * @param container {@link Container}
     * @return {@link Container}
     */
    @Transactional
    public Container register(Container container) {
        this.save(container);
        sendContainerConfirmMail(container);
        return container;
    }

    private void sendContainerConfirmMail(Container container) {
        container = this.findById(container.getContainerPK());
//        log.debug(">>>>>>>>> sendContainerConfirmMail container : " + container);
        User user = userService.findById(container.getContainerPK().getUsername());
//        log.debug(">>>>>>>>> sendContainerConfirmMail user : " + user);
        Context context = new Context();
        context.setVariable("name", user.getName());
        context.setVariable("username", user.getUsername());
        context.setVariable("containerName", container.getContainerName());
        context.setVariable("confirmUrl", request.getServerName() + ":" + request.getServerPort() + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId());
        if (user.isGroupUser()) {
            User group = userService.findById(user.getParentUsername());
//            log.debug(">>>>>>>>> sendContainerConfirmMail group admin email : " + group.getEmail());
            mailService.sendTemplateMessage(group.getEmail(), messageSource.getMessage("mail.confirmContainer.title"), Code.MAIL_TEMPLATE.CONFIRM_CONTAINER, context);
        } else if (user.isGroupAdmin() || user.isGeneralUser()) {
            List<User> userList = userService.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN);
            for (User admin : userList) {
//                log.debug(">>>>>>>>> sendContainerConfirmMail system admin email : " + admin.getEmail());
                mailService.sendTemplateMessage(admin.getEmail(), messageSource.getMessage("mail.confirmContainer.title"), Code.MAIL_TEMPLATE.CONFIRM_CONTAINER, context);
            }
        }
    }


    /**
     * getContainerSpecification
     * @param containerCriteria {@link ContainerCriteria}
     * @return {@link Specification<Container>}
     */
    private Specification<Container> getContainerSpecification(ContainerCriteria containerCriteria) {
        return where(equalsUsername(containerCriteria.getUsername()))
//                .and(equalsContainerId(containerCriteria.getContainerId()))
                .and(equalsParentUsername(containerCriteria.getParentUsername(), containerCriteria.getOnlyGroupUser()))
                .and(equalsStatusCode(containerCriteria.getStatusCode()))
                .and(inStatusCode(containerCriteria.getStatusCodeList()))
                .and(equalsNodeSn(containerCriteria.getNodeSn()))
                .and(equalsGpuSn(containerCriteria.getGpuSn()));

    }

    /**
     * 컨테이너 삭제
     * @param containerPK {@link ContainerPK}
     */
    @Transactional
    public void delete(ContainerPK containerPK) {
        podConditionRepository.deleteByUsernameAndContainerId(containerPK.getUsername(), containerPK.getContainerId());
        deploymentConditionRepository.deleteByUsernameAndContainerId(containerPK.getUsername(), containerPK.getContainerId());
        containerHistoryService.delete(containerPK);
        containerRepository.deleteById(containerPK);
        containerRepository.flush();
    }

    @Transactional
    public void deleteAll(List<Container> containerList) {
        for (Container container : containerList) {
            this.delete(container.getContainerPK());
        }
    }


    public Container findById(ContainerPK containerPK) {
        Container container = containerRepository.findById(containerPK).orElseThrow(EntityNotFoundException::new);
        setExtraInfo(container);
        return  container;
    }

    public void setExtraInfo(Container container) {
        if (StringUtils.isNotEmpty(container.getGroupTargetUser())) {
            String[] groupTargetUsers = container.getGroupTargetUser().split(",");
            List<User> groupTargetUserList = userService.findAllByUsernameIn(groupTargetUsers);
            container.setGroupTargetUserList(groupTargetUserList);
        }
        List<Container> groupContainerList = containerRepository.findAllByParentContainerId(container.getContainerPK().getContainerId());
        container.setGroupContainerList(groupContainerList);
    }

    public List<Container> findAllByParentContainerId(String parentContainerId) {
        return containerRepository.findAllByParentContainerId(parentContainerId);
    }

    public Container findByParentContainerId(String parentContainerId, String username) {
        return containerRepository.findOneByParentContainerIdAndContainerPKUsername(parentContainerId, username);
    }


    @Transactional
    public boolean createContainer(Container container, boolean checkExsits) throws KubernetesException {
        if (checkExsits) {
            V1Deployment deployment = kubernetesAPIService.getDeployment(container);
            V1Service service = kubernetesAPIService.getService(container);
            V1PersistentVolumeClaim persistentVolumeClaim = kubernetesAPIService.getPersistentVolumeClaim(container);
            V1PersistentVolume persistentVolume = kubernetesAPIService.getPersistentVolume(container);
            if (deployment != null || service != null || persistentVolumeClaim != null || persistentVolume != null) {
                throw new KubernetesException(Kubernetes.KIND.RETURN, "Kubernetes.recreate.return.notyet");
            }
        }
        String token = container.getTokenYn() ? StringUtils.randomAlphanumeric(20) : "";
        log.debug(">>>>>>>>>>> createContainer token : " + token);
        container.setNotebookToken(token);
        String sshPassword = StringUtils.randomAlphanumeric(6);// "ssh password";
        container.setSshPassword(sshPassword);
        log.debug(">>>>>>>>>>>>>>>>> accelator : container.getGpu(): " + container.getGpu() + "," + container.getGpuSn());
        String accelator = (Code.CLOUD_TYPE.ON_PREMISE.equals(container.getGpu().getCloudType()) ? container.getGpu().getGpuLabel() : container.getInstanceId());
        log.debug(">>>>>>>>>>> createContainer accelator : " + accelator);
        boolean result = kubernetesAPIService.createAll(container, token, sshPassword, accelator);
        log.debug(">>>>>>>>>>> createContainer result : " + result);
        if (result) {
            String serverName = "cheetah.n3ncloud.co.kr";
            int serverPort = 80;
            try {
                serverName = request.getServerName();
                serverPort = request.getServerPort();
            } catch (Exception e) {
                log.error(">>>>>>>>>>>>> createContainer result for k8s exception request.getServerXXX");
            }
            kubernetesWatch.watchPod(kubernetesAPIService.generateLabel(container), serverName, serverPort);
        }
        if (result) {
            container.setStatusCode(Code.CONTAINER_STATUS.CREATE);
            log.debug(">>>>>>>>>>> createContainer save here : " + container);
            this.save(container);
        } else {
            initKubernetesInfo(container);
            this.save(container);
            log.debug(">>>>>>>>>>> createContainer init : " + container);
        }
        return result;
    }

    public void initKubernetesInfo(Container container) {
        container.setNodeSn(null);
        container.setInternalIp(null);
        container.setExternalIp(null);
        container.setNotebookPort(0);
        container.setSshPort(0);
        container.setSshPassword(null);
        container.setNotebookToken(null);
        container.setPodStatusCode(null);
        container.setPodName(null);
    }



    public boolean restartContainer(Container container) {
        boolean result = kubernetesAPIService.deletePod(container);
        if (result) {
            container.setStatusCode(Code.CONTAINER_STATUS.STOP);
            this.save(container);
        }
        return result;
    }

    @Transactional
    public boolean returnContainer(Container container) {

        boolean result = kubernetesAPIService.deleteAll(container);
        log.debug(">>>>>>>>>>>>>> returnContainer result : " + result);

        if (result || Code.CONTAINER_STATUS.STOP.equals(container.getStatusCode())) {
            returnContainerStatus(container);
            result = true;
        }
        return result;
    }

    public void returnContainerStatus(Container container) {
        container.setStatusCode(Code.CONTAINER_STATUS.RETURN);
        container.setReturnDate(LocalDateTime.now());
        initKubernetesInfo(container);
        this.save(container);
        // 컨테이너 이력의 returnDate 넣어주기
        ContainerHistory containerHistory = container.getContainerHistoryList().stream().filter(h -> h.getReturnDate() == null).findFirst().orElse(null);
        if (containerHistory != null) {
            containerHistory.setReturnDate(container.getReturnDate());
            containerHistoryService.save(containerHistory);
        }
    }

    public boolean initContainer(Container container) {
        boolean result = kubernetesAPIService.deleteAll(container);
        log.debug(">>>>>>>>>>>>>> initContainer result : " + result);
        container.setStatusCode(Code.CONTAINER_STATUS.REQUEST);
        initKubernetesInfo(container);
        this.save(container);
        return result;
    }

    public Container findByContainerId(String containerId) {
        Container container = containerRepository.findByContainerPKContainerId(containerId);
//        setExtraInfo(container);
        return container;
    }


    public String generateContainerId(String username) {
        while (true) {
            String randomKey = StringUtils.randomAlphanumeric(6).toLowerCase();
            ContainerPK containerPK = new ContainerPK(username, randomKey);
            if (!containerRepository.existsById(containerPK)) {
                return randomKey;
            }
        }
    }

    public Container findByPodName(String hostname) {
        return containerRepository.findByPodName(hostname);
    }

    public int findUseGpuQuantityByNode(long nodeSn) {
        return containerRepository.findAllByNodeSnAndStatusCode(nodeSn, Code.CONTAINER_STATUS.START).stream().mapToInt(c -> c.getGpuQuantity()).sum();
    }

    public List<Container> findUseContainerByNode(long nodeSn) {
        return containerRepository.findAllByNodeSnAndStatusCode(nodeSn, Code.CONTAINER_STATUS.START);
    }

    public List<Container> findUseContainerByGpu(long gpuSn) {
        return containerRepository.findAllByGpuSnAndStatusCode(gpuSn, Code.CONTAINER_STATUS.START);
    }

    public List<Container> findAllByNode(Long nodeSn) {
        return containerRepository.findAllByNodeSn(nodeSn);
    }

    @Transactional
    public boolean createInstance(Container container) {
        String instanceId = instanceService.createInstance(container);

        if (instanceId != null) {

            container.setInstanceId(instanceId); //
            container.setStatusCode(Code.CONTAINER_STATUS.CREATE);
            container.setPodStatusCode(Code.POD_STATUS.CREATE_INSTANCE);

            this.save(container);
            return true;
        }
        return false;
    }

    @Transactional
    public boolean deleteInstance(Container container) {

        container.setStatusCode(Code.CONTAINER_STATUS.RETURNING);
        this.save(container);

        if (container.getGroupSharedYn()) {
            for (Container groupContainer : container.getGroupContainerList()) {
                if (!Code.CONTAINER_STATUS.RETURN.equals(groupContainer.getStatusCode())) {
                    groupContainer.setStatusCode(Code.CONTAINER_STATUS.RETURNING);
                    this.save(groupContainer);
                }
            }
        }

        boolean result = kubernetesAPIService.deleteAll(container);
        log.debug(">>>>>>>>>>>>>> returnContainer result : " + result);
        if (container.getGroupSharedYn()) {
            for (Container groupContainer : container.getGroupContainerList()) {
                returnContainer(groupContainer);
            }
        }

        instanceService.delete(container.getInstanceId());


        return true;
    }

    public Container findByInstanceId(String instanceId) {
        Container container = containerRepository.findByInstanceIdAndParentContainerIdIsNull(instanceId);
        setExtraInfo(container);
        return container;
    }

    public Container copyContainerToGroupUser(Container container, User user) {
        Container containerForGroupUser = new Container(container);
        containerForGroupUser.setContainerPK(new ContainerPK(user.getUsername(), generateContainerId(user.getUsername())));
        containerForGroupUser.setUser(user);
        containerForGroupUser.setGpu(container.getGpu());
        containerForGroupUser.setPodFormat(container.getPodFormat());
        containerForGroupUser.setParentContainer(container);

        containerForGroupUser.setParentContainerId(container.getContainerPK().getContainerId());
        containerForGroupUser.setInstanceId(container.getInstanceId());

        log.debug(" ################### copyContainerToGroupUser : container.getContainerVolumeList() : {}", container.getContainerVolumeList().size());

        if (container.getContainerVolumeList().size() > 0) {
            List<ContainerVolume> containerVolumeList = new ArrayList<>();
            for (ContainerVolume sourceContainerVolume : container.getContainerVolumeList()) {
                ContainerVolume containerVolume = new ContainerVolume(sourceContainerVolume);
                containerVolume.setContainerVolumePK(new ContainerVolumePK(containerForGroupUser.getContainerPK().getUsername(), containerForGroupUser.getContainerPK().getContainerId(), sourceContainerVolume.getContainerVolumePK().getContainerVolumeNo()));
                containerVolumeList.add(containerVolume);
            }
//                log.debug(" ################### copyContainerToGroupUser : containerVolumeList : {}", containerVolumeList);
            containerForGroupUser.setContainerVolumeList(containerVolumeList);
        }

        log.debug("################### copyContainerToGroupUser containerForGroupUser : {}", containerForGroupUser.getContainerVolumeList());
        return containerRepository.saveAndFlush(containerForGroupUser);
    }

    public void copyContainerToGroupUsers(Container container) {
        log.debug("################### copyContainerToGroupUser container.getGroupTargetUser() : {}", container.getGroupTargetUser());
        if (StringUtils.isNotEmpty(container.getGroupTargetUser())) {
            String[] groupTargetUsers = container.getGroupTargetUser().split(",");
            List<User> groupTargetUserList = userService.findAllByUsernameIn(groupTargetUsers);
            container.setGroupTargetUserList(groupTargetUserList);
        }
        log.debug("################### copyContainerToGroupUser container.getGroupTargetUserList() : {}", container.getGroupTargetUserList().size());
        for (User user : container.getGroupTargetUserList()) {
            copyContainerToGroupUser(container, user);
        }
    }

    public Boolean createContainerWithCloud(Container container) {
        boolean result = false;
        container.setPodStatusCode(Code.POD_STATUS.CREATE_CONTAINER);
        this.save(container);
        result = this.createContainer(container, false);

        if (container.getGroupSharedYn()) {
            copyAndCreateGroupContainers(container);
        }
        log.debug(">>>>>>>>>>> createContainer result : " + result);
        return result;
    }

    /**
     * ON_PREMISE 환경에서 컨테이너 생성
     * @param container
     * @return
     */
    public Boolean createContainerWithoutCloud(Container container) {
        if (container.getGroupSharedYn() && Code.RESOURCE_TYPE.CPU.equals(container.getGpu().getResourceType())) {
            int countOfGroupContainers = container.getGroupContainerListSize() + 1;
            int cpuLimit = (int) (container.getGroupSharedSize() * 1000);
//            int memoryLimit = (int)((container.getGpu().getSystemMemory() * 1000.0) - 50) / (container.getGpu().getCpuCore() - 2);
            int memoryLimit = (int) Math.floor((((container.getGpu().getSystemMemory() * 1000.0) - 50) / (container.getGpu().getCpuCore() - 2)) * container.getGroupSharedSize());
            log.debug(">>>>>>>> ON_PREMISE cpuLimit : " + cpuLimit);
            log.debug(">>>>>>>> ON_PREMISE memoryLimit : " + memoryLimit);
            container.setCpuLimit(cpuLimit);
            container.setMemoryLimit(memoryLimit);
            containerRepository.saveAndFlush(container);
        }

        boolean result = this.createContainer(container, false);

        if (container.getGroupSharedYn()) {
            this.copyAndCreateGroupContainers(container);
        }
        return result;
    }
    /**
     * ON_PREMISE 환경에서 컨테이너 반납
     * @param container
     * @return
     */
    public Boolean returnContainerWithoutCloud(Container container) {
        boolean result = this.returnContainer(container);

        if (container.getGroupSharedYn() && org.apache.commons.lang.StringUtils.isEmpty(container.getParentContainerId())) {
            for (Container groupContainer : container.getGroupContainerList()) {
                this.returnContainer(groupContainer);
            }
        }
        return result;
    }

    public void copyAndCreateGroupContainers(Container container) {
        // 1. copy container
        this.copyContainerToGroupUsers(container);
        List<Container> groupContainerList = this.findAllByParentContainerId(container.getContainerPK().getContainerId());

        // 2. create container
        log.debug("################### container.getGroupContainerList() : " + groupContainerList.size());
        for (Container groupContainer : groupContainerList) {
            log.debug("################### groupContainer : " + groupContainer.getContainerPK() + "," + groupContainer.getGroupSharedYn() + "," + groupContainer.getGroupSharedSize() + "," + groupContainer.getGpu());
            this.createContainer(groupContainer, false);
        }
        log.debug("################### copy end ");
    }

    @Transactional
    public void copyAndCreateGroupContainer(Container container, User user) {
        // 1. copy container
        Container groupContainer = this.copyContainerToGroupUser(container, user);

        groupContainer.setStatusCode(Code.CONTAINER_STATUS.CREATE);
        this.save(groupContainer);
//        Container groupContainer = this.findByParentContainerId(container.getContainerPK().getContainerId(), user.getUsername());

        // 2. create container
        log.debug("################### groupContainer : " + groupContainer.getContainerPK() + "," + groupContainer.getGroupSharedYn() + "," + groupContainer.getGroupSharedSize() + "," + groupContainer.getGpu());
        this.createContainer(groupContainer, false);
        log.debug("################### copy one end ");

        // 3. add username to container.groupTargetUser
        container.setGroupTargetUser(container.getGroupTargetUser() + "," + user.getUsername());
        this.save(container);
//        container.setGroupTargetUser();

    }

}
