package com.mybclab.cheetah.container.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 컨테이너 Form
 */
@Getter
@Setter
@ToString
@Slf4j
public class ContainerForm implements Serializable {


    /**
     * 컨테이너 아이디
     */
    private String containerId;

    @NotBlank(message = "NotBlank.containerName")
    @Size(max = 100)
    private String containerName;

    /**
     * 사용자 명
     */
    private String username;

    /**
     * GPU 일련번호
     */
    @NotNull(message = "NotNull.gpuSn")
    private Long gpuSn;


    @NotNull
    private Map<String, Long> podOptionSnMap;

    /**
     * 파드 포맷 일련번호
     */
    private Long podFormatSn;

    /**
     * GPU 수량
     */
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    @NotNull
    private int gpuQuantity;

    /**
     * 설명
     */
    @NotBlank(message = "NotBlank.description")
    private String description;

    /**
     * 과금 방법 코드
     * {@link Code.CHARGING_METHOD}
     */
    @NotNull
    private Code.CHARGING_METHOD chargingMethodCode;

    private Boolean groupSharedYn = false;

    private Double groupSharedSize;

    private Boolean tokenYn = false;

    private String groupTargetUser;

    public String[] getGroupTargetUsers() {
        return StringUtils.isNotEmpty(this.groupTargetUser) ? this.groupTargetUser.split(",") : null;
    }

    public void setGroupTargetUsers(String[] groupTargetUsers) {
        StringBuilder groupTargetUser = new StringBuilder();
        for (int i = 0; i < groupTargetUsers.length; i++) {
            if (i != 0) {
                groupTargetUser.append(",");
            }
            groupTargetUser.append(groupTargetUsers[i]);
        }
        setGroupTargetUser(groupTargetUser.toString());
    }

    private List<ContainerVolumeForm> containerVolumes = new ArrayList<>();


}
