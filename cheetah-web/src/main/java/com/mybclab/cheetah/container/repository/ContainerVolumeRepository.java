package com.mybclab.cheetah.container.repository;

import com.mybclab.cheetah.container.domain.ContainerVolume;
import com.mybclab.cheetah.container.domain.ContainerVolumePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContainerVolumeRepository extends JpaRepository<ContainerVolume, ContainerVolumePK> {

    @Query(" select coalesce(max(v.containerVolumePK.containerVolumeNo), 0) " +
            "  from ContainerVolume v" +
            " where v.containerVolumePK.username = :username" +
            "   and v.containerVolumePK.containerId = :containerId")
    int findMaxContainerVolumeNo(@Param("username") String username, @Param("containerId") String containerId);
}
