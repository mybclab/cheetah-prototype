package com.mybclab.cheetah.common.constant;

import lombok.Getter;

import java.io.Serializable;

public class Code implements Serializable {
    /**
     * role
     */
    public enum ROLE {
        SYSTEM_ADMIN,
        GROUP_ADMIN,
        GROUP_USER,
        GENERAL_USER
    }

    public enum GROUP_TYPE {
        SCHOOL, COMPANY, ORGANIZATION, ETC
    }

    public enum NODE_STATUS {
        READY, NOT_READY
    }

    public enum CONTAINER_STATUS {
        REQUEST("info"), CONFIRM("primary"), CREATE("primary"), START("success"), RECREATE("success"), RESTART("success"), FAIL("danger"), STOP("warning"), RETURN("dark")
        , CREATE_INSTANCE("primary"), RETURN_INSTANCE("dark"), RETURNING("warning");

        private String badgeClass;

        CONTAINER_STATUS(String badgeClass) {
            this.badgeClass = badgeClass;
        }

        public String getBadgeClass() {
            return "badge-" + this.badgeClass;
        }
    }

    public enum POD_STATUS {
        PENDING(CONTAINER_STATUS.CREATE, "primary"), RUNNING(CONTAINER_STATUS.START, "success")
        , SUCCEEDED(CONTAINER_STATUS.STOP, "warning"), FAILED(CONTAINER_STATUS.STOP, "warning"), COMPLETED(CONTAINER_STATUS.STOP, "success")
        , CRASHLOOKBACKOFF(CONTAINER_STATUS.FAIL, "dark"), CREATE_INSTANCE(CONTAINER_STATUS.CREATE, "primary")
        , ATTACH_VOLUME(CONTAINER_STATUS.CREATE, "primary"), CREATE_CONTAINER(CONTAINER_STATUS.CREATE, "primary")
        , UNKNOWN(null, "dark"); // Terminating

        private CONTAINER_STATUS containerStatus;

        private String badgeClass;

        POD_STATUS(CONTAINER_STATUS containerStatus, String badgeClass) {
            this.containerStatus = containerStatus;
            this.badgeClass = badgeClass;
        }

        public CONTAINER_STATUS getContainerStatus() {
            return this.containerStatus;
        }

        public String getBadgeClass() {
            return "badge-" + this.badgeClass;
        }
    }

    public enum EVENT_TYPE {
        REQUEST, CONFIRM, START, STOP, RETURN
    }

    public enum CREDIT_TYPE {
        WELCOME_GROUP_ADMIN, WELCOME_GENERAL_USER, BONUS
    }

    public enum CREDIT_EXPIRY_TYPE {
        NONE, YEAR, MONTH, WEEK, DAY
    }

    public enum PAYMENT_STATUS {
        READY("dark"), PAID("success"), CANCELLED("warning"), FAILED("danger");

        private String badgeClass;

        PAYMENT_STATUS(String badgeClass) {
            this.badgeClass = badgeClass;
        }

        public String getBadgeClass() {
            return "badge-" + this.badgeClass;
        }
    }

    @Getter
    public enum MAIL_TEMPLATE {
        PASSWORD_RECREATION("mail/passwordRecreation"),
        INVITE_GROUP("mail/inviteGroup"),
        CONFIRM_USER("mail/confirmUser"),
        CONFIRM_CONTAINER("mail/confirmContainer"),
        SSH_PASSWORD("mail/sshPassword"),
        JUPYTER_TOKEN("mail/jupyterToken");

        private String template;

        MAIL_TEMPLATE(String template) {
            this.template = template;
        }
    }

    public enum POD_CONDITION_TYPE {
        INITIALIZED, READY, CONTAINERSREADY, PODSCHEDULED, UNSCHEDULABLE;
    }

    public enum DEPLOYMENT_CONDITION_TYPE {
        AVAILABLE, PROGRESSING;
    }

    public enum BOARD_TYPE {
        QNA, FAQ, NOTICE
    }

    public enum POD_OPTION_TYPE {
        REQUIRED, LIBRARY
    }

    public enum CHARGING_METHOD {
        MINUTELY, HOURLY, DAILY, MONTHLY, MONTHLY_CONTRACT_1, MONTHLY_CONTRACT_3, MONTHLY_CONTRACT_6, MONTHLY_CONTRACT_12, MONTHLY_CONTRACT_24;
    }

    public enum CLOUD_TYPE {
        ON_PREMISE(""), AWS("#ff8e05"), GCP("#397AF2"), TENCENT("#01a252"), AZURE("#0067BF");

        private String badgeClass;

        CLOUD_TYPE(String badgeClass) {
            this.badgeClass = badgeClass;
        }

        public String getBadgeClass() {
            return this.badgeClass;
        }
    }

    public enum RESOURCE_TYPE {
        GPU, CPU
    }

    public enum LOGIN_HISTORY {
        SUCCESS, FAIL_WRONG_PASSWORD, FAIL_NOT_CONFIRMED, FAIL_WITHDRAWAL, FAIL_ALL
    }
}
