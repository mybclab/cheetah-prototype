package com.mybclab.cheetah.common.service;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.cheetah.container.domain.Container;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * email service
 */
@Service
@Slf4j
public class MailService {

    /**
     * {@link JavaMailSender}
     */
    private JavaMailSender mailSender;
    /**
     * {@link SpringTemplateEngine }
     */
    private SpringTemplateEngine templateEngine;

    private CheetahMessageSource messageSource;

    @Value("${spring.mail.username}")
    private String username;
    @Value("${spring.mail.password}")
    private String password;
    @Value("${spring.mail.host}")
    private String host;
    @Value("${spring.mail.port}")
    private String port;
    /**
     * from
     */
    @Value("${spring.mail.username}")
    private String from;

    /**
     * constructor
     *
     * @param mailSender     {@link JavaMailSender}
     * @param templateEngine {@link SpringTemplateEngine}
     */
    @Autowired
    public MailService(JavaMailSender mailSender, SpringTemplateEngine templateEngine, CheetahMessageSource messageSource) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.messageSource = messageSource;
    }



    /**
     * send template message
     */
    @Async("threadPoolSendEmailTaskExecutor")
    public void sendTemplateMessage(String recipient, String title, Code.MAIL_TEMPLATE template, Context context) {
        if (checkMailProperties()) {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setFrom(from, "Cheetah GPU CLOUD");
                messageHelper.setTo(recipient);
                messageHelper.setSubject(title);
                messageHelper.setText(templateEngine.process(template.getTemplate(), context), true);
            };
            try {
                mailSender.send(messagePreparator);
            } catch (MailException e) {
                log.error("Mail send fail", e);
            }
        }
    }

    private boolean checkMailProperties() {
        return StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password) && StringUtils.isNotEmpty(host) && StringUtils.isNotEmpty(port);
    }

    @Async("threadPoolSendEmailTaskExecutor")
    public void sendSshPassword(Container container) {
        if (checkMailProperties()) {
            // email token
            Context context = new Context();
            context.setVariable("password", container.getSshPassword());
            context.setVariable("name", container.getUser().getName());
            context.setVariable("username", container.getUser().getUsername());
            context.setVariable("containerName", container.getContainerName());
            this.sendTemplateMessage(container.getUser().getEmail(), messageSource.getMessage("mail.ssh.password.title"), Code.MAIL_TEMPLATE.SSH_PASSWORD, context);
        }
    }
    @Async("threadPoolSendEmailTaskExecutor")
    public void sendJupyterToken(Container container) {
        if (checkMailProperties() && container.getTokenYn()) {
            // email token
            Context context = new Context();
            context.setVariable("token", container.getNotebookToken());
            context.setVariable("name", container.getUser().getName());
            context.setVariable("username", container.getUser().getUsername());
            context.setVariable("containerName", container.getContainerName());
            this.sendTemplateMessage(container.getUser().getEmail(), messageSource.getMessage("mail.jupyter.token.title"), Code.MAIL_TEMPLATE.JUPYTER_TOKEN, context);
        }
    }



}
