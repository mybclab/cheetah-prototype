package com.mybclab.cheetah.common.formatter;

import java.time.format.DateTimeFormatter;

/**
 * CheetahFormatter
 */
public class CheetahFormatter {

    public final static DateTimeFormatter FOR_DATE_WITH_DASH = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public final static DateTimeFormatter FOR_FULL_DATE_WITH_DASH = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    public final static DateTimeFormatter FOR_YEAR_MONTH_WITH_DASH = DateTimeFormatter.ofPattern("yyyy-MM");
    public final static DateTimeFormatter FOR_DATE = DateTimeFormatter.ofPattern("yyyyMMdd");
    public final static DateTimeFormatter FOR_YEAR_MONTH = DateTimeFormatter.ofPattern("yyyyMM");
    public final static DateTimeFormatter FOR_CARD_EXPIRY = DateTimeFormatter.ofPattern("MMyy");
    public final static DateTimeFormatter FOR_CARD_EXPIRY_YEAR_MONTH = DateTimeFormatter.ofPattern("MMyy");
}
