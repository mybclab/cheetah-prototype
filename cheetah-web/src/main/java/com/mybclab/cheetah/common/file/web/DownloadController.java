package com.mybclab.cheetah.common.file.web;

import com.mybclab.cheetah.common.exception.CheetahRuntimeException;
import com.mybclab.cheetah.common.file.domain.DbFile;
import com.mybclab.cheetah.common.file.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@RestController
public class DownloadController {

    @Value("${spring.servlet.multipart.location}")
    private String BASE_PATH;

    private FileService fileService;


    @Autowired
    public DownloadController(FileService fileService) {
        this.fileService = fileService;
    }


    @GetMapping("/download/{fileSn}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long fileSn, HttpServletRequest request){
        DbFile dbFile = fileService.findById(fileSn);
//        log.debug(">>>>>>> dbFile : " + dbFile);
        Resource resource = null;
        String contentType = null;

        try {
            Path filePath = Paths.get(BASE_PATH).resolve(dbFile.getFilePath() + File.separator + dbFile.getPhysicalFileName()).normalize();
//            log.debug(">>>>>>> filePath : " + filePath);
            resource = new UrlResource(filePath.toUri());
//            log.debug(">>>>>>> resource : " + resource);

            if (!resource.exists()) {
                throw new CheetahRuntimeException("Error.noExistsFile");
            }

            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                log.info("Could not determine file type.");
            }

            if(contentType == null) {
                contentType = "application/octet-stream";
            }
        } catch(MalformedURLException e) {
            throw new CheetahRuntimeException("Error.noExistsFile");
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getLogicalFileName() + "\"")
                .body(resource);
    }
}
