package com.mybclab.cheetah.common.file.service;

import com.mybclab.cheetah.common.exception.StorageException;
import com.mybclab.cheetah.common.file.domain.DbFile;
import com.mybclab.cheetah.common.file.repository.FileRepository;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Slf4j
@Service
public class FileService implements Serializable {

    @Value("${spring.servlet.multipart.location}")
    private String BASE_PATH;

    @Value("${cheetah.multipart.allowed-file-extensions}")
    private String ALLOWED_FILE_EXTENSIONS;

    @Value("${cheetah.multipart.not-allowed-file-extensions}")
    private String NOW_ALLOWED_FILE_EXTENSIONS;

    private FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public DbFile findById(Long fileSn) {
        return fileRepository.findById(fileSn).orElseThrow(EntityNotFoundException::new);
    }

    public DbFile uploadFile(MultipartFile file, String createdBy) {
        return this.uploadFile(file, createdBy, null);
    }

    @Transactional
    public DbFile uploadFile(MultipartFile file, String createdBy, String[] allowedExtensions) throws StorageException {
        String filename = org.springframework.util.StringUtils.cleanPath(file.getOriginalFilename());
        checkFile(file);

        String filePath = makeFilePathFromNow();
        String physicalFileName = makeRandomPhysicalFileName();
        String fileExtension = getFileExtension(filename);

        checkFileExtension(fileExtension);
        if (allowedExtensions != null) {
            checkCustomFileExtension(fileExtension, allowedExtensions);
        }


        try (InputStream inputStream = file.getInputStream()) {
            String fullPath = BASE_PATH + File.separator + filePath;
            checkPath(fullPath);
            Files.copy(inputStream, Paths.get(fullPath).resolve(physicalFileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Valid.file.empty");
        }

        DbFile dbFile = new DbFile();
        dbFile.setLogicalFileName(filename);
        dbFile.setPhysicalFileName(physicalFileName);
        dbFile.setFilePath(filePath);
        dbFile.setFileSize(file.getSize());
        dbFile.setFileExtension(fileExtension);
        dbFile.setCreatedAt(LocalDateTime.now());
        dbFile.setCreatedBy(createdBy);

        return fileRepository.saveAndFlush(dbFile);
    }

    /**
     * 파일 유효성 체크
     * @param file {@link MultipartFile}
     */
    private void checkFile(MultipartFile file) {
        if (file.isEmpty()) {
            throw new StorageException("Valid.file.empty");
        }
        if (file.getOriginalFilename().contains("..")) {
            throw new StorageException("Valid.file.invalidFilename");
        }
    }

    /**
     * 파일 확장자 검사
     * @param ext 확장자
     */
    private void checkFileExtension(String ext) {
        String[] nowAllowedFileExtensions = NOW_ALLOWED_FILE_EXTENSIONS.split(",");
        for (String nowAllowedFileExtension : nowAllowedFileExtensions) {
            if (ext.equals(nowAllowedFileExtension)) {
                throw new StorageException("Valid.file.notAllowedExtension");
            }
        }
        checkCustomFileExtension(ext, ALLOWED_FILE_EXTENSIONS.split(","));
    }
    /**
     * 커스텀 파일 확장자 검사
     * @param ext 확장자
     * @param allowFileExtensions 허용된 확장자 배열
     */
    private void checkCustomFileExtension(String ext, String[] allowFileExtensions) {
        boolean result = false;
        for (String allowFileExtension : allowFileExtensions) {
            if (ext.equals(allowFileExtension)) {
                result = true;
                break;
            }
        }
        if (!result) {
            throw new StorageException("Valid.file.notAllowedExtension");
        }
    }

    /**
     * path 가 존재하지 않으면 create
     * @param fullPath path
     * @throws IOException
     */
    private void checkPath(String fullPath) throws IOException {
        if (!Files.exists(Paths.get(fullPath))) {
            Files.createDirectory(Paths.get(fullPath));
        }
    }

    /**
     * 파일명에서 확장자를 추출
     * @param filename 파일명
     * @return 확장자
     */
    private String getFileExtension(String filename) {
        return filename.contains(".") ? filename.split("[.]")[1] : "";
    }

    /**
     * 랜덤한 물리적 파일명을 생성
     * @return randomPhysicalFileName
     */
    private String makeRandomPhysicalFileName() {
        while (true) {
            String physicalFileName = StringUtils.randomLowerAlphanumeric(20);
            if (!fileRepository.existsByPhysicalFileName(physicalFileName)) {
                return physicalFileName;
            }
        }
    }

    /**
     * yyyy/MM 형태의 path 를 반환
     * @return filePathFromNow
     */
    private String makeFilePathFromNow() {
        return LocalDate.now().getYear() + File.separator + StringUtils.leftPad(Integer.toString(LocalDate.now().getMonthValue()), 2, "0");
    }


}
