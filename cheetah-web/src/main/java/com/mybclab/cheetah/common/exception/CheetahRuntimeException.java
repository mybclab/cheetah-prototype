package com.mybclab.cheetah.common.exception;

public class CheetahRuntimeException extends RuntimeException {
    public CheetahRuntimeException(String message) {
        super(message);
    }
}
