package com.mybclab.cheetah.common.constant;

/**
 * 코드 그룹
 */
public enum CodeGroupID {
    /**
     * 권한
     */
    ROLE("UR"),
    GROUP_TYPE("GT"),
    NODE_STATUS("NS"),
    CREDIT_TYPE("CT"),
    CREDIT_EXPIRY_TYPE("CE"),
    CONTAINER_STATUS("CS"),
    POD_OPTION_GROUP_TYPE("OG"),
    CHARGING_METHOD("CM"),
    CLOUD_TYPE("CL"),
    RESOURCE_TYPE("RT"),
    LOGIN_HISTORY("LH");

    private String codeGroupID;

    CodeGroupID(String codeGroupId) {
        this.codeGroupID = codeGroupId;
    }

    public String getCodeGroupID() {
        return this.codeGroupID;
    }
}
