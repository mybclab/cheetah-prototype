package com.mybclab.cheetah.common.formatter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class DateFormatter implements Formatter<LocalDateTime> {
    public DateFormatter() {
        super();
    }

    @Override
    public LocalDateTime parse(String text, Locale locale) throws ParseException {
        return LocalDateTime.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", locale));
    }

    @Override
    public String print(LocalDateTime object, Locale locale) {
        DateTimeFormatter dateTimeFormatter = createDateFormat(locale);

        return object.format(dateTimeFormatter);
    }

    private DateTimeFormatter createDateFormat(final Locale locale) {
        return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.MEDIUM).withLocale(locale);
        //return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", locale);
    }
}
