package com.mybclab.cheetah.common.batch.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
@Profile("batch")
public class BatchScheduler {
    private final SimpleJobLauncher jobLauncher;

    private final Job paymentMonthlyBatchJob;

    private Job retryFailedPaymentBatchJob;

    /**
     * consturctor
     *
     * @param jobLauncher          {@link SimpleJobLauncher}
     * @param paymentMonthlyBatchJob  {@link Job}
     * @param retryFailedPaymentBatchJob  {@link Job}
     */
    public BatchScheduler(SimpleJobLauncher jobLauncher, Job paymentMonthlyBatchJob, Job retryFailedPaymentBatchJob) {
        this.jobLauncher = jobLauncher;
        this.paymentMonthlyBatchJob = paymentMonthlyBatchJob;
        this.retryFailedPaymentBatchJob = retryFailedPaymentBatchJob;
    }

    /**
     * paymentMonthlyBatchScheduler
     * @throws Exception exception
     */
    @Scheduled(cron = "0 0 1 1 * ?")
//    @Profile("batch")
    public void paymentMonthlyBatchScheduler() throws Exception {
        log.info(" Job Started at :" + LocalDateTime.now());

        JobParameters param = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis())).toJobParameters();
        JobExecution execution = jobLauncher.run(paymentMonthlyBatchJob, param);

        log.info("Job finished with status :" + execution.getStatus());
    }


    /**
     * retryFailedPaymentBatchScheduler
     * @throws Exception exception
     */
    @Scheduled(cron = "0 0 1 2/2 * ?")
//    @Profile("batch")
    public void retryFailedPaymentBatchScheduler() throws Exception {
        log.info(" Job Started at :" + LocalDateTime.now());

        JobParameters param = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis())).toJobParameters();
        JobExecution execution = jobLauncher.run(retryFailedPaymentBatchJob, param);

        log.info("Job finished with status :" + execution.getStatus());
    }


}
