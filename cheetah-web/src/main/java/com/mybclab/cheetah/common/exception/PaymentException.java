package com.mybclab.cheetah.common.exception;

/**
 * 결제 Exception
 */
public class PaymentException extends Exception {

    /**
     * errorMessage
     */
    private String errorMessage;

    public PaymentException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    /**
     * @return errorMessage
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }
}
