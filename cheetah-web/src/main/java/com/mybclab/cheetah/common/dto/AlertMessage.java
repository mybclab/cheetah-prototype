package com.mybclab.cheetah.common.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Alert Message
 */
@Data
@Builder
@ToString
public class AlertMessage implements Serializable {

    /**
     * level
     */
    private ALERT_MESSAGE_LEVEL level;

    /**
     * message code
     */
    private String message;

    /**
     * message code
     */
    private String messageCode;

    /**
     * arguments
     */
    private String[] arguments;

//    /**
//     * default constructor
//     */
//    public AlertMessage() {
//    }
//
//    /**
//     * Constoructor
//     *
//     * @param level       level
//     * @param messageCode message code
//     */
//    public AlertMessage(ALERT_MESSAGE_LEVEL level, String messageCode) {
//        this.level = level;
//        this.messageCode = messageCode;
//    }
//
//    /**
//     * Constoructor
//     *
//     * @param level level
//     * @param messageCode message code
//     * @param arguments arguments
//     */
//    public AlertMessage(ALERT_MESSAGE_LEVEL level, String messageCode, String... arguments) {
//        this.level = level;
//        this.messageCode = messageCode;
//        this.arguments = arguments;
//    }

    public enum ALERT_MESSAGE_LEVEL {
        SUCCESS("success"),
        INFO("info"),
        WARNING("warning"),
        ERROR("error");

        private String level;

        ALERT_MESSAGE_LEVEL(String level) {
            this.level = level;
        }

        public String toString() {
            return this.level;
        }
    }
}
