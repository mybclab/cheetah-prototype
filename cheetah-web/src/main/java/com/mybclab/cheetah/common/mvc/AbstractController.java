package com.mybclab.cheetah.common.mvc;

import com.mybclab.common.result.JsonResult;
import org.springframework.http.ResponseEntity;

/**
 * Created by hkwon on 2018. 6. 27..
 */
public class AbstractController {
    protected static final String REDIRECT_PREFIX = "redirect:";
    protected static final String FORWARD_PREFIX = "forward:";
    protected static final String ROOT = "/";

    protected String redirect(String path) {
        return REDIRECT_PREFIX + path;
    }

    protected String foward(String path) {
        return FORWARD_PREFIX + path;
    }

    protected String goRoot() {
        return REDIRECT_PREFIX + ROOT;
    }

    protected ResponseEntity<Object> buildResponseEntity(JsonResult restResult) {
        return new ResponseEntity<>(restResult, restResult.getHttpStatus());
    }
}
