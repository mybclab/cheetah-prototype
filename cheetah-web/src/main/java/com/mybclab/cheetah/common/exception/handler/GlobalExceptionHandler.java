package com.mybclab.cheetah.common.exception.handler;

import com.mybclab.cheetah.common.exception.CheetahRuntimeException;
import com.mybclab.cheetah.common.exception.UnauthorizedException;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

@ControllerAdvice(basePackages = "com.mybclab.cheetah")
@Slf4j
public class GlobalExceptionHandler {
    private static final String ERROR_VIEW_AJAX = "error/ajax";
    public static final String ERROR_VIEW_BAD_REQUEST = "error/error";
    public static final String ERROR_VIEW_UNAUTHORIZED = "error/error";
    public static final String ERROR_VIEW_ACCESS_DENIED = "error/error";
    private static final String ERROR_VIEW_FILE_NOT_FOUND = "error/error";
    private static final String ERROR_VIEW_INTERNAL_SERVER_ERROR = "error/error";

    private final CheetahMessageSource messageSource;

    public GlobalExceptionHandler(CheetahMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ModelAttribute("trackingId")
    public String addOneObject(@RequestHeader("X-Tracking-Id") Optional<String> trackingId) {
        return trackingId.orElse(UUID.randomUUID().toString());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handleENoHandlerFoundException(HttpServletRequest request, NoHandlerFoundException e) {
        log.debug("NoHandlerFoundException handled!!!!!!!!!!!!!");
        return getModelAndView(request, e, HttpStatus.UNAUTHORIZED, ERROR_VIEW_FILE_NOT_FOUND);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handleEntityNotFoundException(HttpServletRequest request, EntityNotFoundException e) {
        log.debug("EntityNotFoundException handled!!!!!!!!!!!!!");
        return getModelAndView(request, e, HttpStatus.UNAUTHORIZED, ERROR_VIEW_FILE_NOT_FOUND);
    }

    @ExceptionHandler(CheetahRuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleCheetahRuntimeException(HttpServletRequest request, CheetahRuntimeException e) {
        return getModelAndView(request, e, HttpStatus.INTERNAL_SERVER_ERROR, ERROR_VIEW_INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ModelAndView handleUnauthorizedException(HttpServletRequest request, Exception e) {
        log.error("UnauthorizedException occurred", e);
        return getModelAndView(request, e, HttpStatus.UNAUTHORIZED, ERROR_VIEW_INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleGlobalException(HttpServletRequest request, Exception e) {
        log.error("Global exception occurred", e);
        return getModelAndView(request, e, HttpStatus.INTERNAL_SERVER_ERROR, ERROR_VIEW_INTERNAL_SERVER_ERROR);
    }

    private ModelAndView getModelAndView(HttpServletRequest request, Exception ex, HttpStatus httpStatus, String viewFile) {
        String message = ex.getMessage();
        String convertedMessage;

        if (StringUtils.isEmpty(message)) {
            convertedMessage = "";
        } else {
            try {
                convertedMessage = messageSource.getMessage(ex.getMessage());
            } catch (NoSuchMessageException nsme) {
                convertedMessage = "";
            }
        }

        ModelAndView mav;
        if (isAjax(request)) {
            mav = new ModelAndView(ERROR_VIEW_AJAX);
            mav.addObject("errorMsg", convertedMessage);
            mav.setStatus(httpStatus);
        } else {
            mav = new ModelAndView(viewFile);
            mav.addObject("errorMsg", convertedMessage);
        }
        return mav;
    }

    private static boolean isAjax(HttpServletRequest httpServletRequest) {
        return "XMLHttpRequest".equals(httpServletRequest.getHeader("X-Requested-With"));
    }
}
