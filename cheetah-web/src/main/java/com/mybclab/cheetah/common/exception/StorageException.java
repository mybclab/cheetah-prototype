package com.mybclab.cheetah.common.exception;

public class StorageException extends RuntimeException {
    public StorageException(String message) {
        super(message);
    }
}
