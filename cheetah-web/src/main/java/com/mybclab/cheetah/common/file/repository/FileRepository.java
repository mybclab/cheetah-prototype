package com.mybclab.cheetah.common.file.repository;

import com.mybclab.cheetah.common.file.domain.DbFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<DbFile, Long> {

    boolean existsByPhysicalFileName(String physicalFileName);
}
