package com.mybclab.cheetah.login;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import com.mybclab.cheetah.admin.user.domain.UserRole;
import com.mybclab.cheetah.admin.user.dto.LoginForm;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.validator.UserFormValidator;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.common.dto.AlertMessage;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.ArrayList;

/**
 * 로그인 Controller
 */
@Slf4j
@Controller
public class LoginController {


    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    /**
     * {@link UserFormValidator}
     */
    private UserFormValidator userFormValidator;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link BCryptPasswordEncoder}
     */
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${cheetah.payment}")
    private Boolean payment;

    /**
     * default constructor
     */
    public LoginController() {}

    /**
     * constructor
     * @param codeGroupService {@link CodeGroupService}
     * @param userFormValidator {@link UserFormValidator}
     * @param modelMapper {@link ModelMapper}
     * @param userService {@link UserService}
     * @param bCryptPasswordEncoder {@link BCryptPasswordEncoder}
     */
    @Autowired
    public LoginController(CodeGroupService codeGroupService, UserFormValidator userFormValidator, ModelMapper modelMapper, UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.codeGroupService = codeGroupService;
        this.userFormValidator = userFormValidator;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @RequestMapping("/login")
    public String login(@ModelAttribute("loginForm") LoginForm loginForm, Model model) {

        model.addAttribute(loginForm);
        return "login/login";
    }

    /**
     * 회원가입 - 회원구분 선택
     * @return 회원구분 선택 화면
     */
    @RequestMapping("/register")
    public String register() {

        log.debug(" >>>>>>>>>> cheetah.payment : " + payment);

        return "login/register";
    }

    @GetMapping("/register/group")
    public String registerGroupUser(Model model) {
        model.addAttribute("userForm", new UserForm());
        return "login/register-group";
    }


    @PostMapping("/register/group")
    public String registerGroupUserPost(@Valid UserForm userForm, BindingResult bindingResult) {

        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUpForGroupInvite.class);
        try {
            String groupInviteKey = userForm.getGroupInviteKey();
            User group = userService.findByGroupInviteKey(groupInviteKey);
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("groupInviteKey", "Valid.notExistsGroup");
        }

        if (bindingResult.hasErrors()) {
            return "login/register-group";
        }
        return "redirect:/register/" + userForm.getGroupInviteKey();
    }

    /**
     * 회원가입
     * @param model {@link Model}
     * @return 회원가입 화면
     */
    @RequestMapping(value = {"/register/form", "register/{groupInviteKey}", "register/group/{groupInviteKey}"})
    public String registerForm(UserForm userForm, String groupInviteKey, Model model) {
        setGroupInfoByInviteKey(userForm);

        model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
        model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));
        model.addAttribute("userForm", userForm);
        return "login/register-form";
    }

    @PostMapping("/register/card")
    public String registerCard(@Valid UserForm userForm, BindingResult bindingResult, Model model) {
        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUp.class, UserForm.ValidationPassword.class);

        // GROUP_ADMIN, GENERAL_USER 일 때 그룹명 중복 확인
        if (Code.ROLE.GROUP_ADMIN.equals(userForm.getUserRoleCode())) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUpForGroupAdmin.class);
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
            model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));
            return "login/register-form";
        }

        model.addAttribute("userForm", userForm);
        return "login/register-card";
    }


    @PostMapping("/register/complete")
    public String registerComplete(@Valid UserForm userForm, BindingResult bindingResult, Model model) {

        String bindingResultErrorPage = "login/register-form";
        // GROUP_ADMIN, GENERAL_USER 일 때 카드 확인
        if (payment && (Code.ROLE.GROUP_ADMIN.equals(userForm.getUserRoleCode()) || Code.ROLE.GENERAL_USER.equals(userForm.getUserRoleCode()))) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationCard.class);
            bindingResultErrorPage = "login/register-card";
        } else if (Code.ROLE.GROUP_ADMIN.equals(userForm.getUserRoleCode())) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUp.class, UserForm.ValidationPassword.class, UserForm.ValidationSignUpForGroupAdmin.class);
        } else if (Code.ROLE.GENERAL_USER.equals(userForm.getUserRoleCode())) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUp.class, UserForm.ValidationPassword.class);
        } else if (Code.ROLE.GROUP_USER.equals(userForm.getUserRoleCode())) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUp.class, UserForm.ValidationPassword.class, UserForm.ValidationSignUpForGroupUser.class);
        }

        if (bindingResult.hasErrors()) {
            setGroupInfoByInviteKey(userForm);
            model.addAttribute("userForm", userForm);
            model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
            model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));
            return bindingResultErrorPage;
        }



        User user = modelMapper.map(userForm, User.class);
        user.setUserRoles(new ArrayList<UserRole>() {{
            add(new UserRole(user.getUsername(), userForm.getUserRoleCode()));
        }});
        user.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        user.setCreatedBy(user.getUsername());

        if (Code.ROLE.GROUP_ADMIN.equals(userForm.getUserRoleCode())) {
            user.setGroupInviteKey(userService.generateGroupInviteKey());
        } else {
            user.setGroupInviteKey(null);
            user.setGroupName(null);
        }
        if (payment && (Code.ROLE.GROUP_ADMIN.equals(userForm.getUserRoleCode()) || Code.ROLE.GENERAL_USER.equals(userForm.getUserRoleCode()))) {
            UserCardPK userCardPK = modelMapper.map(userForm, UserCardPK.class);
            UserCard userCard = modelMapper.map(userForm, UserCard.class);
            userCard.setUserCardPK(userCardPK);
            userCard.setUseYn(true);
            user.setUserCardList(new ArrayList<UserCard>() {{
                add(userCard);
            }});
        }
        userService.register(user);

        return "redirect:/login?username=" + user.getUsername();
    }

    private void setGroupInfoByInviteKey(UserForm userForm) {
        if (userForm.getGroupInviteKey() != null && !userForm.getGroupInviteKey().equals("")) {
            User group = userService.findByGroupInviteKey(userForm.getGroupInviteKey());
            userForm.setUserRoleCode(Code.ROLE.GROUP_USER);
            userForm.setParentUsername(group.getUsername());
            userForm.setGroupName(group.getGroupName());
        }
    }

    @GetMapping("/forgot-password")
    public String forgotPassword(LoginForm loginForm, Model model) {

        model.addAttribute("loginForm", loginForm);
        return "login/forgot-password";
    }

    @PostMapping("/forgot-password")
    public String resetPassword(LoginForm loginForm, Model model, RedirectAttributes redirectAttributes) {
        try {
            userService.resetPassword(loginForm.getUsername(), loginForm.getEmail());
            model.addAttribute("resetSuccess", true);
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("mail.resetpassword.success").build());
        } catch (EntityNotFoundException e) {
            model.addAttribute("notExistUser", true);
            return "login/forgot-password";
        }

        return "redirect:/login";
    }
}
