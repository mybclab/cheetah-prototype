package com.mybclab.cheetah.payment.service;

import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.exception.PaymentException;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.domain.PaymentPK;
import com.mybclab.cheetah.payment.domain.UserCardPayment;
import com.mybclab.cheetah.payment.domain.UserCreditPayment;
import com.mybclab.cheetah.payment.dto.PaymentDto;
import com.mybclab.cheetah.payment.repository.PaymentRepository;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

/**
 * 결제 Service
 */
@Service
@Slf4j
public class PaymentService implements Serializable {

    /**
     * {@link PaymentRepository}
     */
    private PaymentRepository paymentRepository;

    /**
     * {@link UserCardPaymentService}
     */
    private UserCardPaymentService userCardPaymentService;

    /**
     * {@link UserCreditPaymentService}
     */
    private UserCreditPaymentService userCreditPaymentService;

    /**
     * {@link ContainerHistoryService}
     */
    private ContainerHistoryService containerHistoryService;

    /**
     * default constuctor
     */
    public PaymentService() {}

    /**
     * constructor
     * @param paymentRepository {@link PaymentRepository}
     * @param userCardPaymentService {@link UserCardPaymentService}
     * @param userCreditPaymentService {@link UserCreditPaymentService}
     * @param containerHistoryService {@link ContainerHistoryService}
     */
    @Autowired
    public PaymentService(PaymentRepository paymentRepository, UserCardPaymentService userCardPaymentService, UserCreditPaymentService userCreditPaymentService, ContainerHistoryService containerHistoryService) {
        this.paymentRepository = paymentRepository;
        this.userCardPaymentService = userCardPaymentService;
        this.userCreditPaymentService = userCreditPaymentService;
        this.containerHistoryService = containerHistoryService;
    }

    /**
     * 저장
     * @param payment {@link Payment}
     * @return {@link Payment}
     */
    @Transactional
    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }

    public Page<Payment> findAll(String username, Pageable pageable) {
        return paymentRepository.findAllByPaymentPKUsernameOrderByPaymentDateDesc(username, pageable);
    }

    public Payment findById(PaymentPK paymentPK) {
        return paymentRepository.findById(paymentPK).orElseThrow(EntityNotFoundException::new);
    }

    private boolean isPaidPaymentForTargetYearMonth(String username, String targetYearMonth) {
        return paymentRepository.existsByPaymentPKUsernameAndTargetYearMonthAndPaymentStatusCode(username, targetYearMonth, Code.PAYMENT_STATUS.PAID);
    }
    /**
     * 월별 결제
     * @param username 사용자아이디
     * @param targetYearMonth 대상년월(YYYY-MM)
     * @return {@link Payment}
     */
    @Transactional
    public Payment paymentMonthly(String username, String targetYearMonth, boolean isGroup) {
        List<ContainerHistory> containerList = containerHistoryService.findAllByMonthly(username, targetYearMonth, isGroup);
        if (containerList != null && containerList.size() > 0) {
            // 1. 총 결제 금액, 총 GPU 수량
            Double totalPaymentAmount = containerList.stream().mapToDouble(ContainerHistory::getMonthlyAmount).sum();
            int totalUseQuantity = containerList.stream().mapToInt(ContainerHistory::getGpuQuantity).sum();

            // 2. 결제정보 생성
            if (!this.isPaidPaymentForTargetYearMonth(username, targetYearMonth)) { // 중복결제 방지처리 (해당 월에 paid 가 있을 경우 중복결제로 판단)
                Payment payment = new Payment(new PaymentPK(username, this.generatePaymentId(username, targetYearMonth)));
                payment.setUseQuantity(totalUseQuantity);
                payment.setTotalPaymentAmount(totalPaymentAmount);
                payment.setTargetYearMonth(targetYearMonth);

                return this.payment(payment);
            }
        }
        return null;
    }

    @Transactional
    public void retryFailedPayment() {
        List<Payment> paymentList = paymentRepository.findAllForRetryPayment(Code.PAYMENT_STATUS.FAILED);
        log.debug(">>>>>>> retry payment list : {}", paymentList);
        paymentList.forEach(payment -> {
            Payment retryPayment = new Payment(new PaymentPK(payment.getPaymentPK().getUsername(), this.generatePaymentId(payment.getPaymentPK().getUsername(), payment.getTargetYearMonth())));
            retryPayment.setTotalPaymentAmount(payment.getTotalPaymentAmount());
            retryPayment.setUseQuantity(payment.getUseQuantity());
            retryPayment.setTargetYearMonth(payment.getTargetYearMonth());
            retryPayment = this.payment(retryPayment);
            log.debug(">>>>>>> retry payment : {}", retryPayment);
        });
    }

    private Payment payment(Payment payment) {
        log.debug(">>>>>>>>>>>>>>>>>>>>> payment : {}", payment);
        YearMonth yearMonth = YearMonth.parse(payment.getTargetYearMonth(), CheetahFormatter.FOR_YEAR_MONTH);
        payment.setUseStartDate(yearMonth.atDay(1).format(CheetahFormatter.FOR_DATE));
        payment.setUseEndDate(yearMonth.atEndOfMonth().format(CheetahFormatter.FOR_DATE));

        payment.setPaymentStatusCode(Code.PAYMENT_STATUS.READY);
        payment.setPaymentDate(LocalDateTime.now());
        try {
            // 3. 크레딧 결제
            Double creditPaymentAmount = 0D;
            UserCreditPayment userCreditPayment = userCreditPaymentService.paymentWithCredit(payment);
            if (userCreditPayment != null) {
                payment.setUserCreditPaymentList(new ArrayList<UserCreditPayment>() {{
                    add(userCreditPayment);
                }});
                creditPaymentAmount = userCreditPayment.getCreditPaymentAmount();
            }
            payment.setCreditPaymentAmount(creditPaymentAmount);

            // 4. 카드 결제
            Double cardPaymentAmount = payment.getTotalPaymentAmount() - creditPaymentAmount;
            payment.setCardPaymentAmount(cardPaymentAmount);
            if (cardPaymentAmount > 0D) {
                UserCardPayment userCardPayment = userCardPaymentService.paymentWithCard(payment);
                payment.setUserCardPaymentList(new ArrayList<UserCardPayment>() {{
                    add(userCardPayment);
                }});
            }
            payment.setPaymentStatusCode(Code.PAYMENT_STATUS.PAID);
        } catch (PaymentException pe) {
            log.debug(">>>>>>>>>>>>>> PaymentException : "+ pe.getErrorMessage());
            if (payment.getUserCreditPaymentList() != null && payment.getUserCreditPaymentList().size() > 0) {
                UserCredit userCredit = payment.getUserCreditPaymentList().get(0).getUserCredit();
                userCredit.setRemainCreditAmount(userCredit.getRemainCreditAmount() + payment.getCreditPaymentAmount());
                log.debug(">>>>>>>>>>>>>> userCredit : {}", userCredit);
            }
            payment.setUserCreditPaymentList(new ArrayList<>());
            payment.setCreditPaymentAmount(0D);
            payment.setCardPaymentAmount(0D);
            payment.setPaymentStatusCode(Code.PAYMENT_STATUS.FAILED);
        }
        log.debug("===============================================================");
        log.debug(" payment : {}", payment);
        log.debug(" payment.getUserCreditPaymentList : {}", payment.getUserCreditPaymentList());
        log.debug(" payment.getUserCardPaymentList : {}", payment.getUserCardPaymentList());
        log.debug("===============================================================");
        // 5. 결제정보 저장
        return this.save(payment);
    }

    /**
     * generate paymentId
     * @param username 사용자아이디
     * @return paymentId
     */
    private String generatePaymentId(String username, String paymentYearMonth) {
        while (true) {
            String randomKey = paymentYearMonth + "-" + StringUtils.randomAlphanumeric(4).toUpperCase();
            PaymentPK paymentPK = new PaymentPK(username, randomKey);
            if (!paymentRepository.existsById(paymentPK)) {
                return randomKey;
            }
        }
    }


    /**
     * 다음 결제 내역
     * @param username 사용자아이디
     * @param isGroup 그룹여부
     * @return {@link PaymentDto}
     */
    public PaymentDto nextPayment(String username, boolean isGroup) {
        YearMonth now = YearMonth.now();

        Double totalPaymentAmount = containerHistoryService.calculatePaymentAmount(username, CheetahFormatter.FOR_YEAR_MONTH.format(now), isGroup);
        return PaymentDto.builder()
                .totalPaymentAmount(totalPaymentAmount)
                .paymentDate(CheetahFormatter.FOR_DATE_WITH_DASH.format(now.atEndOfMonth()))
                .build();
    }

    /**
     * 마지막 결제 내역
     * @param username 사용자아이디
     * @return {@link PaymentDto}
     */
    public PaymentDto lastPayment(String username) {
        Page<Payment> paymentPage = this.findAll(username, PageRequest.of(0, 1, Sort.Direction.DESC, "paymentDate"));
        Payment payment = paymentPage != null && paymentPage.getContent() != null && paymentPage.getContent().size() > 0 ? paymentPage.getContent().get(0) : null;
        if (payment != null) {
            return PaymentDto.builder()
                    .totalPaymentAmount(payment.getTotalPaymentAmount())
                    .creditPaymentAmount(payment.getCreditPaymentAmount())
                    .cardPaymentAmount(payment.getCardPaymentAmount())
                    .paymentStatusCode(payment.getPaymentStatusCode())
                    .paymentDate(CheetahFormatter.FOR_DATE_WITH_DASH.format(payment.getPaymentDate()))
                    .build();
        }
        return null;
    }

    public void refundPayment(String username, String paymentId) {
        PaymentPK paymentPK = new PaymentPK(username, paymentId);
        Payment payment = this.findById(paymentPK);

        payment.setPaymentStatusCode(Code.PAYMENT_STATUS.FAILED);

        // 크레딧 환불처리
//        List<UserCreditPayment> userCreditPaymentList = payment.getUserCreditPaymentList();
        userCreditPaymentService.refundCredit(payment);


        this.save(payment);

    }
}
