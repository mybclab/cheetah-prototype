package com.mybclab.cheetah.payment.service;

import com.google.gson.Gson;
import com.mybclab.cheetah.payment.dto.IamportCardDto;
import com.mybclab.cheetah.payment.dto.IamportCustomer;
import com.siot.IamportRestClient.IamportClient;
import com.siot.IamportRestClient.exception.IamportResponseException;
import com.siot.IamportRestClient.request.AgainPaymentData;
import com.siot.IamportRestClient.response.AccessToken;
import com.siot.IamportRestClient.response.IamportResponse;
import com.siot.IamportRestClient.response.Payment;
import lombok.extern.slf4j.Slf4j;
//import okhttp3.*;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * iamport Service
 */
@Service
@Slf4j
public class IamportService {

    @Value("${cheetah.iamport.api.key}")
    private String IAMPORT_API_KEY;

    @Value("${cheetah.iamport.api.secret}")
    private String IAMPORT_API_SECRET;

    @Value("${cheetah.iamport.rest.customer.uid}")
    private String IAMPORT_CUSTOMER_UID_URL; // https://api.iamport.kr/subscribe/customers

    /**
     * customer_uid 발급 및 저장
     * @param cardDto {@link IamportCardDto}
     * @return customer_uid
     */
    public IamportResponse<IamportCustomer> postSubscribeCustomers(IamportCardDto cardDto) {
        try {
            IamportClient iamportClient = new IamportClient(IAMPORT_API_KEY, IAMPORT_API_SECRET);
            IamportResponse<AccessToken> auth_response = iamportClient.getAuth();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody requestBody = RequestBody.create(mediaType, new Gson().toJson(cardDto));

            log.debug(" requestBody : " + new Gson().toJson(cardDto));
            Request request = new Request.Builder()
                    .url(IAMPORT_CUSTOMER_UID_URL + "/" + cardDto.getCustomer_uid())
                    .post(requestBody)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", auth_response.getResponse().getToken())
                    .addHeader("cache-control", "no-cache")
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String responseBody = response.body().string();

            Gson gson = new Gson();
            log.debug(" customerResponse.responseBody : {}", responseBody);
            IamportResponse<IamportCustomer> customerResponse = gson.fromJson(responseBody, IamportResponse.class);
//            log.debug(" customerResponse : {}", customerResponse);
            log.debug(" customerResponse.getResponse : {}", customerResponse.getResponse());
            return customerResponse;
        } catch (IamportResponseException e) {
            log.debug(e.getMessage());
            switch(e.getHttpStatusCode()) {
                case 401 :
                    break;
                case 500 :
                    break;
            }
        } catch (IOException e) {
            log.debug(e.getMessage());
        }

        return null;
    }

    public IamportResponse<Payment> postSubscribePaymentsAgain(AgainPaymentData againPaymentData) {
        try {
            IamportClient iamportClient = new IamportClient(IAMPORT_API_KEY, IAMPORT_API_SECRET);

            IamportResponse<Payment> payment_response = iamportClient.againPayment(againPaymentData);

            log.debug(">>>>>>>> payment_response: {}", payment_response);
            log.debug(">>>>>>>> payment_response.getCode: {}", payment_response.getCode());
            log.debug(">>>>>>>> payment_response.getMessage: {}", payment_response.getMessage());
            return payment_response;
        } catch (IamportResponseException e) {
            log.debug(e.getMessage());

            switch(e.getHttpStatusCode()) {
                case 401 :
                    break;
                case 500 :
                    break;
            }
        } catch (IOException e) {
            log.debug(e.getMessage());
        }
        return null;
    }


}
