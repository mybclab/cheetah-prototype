package com.mybclab.cheetah.payment.repository;

import com.mybclab.cheetah.payment.domain.UserCreditPayment;
import com.mybclab.cheetah.payment.domain.UserCreditPaymentPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 사용자 크레딧 결제 Repository
 */
@Repository
public interface UserCreditPaymentRepository extends JpaRepository<UserCreditPayment, UserCreditPaymentPK> {
}
