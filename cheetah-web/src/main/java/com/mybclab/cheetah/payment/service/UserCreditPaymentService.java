package com.mybclab.cheetah.payment.service;

import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.domain.UserCreditPayment;
import com.mybclab.cheetah.payment.domain.UserCreditPaymentPK;
import com.mybclab.cheetah.payment.repository.UserCreditPaymentRepository;
import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.admin.user.repository.UserCreditRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 사용자 크레딧 결제 Service
 */
@Service
@Slf4j
public class UserCreditPaymentService implements Serializable {

    /**
     * {@link UserCreditRepository}
     */
    private UserCreditRepository userCreditRepository;
    /**
     * {@link UserCreditPaymentRepository}
     */
    private UserCreditPaymentRepository userCreditPaymentRepository;

    /**
     * default constructor
     */
    public UserCreditPaymentService() {}

    /**
     * constructor
     * @param userCreditPaymentRepository {@link UserCreditPaymentRepository}
     * @param userCreditRepository {@link UserCreditRepository}
     */
    @Autowired
    public UserCreditPaymentService(UserCreditPaymentRepository userCreditPaymentRepository, UserCreditRepository userCreditRepository) {
        this.userCreditPaymentRepository = userCreditPaymentRepository;
        this.userCreditRepository = userCreditRepository;
    }


    /**
     * 크레딧 결제
     * @param payment {@link Payment}
     * @return {@link UserCreditPayment}
     */
    @Transactional
    public UserCreditPayment paymentWithCredit(Payment payment) {
        UserCreditPayment userCreditPayment = null;

        Double totalPaymentAmount = payment.getTotalPaymentAmount();
        Double creditPaymentAmount = 0D;
        List<UserCredit> userCreditList = userCreditRepository.findAllByValidCredit(payment.getPaymentPK().getUsername());
//        log.debug(" ::::::::::: 유효 크레딧 목록 : {}", userCreditList);
        if (userCreditList != null && userCreditList.size() > 0) {
            UserCredit userCredit = userCreditList.get(0);
            Double remainCreditAmount = userCredit.getRemainCreditAmount();
            creditPaymentAmount = remainCreditAmount < totalPaymentAmount ? remainCreditAmount : totalPaymentAmount;

            userCreditPayment = new UserCreditPayment(payment.getPaymentPK().getUsername(), payment.getPaymentPK().getPaymentId(), userCredit.getUserCreditPK().getUserCreditNo());
            userCreditPayment.setCreditPreviousAmount(remainCreditAmount);
            userCreditPayment.setCreditPaymentAmount(creditPaymentAmount);
            userCreditPayment.setCreditRemainAmount(remainCreditAmount - creditPaymentAmount);
            userCreditPayment.setPaymentDate(payment.getPaymentDate());
//            log.debug(" ::::::::::: 총결제금액 : " + totalPaymentAmount + ", 이전크레딧 : " + remainCreditAmount + ", 결제크레딧 : " + creditPaymentAmount + ", 남은크레딧 : " + (remainCreditAmount - creditPaymentAmount));

            userCredit.setRemainCreditAmount(remainCreditAmount - creditPaymentAmount);
            userCreditPayment.setUserCredit(userCredit);
        }
        return userCreditPayment;
    }

    public void refundCredit(Payment payment) {
        List<UserCreditPayment> userCreditList = payment.getUserCreditPaymentList();
        if (userCreditList != null && userCreditList.size() > 0) {
            UserCreditPayment userCreditPayment = userCreditList.get(0);
            Double creditPaymentAmount = userCreditPayment.getCreditPaymentAmount();
//            UserCreditPayment refundUserCreditPayment = new UserCreditPayment();
//            UserCreditPaymentPK userCreditPaymentPK = new UserCreditPaymentPK();
            // PK 재 생성이 아니라... 수정이 아니라 삭제를 해얗 ㅏ나.. ㅁㄴ아럼ㄴ이ㅏ러
            userCreditPayment.setCreditPaymentAmount(0D);
//            userCreditPayment.setCreditPreviousAmount();
//            userCreditPayment.setCreditRemainAmount(userCreditPayment.getCreditRemainAmount());
//            refundUserCreditPayment.
        }
    }
}
