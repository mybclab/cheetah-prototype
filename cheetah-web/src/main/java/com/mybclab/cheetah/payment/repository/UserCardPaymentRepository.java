package com.mybclab.cheetah.payment.repository;

import com.mybclab.cheetah.payment.domain.UserCardPayment;
import com.mybclab.cheetah.payment.domain.UserCardPaymentPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 사용자 카드 결제 Repository
 */
@Repository
public interface UserCardPaymentRepository extends JpaRepository<UserCardPayment, UserCardPaymentPK> {
}
