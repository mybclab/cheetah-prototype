package com.mybclab.cheetah.payment.domain;

import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 사용자 크레딧 결제
 */
@Setter
@Getter
@ToString(exclude = {"payment"})
@Entity
public class UserCreditPayment implements Serializable {

    /**
     * {@link UserCreditPaymentPK}
     */
    @EmbeddedId
    private UserCreditPaymentPK userCreditPaymentPK;

    /**
     * 크레딧 결제 금액
     */
    @CurrencyFormat
    private Double creditPaymentAmount;
    /**
     * 크레딧 이전 금액
     */
    @CurrencyFormat
    private Double creditPreviousAmount;
    /**
     * 크레딧 잔여 금액
     */
    @CurrencyFormat
    private Double creditRemainAmount;
    /**
     * 결제 일시
     */
    private LocalDateTime paymentDate;

    /**
     * {@link Payment}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "paymentId", referencedColumnName = "paymentId", insertable = false, updatable = false)
    })
    private Payment payment;

    /**
     * {@link UserCredit}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "userCreditNo", referencedColumnName = "userCreditNo", insertable = false, updatable = false)
    })
    private UserCredit userCredit;



    /**
     * default constructor
     */
    public UserCreditPayment() {}

    /**
     * constructor
     * @param userCreditPaymentPK {@link UserCreditPaymentPK}
     */
    public UserCreditPayment(UserCreditPaymentPK userCreditPaymentPK) {
        this.userCreditPaymentPK = userCreditPaymentPK;
    }

    /**
     * constructor
     * @param username 사용자아이디
     * @param paymentId 결제 아이디
     * @param userCreditNo 사용자 크레딧 번호
     */
    public UserCreditPayment(String username, String paymentId, int userCreditNo) {
        this.userCreditPaymentPK = new UserCreditPaymentPK(username, paymentId, userCreditNo);
    }
}
