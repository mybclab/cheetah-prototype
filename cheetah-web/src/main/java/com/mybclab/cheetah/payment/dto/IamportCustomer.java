package com.mybclab.cheetah.payment.dto;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class IamportCustomer {
    /**
     * 카드사 코드번호(금융결제원 표준코드번호) =
     * ['361(BC카드)', '364(광주카드)', '365(삼성카드)', '366(신한카드)', '367(현대카드)', '368(롯데카드)', '369(수협카드)', '370(씨티카드)', '371(NH카드)'
     * , '372(전북카드)', '373(제주카드)', '374(하나SK카드)', '381(KB국민카드)', '041(우리카드)', '071(우체국)']
     * ,
     */
    private String card_code;
    /**
     * 카드사 명칭
     */
    private String card_name;

    private String customer_addr;
    private String customer_email;
    private String customer_name;
    private String customer_postcode;
    private String customer_tel;
    private String customer_uid;
    /**
     * 빌키가 등록된 시각 UNIX timestamp
     */
    private int inserted;
    /**
     * 빌키가 업데이트된 시각 UNIX timestamp
     */
    private int updated;
}
