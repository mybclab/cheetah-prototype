package com.mybclab.cheetah.payment.dto;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;


/**
 * 결제 Dto
 */
@Getter
@Setter
@ToString
@Builder
public class PaymentDto implements Serializable {

    /**
     * 총 결제 금액
     */
    @CurrencyFormat
    private Double totalPaymentAmount;

    /**
     * 크레딧 결제 금액
     */
    @CurrencyFormat
    private Double creditPaymentAmount;

    /**
     * 카드 결제 금액
     */
    @CurrencyFormat
    private Double cardPaymentAmount;

    /**
     * 결제 일시
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String paymentDate;

    /**
     * 결제 상태 코드
     * {@link Code.PAYMENT_STATUS}
     */
    private Code.PAYMENT_STATUS paymentStatusCode;

}
