package com.mybclab.cheetah.payment.batch;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.batch.listener.JobCompletionNotificationListener;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.YearMonth;
import java.util.List;

@Component
@Slf4j
//@Profile("batch")
public class PaymentBatch {

    /**
     * {@link JobBuilderFactory}
     */
    private JobBuilderFactory jobBuilderFactory;

    /**
     * {@link StepBuilderFactory}
     */
    private StepBuilderFactory stepBuilderFactory;

    /**
     * {@link UserService}
     */
    private UserService userService;
    /**
     * {@link PaymentService}
     */
    private PaymentService paymentService;

    @Autowired
    public PaymentBatch(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, UserService userService, PaymentService paymentService) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.userService = userService;
        this.paymentService = paymentService;
    }


    @Bean(name = "paymentMonthlyBatchJob")
    public Job paymentMonthlyBatchJob(JobCompletionNotificationListener listener) {
        return jobBuilderFactory.get("paymentMonthlyBatchJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .start(paymentMonthlyBatchStep1())
                .build();
    }

    @Bean(name = "retryFailedPaymentBatchJob")
    public Job retryFailedPaymentBatchJob(JobCompletionNotificationListener listener) {
        return jobBuilderFactory.get("retryFailedPaymentBatchJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .start(retryFailedPaymentBatchStep1())
                .build();
    }

    /**
     * step1
     *
     * @return {@link Step}
     */
    protected Step paymentMonthlyBatchStep1() {
        return stepBuilderFactory.get("#paymentMonthlyBatch-step1")
                .tasklet((contribution, chunkContext) -> {
                    List<User> userList = userService.findAllForPayment();
                    YearMonth targetYearMonth = YearMonth.now().minusMonths(1);
                    userList.forEach(u -> {
                        log.debug(">>>>>>>> 배치 paymentService.paymentMonthly : " + u.getUsername()+ "," + CheetahFormatter.FOR_YEAR_MONTH.format(targetYearMonth) + "," + u.isGroupAdmin());
                        paymentService.paymentMonthly(u.getUsername(), CheetahFormatter.FOR_YEAR_MONTH.format(targetYearMonth), u.isGroupAdmin());
                    });
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    protected Step retryFailedPaymentBatchStep1() {
        return stepBuilderFactory.get("#retryFailedPaymentBatch-step1")
                .tasklet((contribution, chunkContext) -> {
                    paymentService.retryFailedPayment();
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
