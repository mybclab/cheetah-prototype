package com.mybclab.cheetah.payment.service;

import com.mybclab.cheetah.common.exception.PaymentException;
import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.domain.UserCardPayment;
import com.mybclab.cheetah.payment.repository.UserCardPaymentRepository;
import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.siot.IamportRestClient.request.AgainPaymentData;
import com.siot.IamportRestClient.response.IamportResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 사용자 카드 결제 Service
 */
@Service
@Slf4j
public class UserCardPaymentService implements Serializable {

    /**
     * {@link UserCardPaymentRepository}
     */
    private UserCardPaymentRepository userCardPaymentRepository;

    /**
     * {@link IamportService}
     */
    private IamportService iamportService;

    /**
     * {@link UserCardService}
     */
    private UserCardService userCardService;

    /**
     * default constructor
     */
    public UserCardPaymentService() {}

    /**
     * constructor
     * @param userCardPaymentRepository {@link UserCardPaymentRepository}
     * @param userCardService {@link UserCardService}
     * @param iamportService {@link IamportService}
     */
    @Autowired
    public UserCardPaymentService(UserCardPaymentRepository userCardPaymentRepository, UserCardService userCardService, IamportService iamportService) {
        this.userCardPaymentRepository = userCardPaymentRepository;
        this.userCardService = userCardService;
        this.iamportService = iamportService;
    }

    /**
     * 카드 결제
     * @param payment {@link Payment}
     * @return {@link UserCardPayment}
     */
    public UserCardPayment paymentWithCard(Payment payment) throws PaymentException {
        log.debug(" ::::::::::: 카드결제금액 : " + payment.getCardPaymentAmount());
        UserCard userCard = userCardService.findValidCardByUsername(payment.getPaymentPK().getUsername());
        if (userCard == null) {
            throw new PaymentException("Error.noValidCard");
        }

        // iamport payment
        log.debug("userCard.getCardNo() {}", userCard.getCardNo());
        log.debug("userCard.getCustomerUid() {}", userCard.getCustomerUid());
        AgainPaymentData againPaymentData = new AgainPaymentData(userCard.getCustomerUid(), payment.getPaymentPK().getPaymentId(), BigDecimal.valueOf(payment.getCardPaymentAmount()));
        againPaymentData.setName(payment.getTargetYearMonth() + " GPU CLOUD 정기결제");
        againPaymentData.setBuyerName(userCard.getUser().getName());
        againPaymentData.setBuyerEmail(userCard.getUser().getEmail());
        againPaymentData.setBuyerTel(userCard.getUser().getPhoneNo());
        IamportResponse<com.siot.IamportRestClient.response.Payment> response = iamportService.postSubscribePaymentsAgain(againPaymentData);
        log.debug("response.getCode() {}", response.getCode());
//        IamportResponse<com.siot.IamportRestClient.response.Payment> response = null;
        if (response == null || 0 != response.getCode()) {
            throw new PaymentException("Error.failPaymentWithCard");
        } else {
            UserCardPayment userCardPayment = new UserCardPayment(payment.getPaymentPK().getUsername(), payment.getPaymentPK().getPaymentId(), userCard.getUserCardPK().getUserCardNo());
            userCardPayment.setTransactionPg("pg-name");
            userCardPayment.setTransactionUid(response.getResponse().getImpUid());
            userCardPayment.setCardApplyNo(response.getResponse().getApplyNum());
            userCardPayment.setPaymentAmount(payment.getCardPaymentAmount());
            userCardPayment.setPaymentStatusCode(payment.getPaymentStatusCode());
            userCardPayment.setPaymentDate(payment.getPaymentDate());
//          log.debug(" userCardPayment : {}", userCardPayment);
            return userCardPayment;
        }


    }
}
