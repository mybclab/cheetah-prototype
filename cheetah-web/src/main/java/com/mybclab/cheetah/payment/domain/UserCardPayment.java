package com.mybclab.cheetah.payment.domain;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 사용자 카드 결제
 */
@Entity
@Getter
@Setter
@ToString
public class UserCardPayment implements Serializable {

    /**
     * {@link UserCardPaymentPK}
     */
    @EmbeddedId
    private UserCardPaymentPK userCardPaymentPK;

    /**
     * 거래 PG
     */
    private String transactionPg;

    /**
     * 거래 고유번호
     */
    private String transactionUid;

    /**
     * 카드 승인 번호
     */
    private String cardApplyNo;

    /**
     * 결제 금액
     */
    private Double paymentAmount;

    /**
     * 결제 상태 코드
     */
    @Enumerated(EnumType.STRING)
    private Code.PAYMENT_STATUS paymentStatusCode;

    /**
     * 결제 일시
     */
    private LocalDateTime paymentDate;

    /**
     * {@link Payment}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "paymentId", referencedColumnName = "paymentId", insertable = false, updatable = false)
    })
    private Payment payment;

    /**
     * default constructor
     */
    public UserCardPayment() {}

    /**
     * constructor
     * @param userCardPaymentPK {@link UserCardPaymentPK}
     */
    public UserCardPayment(UserCardPaymentPK userCardPaymentPK) {
        this.userCardPaymentPK = userCardPaymentPK;
    }

    /**
     * constructor
     * @param username 사용자아이디
     * @param paymentId 결제 아이디
     * @param userCardNo 카드 번호
     */
    public UserCardPayment(String username, String paymentId, int userCardNo) {
        this.userCardPaymentPK = new UserCardPaymentPK(username, paymentId, userCardNo);
    }
}
