package com.mybclab.cheetah.payment.web;

import com.mybclab.cheetah.admin.user.dto.UserCardForm;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.cheetah.payment.dto.IamportCardDto;
import com.mybclab.cheetah.payment.dto.IamportCustomer;
import com.mybclab.cheetah.payment.service.IamportService;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.siot.IamportRestClient.response.IamportResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * iamport rest Controller
 */
@Slf4j
@RestController
@RequestMapping
public class IamportRestController {

    private UserCardService userCardService;

    private IamportService iamportService;
    /**
     * {@link CheetahMessageSource}
     */
    private CheetahMessageSource messageSource;
    /**
     * default constructor
     */
    public IamportRestController() {}

    /**
     * constructor
     * @param userCardService {@link UserCardService}
     */
    @Autowired
    public IamportRestController(UserCardService userCardService, IamportService iamportService, CheetahMessageSource messageSource) {
        this.userCardService = userCardService;
        this.iamportService = iamportService;
        this.messageSource = messageSource;
    }

    /**
     * 카드정보 등록하고 customerUID 생성
     * @param userCardForm {@link UserCardForm}
     * @return
     */
    @PostMapping("/iamport/customer-uid")
    public ResponseEntity<JsonResult> customerId(@Valid UserCardForm userCardForm, BindingResult bindingResult) {
        JsonResult result = new JsonResult();
//        log.debug(">>>>>>>> userCardForm : {}", userCardForm);
//        log.debug(">>>>>>>> bindingResult : {}", bindingResult);

        if (bindingResult.hasErrors()) {
            result.setStatus(ResultStatus.FAIL);
            result.setData("message", messageSource.getMessage("Iamport.emptyCardInfo"));
            return new ResponseEntity<>(result, HttpStatus.OK);
        }

        String customerUid = userCardService.generateCustomerUid(userCardForm.getCardNo());

        IamportCardDto cardDto = IamportCardDto.builder()
                .customer_uid(customerUid)
                .card_number(userCardForm.getCardNo()) // dddd-dddd-dddd-dddd
                .expiry(userCardForm.getCardExpiryYYYYMM()) // YYYYMM
                .birth(userCardForm.getCardBirth()) // YYMMDD
                .pwd_2digit(userCardForm.getCardPassword()).build();

        IamportResponse<IamportCustomer> customerResponse = iamportService.postSubscribeCustomers(cardDto);

//        log.debug(">>>>>>>> customerResponse : {}", customerResponse);
        if (customerResponse != null && customerResponse.getCode() == 0) {
            result.setStatus(ResultStatus.SUCCESS);
            result.setData("customerUid", customerUid);
        } else {
            result.setStatus(ResultStatus.FAIL);
            result.setData("message", customerResponse != null ? customerResponse.getMessage() : "customerResponse is null");
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
