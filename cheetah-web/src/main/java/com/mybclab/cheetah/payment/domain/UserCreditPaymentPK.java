package com.mybclab.cheetah.payment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 사용자 크레딧 결제 PK
 */
@Setter
@Getter
@Embeddable
@ToString
public class UserCreditPaymentPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 결제 아이디
     */
    private String paymentId;

    /**
     * 사용자 크레딧 번호
     */
    private int userCreditNo;


    /**
     * default constructor
     */
    public UserCreditPaymentPK() {}

    /**
     * constructor
     * @param username 사용자아이디
     * @param paymentId 결제 아이디
     * @param userCreditNo 사용자 크레딧 번호
     */
    public UserCreditPaymentPK(String username, String paymentId, int userCreditNo) {
        this.username = username;
        this.paymentId = paymentId;
        this.userCreditNo = userCreditNo;
    }
}
