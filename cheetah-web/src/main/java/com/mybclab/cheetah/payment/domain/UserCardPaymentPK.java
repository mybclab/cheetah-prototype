package com.mybclab.cheetah.payment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 사용자 카드 결제 PK
 */
@Setter
@Getter
@ToString
@Embeddable
public class UserCardPaymentPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 결제 아이디
     */
    private String paymentId;
    /**
     * 사용자 카드 번호
     */
    private int userCardNo;

    /**
     * default constructor
     */
    public UserCardPaymentPK() {}

    /**
     * constructor
     * @param username 사용자아이디
     * @param paymentId 결제 아이디
     * @param userCardNo 카드 번호
     */
    public UserCardPaymentPK(String username, String paymentId, int userCardNo) {
        this.username = username;
        this.paymentId = paymentId;
        this.userCardNo = userCardNo;
    }
}
