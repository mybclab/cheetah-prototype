package com.mybclab.cheetah.payment.repository;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.domain.PaymentPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 결제 Repository
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, PaymentPK> {


    Page<Payment> findAllByPaymentPKUsernameOrderByPaymentDateDesc(String username, Pageable pageable);

    List<Payment> findAllByPaymentStatusCode(Code.PAYMENT_STATUS paymentStatus);

    @Query(" select p " +
            "  from Payment p" +
            " where p.paymentStatusCode = :paymentStatus" +
            "   and p.paymentDate =  " +
            "       (select max(m.paymentDate)" +
            "          from Payment m" +
            "         where m.paymentPK.username = p.paymentPK.username" +
            "           and m.targetYearMonth = p.targetYearMonth" +
            "         group by m.paymentPK.username, m.targetYearMonth)")
    List<Payment> findAllForRetryPayment(@Param("paymentStatus") Code.PAYMENT_STATUS paymentStatus);

    boolean existsByPaymentPKUsernameAndTargetYearMonthAndPaymentStatusCode(String username, String targetYearMonth, Code.PAYMENT_STATUS paymentStatusCode);
}
