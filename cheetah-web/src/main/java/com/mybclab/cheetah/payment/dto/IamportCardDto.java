package com.mybclab.cheetah.payment.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class IamportCardDto {

    private String card_number;

    private String expiry;

    private String birth;

    private String pwd_2digit;

    private String customer_uid;

}
