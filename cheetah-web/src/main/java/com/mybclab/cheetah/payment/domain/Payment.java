package com.mybclab.cheetah.payment.domain;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 결제
 */
@Setter
@Entity
@Getter
@ToString(exclude = {"userCreditPaymentList", "userCardPaymentList"})
public class Payment implements Serializable {

    /**
     * {@link PaymentPK}
     */
    @EmbeddedId
    private PaymentPK paymentPK;

    /**
     * 대상 년 월
     */
    private String targetYearMonth;

    /**
     * 이용 시작 일자
     */
    private String useStartDate;

    /**
     * 이용 종료 일
     */
    private String useEndDate;

    /**
     * 총 결제 금액
     */
    @CurrencyFormat
    private Double totalPaymentAmount;

    /**
     * 크레딧 결제 금액
     */
    @CurrencyFormat
    private Double creditPaymentAmount;

    /**
     * 카드 결제 금액
     */
    @CurrencyFormat
    private Double cardPaymentAmount;

    /**
     * 사용자 카드 번호
     */
    private int userCardNo;

    /**
     * 사용자 크레딧 번호
     */
    private int userCreditNo;

    /**
     * 이용 수량
     */
    private int useQuantity;

    /**
     * 결제 상태 코드
     */
    @Enumerated(EnumType.STRING)
    private Code.PAYMENT_STATUS paymentStatusCode;

    /**
     * 결제 일시
     */
    private LocalDateTime paymentDate;


    /**
     * default constructor
     */
    public Payment() {}

    /**
     * constructor
     * @param paymentPK {@link PaymentPK}
     */
    public Payment(PaymentPK paymentPK) {
        this.paymentPK = paymentPK;
    }
    /**
     * 사용자 크레딧 결제 목록
     * {@link UserCreditPayment}
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "paymentId", referencedColumnName = "paymentId", insertable = false, updatable = false)
    })
    private List<UserCreditPayment> userCreditPaymentList = new ArrayList<>();

    /**
     * 사용자 카드 결제 목록
     * {@link UserCardPayment}
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumns({
        @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
        @JoinColumn(name = "paymentId", referencedColumnName = "paymentId", insertable = false, updatable = false)
    })
    private List<UserCardPayment> userCardPaymentList = new ArrayList<>();

    @Transient
    private List<ContainerHistory> targetContainerList = new ArrayList();

}
