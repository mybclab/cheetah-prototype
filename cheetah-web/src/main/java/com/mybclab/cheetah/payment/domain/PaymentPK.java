package com.mybclab.cheetah.payment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 결제 PK
 */
@Embeddable
@Getter
@Setter
@ToString
public class PaymentPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 결제 아이디
     */
    private String paymentId;

    /**
     * default constructor
     */
    public PaymentPK() {
    }

    /**
     * constuctor
     * @param username 사용자아이디
     * @param paymentId 결제 아이디
     */
    public PaymentPK(String username, String paymentId) {
        this.username = username;
        this.paymentId = paymentId;
    }
}
