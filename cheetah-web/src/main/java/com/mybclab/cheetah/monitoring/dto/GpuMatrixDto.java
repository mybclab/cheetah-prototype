package com.mybclab.cheetah.monitoring.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GpuMatrixDto {

    private String gpuTitle;

    private String currentValue;

    private String maxValue;

    private String avgValue;
}
