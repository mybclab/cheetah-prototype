package com.mybclab.cheetah.monitoring.constants;

public class GpuMatrixConstants {

    public static final String QUERY_GPU_TEMP = "dcgm_gpu_temp";
    public static final String QUERY_POWER_USAGE = "dcgm_power_usage";
    public static final String QUERY_GPU_UTILIZATION = "dcgm_gpu_utilization";
}
