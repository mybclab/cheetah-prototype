package com.mybclab.cheetah.monitoring.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PrometheusResultDto {

    private String status;
    private DataDto data;

    @Getter
    @Setter
    @ToString
    public class DataDto {
        private String resultType;
        private List<DataRetulsDto> result;

    }

    @Getter
    @Setter
    @ToString
    public class DataRetulsDto {
        private metricDto metric;
        private List<String> value;
        private List<List<String>> values;
    }

    @Getter
    @Setter
    @ToString
    public class metricDto {
        @SerializedName("__name__")
        private String name;
        @SerializedName("container_name")
        private String containerName;
        private String gpu;
        private String instance;
        private String job;
        @SerializedName("pod_name")
        private String podName;
        @SerializedName("pod_namespace")
        private String podNamespace;
        private String uuid;

        private String label;
    }
}
