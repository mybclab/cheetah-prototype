package com.mybclab.cheetah.monitoring.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.monitoring.dto.GpuMatrixDto;
import com.mybclab.cheetah.monitoring.dto.PrometheusResultDto;
import com.mybclab.cheetah.monitoring.service.MonitoringService;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;


@Slf4j
@RestController
@RequestMapping("/api/monitoring")
public class MonitoringRestController {

    private UserService userService;

    @Value("${cheetah.kubernetes.prefix}")
    private String prefix;

    @Value("${cheetah.kubernetes.delimiter}")
    private String delimiter;

    private MonitoringService monitoringService;

    public MonitoringRestController() {}

    @Autowired
    public MonitoringRestController(UserService userService, MonitoringService monitoringService) {
        this.userService = userService;
        this.monitoringService = monitoringService;
    }

    @RequestMapping("/{username}/gpu/temp/current")
    public ResponseEntity<Object> currentGpuTemp(@PathVariable String username) {
        User user = SecurityUtils.getPrincipal();
        JsonResult result = new JsonResult();

        List<GpuMatrixDto> gpuTempList = monitoringService.gpuMatrix(null, username, "dcgm_gpu_temp");
        result.setData("gpuTempList", gpuTempList);
        result.setStatus(ResultStatus.SUCCESS);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
///*
    @RequestMapping("/{username}/gpu/{queryType:temp|power}")
    public ResponseEntity<Object> gpuMatrix(@PathVariable String username, @PathVariable String queryType, int periodMinute, int stepSeconds) {
        JsonResult result = new JsonResult();
        User user = userService.findById(username);

        periodMinute = (periodMinute == 0) ? 120 : periodMinute;
        stepSeconds = (stepSeconds == 0) ? 20 : stepSeconds;

        String groupId = user.isGroupAdmin() ? username : null;
        username = user.isGroupUser() || user.isGeneralUser() ? username : null;

        queryType = "temp".equals(queryType) ? "dcgm_gpu_temp" : "dcgm_power_usage";


//        log.debug(">>>>>>>> change : " + groupId + "," + username);

        DateTimeFormatter timeFormmater = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String url = monitoringService.makePrometheusURL("query_range", queryType, null
                , groupId, username, null
                , periodMinute, stepSeconds);
//        log.info(" >>>>>>>> monitoring prometheus url : " + url);
        if (url != null) {
            PrometheusResultDto resultDto = monitoringService.parsePrometheus(url);

            if ("success".equals(resultDto.getStatus())) {
                List<PrometheusResultDto.DataRetulsDto> gpuList = resultDto.getData().getResult();
                if (gpuList != null && gpuList.size() > 0) {
                    // find max values size
                    int maxSize = 0;
                    for (PrometheusResultDto.DataRetulsDto retulsDto : gpuList) {
                        List<List<String>> values = retulsDto.getValues();
                        maxSize = maxSize < values.size() ? values.size() : maxSize;
                    }
                    String[][] series = new String[gpuList.size()][maxSize];
                    String[][] labels = new String[gpuList.size()][maxSize];
                    //                log.debug(">>>> maxSize: " + maxSize);
                    for (int i = 0; i < gpuList.size(); i++) {
                        PrometheusResultDto.DataRetulsDto retulsDto = gpuList.get(i);
                        PrometheusResultDto.metricDto metric = retulsDto.getMetric();
                        metric.setLabel(monitoringService.findContainerName(metric.getContainerName()) + "-gpu" + metric.getGpu());
                        List<List<String>> values = retulsDto.getValues();

                        for (int j = 0; j < values.size(); j++) {
                            String timestamp = values.get(j).get(0);
                            timestamp = timestamp.replaceAll("[[.]]", "");
                            LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(timestamp)), TimeZone.getTimeZone("Asia/Seoul").toZoneId());
                            //                        log.debug(">>>>>>>> labels["+i+"]["+j+"] :"  + values.get(j).get(0));
                            //                        log.debug(">>>>>>>> series["+i+"]["+j+"] :"  + values.get(j).get(1));
                            //                        log.debug(">>>>>>>>>>>> timeFormmater :" + date.format(timeFormmater));
                            labels[i][j] = date.format(timeFormmater);
                            series[i][j] = values.get(j).get(1);
                        }
                    }
                    result.setData("labels", labels);
                    result.setData("series", series);
                }
                result.setData("gpuList", gpuList);
                result.setStatus(ResultStatus.SUCCESS);
            } else {
                result.setStatus(ResultStatus.FAIL);
            }
        } else {
            result.setStatus(ResultStatus.FAIL);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
