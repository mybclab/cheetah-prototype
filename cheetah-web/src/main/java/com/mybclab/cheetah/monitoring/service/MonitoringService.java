package com.mybclab.cheetah.monitoring.service;

import com.google.gson.Gson;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.monitoring.dto.GpuMatrixDto;
import com.mybclab.cheetah.monitoring.dto.PrometheusResultDto;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class MonitoringService {

    @Value("${cheetah.monitoring.prometheus.range-url}")
    private String PROMETHEUS_QUERY_RANGE_URL;

    @Value("${cheetah.kubernetes.prefix}")
    private String prefix;

    @Value("${cheetah.kubernetes.delimiter}")
    private String delimiter;

    private ContainerRepository containerRepository;

    private DateTimeFormatter prometheusTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private final static String SUCCESS = "success";

    public MonitoringService() {}

    @Autowired
    public MonitoringService(ContainerRepository containerRepository) {
        this.containerRepository = containerRepository;
    }

    public List<GpuMatrixDto> gpuMatrix(String groupId, String username, String queryType) {
        List<GpuMatrixDto> gpuTempList = new ArrayList<>();
        String url = this.makePrometheusURL("query", queryType, null, groupId, username,null,0,0);
        log.debug(">>>>>>>> gpuMatrix url : " + url);
        PrometheusResultDto resultDto = this.parsePrometheus(url);

        if (SUCCESS.equals(resultDto.getStatus())) {
            List<PrometheusResultDto.DataRetulsDto> gpuList = resultDto.getData().getResult();
            if (gpuList != null && gpuList.size() > 0) {
                for (PrometheusResultDto.DataRetulsDto gpu : gpuList) {
                    log.debug(" >>>>>>>>>>>>>>>> gpu : {}", gpu);
                    String currentValue = gpu.getValue().get(1);
                    PrometheusResultDto.metricDto metric = gpu.getMetric();
                    String gpuTitle = findContainerName(metric.getContainerName()) + "-gpu" + metric.getGpu();
                    GpuMatrixDto gpuMatrixDto = new GpuMatrixDto();
                    gpuMatrixDto.setCurrentValue(currentValue);
                    gpuMatrixDto.setGpuTitle(gpuTitle);
                    // TODO max, avg
                    gpuMatrixDto.setMaxValue(gpuMatrixByGroup(queryType, "max", null, null, metric.getUuid()));
                    gpuMatrixDto.setAvgValue(gpuMatrixByGroup(queryType, "avg", null, null, metric.getUuid()));

                    gpuTempList.add(gpuMatrixDto);
                }
            }
        }
        return gpuTempList;
    }

    private String gpuMatrixByGroup(String queryType, String operation, String groupId, String username, String uuid) {
        String gpuTemp = "-";
        String urlForMax = this.makePrometheusURL("query_range", queryType, operation, groupId, username, uuid,60, 1);
        PrometheusResultDto resultDtoForMax = this.parsePrometheus(urlForMax);
        if (SUCCESS.equals(resultDtoForMax.getStatus())) {

            Double result = 0D;
            if ("max".equals(operation)) {
                result = resultDtoForMax.getData().getResult().get(0).getValues().stream().mapToDouble(v -> Double.parseDouble(v.get(1))).max().orElse(0D);
            } else {
                result = resultDtoForMax.getData().getResult().get(0).getValues().stream().mapToDouble(v -> Double.parseDouble(v.get(1))).average().orElse(0D);
            }
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            gpuTemp = decimalFormat.format(result);
        }
        return gpuTemp;
    }


    public String makePrometheusURL(String path, String queryType, String operation, String groupId, String username, String uuid, int periodMinute, int stempSeconds) {
        if (StringUtils.isNotEmpty(PROMETHEUS_QUERY_RANGE_URL)) {
            String query = queryType;
            if (groupId != null) {
                List<Container> containerList = containerRepository.findAllByGroupAndStatusCode(groupId, Code.CONTAINER_STATUS.START);
                query += "{container_name=~\"" + generateFilterContainerName(containerList) + "\"}";
            } else if (username != null) {
                List<Container> containerList = containerRepository.findAllByContainerPKUsernameAndStatusCode(username, Code.CONTAINER_STATUS.START);
                query += "{container_name=~\"" + generateFilterContainerName(containerList) + "\"}";
            } else if (uuid != null) {
                query += "{uuid=\"" + uuid + "\"}";
            } else {
                List<Container> containerList = containerRepository.findAllByStatusCode(Code.CONTAINER_STATUS.START);
                query += "{container_name=~\"" + generateFilterContainerName(containerList) + "\"}";
            }
            if (operation != null) {
                query = operation + "(" + query + ")";
            }
            String url = PROMETHEUS_QUERY_RANGE_URL + path + "?query=" + query;
            if (periodMinute != 0) {
                LocalDateTime now = LocalDateTime.now(Clock.systemUTC()).withSecond(0);
                String start = now.minusMinutes(periodMinute).format(prometheusTimeFormat);
                String end = now.format(prometheusTimeFormat);
                String step = stempSeconds + "s";
                url += "&start=" + start + "&end=" + end + "&step=" + step;
            }
//        log.debug(">>>>>>>>> makePrometheusURL url " + url);
            return url;
        }
        return null;
    }

    private String generateFilterContainerName(List<Container> containerList) {
        if (containerList != null && containerList.size() > 0) {
            String filterContainerName = "";
            for (int i = 0; i < containerList.size(); i++) {
                Container container = containerList.get(i);
                if (i != 0) filterContainerName += "|";
                filterContainerName += prefix + delimiter + container.getContainerPK().getUsername() + delimiter + container.getContainerPK().getContainerId();
            }
            return filterContainerName;
        } else {
            return "nocontainers";
        }
    }


    public PrometheusResultDto parsePrometheus(String url) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(url)
                    .get()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache").build();
            Response response = client.newCall(request).execute();

            BufferedReader in = new BufferedReader(new InputStreamReader(response.body().byteStream()));
            String inputLine;
            StringBuffer repStr = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                repStr.append(inputLine);
            }
            in.close();
//            log.debug(">>>>>>>>>> repStr : {}", repStr.toString());
            Gson gson = new Gson();
            return gson.fromJson(repStr.toString(), PrometheusResultDto.class);
        } catch (IOException e) {
            return null;
        }
    }

    @Cacheable(cacheNames = "monitoring", key = "#containerName")
    public String findContainerName(String containerName) {
        if (containerName != null && containerName.contains(delimiter)) {
            String[] splitContainerName = containerName.split(delimiter);
            Container container = containerRepository.findByContainerPKContainerId(splitContainerName[2]);
            return container != null ? container.getContainerName() : splitContainerName[2];
        } else {
            return "(none)";
        }
    }


}
