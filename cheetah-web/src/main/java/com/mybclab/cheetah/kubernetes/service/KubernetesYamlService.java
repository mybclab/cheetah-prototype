package com.mybclab.cheetah.kubernetes.service;

import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.domain.KubernetesYaml;
import com.mybclab.cheetah.kubernetes.repository.KubernetesYamlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class KubernetesYamlService {

    private KubernetesYamlRepository kubernetesYamlRepository;

    public KubernetesYamlService() {}

    @Autowired
    public KubernetesYamlService(KubernetesYamlRepository kubernetesYamlRepository) {
        this.kubernetesYamlRepository = kubernetesYamlRepository;
    }

    @Cacheable(cacheNames = "yaml", key="#kind")
    public KubernetesYaml findById(Kubernetes.KIND kind) {
        return kubernetesYamlRepository.findById(kind).orElseThrow(EntityNotFoundException::new);
    }

    public List<KubernetesYaml> findAll() {
        return kubernetesYamlRepository.findAll();
    }

    @CacheEvict(cacheNames = "yaml", allEntries = true)
    public KubernetesYaml save(KubernetesYaml kubernetesYaml) {
        return kubernetesYamlRepository.save(kubernetesYaml);
    }

    /**
     * 캐쉬 yaml 전체 삭제
     */
    @CacheEvict(cacheNames = "yaml", allEntries = true)
    public void cacheReloadAll() {
    }
}
