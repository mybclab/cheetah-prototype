package com.mybclab.cheetah.kubernetes.domain;

import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
public class KubernetesYaml implements Serializable {
    @Id
    @Column(unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    private Kubernetes.KIND kind;

    private String yaml;

}
