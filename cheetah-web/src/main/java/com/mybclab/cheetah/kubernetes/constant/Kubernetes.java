package com.mybclab.cheetah.kubernetes.constant;

import java.io.Serializable;

public class Kubernetes implements Serializable {

    public enum KIND {
        CONFIG, DEPLOYMENT, SERVICE, VOLUME, VOLUME_CLAIM, POD, RETURN
    }

}
