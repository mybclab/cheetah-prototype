package com.mybclab.cheetah.kubernetes.web;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.exception.CheetahRuntimeException;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.kubernetes.service.KubernetesAPIService;
import com.mybclab.cheetah.kubernetes.service.KubernetesService;
import com.mybclab.common.notifier.SlackNotifier;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.mybclab.common.utils.SecurityUtils;
import io.kubernetes.client.models.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@Slf4j
@RequestMapping("/kube")
public class KubernetesController {

    private ContainerService containerService;

    private KubernetesAPIService kubernetesAPIService;

    private KubernetesService kubernetesService;

    private NodeService nodeService;

    private SlackNotifier slackNotifier;

    private UserService userService;

    @Value("${cheetah.kubernetes.delimiter}")
    private String delimiter;

    public KubernetesController() {}

    @Autowired
    public KubernetesController(ContainerService containerService, KubernetesAPIService kubernetesAPIService
            , KubernetesService kubernetesService, NodeService nodeService, SlackNotifier slackNotifier, UserService userService) {
        this.containerService = containerService;
        this.kubernetesAPIService = kubernetesAPIService;
        this.kubernetesService = kubernetesService;
        this.nodeService = nodeService;
        this.slackNotifier = slackNotifier;
        this.userService = userService;
    }

    @ResponseBody
    @RequestMapping("/pre-stop/{username}/{containerId}")
    public ResponseEntity<JsonResult> preStop(@PathVariable String username, @PathVariable String containerId, @RequestBody Map<String, String> data, HttpServletRequest request) {
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> preStop");
        log.debug(">>>>>>>>>>>>>>>>>>>> username : " + username);
        log.debug(">>>>>>>>>>>>>>>>>>>> containerId : " + containerId);
        log.debug(">>>>>>>>>>>>>>>>>>>> data : " + data);
        String hostname = data.get("hostname");
        log.debug(">>>>>>>>>>>>>>>>>>>> hostname : " + hostname);

        Container container = containerService.findById(new ContainerPK(username, containerId));
        if (container != null && !Code.CONTAINER_STATUS.RETURN.equals(container.getStatusCode())) {
            container.setPodName(null);
            container.setStatusCode(Code.CONTAINER_STATUS.STOP);
            container.setPodStatusCode(null);
//            containerService.save(container);
            User containerUser = container.getUser();

            slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                    SlackNotifier.SlackMessageAttachement.builder()
                            .title("컨테이너 중지 (preStop)")
                            .title_link((request.getServerName() + ":" + request.getServerPort() + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId()))
                            .text(String.format("%s(%s) 님의 '%s' 컨테이너가 중지되었습니다.", containerUser.getName(), containerUser.getUsername(), container.getContainerName()))
                            .build(), userService.findSlackTargetList(containerUser));
        }
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @ResponseBody
    @RequestMapping("/post-start/{username}/{containerId}")
    public ResponseEntity<JsonResult> postStart(@PathVariable String username, @PathVariable String containerId, @RequestBody Map<String, String> data, HttpServletRequest request) {
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> postStart");
        log.debug(">>>>>>>>>>>>>>>>>>>> post-start username : " + username);
        log.debug(">>>>>>>>>>>>>>>>>>>> post-start containerId : " + containerId);
        log.debug(">>>>>>>>>>>>>>>>>>>> post-start data : " + data);

        String hostname = data.get("hostname");
        log.debug(">>>>>>>>>>>>>>>>>>>> post-start hostname : " + hostname);

        Container container = containerService.findById(new ContainerPK(username, containerId));
        log.debug(">>>>>>>>>>>>>>>>>>>> post-start hostname vs container.getPodName() " + hostname + " vs " + container.getPodName());
        if (!hostname.equals(container.getPodName()) && !"hostname".equals(hostname)) {
            // TODO
            container.setStatusCode(Code.CONTAINER_STATUS.START);
            // update podName
            container.setPodName(hostname);
            // get pod
            V1Pod pod = kubernetesAPIService.getPod(container);
            if (pod != null) {
                // nodeSn, externalIp
                String nodeName = pod.getSpec().getNodeName();
                if (nodeName != null) {
                    Node node = nodeService.findOneByNodeName(nodeName);
                    if (!container.getNodeSn().equals(node.getNodeSn())) {
                        container.setNodeSn(node.getNodeSn());
                        container.setExternalIp(node.getExternalIp());
                    }
                }

                log.debug(">>>>>>>>>> post-start update container : " + container.getStatusCode() + "," + container.getPodName() + "," + container.getNodeSn() + "," + container.getExternalIp());
                containerService.save(container);
                // slack notify
                User containerUser = container.getUser();


                slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                        SlackNotifier.SlackMessageAttachement.builder()
                                .title("컨테이너 재시작 (postStart)")
                                .title_link((request.getServerName() + ":" + request.getServerPort() + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId()))
                                .text(String.format("%s(%s) 님의 '%s' 컨테이너가 재시작되었습니다.", containerUser.getName(), containerUser.getUsername(), container.getContainerName()))
                                .build(), userService.findSlackTargetList(containerUser));
            }
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @RequestMapping("/{username}/{containerId}/info")
    public String info(@ModelAttribute ContainerPK containerPK, Model model) {

        Container container = containerService.findById(containerPK);

        V1Deployment deployment = kubernetesAPIService.getDeployment(container);
        V1Pod pod = kubernetesAPIService.getPod(container);
        V1Service service = kubernetesAPIService.getService(container);
        V1PersistentVolume persistentVolume = kubernetesAPIService.getPersistentVolume(container);
        V1PersistentVolumeClaim persistentVolumeClaim = kubernetesAPIService.getPersistentVolumeClaim(container);

        model.addAttribute("deployment", deployment);
        model.addAttribute("pod", pod);
        model.addAttribute("service", service);
        model.addAttribute("container", container);
        model.addAttribute("persistentVolume", persistentVolume);
        model.addAttribute("persistentVolumeClaim", persistentVolumeClaim);

        return "kubernetes/info";
    }

    @RequestMapping("/node")
    public String nodeList(Model model) {

        V1NodeList nodeList = kubernetesAPIService.getNodeList();
        model.addAttribute("nodeList", nodeList);
        return "kubernetes/node-list";
    }

    @PostMapping("/node/{nodeName}/update")
    public String updateNode(@PathVariable String nodeName) {

        kubernetesService.updateNode(nodeName);

        return "redirect:/kube/node";
    }

    @ResponseBody
    @PostMapping("/node/{nodeName}")
    public ResponseEntity<JsonResult> updateNodeRest(@PathVariable String nodeName, String externalIp) {
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);
        log.debug(">>>>>>>>> externalIp : " + externalIp);
        boolean updateResult = kubernetesService.updateNode(nodeName, externalIp);
        result.setData("result", updateResult);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @PostMapping("/{username}/{containerId}/update")
    public String update(@ModelAttribute ContainerPK containerPK) {
        Container container = containerService.findById(containerPK);

        kubernetesService.updateContainer(container);

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }



    @PostMapping("/{username}/{containerId}/fail")
    public String fail(@ModelAttribute ContainerPK containerPK) {
        Container container = containerService.findById(containerPK);

        container.setStatusCode(Code.CONTAINER_STATUS.FAIL);
        containerService.save(container);

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @RequestMapping("/webconsole/{hostname}")
    public String webconsole(@PathVariable("hostname") String hostname, Model model) {
        User user = SecurityUtils.getPrincipal();

        Container container = containerService.findByPodName(hostname);
        if (container == null) {
            throw new EntityNotFoundException("Error.noContainer");
        }

        User containerUser = container.getUser();

        if (!(user.getUsername().equals(containerUser.getUsername()) || user.getUsername().equals(containerUser.getParentUsername())
                || user.isSystemAdmin())) {
            throw new CheetahRuntimeException("Authentication.accessDenied");
        }

        model.addAttribute("hostname", hostname);
        model.addAttribute("externalIp", container.getExternalIp());
        model.addAttribute("websocketPort", container.getWebsocketPort());
        return "webconsole/webconsole";
    }
}
