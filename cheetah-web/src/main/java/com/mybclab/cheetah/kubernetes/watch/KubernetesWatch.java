package com.mybclab.cheetah.kubernetes.watch;

import com.google.gson.reflect.TypeToken;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.repository.UserRepository;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.service.MailService;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.kubernetes.service.KubernetesService;
import com.mybclab.common.notifier.SlackNotifier;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.JSON;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class KubernetesWatch {

    private ApiClient client;
    private CoreV1Api coreV1Api;

    @Autowired
    private KubernetesService kubernetesService;

    @Autowired
    private ContainerRepository containerRepository;

    @Autowired
    private ContainerHistoryService containerHistoryService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailService mailService;


    private String namespace;

    private String delimiter;

    @Autowired
    private SlackNotifier slackNotifier;

    @Autowired
    public KubernetesWatch(@Value("${cheetah.kubernetes.namespace}") String namespace
                            , @Value("${cheetah.kubernetes.delimiter}") String delimiter
                            , @Value("${cheetah.kubernetes.master-node-ip}") String masterIp
                            , @Value("${cheetah.kubernetes.bearer-token}") String bearerToken) {
        //            client = Config.defaultClient();

        client = Config.fromToken("https://" + masterIp + ":6443", bearerToken, false);
        this.namespace = namespace;
        this.delimiter = delimiter;

        client.getHttpClient().setReadTimeout(0, TimeUnit.MILLISECONDS);

        JSON json = new JSON();
        json.setLenientOnJson(true);
        client.setJSON(json);

        Configuration.setDefaultApiClient(client);
        coreV1Api = new CoreV1Api();

    }

    @Async
    public void watchPod(String label, String serverName, int serverPort) {
        try {
            log.debug(" WatchHandlerForPod >> label : " + label + "," + namespace);
            Watch<V1Pod> watchForPod = Watch.createWatch(
                    client,
                    coreV1Api.listNamespacedPodCall(namespace, null, null, null, null, "app in (" + label + ")", null, null, null, true, null, null),
                    new TypeToken<Watch.Response<V1Pod>>() {}.getType());
            final ExecutorService executorServiceForPod = Executors.newSingleThreadExecutor();
            executorServiceForPod.execute(() -> {
                while (true) {
//                log.debug(new Date()+"WatchHandlerForPod Runnable");
                    try {
                        watchForPod.forEach(response -> {
                            V1Pod pod = response.object;

                            log.debug(" WatchHandlerForPod >> pod : " + pod.getMetadata().getName());
                            String podName = pod.getMetadata().getName();
                                // 1. find container by podName
                                String containerId = podName.split(delimiter)[2];
                                log.debug("WatchHandlerForPod >> containerId : {}", containerId);
                                Container container = containerRepository.findByContainerPKContainerId(containerId);




                                log.debug("WatchHandlerForPod >> container : {}", container);
                                if (container != null
                                        && (container.getStatusCode() == Code.CONTAINER_STATUS.CREATE)) {



                                    kubernetesService.updateContainer(container, pod, (containerUser, reason) -> {
                                        log.debug(">>>>>> WatchHandlerForPod success");

                                        try {
                                            List<SlackNotifier.SlackHook> slackList = new ArrayList<>();
                                            if (containerUser.isGroupUser()) {
                                                User group = containerUser.getGroup();
                                                slackList.add(new SlackNotifier.SlackHook(group.getSlackHookUrl(), group.getSlackChannel()));
                                            }
                                            List<User> userList = userRepository.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN.toString());
                                            for (User admin : userList) {
                                                slackList.add(new SlackNotifier.SlackHook(admin.getSlackHookUrl(), admin.getSlackChannel()));
                                            }
                                            // 컨테이너 이력 쌓기
                                            containerHistoryService.save(new ContainerHistory(container));

                                            log.debug("WatchHandlerForPod >> notify email start");
                                            mailService.sendSshPassword(container);
                                            mailService.sendJupyterToken(container);
                                            log.debug("WatchHandlerForPod >> notify slack start");
                                            slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                                                    SlackNotifier.SlackMessageAttachement.builder()
                                                            .title("컨테이너 생성 성공")
                                                            .title_link((serverName + ":" + serverPort + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId()))
                                                            .text(String.format("%s(%s) 님의 '%s' 컨테이너 생성에 성공하였습니다.", containerUser.getName(), containerUser.getUsername(), container.getContainerName()))
                                                            .build(), slackList);
                                            log.debug("WatchHandlerForPod >> notify end");
                                        } catch (Exception e) {
                                            log.error("WatchHandlerForPod >> 컨테이너 생성 성공 알림 오류 {}", e.getMessage());
                                        }

                                        executorServiceForPod.shutdown();

                                    }, (containerUser, reason) -> {
                                        log.debug(">>>>>>>> WatchHandlerForPod fail");
                                        try {
                                            List<SlackNotifier.SlackHook> slackList = new ArrayList<>();
                                            if (containerUser.isGroupUser()) {
                                                User group = containerUser.getGroup();
                                                slackList.add(new SlackNotifier.SlackHook(group.getSlackHookUrl(), group.getSlackChannel()));
                                            }
                                            List<User> userList = userRepository.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN.toString());
                                            for (User admin : userList) {
                                                slackList.add(new SlackNotifier.SlackHook(admin.getSlackHookUrl(), admin.getSlackChannel()));
                                            }
                                            log.debug("WatchHandlerForPod >> notify fail start");
                                            slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                                                    SlackNotifier.SlackMessageAttachement.builder()
                                                            .title("컨테이너 생성 실패")
                                                            .title_link((serverName + ":" + serverPort + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId()))
                                                            .text(String.format("%s(%s) 님의 '%s' 컨테이너 생성에 실패하였습니다. (%s)", containerUser.getName(), containerUser.getUsername(), container.getContainerName(), reason))
                                                            .build(), slackList);
                                            log.debug("WatchHandlerForPod >> notify fail end");
                                        } catch (Exception e) {
                                            log.error("WatchHandlerForPod >> 컨테이너 생성 실패 알림 오류 {}", e.getMessage());
                                        }
                                        executorServiceForPod.shutdown();
                                    });
                                }
                        });
                    } catch (Throwable e) {
                        log.error("WatchHandlerForPod >> e : {}", e);
                        try {
                            Thread.sleep(1000*5);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
        } catch (ApiException e) {
            log.error(">>>>>>>>>> watchPod : " + e.getMessage());
        }
    }

}
