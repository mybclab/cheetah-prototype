package com.mybclab.cheetah.kubernetes.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class KubernetesYamlForm {

    @NotBlank
    private String kind;

    @NotBlank
    private String yaml;
}
