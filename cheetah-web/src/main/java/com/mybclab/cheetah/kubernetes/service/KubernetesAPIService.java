package com.mybclab.cheetah.kubernetes.service;

import com.google.gson.JsonSyntaxException;
import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerVolume;
import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.exception.KubernetesException;
import com.mybclab.common.utils.StringUtils;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.JSON;
import io.kubernetes.client.apis.AppsV1Api;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.*;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;
import io.kubernetes.client.util.Yaml;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.StringReader;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class KubernetesAPIService {


    private String namespace;

    private String prefix;

    private String delimiter;

    private String basepath;

    private static final String STATUS_SUCCESS = "Success";

    private static final String VOLUME_SUCCESS_STATUS = "Available";

    private static final String VOLUME_CLAIM_SUCCESS_STATUS = "Bound";

    private ApiClient client;

    private AppsV1Api appsV1Api = null;

    private CoreV1Api coreV1Api = null;

    private String registry;

    @Autowired
    private KubernetesYamlService kubernetesYamlService;

    private Watch<V1Pod> watchForPod;

    private String customVolumePrefix = "custom";


    private String cloudBasePath;


    @Autowired
    public KubernetesAPIService(@Value("${cheetah.kubernetes.namespace}") String namespace
                                , @Value("${cheetah.kubernetes.prefix}") String prefix
                                , @Value("${cheetah.kubernetes.delimiter}") String delimiter
                                , @Value("${cheetah.kubernetes.basepath}") String basepath
                                , @Value("${cheetah.kubernetes.cloud-basepath}") String cloudBasePath
                                , @Value("${cheetah.kubernetes.registry}") String registry
                                , @Value("${cheetah.kubernetes.master-node-ip}") String masterIp
                                , @Value("${cheetah.kubernetes.bearer-token}") String bearerToken) throws KubernetesException {

        this.namespace = namespace;
        this.prefix = prefix;
        this.delimiter = delimiter;
        this.basepath = basepath;
        this.cloudBasePath = cloudBasePath;
        this.registry = registry;

//            client = Config.defaultClient();
        client = Config.fromToken("https://" + masterIp + ":6443", bearerToken, false);


        client.getHttpClient().setReadTimeout(0, TimeUnit.MILLISECONDS);

        JSON json = new JSON();
        json.setLenientOnJson(true);
        client.setJSON(json);

        client.setDebugging(true);
        Configuration.setDefaultApiClient(client);

        appsV1Api = new AppsV1Api();
        coreV1Api = new CoreV1Api();
    }

    /**
     * volume, volumeClaim, deployment, service 모두 생성
     * 오류날 경우 삭제
     *
     * @param container {@link Container}
     * @param token jupyter Token
     * @return 생성결과
     */
    public boolean createAll(Container container, String token, String password, String accelerator) {
        try {
            int customVolumeSize = 0;
            if (container.getContainerVolumeList().size() > 0) {
                customVolumeSize = createCustomVolumeAndClaim(container);
            }
            return createVolume(container) && createVolumeClaim(container) && createDeployment(container, token, password, accelerator, customVolumeSize) && createService(container);
        } catch (KubernetesException e) {
            switch (e.getKind()) {
                case SERVICE:
                    deleteByKind(Kubernetes.KIND.DEPLOYMENT, container);
                case DEPLOYMENT:
                    deleteByKind(Kubernetes.KIND.VOLUME_CLAIM, container);
                case VOLUME_CLAIM:
                    deleteByKind(Kubernetes.KIND.VOLUME, container);
                    break;
            }
            return false;
        }
    }

    public boolean createAll(Container container, String token, String password) {
        return createAll(container, token, password, container.getGpu().getGpuLabel());
    }

    private int createCustomVolumeAndClaim(Container container) throws KubernetesException {
        String label = generateLabel(container);
        for (int i = 0; i < container.getContainerVolumeList().size(); i++) {
            ContainerVolume containerVolume = container.getContainerVolumeList().get(i);
            String capacity = containerVolume.getVolumeSize() + "Gi";
            String name = customVolumePrefix + (i + 1);
            boolean result = createVolume(label + delimiter + name, containerVolume.getVolumeMountPath(), capacity);
            if (result) createVolumeClaim(label + delimiter + name, capacity);
        }
        return container.getContainerVolumeList().size();
    }


    private boolean createVolume(String name, String path, String capacity) throws KubernetesException {
        try {
            String volumeYaml = kubernetesYamlService.findById(Kubernetes.KIND.VOLUME).getYaml();
            volumeYaml = volumeYaml.replaceAll("\\{label\\}", name);
            volumeYaml = volumeYaml.replaceAll("\\{jupyterPath\\}", path);
            volumeYaml = volumeYaml.replaceAll("\\{namespace\\}", namespace);
            volumeYaml = volumeYaml.replaceAll("\\{capacity\\}", capacity);
            Yaml yaml = new Yaml();
            V1PersistentVolume persistentVolume = yaml.loadAs(new StringReader(volumeYaml), V1PersistentVolume.class);

            V1PersistentVolume createdPersistentVolume = coreV1Api.createPersistentVolume(persistentVolume, null, null, null);

            return createdPersistentVolume != null && createdPersistentVolume.getMetadata().getCreationTimestamp() != null;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> createVolume ApiException : {}", e);
            throw new KubernetesException(Kubernetes.KIND.VOLUME, e.getMessage());
        }
    }

    private boolean createVolume(Container container) throws KubernetesException {
        String label = generateLabel(container);
        PodFormat podFormat = container.getPodFormat();
        String volumeBasePath = container.isContainerWithCloud() ? cloudBasePath : basepath;
        log.error(">>>>>>>>>>>>>>>>> createVolume isContainerWithCloud, volumeBasePath : {} : {}", container.isContainerWithCloud(), volumeBasePath);
        return createVolume(label, volumeBasePath + File.separator + container.getContainerPK().getContainerId(), podFormat.getCapacity()) ;
    }

    private boolean createVolumeClaim(Container container) throws KubernetesException {
        String label = generateLabel(container);
        PodFormat podFormat = container.getPodFormat();
        return createVolumeClaim(label, podFormat.getCapacity());
    }

    private boolean createVolumeClaim(String name, String capacity) throws KubernetesException {
        return createVolumeClaim(name, name, capacity);
    }
    private boolean createVolumeClaim(String name, String pvName, String capacity) throws KubernetesException {
        try {
            String volumeClaimYaml = kubernetesYamlService.findById(Kubernetes.KIND.VOLUME_CLAIM).getYaml();
            volumeClaimYaml = volumeClaimYaml.replaceAll("\\{label\\}", name);
            volumeClaimYaml = volumeClaimYaml.replaceAll("\\{pvLabel\\}", pvName);
            volumeClaimYaml = volumeClaimYaml.replaceAll("\\{namespace\\}", namespace);
            volumeClaimYaml = volumeClaimYaml.replaceAll("\\{capacity\\}", capacity);

            Yaml yaml = new Yaml();
            V1PersistentVolumeClaim persistentVolumeClaim = yaml.loadAs(new StringReader(volumeClaimYaml), V1PersistentVolumeClaim.class);

            V1PersistentVolumeClaim createdPersistentVolumeClaim = coreV1Api.createNamespacedPersistentVolumeClaim(namespace, persistentVolumeClaim, null, null, null);

            return createdPersistentVolumeClaim != null && createdPersistentVolumeClaim.getMetadata().getCreationTimestamp() != null;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> createVolumeClaim ApiException : {}", e);
            throw new KubernetesException(Kubernetes.KIND.VOLUME_CLAIM, e.getMessage());
        }
    }

    private boolean createDeployment(Container container, String token, String password, String accelerator, int customVolumeSize) throws KubernetesException {

        try {
            String label = generateLabel(container);

            PodFormat podFormat = container.getPodFormat();
            String deploymentYaml = kubernetesYamlService.findById(Kubernetes.KIND.DEPLOYMENT).getYaml();;
            deploymentYaml = deploymentYaml.replaceAll("\\{namespace\\}", namespace);
            deploymentYaml = deploymentYaml.replaceAll("\\{label\\}", label);
            deploymentYaml = deploymentYaml.replaceAll("\\{gpu\\}", accelerator);
            Gpu gpu = container.getGpu();
            if (Code.RESOURCE_TYPE.CPU.equals(gpu.getResourceType())) {
                if (container.getGroupSharedYn()) {
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuLabel\\}", "cpu");
                    log.debug(">>>>>>>>> core, memory Size : " + container.getCpuLimit() + "core , " + container.getMemoryLimit() + "Mi");
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuQuantity\\}", container.getCpuLimit() + "m");
                    deploymentYaml = deploymentYaml.replaceAll("\\{memorySize\\}", container.getMemoryLimit() + "Mi");
                } else {
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuLabel\\}: \\{gpuQuantity\\}", ""); // TODO cpu 제한 없음
                    deploymentYaml = deploymentYaml.replaceAll("memory: \\{memorySize\\}", ""); // TODO memory 제한 없음
                }
            } else {
                if (container.getGroupSharedYn()) {
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuLabel\\}", "aliyun.com/gpu-mem");
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuQuantity\\}", Double.toString(container.getGroupSharedSize()));
                } else {
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuLabel\\}", "nvidia.com/gpu");
                    deploymentYaml = deploymentYaml.replaceAll("\\{gpuQuantity\\}", Integer.toString(container.getGpuQuantity()));
                }
                deploymentYaml = deploymentYaml.replaceAll("memory: \\{memorySize\\}", ""); // TODO memory 제한 없음
            }
            deploymentYaml = deploymentYaml.replaceAll("\\{image\\}", registry != null ? registry + "/" + podFormat.getImage() : podFormat.getImage());
            deploymentYaml = deploymentYaml.replaceAll("\\{command\\}", podFormat.getCommand());
            deploymentYaml = deploymentYaml.replaceAll("\\{token\\}", token);
            deploymentYaml = deploymentYaml.replaceAll("\\{password\\}", password);
            deploymentYaml = deploymentYaml.replaceAll("\\{username\\}", container.getContainerPK().getUsername());
            deploymentYaml = deploymentYaml.replaceAll("\\{containerId\\}", container.getContainerPK().getContainerId());

            Yaml yaml = new Yaml();
            V1Deployment deployment = yaml.loadAs(new StringReader(deploymentYaml), V1Deployment.class);

            if (customVolumeSize > 0) {
                for (int i = 0; i < container.getContainerVolumeList().size(); i++) {
                    ContainerVolume containerVolume = container.getContainerVolumeList().get(i);
                    V1Volume volume = new V1Volume();
                    String customVolumeName = customVolumePrefix + (i + 1);
                    volume.setName(customVolumeName);
                    volume.setPersistentVolumeClaim(new V1PersistentVolumeClaimVolumeSource().claimName(label + delimiter + customVolumeName));
                    deployment.getSpec().getTemplate().getSpec().getVolumes().add(volume);
                    log.debug(">>>>>>>>>>>>>>>>> customVolume : {}", volume);

                    V1VolumeMount volumeMount = new V1VolumeMount();
                    volumeMount.setName(customVolumeName);
                    volumeMount.setMountPath(containerVolume.getVolumeMountPath());
                    deployment.getSpec().getTemplate().getSpec().getContainers().get(0).getVolumeMounts().add(volumeMount);
                    log.debug(">>>>>>>>>>>>>>>>> customVolumeMount : {}", volumeMount);
                }
            }

            V1Deployment createdDeployment = appsV1Api.createNamespacedDeployment(namespace, deployment, null, null, null);

            return createdDeployment != null && createdDeployment.getMetadata().getCreationTimestamp() != null;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> createDeployment ApiException : {}", e);
            throw new KubernetesException(Kubernetes.KIND.DEPLOYMENT, e.getMessage());
        }
    }

    private boolean createService(Container container) throws KubernetesException {
        try {

            String label = generateLabel(container);

            PodFormat podFormat = container.getPodFormat();
            String serviceYaml = kubernetesYamlService.findById(Kubernetes.KIND.SERVICE).getYaml();;

            serviceYaml = serviceYaml.replaceAll("\\{label\\}", label);
            serviceYaml = serviceYaml.replaceAll("\\{namespace\\}", namespace);

            Yaml yaml = new Yaml();

            V1Service service = yaml.loadAs(new StringReader(serviceYaml), V1Service.class);
            V1Service createdService = coreV1Api.createNamespacedService(namespace, service, null, null, null);

            return createdService != null && createdService.getMetadata().getCreationTimestamp() != null;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> createService ApiException : {}", e);
            throw new KubernetesException(Kubernetes.KIND.SERVICE, e.getMessage());
        }
    }

    /**
     * service, deployment, volumeClaim, volume 모두 삭제
     * 하나만 삭제되도 서비스가 불가능하므로 || 처리
     * @param container {@link Container}
     * @return 삭제결과
     */
    public boolean deleteAll(Container container) {
        boolean resultService = deleteByKind(Kubernetes.KIND.SERVICE, container);
        boolean resultDeployment = deleteByKind(Kubernetes.KIND.DEPLOYMENT, container);
        boolean resultVolumeClaim = deleteByKind(Kubernetes.KIND.VOLUME_CLAIM, container);
        boolean resultVolume = deleteByKind(Kubernetes.KIND.VOLUME, container);
        boolean resultVolumeClaimCustom = true, resultVolumeCustom = true;

        if (container.getContainerVolumeList().size() > 0) {
            for (int i = 0; i < container.getContainerVolumeList().size(); i++) {
                String customVolumeName = customVolumePrefix + (i + 1);
                resultVolumeClaimCustom = deleteByKind(Kubernetes.KIND.VOLUME_CLAIM, container, customVolumeName);
                resultVolumeCustom = deleteByKind(Kubernetes.KIND.VOLUME, container, customVolumeName);
            }
        }

//        return resultService || resultDeployment || resultVolumeClaim || resultVolume || resultVolumeClaimCustom || resultVolumeCustom;
        return resultService || resultDeployment || resultVolumeClaim || resultVolume || resultVolumeClaimCustom;
    }

    public boolean deletePod(Container container) {
        return deleteByKind(Kubernetes.KIND.POD, container);
    }

    private boolean deleteByKind(Kubernetes.KIND kind, Container container) {
        return deleteByKind(kind, container, null);
    }
    private boolean deleteByKind(Kubernetes.KIND kind, Container container, String customName) {
        try {
            String label = generateLabel(container);
            if (StringUtils.isNotEmpty(customName)) {
                label += delimiter + customName;
            }
            V1DeleteOptions options = new V1DeleteOptions();
            V1Status status = null;
            switch (kind) {
                case VOLUME :
                    status = coreV1Api.deletePersistentVolume(label, options, null, null, null, null, null);
                    break;
                case VOLUME_CLAIM:
                    status = coreV1Api.deleteNamespacedPersistentVolumeClaim(label, namespace, options, null, null, null, null, null);
                    break;
                case DEPLOYMENT :
                    status = appsV1Api.deleteNamespacedDeployment(label, namespace, options, null, null, null, null, null);
                    break;
                case SERVICE :
                    status = coreV1Api.deleteNamespacedService(label, namespace, options, null, null, null, null, null);
                    break;
                case POD :
                    String podName = container.getPodName();
                    status = coreV1Api.deleteNamespacedPod(podName, namespace, options, null, null, null, null, null);
                    break;
            }

            return STATUS_SUCCESS.equals(status.getStatus());
        } catch (JsonSyntaxException e) {
//            log.error(">>>>>>>>>>>>>>>>> delete : kind : " + kind + " JsonSyntaxException : {}", e);
            if (e.getCause() instanceof IllegalStateException) {
                IllegalStateException ise = (IllegalStateException) e.getCause();
                if (ise.getMessage() != null && ise.getMessage().contains("Expected a string but was BEGIN_OBJECT")) {
//                    log.error("Catching exception because of issue https://github.com/kubernetes-client/java/issues/86", e);
                    return true;
                }
            }
            // TODO parsing 하다 나는 오류... true 로 일단 리턴 (https://github.com/kubernetes-client/java/issues/86)
            return false;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> delete : kind : " + kind + " ApiException : {}", e);
            return false;
        }
    }

    public String generateLabel(Container container) {
        return prefix + delimiter + container.getContainerPK().getUsername() + delimiter + container.getContainerPK().getContainerId();
    }


    /**
     * service 의 nodePort 를 가져온다. (jupyter notebook port)
     *
     * @param container {@link Container}
     * @param name jupyter | websocket
     * @return nodePort (jupyter notebook port)
     */
    public int getNodePort(Container container, String name) {
        try {
            String label = generateLabel(container);

            V1Service service = coreV1Api.readNamespacedService(label, namespace, null, null, null);

            for (V1ServicePort port : service.getSpec().getPorts()) {
                if (name.equals(port.getName())) {
                    return port.getNodePort();
                }
            }
            return 0;
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getNodePort ApiException : {}", e);
            return 0;
        }
    }

    public V1Deployment getDeployment(Container container) {
        try {
            String label = generateLabel(container);
            return appsV1Api.readNamespacedDeployment(label, namespace, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getDeployment ApiException : {}", e);
        }
        return null;
    }

    public V1Pod getPod(Container container) {

        try {
            String label = generateLabel(container);
            V1PodList podList = coreV1Api.listNamespacedPod(namespace, null, null, null, null, "app in (" + label + ")", null, null, null, null);

            if (podList.getItems() != null && podList.getItems().size() > 0) {
                V1Pod pod = podList.getItems().get(0);
                return pod;
            }

        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getPod ApiException : {}", e);
        }
        return null;
    }

    public V1Service getService(Container container) {

        try {
            String label = generateLabel(container);
            return coreV1Api.readNamespacedService(label, namespace, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getService ApiException : {}", e);
        }
        return null;
    }

    public V1PersistentVolumeClaim getPersistentVolumeClaim(Container container) {
        try {
            String label = generateLabel(container);
            return coreV1Api.readNamespacedPersistentVolumeClaim(label, namespace, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getPersistentVolumeClaim ApiException : {}", e);
        }
        return null;
    }

    public V1PersistentVolume getPersistentVolume(Container container) {
        try {
            String label = generateLabel(container);
            return coreV1Api.readPersistentVolume(label, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getPersistentVolumeClaim ApiException : {}", e);
        }
        return null;
    }

    public V1NodeList getNodeList() {
        try {
            return coreV1Api.listNode(null, null, null, null, null, null, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getNodeList ApiException : {}", e);
        }
        return null;
    }

    public V1Node getNode(String nodeName) {
        try {
            return coreV1Api.readNode(nodeName, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> getNodeList ApiException : {}", e);
        }
        return null;
    }

    public boolean deleteNode(String nodeName) {
        try {
            V1DeleteOptions deleteOptions = new V1DeleteOptions();
            coreV1Api.deleteNode(nodeName, deleteOptions, null, null, null, null, null);
        } catch (ApiException e) {
            log.error(">>>>>>>>>>>>>>>>> deleteNode ApiException : {}", e);
        }
        return true;
    }





}
