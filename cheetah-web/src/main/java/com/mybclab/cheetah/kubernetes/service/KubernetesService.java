package com.mybclab.cheetah.kubernetes.service;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.admin.pod.domain.DeploymentCondition;
import com.mybclab.cheetah.admin.pod.domain.PodCondition;
import com.mybclab.cheetah.admin.pod.repository.DeploymentConditionRepository;
import com.mybclab.cheetah.admin.pod.repository.PodConditionRepository;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.models.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class KubernetesService {


    private NodeService nodeService;

    private ContainerRepository containerRepository;
    private PodConditionRepository podConditionRepository;
    private DeploymentConditionRepository deploymentConditionRepository;

    private ContainerHistoryService containerHistoryService;
    private KubernetesAPIService kubernetesAPIService;


    private DateTimeFormatter kubeTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    public KubernetesService() {}

    @Autowired
    public KubernetesService(NodeService nodeService, ContainerRepository containerRepository, PodConditionRepository podConditionRepository, DeploymentConditionRepository deploymentConditionRepository, KubernetesAPIService kubernetesAPIService, ContainerHistoryService containerHistoryService) {
        this.nodeService = nodeService;
        this.containerRepository = containerRepository;
        this.podConditionRepository = podConditionRepository;
        this.deploymentConditionRepository = deploymentConditionRepository;
        this.kubernetesAPIService = kubernetesAPIService;
        this.containerHistoryService = containerHistoryService;
    }

    @Transactional
    public boolean updateContainer(Container container, V1Pod pod, UpdateContainerPostExec successExec, UpdateContainerPostExec failExec) {
        if (pod != null) {

            Code.CONTAINER_STATUS origStatusCode = container.getStatusCode();

            String nodeName = pod.getSpec().getNodeName();
            if (nodeName != null) {
                Node node = nodeService.findOneByNodeName(nodeName);
                container.setNodeSn(node.getNodeSn());
                container.setExternalIp(node.getExternalIp());
            }

            // 2. podName
            String podName = pod.getMetadata().getName();
            container.setPodName(podName);

            // 3. status from podConditions
            String reason = "";
            boolean podConditionStatus = true;
            for (V1PodCondition condition : pod.getStatus().getConditions()) {
                PodCondition podCondition = new PodCondition(container.getContainerPK());
                podCondition.setPodName(podName);
                if (condition.getLastProbeTime() != null)
                    podCondition.setLastProbeTime(LocalDateTime.parse(condition.getLastProbeTime().toString(), kubeTimeFormat));
                podCondition.setStatus(!"Unknown".equals(condition.getStatus()) ? Boolean.valueOf(condition.getStatus()) : null);
                podCondition.setTypeCode(Code.POD_CONDITION_TYPE.valueOf(condition.getType().toUpperCase()));
                podCondition.setMessage(condition.getMessage());
                podCondition.setReason(condition.getReason());
                podCondition.setLastTransitionTime(LocalDateTime.parse(condition.getLastTransitionTime().toString(), kubeTimeFormat));
                podCondition.setCreatedAt(LocalDateTime.now());
                podConditionRepository.saveAndFlush(podCondition);
                log.debug("@@@@@@@@@@@@@@@@@@ checkContainerResult : check success of fail : podConditionStatus : " + podConditionStatus + ", podConditionStatus : " + condition.getStatus() + ", condition.getReason() : " + condition.getReason());
                if (podConditionStatus && !Boolean.valueOf(condition.getStatus()) && !"ContainersNotReady".equals(condition.getReason()) && !"Unschedulable".equals(condition.getReason())  && !"MinimumReplicasUnavailable".equals(condition.getReason())) {
                    log.debug("@@@@@@@@@@@@@@@@@@ checkContainerResult : " + podConditionStatus + ", podConditionStatus : " + condition.getStatus() + ", condition.getReason() : " + condition.getReason() + ";");
                    podConditionStatus = false; // 진짜 실패
                    reason = condition.getReason() + " - " + condition.getMessage();
                }
            }

            // 4. status from deploymentConditions
            boolean deploymentStatus = true;
            V1Deployment deployment = kubernetesAPIService.getDeployment(container);
            if (deployment != null) {
                List<V1DeploymentCondition> deploymentConditions = deployment.getStatus().getConditions();
                for (V1DeploymentCondition condition : deploymentConditions) {
                    DeploymentCondition deploymentCondition = new DeploymentCondition(container.getContainerPK());
                    deploymentCondition.setDeploymentName(podName);
                    if (condition.getLastUpdateTime() != null)
                        deploymentCondition.setLastUpdateTime(LocalDateTime.parse(condition.getLastUpdateTime().toString(), kubeTimeFormat));
                    deploymentCondition.setStatus(!"Unknown".equals(condition.getStatus()) ? Boolean.valueOf(condition.getStatus()) : null);
                    deploymentCondition.setTypeCode(Code.DEPLOYMENT_CONDITION_TYPE.valueOf(condition.getType().toUpperCase()));
                    deploymentCondition.setMessage(condition.getMessage());
                    deploymentCondition.setReason(condition.getReason());
                    deploymentCondition.setLastTransitionTime(LocalDateTime.parse(condition.getLastTransitionTime().toString(), kubeTimeFormat));
                    deploymentCondition.setCreatedAt(LocalDateTime.now());
                    deploymentConditionRepository.saveAndFlush(deploymentCondition);

                    if (deploymentStatus) {
                        if (!"MinimumReplicasUnavailable".equals(condition.getReason())) {
                            deploymentStatus = Boolean.valueOf(condition.getStatus());
                            reason = condition.getReason() + " - " + condition.getMessage();
                        }
                    }
                }
            }

            String phase = pod.getStatus().getPhase();
            Code.POD_STATUS podStatus = Code.POD_STATUS.valueOf(phase.toUpperCase());
            log.debug("@@@@@@@@@@@@@@@@@@ checkContainerResult : phase : " + phase);
            log.debug("@@@@@@@@@@@@@@@@@@ checkContainerResult : before podStatus : " + podStatus + ", podStatus.getContainerStatus() : " + podStatus.getContainerStatus() + ", podConditionStatus : " + podConditionStatus + ", deploymentStatus : " + deploymentStatus);
            if (Code.POD_STATUS.RUNNING.equals(podStatus) && (!podConditionStatus || !deploymentStatus)) {
                podStatus = Code.POD_STATUS.PENDING;
            }
            log.debug("@@@@@@@@@@@@@@@@@@ checkContainerResult : after podStatus : " + podStatus + ", podStatus.getContainerStatus() : " + podStatus.getContainerStatus());

            container.setPodStatusCode(podStatus);
            if (!podConditionStatus) {
                container.setStatusCode(Code.CONTAINER_STATUS.FAIL);
            } else {
                container.setStatusCode(podStatus.getContainerStatus());
                if (origStatusCode != Code.CONTAINER_STATUS.START && podStatus.getContainerStatus() == Code.CONTAINER_STATUS.START) {
                    if (container.getStartDate() == null) {
                        container.setStartDate(LocalDateTime.now());
                    }
                    container.setReturnDate(null);
                }
            }

            // 5. port setting from service
            V1Service service = kubernetesAPIService.getService(container);
            if (service != null) {
                List<V1ServicePort> ports = service.getSpec().getPorts();
                for (V1ServicePort port : ports) {
                    switch (port.getName()) {
                        case "jupyter":
                            container.setNotebookPort(port.getNodePort());
                            break;
                        case "websocket":
                            container.setWebsocketPort(port.getNodePort());
                            break;
                        case "ssh":
                            container.setSshPort(port.getNodePort());
                            break;
                        case "tensorboard":
                            container.setTensorboardPort(port.getNodePort());
                            break;
                    }
                }
            }


            containerRepository.saveAndFlush(container);

            User containerUser =  container.getUser();

            if (successExec != null) {
                if (container.getStatusCode() == Code.CONTAINER_STATUS.START && !origStatusCode.equals(container.getStatusCode())) {
                    successExec.doit(containerUser, "");
                }
            }
            if (failExec != null) {
                if (container.getStatusCode() == Code.CONTAINER_STATUS.FAIL) {
                    failExec.doit(containerUser, reason);
                }
            }
            return true;
        }

        return false;
    }

    public interface UpdateContainerPostExec {
        void doit(User user, String reason);
    }

    @Transactional
    public boolean updateContainer(Container container) {
        // 1. node setting
        V1Pod pod = kubernetesAPIService.getPod(container);
        return updateContainer(container, pod, (containerUser, reason) -> {
            // 컨테이너 이력 쌓기
            containerHistoryService.save(new ContainerHistory(container));
        }, null);
    }

    @Transactional
    public boolean updateNode(String nodeName) {
        return updateNode(nodeName, null);
    }

    @Transactional
    public boolean updateNode(String nodeName, String externalIp) {
        Node node = null;
        try {
            node = nodeService.findOneByNodeName(nodeName);
        } catch (EntityNotFoundException e) {
            node = new Node();
        }

        V1Node v1Node = kubernetesAPIService.getNode(nodeName);
        if (v1Node != null) {
            // nodeName
            node.setNodeName(v1Node.getMetadata().getName());
            // internalIp, internalDomain
            List<V1NodeAddress> addresses = v1Node.getStatus().getAddresses();
            for (V1NodeAddress address : addresses) {
                if ("InternalIP".equals(address.getType())) {
                    node.setInternalIp(address.getAddress());
                    node.setExternalIp(externalIp == null ? address.getAddress() : externalIp);
                } else if ("Hostname".equals(address.getType())) {
                    node.setInternalDomain(address.getAddress());
                }
            }
            // gpu label, masterYn
            Map<String, String> labels = v1Node.getMetadata().getLabels();
            String gpuLabel = labels.get("accelerator");
            node.setGpuLabel(gpuLabel);
            node.setMasterYn(labels.keySet().contains("node-role.kubernetes.io/master"));
            // gpu quantity
            Map<String, Quantity> capacity = v1Node.getStatus().getCapacity();
            Quantity quantity = capacity.get("nvidia.com/gpu");
            node.setGpuQuantity(quantity != null ? Integer.parseInt(String.valueOf(quantity.getNumber())) : 0);
            // status
            node.setStatusCode(Code.NODE_STATUS.NOT_READY);
            List<V1NodeCondition> conditions = v1Node.getStatus().getConditions();
            for (V1NodeCondition v1NodeCondition : conditions) {
                if ("Ready".equals(v1NodeCondition.getType()) && Boolean.valueOf(v1NodeCondition.getStatus())) {
                    node.setStatusCode(Code.NODE_STATUS.READY);
                }
            }

            nodeService.save(node);
            return true;
        }
        return false;
    }
}
