package com.mybclab.cheetah.kubernetes.repository;

import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.domain.KubernetesYaml;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KubernetesYamlRepository extends JpaRepository<KubernetesYaml, Kubernetes.KIND> {


}
