package com.mybclab.cheetah.kubernetes.exception;

import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import lombok.Getter;

@Getter
public class KubernetesException extends RuntimeException {
    private Kubernetes.KIND kind;

    public KubernetesException(Kubernetes.KIND kind, String message) {

        super(message);
        this.kind = kind;
    }


}
