package com.mybclab.cheetah;

import com.mybclab.common.repository.CheetahRepositoryImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EntityScan(
        basePackageClasses = {CheetahApplication.class, Jsr310JpaConverters.class}
)
@ComponentScan({"com.mybclab.cheetah", "com.mybclab.common"})
@EnableJpaRepositories(repositoryBaseClass = CheetahRepositoryImpl.class)
@SpringBootApplication
@EnableJpaAuditing
@EnableTransactionManagement
@Slf4j
public class CheetahApplication {


    public static void main(String[] args) {
        SpringApplication.run(CheetahApplication.class, args);
    }

}

