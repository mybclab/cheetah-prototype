package com.mybclab.cheetah.code.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "code_group")
@Getter
@Setter
@ToString(exclude = "codes")
public class CodeGroup implements Serializable {
    @Id
    private String codeGroupId;

    private String codeGroupName;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "codeGroupId", referencedColumnName = "codeGroupId", insertable = false, updatable = false)
    @OrderBy(value = "ODR ASC")
    private List<Code> codes = new ArrayList<>();

    public CodeGroup() {
    }
}
