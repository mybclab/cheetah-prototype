package com.mybclab.cheetah.code.repository;

import com.mybclab.cheetah.code.domain.Code;
import com.mybclab.cheetah.code.domain.CodePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CodeRepository extends JpaRepository<Code, CodePK> {

    /**
     * 코드 그룹 Id, 활성화여부로 코드 목록 조회
     *
     * @param codeGroupId 코드 그룹 ID
     * @return 코드 목록
     */
    List<Code> findAllByCodePK_CodeGroupIdAndActiveYnOrderByOdrAsc(String codeGroupId, boolean activeYn);

    /**
     * 코드 그룹 Id로 코드 목록 조회
     *
     * @param codeGroupId 코드 그룹 ID
     * @return 코드 목록
     */
    List<Code> findAllByCodePKCodeGroupId(String codeGroupId);

    /**
     * 코드 그룹 ID, 코드 명으로 Code 조회
     *
     * @param codeGroupId 코드 그룹 ID
     * @param codeName    코드 명
     * @return 코드
     */
    Code findByCodePKCodeGroupIdAndCodeName(String codeGroupId, String codeName);
}
