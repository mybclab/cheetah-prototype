package com.mybclab.cheetah.code.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "code")
@Getter
@Setter
@ToString(exclude = "codeGroup")
public class Code implements Serializable {


    @EmbeddedId
    private CodePK codePK;

    private String codeName;

    private String codeDescription;

    private Integer odr;

    private boolean activeYn;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codeGroupId", referencedColumnName = "codeGroupId", insertable = false, updatable = false)
    private CodeGroup codeGroup;

    public Code() {
    }
}
