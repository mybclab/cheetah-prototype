package com.mybclab.cheetah.code.service;

import com.mybclab.cheetah.code.domain.Code;
import com.mybclab.cheetah.code.domain.CodePK;
import com.mybclab.cheetah.code.repository.CodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import java.util.List;

/**
 * CodeService
 */
@Service
public class CodeService {

    /**
     * {@link CodeRepository}
     */
    private final CodeRepository codeRepository;

    /**
     * Constructor
     * @param codeRepository {@link CodeRepository}
     */
    @Autowired
    public CodeService(CodeRepository codeRepository) {
        this.codeRepository = codeRepository;
    }


    /**
     * 코드 그룹 ID로 목록조회
     *
     * @param groupId 코드 그룹 ID
     * @return 코드 목록
     */
    public List<Code> findAllByGroupId(String groupId) {
        return codeRepository.findAllByCodePKCodeGroupId(groupId);
    }

    /**
     * code 조회
     * @param codePK {@link CodePK}
     * @return Code {@link Code}
     */
    public Code findById(CodePK codePK) {
        return codeRepository.findById(codePK).orElseThrow(EntityExistsException::new);
    }

    /**
     * 코드 수정
     *
     * @param code {@link Code}
     */
    @Transactional
    @CacheEvict(cacheNames = "codeGroup", key = "#code.codePk.codeGroupId")
    public void modify(Code code) {
        codeRepository.saveAndFlush(code);
    }

    /**
     * code의 활성황 여부 변경
     * @param codeGroupId codeGroupId
     * @param codeId codeId
     */
    @Transactional
    @CacheEvict(cacheNames = "codeGroup", key = "#code.codePk.codeGroupId")
    public void modifyActiveYn(String codeGroupId, String codeId) {

        Code code = this.findById(new CodePK(codeGroupId, codeId));
        code.setActiveYn(!code.isActiveYn());
        this.modify(code);
    }
}
