package com.mybclab.cheetah.code.service;

import com.mybclab.cheetah.code.domain.Code;
import com.mybclab.cheetah.code.domain.CodeGroup;
import com.mybclab.cheetah.code.repository.CodeGroupRepository;
import com.mybclab.cheetah.code.repository.CodeRepository;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.List;

@Service
public class CodeGroupService {

    /**
     * {@link CodeGroupRepository}
     */
    private final CodeGroupRepository codeGroupRepository;

    /**
     * {@link CodeRepository}
     */
    private final CodeRepository codeRepository;

    /**
     * constructor
     *
     * @param codeGroupRepository {@link CodeGroupRepository}
     * @param codeRepository      {@link CodeRepository}
     */
    @Autowired
    public CodeGroupService(CodeGroupRepository codeGroupRepository, CodeRepository codeRepository) {
        this.codeGroupRepository = codeGroupRepository;
        this.codeRepository = codeRepository;
    }

//    /**
//     * 코드 그룹 조회
//     *
//     * @param codeGroupId 코드 그룹
//     * @return 코드 그룹
//     */
//    public CodeGroup getCodeGroup(CodeGroupId codeGroupId) {
//        return codeGroupRepository.findById(codeGroupId.getCodeGroupId()).orElseThrow(EntityNotFoundException::new);
//    }

    /**
     * (cache) active 코드 목록조회
     *
     * @param codeGroupID 코드 그룹
     * @return 코드 목록
     */
    @Cacheable(cacheNames = "codeGroup", key = "#codeGroupID.codeGroupID")
    public List<Code> getCodeList(final CodeGroupID codeGroupID) {
        return codeRepository.findAllByCodePK_CodeGroupIdAndActiveYnOrderByOdrAsc(codeGroupID.getCodeGroupID(), true);
    }

    /**
     * 코드 그룹 목록조회
     *
     * @return 코드 그룹 목록
     */
    public List<CodeGroup> findAll() {
        return codeGroupRepository.findAll();
    }

    /**
     * 코드그룹ID로 코드그룹을 조회
     *
     * @param codeGroupId 코드 그룹 ID
     * @return {@link CodeGroup}
     */
    public CodeGroup findById(String codeGroupId) {
        return codeGroupRepository.findById(codeGroupId).orElseThrow(EntityExistsException::new);
    }

    /**
     * 캐쉬 codeGroup 전체 삭제
     */
    @CacheEvict(cacheNames = "codeGroup", allEntries = true)
    public void cacheReloadAll() {
    }

    /**
     * 캐쉬 CodeGroup wnd codeGroupId만 삭제
     *
     * @param codeGroupId 코드 그룹 ID
     */
    @CacheEvict(cacheNames = "codeGroup", key = "#codeGroupId")
    public void cacheReloadByCodeGroupId(String codeGroupId) {
    }
}
