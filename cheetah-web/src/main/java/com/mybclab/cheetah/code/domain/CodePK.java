package com.mybclab.cheetah.code.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Embeddable
public class CodePK implements Serializable {

    /**
     * 코드
     */
    private String code;

    /**
     * 코드 그룹 ID
     */
    private String codeGroupId;

    /**
     * default constructor
     */
    public CodePK() {
    }

    /**
     * constructor
     *
     * @param codeGroupId 코드 그룹 ID
     * @param code        코드
     */
    public CodePK(String codeGroupId, String code) {
        this.codeGroupId = codeGroupId;
        this.code = code;
    }

}
