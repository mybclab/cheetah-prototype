package com.mybclab.cheetah.config.security;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.admin.user.service.UserDetailsServiceImpl;
import com.mybclab.cheetah.common.constant.Code;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Enumeration;


@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private HttpServletRequest request;

    /**
     * {@link UserDetailsServiceImpl}
     */
    private final UserDetailsServiceImpl userDetailService;


    /**
     * {@link BCryptPasswordEncoder}
     */
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public CustomAuthenticationProvider(UserDetailsServiceImpl userDetailService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailService = userDetailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.debug("인증 시작");

        User user;
        LoginHistory userLoginHistory = new LoginHistory();
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
//        String otpCode = ((CustomWebAuthenticationDetails )authentication.getDetails()).getOtpCode();

        username = username.toLowerCase();
        log.debug("username is '{}'", username);


        try {
            user = (User) userDetailService.loadUserByUsername(username);
            userLoginHistory.setUsername(username);
            userLoginHistory.setLoginDate(LocalDateTime.now());
            userLoginHistory.setLoginIp(getRemoteAddr(request));

            log.debug("loadUserByUsername {}", user);

            // 패스워드 매치 체크
            if (!bCryptPasswordEncoder.matches(password, user.getPassword())) {
                userLoginHistory.setLoginHistoryCode(Code.LOGIN_HISTORY.FAIL_WRONG_PASSWORD);
                addLoginHistory(userLoginHistory);
                throw new BadCredentialsException("AuthenticationProvider.invalidPassword");
            }

            log.debug("password check ok");

            if (user.getWithdrawalDate() != null) {
                userLoginHistory.setLoginHistoryCode(Code.LOGIN_HISTORY.FAIL_WITHDRAWAL);
                addLoginHistory(userLoginHistory);
                throw new BadCredentialsException("AuthenticationProvider.userWithdrawal");
            }

            if (!user.getConfirmYn()) {
                userLoginHistory.setLoginHistoryCode(Code.LOGIN_HISTORY.FAIL_NOT_CONFIRMED);
                addLoginHistory(userLoginHistory);
                throw new BadCredentialsException("AuthenticationProvider.userNotConfirm");
            }

            userLoginHistory.setLoginHistoryCode(Code.LOGIN_HISTORY.SUCCESS);
//            // 2FA 체크
//            Totp totp = new Totp(user.getOtpSecretKey());
//            if (!isValidLong(otpCode) || !totp.verify(otpCode)) {
//                throw new BadCredentialsException("AuthenticationProvider.invalidOtpCode");
//            }

//            log.debug("otp check ok");


        } catch (UsernameNotFoundException unfe) {
            throw new BadCredentialsException("AuthenticationProvider.userNotFound", unfe);
        }

        addLoginHistory(userLoginHistory);

        log.debug("인증 종료 {}", user.getAuthorities());

        return new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());
    }

    private void addLoginHistory(LoginHistory userLoginHistory) {
        try {
            userDetailService.addLoginHistory(userLoginHistory);
        } catch (Exception e) {
            log.error("addLoginHistory (로그인 이력 쌓기) 실패");
        }
    }

    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.authentication.AuthenticationProvider#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private String getRemoteAddr(HttpServletRequest request) {
        String ip = null;
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            log.debug(" getRemoteAddr headerNames : " + headerName + "," + request.getHeader(headerName));
        };
//        2019-10-01 07:45:41.819 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : host,cheetah.n3ncloud.co.kr
//2019-10-01 07:45:41.820 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : connection,keep-alive
//2019-10-01 07:45:41.820 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : content-length,33
//2019-10-01 07:45:41.820 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : cache-control,max-age=0
//2019-10-01 07:45:41.821 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : origin,http://cheetah.n3ncloud.co.kr
//2019-10-01 07:45:41.821 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : upgrade-insecure-requests,1
//2019-10-01 07:45:41.821 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : content-type,application/x-www-form-urlencoded
//2019-10-01 07:45:41.822 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : user-agent,Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36
//2019-10-01 07:45:41.822 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : accept,text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
//        2019-10-01 07:45:41.822 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : referer,http://cheetah.n3ncloud.co.kr/login
//        2019-10-01 07:45:41.823 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : accept-encoding,gzip, deflate
//        2019-10-01 07:45:41.823 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : accept-language,ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7
//        2019-10-01 07:45:41.824 [DEBUG] - [080-exec-6] c.m.c.c.s.CustomAuthenticationProvider   [       getRemoteAddr:135] :  getRemoteAddr headerNames : cookie,_ga=GA1.3.2086910933.1569305945; _wp_uid=1-a19a7deea5c0e76075de288f72dc742a-s1569307406.4634|mac_osx|chrome-1pginni; dable_uid=10152183.1569307406499; _fbp=fb.2.1569307406678.392714975; _gid=GA1.3.1653553848.1569804464; JSESSIONID=95AA7E63E4579DB6F723736D9DDE7D5A

        ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
            log.info(">>>> Proxy-Client-IP : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
            log.info(">>>> WL-Proxy-Client-IP : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
            log.info(">>>> HTTP_CLIENT_IP : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            log.info(">>>> HTTP_X_FORWARDED_FOR : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
            log.info(">>>> X-Real-IP : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-RealIP");
            log.info(">>>> X-RealIP : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("REMOTE_ADDR");
            log.info(">>>> REMOTE_ADDR : IP Address : "+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            log.info(">>>> unknown : IP Address : "+ip);
            ip = request.getRemoteAddr();
        }

        log.info(">>>> Result : IP Address : "+ip);

        return ip;

    }
}
