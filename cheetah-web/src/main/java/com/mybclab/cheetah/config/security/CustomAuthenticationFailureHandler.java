package com.mybclab.cheetah.config.security;

//import LoginForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Slf4j
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    private boolean forwardToDestination = false;
    private boolean allowSessionCreation = true;

    public CustomAuthenticationFailureHandler() {
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.debug("Authentication failure exception is", exception);
        log.debug("request is {}", request);

//        LoginForm loginForm = new LoginForm();
//        loginForm.setUsername(request.getParameter("username"));
//
//        log.debug("loginForm is {}", loginForm);
//        saveLoginForm(request, loginForm);

        log.debug("request is {}", request);

        super.onAuthenticationFailure(request, response, exception);
    }

    /**
     * Caches the {@code LoginForm} for use in view rendering.
     */
//    private void saveLoginForm(HttpServletRequest request, LoginForm loginForm) {
//        if (forwardToDestination) {
//            request.setAttribute("loginForm", loginForm);
//        } else {
//            HttpSession session = request.getSession(false);
//
//            if (session != null || allowSessionCreation) {
//                request.getSession().setAttribute("loginForm", loginForm);
//            }
//        }
//    }

    protected boolean isUseForward() {
        return forwardToDestination;
    }

    /**
     * If set to <tt>true</tt>, performs a forward to the failure destination URL
     * instead of a redirect. Defaults to <tt>false</tt>.
     */
    public void setUseForward(boolean forwardToDestination) {
        this.forwardToDestination = forwardToDestination;
    }

    protected boolean isAllowSessionCreation() {
        return allowSessionCreation;
    }

    public void setAllowSessionCreation(boolean allowSessionCreation) {
        this.allowSessionCreation = allowSessionCreation;
    }
}

