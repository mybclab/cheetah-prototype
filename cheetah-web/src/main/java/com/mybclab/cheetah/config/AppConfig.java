package com.mybclab.cheetah.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@Slf4j
public class AppConfig {
    @Value("${spring.messages.basename}")
    String messagesBasename = null;

    @Value("${spring.messages.encoding}")
    String messagesEncoding = null;

    /**
     * Messagesource config
     * @return messageSource
     */
    @Bean
    public MessageSource messageSource(){
        log.debug("messagesBasename {}", messagesBasename);
        log.debug("messagesEncoding {}", messagesEncoding);

        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(messagesBasename.split(","));
        messageSource.setDefaultEncoding(messagesEncoding);

        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }
}

