package com.mybclab.cheetah.config;

import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
@Configuration
public class PropertiesConfig {
    private static String confHome = System.getProperty("conf.home");

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws IOException {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        if (confHome != null) {
            boolean exists = Files.exists(Paths.get(confHome));
            log.debug(">>>>>>>>>> from external config yml : confHome : {}, exists {}", confHome, exists); // /Users/want813/development/workspace/ext-conf
            if (StringUtils.isNotEmpty(confHome) && exists) {
                FileSystemResource[] list = Files.list(Paths.get(confHome))
                        .filter(path -> {
                            File file = path.toFile();
                            return file.exists() && file.isFile()
                                    && file.getName().endsWith(".yml"); // file이 yml  확장자인지 검사
                        })
                        .map(path -> new FileSystemResource(path.toFile()))
                        .toArray(FileSystemResource[]::new);
                YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
                yaml.setResources(list);
                log.debug(">>>>>>>>>> from external config yml : {}", yaml.getObject());
                propertySourcesPlaceholderConfigurer.setLocalOverride(true);
                propertySourcesPlaceholderConfigurer.setProperties(yaml.getObject());
            }
        }
        return propertySourcesPlaceholderConfigurer;
    }
}