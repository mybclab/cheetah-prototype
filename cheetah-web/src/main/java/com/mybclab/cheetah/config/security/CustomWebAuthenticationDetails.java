package com.mybclab.cheetah.config.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {
    private String otpCode;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        otpCode = request.getParameter("otpCode");
    }

    public String getOtpCode() {
        return otpCode;
    }
}
