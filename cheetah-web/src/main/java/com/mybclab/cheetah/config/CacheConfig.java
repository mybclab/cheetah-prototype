package com.mybclab.cheetah.config;


import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.interceptor.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

@org.springframework.context.annotation.Configuration
@EnableCaching
@Lazy
public class CacheConfig implements CachingConfigurer {
    final static String CACHE_POLICY = "LRU";

    private CacheManager ehCacheManager() {
        Configuration config = new Configuration();
        config.addCache(cacheConfig("monitoring", 1000));
        config.addCache(cacheConfig("codeGroup", 1000));
        config.addCache(cacheConfig("gpu", 1000));
        config.addCache(cacheConfig("node", 1000));
        config.addCache(cacheConfig("podFormat", 1000));
        config.addCache(cacheConfig("podOptionGroup", 1000));
        config.addCache(cacheConfig("podOption", 1000));
        config.addCache(cacheConfig("yaml", 1000));
        config.addCache(cacheConfig("credit", 1000));

        return CacheManager.newInstance(config);
    }

    private CacheConfiguration cacheConfig(final String name, final long maxEntries) {
        return this.cacheConfig(name, maxEntries, 0);
    }

    private CacheConfiguration cacheConfig(final String name, final long maxEntries, final long timeToLive) {
        CacheConfiguration config = new CacheConfiguration();
        config.setName(name);
        config.setMaxEntriesLocalHeap(maxEntries);
        config.setMemoryStoreEvictionPolicy(CACHE_POLICY);
        config.timeToLiveSeconds(timeToLive);
        return config;
    }

    @Bean
    @Override
    public org.springframework.cache.CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheManager());
    }

    @Bean
    @Override
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }

    @Bean
    @Override
    public CacheResolver cacheResolver() {
        return new SimpleCacheResolver(cacheManager());
    }

    @Bean
    @Override
    public CacheErrorHandler errorHandler() {
        return new SimpleCacheErrorHandler();
    }
}
