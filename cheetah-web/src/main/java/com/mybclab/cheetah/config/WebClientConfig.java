package com.mybclab.cheetah.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Configuration
public class WebClientConfig {


    @Bean
    public WebClient configureWebClient() {
        return WebClient.builder()
//                .filter(printlnFilter)
                .build();
    }

    private ExchangeFilterFunction printlnFilter= (request, next) -> {
        log.debug("############# WebClient printlnFilter : method : " + request.method().toString().toUpperCase() + ", URL:"
                + request.url().toString() + ", Headers:" + request.headers().toString() + ", Attributes:"
                + request.attributes() + ", body:" + request.body());

        return next.exchange(request);
    };

}
