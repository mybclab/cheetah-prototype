package com.mybclab.cheetah.config;

import com.mybclab.cheetah.admin.user.service.UserDetailsServiceImpl;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.config.security.CustomAccessDeniedHandler;
import com.mybclab.cheetah.config.security.CustomAuthenticationFailureHandler;
import com.mybclab.cheetah.config.security.CustomAuthenticationProvider;
import com.mybclab.cheetah.config.security.CustomAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.session.HttpSessionEventPublisher;

/**
 * Created by hkwon on 2018. 6. 7..
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final String SIGN_IN = "/login";
    /**
     * {@link UserDetailsServiceImpl}
     */
    private UserDetailsServiceImpl userDetailService;

    @Autowired
    public SecurityConfig(UserDetailsServiceImpl userDetailService) {
        this.userDetailService = userDetailService;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
                .ignoringAntMatchers("/**")
                .and()
                .authorizeRequests()
                .antMatchers("/login", "/register/**", "/terms", "/error/*", "/help/**", "/forgot-password/**", "/kube/post-start/**", "/kube/pre-stop/**").permitAll()
                .antMatchers("/iamport/**", "/kube/node/**").permitAll()
                .antMatchers("/admin/**", "/kube/**/info").hasAnyAuthority(Code.ROLE.SYSTEM_ADMIN.name())
                .antMatchers("/management/group/**").hasAnyAuthority(Code.ROLE.GROUP_ADMIN.name())
                .anyRequest().authenticated()
                .and()
                .formLogin()
//                .authenticationDetailsSource(authenticationDetailsSource)
                .loginPage(SIGN_IN)
                .successHandler(successHandler())
                .failureHandler(failureHandler())
                .and()
                .logout()
                .logoutUrl("/logout")
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .logoutSuccessUrl(SIGN_IN)
                //.deleteCookies("JSESSIONID,SPRING_SECURITY_REMEMBER_ME_COOKIE")
                .and()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler());
        http.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
        web.ignoring()
                .antMatchers("/assets/**")
                .antMatchers("/favicon.ico")
                .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**", "/swagger-ui.html", "/webjars/**");

        web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new CustomAuthenticationProvider(userDetailService, bCryptPasswordEncoder());
    }

    @Bean
    public AuthenticationFailureHandler failureHandler() {
        CustomAuthenticationFailureHandler authenticationFailureHandler = new CustomAuthenticationFailureHandler();
        authenticationFailureHandler.setDefaultFailureUrl(SIGN_IN + "?error=true");
        authenticationFailureHandler.setUseForward(true);

        return authenticationFailureHandler;
    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        CustomAuthenticationSuccessHandler successHandler = new CustomAuthenticationSuccessHandler();
        successHandler.setDefaultTargetUrl("/dashboard");
        successHandler.setAlwaysUseDefaultTargetUrl(true);

        return successHandler;
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }
}
