package com.mybclab.cheetah.config.component;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class CheetahMessageSource {
    private final MessageSourceAccessor accessor;

    public CheetahMessageSource(MessageSource messageSource) {
        this.accessor = new MessageSourceAccessor(messageSource, LocaleContextHolder.getLocale());
    }

    /**
     * 기본 Locale 설정으로 메세지를 반환한다.
     *
     * @param code  message code
     * @return message
     */
    public String getMessage(String code) {
        return accessor.getMessage(code);
    }

    /**
     * 기본 Locale 설정으로 메세지를 반환한다.
     *
     * @param code  message code
     * @param args  arguments list
     * @return message
     */
    public String getMessage(String code, Object[] args) {
        return accessor.getMessage(code, args);
    }
}