package com.mybclab.cheetah.config;

import com.mybclab.cheetah.admin.user.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@Configuration
//@EnableJpaAuditing
public class JpaConfig {
    @Bean
    public AuditorAware<String> auditorAware() {
        return () -> {
            if (SecurityContextHolder.getContext().getAuthentication() != null) {
                Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                if (object instanceof UserDetails) {
                    return Optional.of(((User) object).getUsername());
                }
            }

            return Optional.empty();
        };
    }
}
