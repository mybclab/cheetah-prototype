package com.mybclab.cheetah.admin.pod.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@Setter
@ToString
public class PodFormatForm {

    /**
     * 운영체제 이미지 일련번호
     */
    private Long podFormatSn;
    /**
     * 운영체제 이미지 명
     */
    @NotBlank
    private String podFormatName;
    /**
     *
     * 이미지
     */
    @NotBlank
    private String image;


    /**
     * 명령어
     */
    @NotBlank
    private String command;
    /**
     * 용량
     */
    @NotBlank
    private String capacity;


    /**
     * 설명
     */
    private String podFormatDescription;
    /**
     * 사용 여부
     */
    @NotNull
    private Boolean useYn;


    private Long[] gpuSns;

    private Map<String, Long> podOptionSnMap;
}
