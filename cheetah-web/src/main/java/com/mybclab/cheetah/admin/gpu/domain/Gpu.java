package com.mybclab.cheetah.admin.gpu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.admin.pod.domain.PodFormatGpu;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.common.domain.HistoryBase;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * GPU
 */
@Entity
@Setter
@Getter
@ToString(exclude = {"containerList", "podFormatGpuList"})
public class Gpu extends HistoryBase implements Serializable {

    /**
     * GPU 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gpuSn;

    /**
     * 클라우드 구분
     */
    @Enumerated(EnumType.STRING)
    private Code.CLOUD_TYPE cloudType;
    /**
     * 자원 구분
     */
    @Enumerated(EnumType.STRING)
    private Code.RESOURCE_TYPE resourceType;

    /**
     * GPU 명
     */
    private String gpuName;

    /**
     * GPU 라벨
     */
    private String gpuLabel;

    /**
     * 제조사 명
     */
    private String manufacturerName;

    /**
     * 사양
     */
    private String spec;

    /**
     * 분단위 금액
     */
    @CurrencyFormat
    private Double minutelyAmount;
    /**
     * 시간단위 금액
     */
    @CurrencyFormat
    private Double hourlyAmount;
    /**
     * 일단위 금액
     */
    @CurrencyFormat
    private Double dailyAmount;
    /**
     * 월단위 금액
     */
    @CurrencyFormat
    private Double monthlyAmount;
    /**
     * 월단위 약정 금액
     */
    @CurrencyFormat
    private Double monthlyContractAmount;
    /**
     * 월단위 약정 금액 3
     */
    @Column(name = "MONTHLY_CONTRACT_AMOUNT_3")
    @CurrencyFormat
    private Double monthlyContractAmount3;
    /**
     * 월단위 약정 금액 6
     */
    @Column(name = "MONTHLY_CONTRACT_AMOUNT_6")
    @CurrencyFormat
    private Double monthlyContractAmount6;
    /**
     * 월단위 약정 금액 12
     */
    @Column(name = "MONTHLY_CONTRACT_AMOUNT_12")
    @CurrencyFormat
    private Double monthlyContractAmount12;
    /**
     * 월단위 약정 금액 24
     */
    @Column(name = "MONTHLY_CONTRACT_AMOUNT_24")
    @CurrencyFormat
    private Double monthlyContractAmount24;

    /**
     * 수량 구성
     */
    private String quantityFormat;
    /**
     * 사용 여부
     */
    private Boolean useYn;

    /**
     * 컨테이너 목록
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "gpuSn", referencedColumnName = "gpuSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<Container> containerList = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "gpuSn", referencedColumnName = "gpuSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<PodFormatGpu> podFormatGpuList = new ArrayList<>();




    @Transient
    private int useGpuQuantity;

    @Transient
    private int useContainerCount;

    @Transient
    private int totalGpuQuantity;
    @Transient
    private int totalNodeCount;
    @Transient
    private int remainMaxGpuQuantityPerNode;

    private int gpuMemory;

    private int cpuCore;

    private int systemMemory;

    private Boolean sharedYn;

    public int getRemainGpuQuantity() {
        return this.totalGpuQuantity - this.useGpuQuantity;
    }

    public Double getAmountByChargingMethod(Code.CHARGING_METHOD chargingMethod) {
        Double amount = 0D;
        switch (chargingMethod) {
            case MINUTELY:
                amount = getMinutelyAmount();
                break;
            case HOURLY:
                amount = getHourlyAmount();
                break;
            case DAILY:
                amount = getDailyAmount();
                break;
            case MONTHLY:
                amount = getMonthlyAmount();
                break;
            case MONTHLY_CONTRACT_1:
                amount = getMonthlyContractAmount();
                break;
            case MONTHLY_CONTRACT_3:
                amount = getMonthlyContractAmount3();
                break;
            case MONTHLY_CONTRACT_6:
                amount = getMonthlyContractAmount6();
                break;
            case MONTHLY_CONTRACT_12:
                amount = getMonthlyContractAmount12();
                break;
            case MONTHLY_CONTRACT_24:
                amount = getMonthlyContractAmount24();
                break;
        }
        return amount;
    }

    public int getMaxQuotaMemory() {
        if (Code.RESOURCE_TYPE.GPU.equals(getResourceType())) {
            return this.gpuMemory != 0 ? (this.gpuMemory - 1) / 2 : 0;
        } else if (Code.RESOURCE_TYPE.CPU.equals(getResourceType())) {
            return getMaxQuotaCore();
        }
        return 0;
    }

    public int getMaxQuotaCore() {
        return this.cpuCore != 0 ? (this.cpuCore - 2) / 2 : 0;
    }


}
