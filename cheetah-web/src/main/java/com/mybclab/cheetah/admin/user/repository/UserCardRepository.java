package com.mybclab.cheetah.admin.user.repository;

import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 사용자 카드 Repository
 */
@Repository
public interface UserCardRepository extends JpaRepository<UserCard, UserCardPK> {

    /**
     * 사용자아이디로 사용자카드 목록조회
     * @param username 사용자아이디
     * @return 사용자카드 목록
     */
    List<UserCard> findAllByUserCardPKUsername(String username);
    /**
     * 사용자아이디로 사용중이 카드 단건조회
     * @param username 사용자아이디
     * @param useYn 사용여부
     * @return {@link UserCard}
     */
    UserCard findByUserCardPKUsernameAndUseYn(String username, boolean useYn);


    @Query(" select coalesce(max(u.userCardPK.userCardNo), 0) " +
            "  from UserCard u" +
            " where u.userCardPK.username = :username")
    int findMaxUserCardNo(@Param("username") String username);

    boolean existsByCustomerUid(String customerUid);

    boolean existsByUserCardPKAndUseYn(UserCardPK userCardPK, boolean userYn);
}
