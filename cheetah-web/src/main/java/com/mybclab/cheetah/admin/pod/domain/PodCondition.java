package com.mybclab.cheetah.admin.pod.domain;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.ContainerPK;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PodCondition implements Serializable {
    /**
     * 파드 컨디션 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long podConditionSn;

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 컨테이너 아이디
     */
    private String containerId;

    private String podName;

    private LocalDateTime lastProbeTime;

    private LocalDateTime lastTransitionTime;

    private String message;

    private String reason;

    private Boolean status;



    @Enumerated(EnumType.STRING)
    private Code.POD_CONDITION_TYPE typeCode;

    private LocalDateTime createdAt;
    public PodCondition() {

    }

    public PodCondition(ContainerPK containerPK) {
        this.containerId = containerPK.getContainerId();
        this.username = containerPK.getUsername();
    }


}
