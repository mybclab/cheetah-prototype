package com.mybclab.cheetah.admin.pod.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class PodFormatGpuPK implements Serializable {

    private Long podFormatSn;
    private Long gpuSn;

}
