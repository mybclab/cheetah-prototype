package com.mybclab.cheetah.admin.pod.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 파드 구성
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"containerList", "podFormatOptionList", "podFormatGpuList"})
public class PodFormat extends HistoryBase implements Serializable {

    /**
     * 파드 구성 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long podFormatSn;

    /**
     * 파드 구성 명
     */
    private String podFormatName;

    private String podOptionHint;

    /**
     * 파드 구성 설명
     */
    private String podFormatDescription;

    /**
     * 이미지
     */
    private String image;

    /**
     * 명령어
     */
    private String command;

    /**
     * 용량
     */
    private String capacity;



    /**
     * 사용 여부
     */
    private Boolean useYn;
    /**
     * 컨테이너 목록
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "podFormatSn", referencedColumnName = "podFormatSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<Container> containerList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "podFormatSn", referencedColumnName = "podFormatSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<PodFormatOption> podFormatOptionList = new ArrayList<>();


    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "podFormatSn", referencedColumnName = "podFormatSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<PodFormatGpu> podFormatGpuList = new ArrayList<>();



    @JsonIgnore
    @Transient
    public Map<String, PodOption> getPodOptionMap() {
        return getPodFormatOptionList().stream().collect(Collectors.toMap(PodFormatOption::getPodOptionGroupId, PodFormatOption::getPodOption));
    }


    @JsonIgnore
    @Transient
    public Map<String, Long> getPodOptionSnMap() {
        return getPodFormatOptionList().stream().collect(Collectors.toMap(PodFormatOption::getPodOptionGroupId, o -> o.getPodFormatOptionPK().getPodOptionSn()));
    }

    @JsonIgnore
    @Transient
    public String getGpuNames() {
        if (getPodFormatGpuList() != null && getPodFormatGpuList().size() > 0) {
            List<String> gpuNameList = getPodFormatGpuList().stream().map(g -> g.getGpu().getGpuName()).collect(Collectors.toList());
            String gpuNames = "";
            for (int i = 0; i <gpuNameList.size(); i++) {
                if (i != 0) { gpuNames += ", "; }
                gpuNames += gpuNameList.get(i);
            }
            return gpuNames;
        }
        return "";
    }

}
