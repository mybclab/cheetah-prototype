package com.mybclab.cheetah.admin.user.service;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.UserCriteria;
import com.mybclab.cheetah.admin.user.repository.UserCardRepository;
import com.mybclab.cheetah.admin.user.repository.UserRepository;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.exception.StorageException;
import com.mybclab.cheetah.common.file.domain.DbFile;
import com.mybclab.cheetah.common.file.service.FileService;
import com.mybclab.cheetah.common.service.MailService;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.cheetah.management.group.dto.GroupInviteForm;
import com.mybclab.common.notifier.SlackNotifier;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.mybclab.cheetah.admin.user.specification.UserSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

/**
 * 사용자 Service
 */
@Service
@Slf4j
public class UserService implements Serializable {

    /**
     * {@link UserRepository}
     */
    private UserRepository userRepository;

    /**
     * {@link UserCreditService}
     */
    private UserCreditService userCreditService;

    /**
     * {@link CreditService}
     */
    private CreditService creditService;

    /**
     * {@link MailService}
     */
    private MailService mailService;

    /**
     * {@link BCryptPasswordEncoder}
     */
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    /**
     * {@link CheetahMessageSource}
     */
    private CheetahMessageSource messageSource;

    @Autowired
    private HttpServletRequest request;
    private SlackNotifier slackNotifier;

    private FileService fileService;

    /**
     * default constuctor
     */
    public UserService() {
    }

    /**
     * constructor
     * @param userRepository {@link UserRepository}
     * @param userCreditService {@link UserCreditService}
     * @param creditService {@link CreditService}
     * @param mailService {@link MailService}
     * @param messageSource {@link CheetahMessageSource}
     */
    @Autowired
    public UserService(UserRepository userRepository, UserCreditService userCreditService, CreditService creditService, MailService mailService, FileService fileService, BCryptPasswordEncoder bCryptPasswordEncoder, CheetahMessageSource messageSource, SlackNotifier slackNotifier) {
        this.userRepository = userRepository;
        this.userCreditService = userCreditService;
        this.creditService = creditService;
        this.mailService = mailService;
        this.fileService = fileService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.messageSource = messageSource;
        this.slackNotifier = slackNotifier;
    }


    /**
     * 저장
     * @param user {@link User}
     * @return {@link User}
     */
    @Transactional
    public User save(User user) {
        user = userRepository.save(user);
        userRepository.flush();
        return user;
    }

    @Transactional
    public User register(User user) {
        this.save(user);
        this.sendUserConfirmMail(user);
        this.sendSlackHook(user);
        return user;
    }

    // TODO user 먼저 save 진행... 고쳐야 하는데... org.springframework.dao.DataIntegrityViolationException: A different object with the same identifier value was already associated with the session 문제로...
    @Transactional
    public User saveWithFile(User user, MultipartFile groupImageFile, MultipartFile userImageFile) throws StorageException {
        user = this.save(user);
        boolean updateImage = false;
        if (groupImageFile != null && !groupImageFile.isEmpty()) {
            DbFile dbFile = fileService.uploadFile(groupImageFile, user.getUpdatedBy(), new String[]{"png", "jpg", "jpeg", "gif"});
            user.setGroupImageFileSn(dbFile.getFileSn());
            updateImage = true;
        }
        if (userImageFile != null && !userImageFile.isEmpty()) {
            DbFile dbFile = fileService.uploadFile(userImageFile, user.getUpdatedBy(), new String[]{"png", "jpg", "jpeg", "gif"});
            user.setUserImageFileSn(dbFile.getFileSn());
            updateImage = true;
        }
        if (updateImage) {
            this.save(user);
        }
        return user;
    }

    @Transactional
    public User confirm(User user) {
        // 웰컵 크레딧 부여
        List<Credit> creditList = new ArrayList<>();
        if (user.isGroupAdmin()) {
            creditList = creditService.findAllByCreditTypeCode(Code.CREDIT_TYPE.WELCOME_GROUP_ADMIN);
        } else if (user.isGeneralUser()) {
            creditList = creditService.findAllByCreditTypeCode(Code.CREDIT_TYPE.WELCOME_GENERAL_USER);
        }
        if (creditList != null && creditList.size() > 0) {
            creditList.forEach(c -> userCreditService.addCreditToUser(user.getUsername(), c.getCreditId()));
        }

        return this.save(user);
    }

    private void sendSlackHook(User user) {
        String title = "회원가입";
        String roleName = "일반사용자";
        if (user.isGroupAdmin()) {
            roleName = "그룹관리자";
        } else if (user.isGroupUser()) {
            roleName = "그룹사용자";
        } else if (user.isSystemAdmin()) {
            roleName = "시스템관리자";
        }

        String message = String.format("%s(%s) 님이 %s로 회원가입하였습니다.", user.getName(), user.getUsername(), roleName);
        if (user.isGroupUser()) {
            User group = this.findById(user.getParentUsername());
            log.debug(">>>>>>> group : {}", group);
            message = String.format("%s(%s) 님이 %s(%s)에 %s로 회원가입하였습니다.", user.getName(), user.getUsername(), group.getGroupName(), group.getUsername(), roleName);
        } else if (user.isGroupAdmin()) {
            message = String.format("%s(%s) 님이 %s(%s)로 회원가입하였습니다.", user.getName(), user.getUsername(), roleName, user.getGroupName());
        }

        slackNotifier.notify(SlackNotifier.SlackTarget.REGISTER_USER, SlackNotifier.SlackMessageAttachement.builder()
                .title(title)
                .text(message)
                .build(), this.findSlackTargetList(user));

    }

    private void sendUserConfirmMail(User user) {
        Context context = new Context();
        context.setVariable("name", user.getName());
        context.setVariable("username", user.getUsername());
        if (user.isGroupUser()) {
            User group = this.findById(user.getParentUsername());
            context.setVariable("confirmUrl", request.getServerName() + ":" + request.getServerPort() + "/management/user/" + user.getUsername());
            context.setVariable("role", "그룹사용자");
            mailService.sendTemplateMessage(group.getEmail(), messageSource.getMessage("mail.confirmUser.title"), Code.MAIL_TEMPLATE.CONFIRM_USER, context);
        } else if (user.isGroupAdmin() || user.isGeneralUser()) {
            List<User> userList = this.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN);
            context.setVariable("confirmUrl", request.getServerName() + ":" + request.getServerPort() + "/admin/user/" + user.getUsername());
            context.setVariable("role", user.isGroupAdmin() ? "그룹관리자" : "일반사용자");
            for (User admin : userList) {
                mailService.sendTemplateMessage(admin.getEmail(), messageSource.getMessage("mail.confirmUser.title"), Code.MAIL_TEMPLATE.CONFIRM_USER, context);
            }
        }
    }

    /**
     * 사용자아이디로 단건 조회
     * @param username 사용자아이디
     * @return {@link User}
     */
    public User findById(String username) {
        return userRepository.findById(username).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * 사용자 목록 조회(페이징)
     * @param userCriteria {@link UserCriteria}
     * @param pageable {@link Pageable}
     * @return 사용자 목록
     */
    public Page<User> findAll(UserCriteria userCriteria, Pageable pageable) {
        return userRepository.findAll(getUserSpecification(userCriteria), pageable);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
    public List<User> findAll(UserCriteria userCriteria, Sort sort) {
        return userRepository.findAll(getUserSpecification(userCriteria), sort);
    }

    /**
     * 그룹 사용자 목록조회
     * @param groupId 그룹 아이디(상위 사용자아이디)
     * @return 그룹 사용자 목록
     */
    public List<User> findAllByGroupId(String groupId) {
        return userRepository.findAllByParentUsernameOrUsername(groupId, groupId);
    }

    /**
     * getUserSpecification
     * @param userCriteria {@link UserCriteria}
     * @return {@link Specification<User>}
     */
    private Specification<User> getUserSpecification(UserCriteria userCriteria) {
        return where(likeName(userCriteria.getName()))
                    .and(equalsParentUsername(userCriteria.getParentUsername()))
                    .and(isGroup(userCriteria.getIsGroup()))
                    .and(equalsWithdrawalYn(userCriteria.getWithdrawalYn()))
                    .and(equalsConfirmYn(userCriteria.getConfirmYn()));
    }


    public List<User> findAllByUserRole(Code.ROLE role) {
        return userRepository.findAllByUserRole(role.toString());
    }

    public List<User> findAllForPayment() {
        List<User> userList = this.findAllByUserRole(Code.ROLE.GROUP_ADMIN);
        userList.addAll(this.findAllByUserRole(Code.ROLE.GENERAL_USER));
        return userList;
    }

    /**
     * 그룹명 중복 확인
     * @param groupName 그룹 명
     * @return 그룹명 중복 여부
     */
    public boolean existsByGroupName(String groupName) {
        return userRepository.existsByGroupName(groupName);
    }


    @Transactional
    public void resetPassword(String username, String email) {
        User user = userRepository.findByUsernameAndEmail(username, email).orElseThrow(EntityNotFoundException::new);

        String newPassword = StringUtils.randomAlphanumeric(8);
        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        this.save(user);
        Context context = new Context();
        context.setVariable("name", user.getName());
        context.setVariable("username", user.getUsername());
        context.setVariable("newPassword", newPassword);
        mailService.sendTemplateMessage(user.getEmail(), messageSource.getMessage("mail.resetpassword.title"), Code.MAIL_TEMPLATE.PASSWORD_RECREATION, context);
    }

    public String generateGroupInviteKey() {
        while (true) {
            String randomKey = StringUtils.randomAlphanumeric(20);
            if (!userRepository.existsByGroupInviteKey(randomKey)) {
                return randomKey;
            }
        }
    }

    public User findByGroupInviteKey(String groupInviteKey) {
        return userRepository.findByGroupInviteKey(groupInviteKey).orElseThrow(EntityNotFoundException::new);
    }

    public void sendGroupInviteMail(User group, GroupInviteForm groupInviteForm) {

        Context context = new Context();
        context.setVariable("username", group.getUsername());
        context.setVariable("name", group.getName());
        context.setVariable("groupName", group.getGroupName());

        context.setVariable("message", groupInviteForm.getMessage());
        context.setVariable("groupInviteKey", group.getGroupInviteKey());

        context.setVariable("groupInviteUrl", request.getServerName() + ":" + request.getServerPort() + "/register/group/" + group.getGroupInviteKey() );
        log.info(">>>>>> groupInviteKey : " + group.getGroupInviteKey());
        String[] emails = groupInviteForm.getEmails();

        if (emails != null && emails.length > 0) {
            for (String email : emails) {
                mailService.sendTemplateMessage(email, messageSource.getMessage("mail.inveteGroup.title"), Code.MAIL_TEMPLATE.INVITE_GROUP, context);
            }
        }
    }

    public List<User> findAllByUsernameIn(String[] usernames) {
        return userRepository.findAllByUsernameIn(usernames);
    }

    public List<SlackNotifier.SlackHook> findSlackTargetList(User user) {
        List<SlackNotifier.SlackHook> slackList = new ArrayList<>();
        if (user.isGroupUser()) {
            User group = user.getGroup();
            if (group == null) {
                group = this.findById(user.getParentUsername());
            }
            slackList.add(new SlackNotifier.SlackHook(group.getSlackHookUrl(), group.getSlackChannel()));
        }
        List<User> userList = this.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN);
        for (User admin : userList) {
            slackList.add(new SlackNotifier.SlackHook(admin.getSlackHookUrl(), admin.getSlackChannel()));
        }
        return slackList;
    }

    @Transactional
    public void delete(User user) {
        userCreditService.deleteByUser(user);
        userRepository.delete(user);
    }

    @Transactional
    public void withdrawal(User user) {
        if (user.isGroupAdmin()) {
            List<User> groupUsers = user.getGroupUsersIncludeAdmin();
            groupUsers.forEach(u -> u.setWithdrawalDate(LocalDateTime.now()));
            userRepository.saveAll(groupUsers);
        } else {
            user.setWithdrawalDate(LocalDateTime.now());
            this.save(user);
        }
    }
}
