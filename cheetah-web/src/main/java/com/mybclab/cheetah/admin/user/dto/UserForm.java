package com.mybclab.cheetah.admin.user.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.RegEx;
import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 사용자 Form
 */
@Getter
@Setter
@ToString
public class UserForm implements Serializable {

    /**
     * 사용자아이디
     */
    @Pattern(regexp = "^[a-z0-9]{4,12}$", message = "Pattern.username")
    @NotBlank(groups = {ValidationSignUp.class})
    @Size(min = 4, max = 100, groups = {ValidationSignUp.class})
    private String username;

    /**
     * 상위 사용자아이디
     */
    @NotBlank(groups = {ValidationSignUpForGroupUser.class})
    private String parentUsername;

    /**
     * 그룹 명
     */
    @NotBlank(groups = {ValidationSignUpForGroupAdmin.class})
    private String groupName;

    private String origGroupName;

    /**
     * 그룹 구분 코드
     * {@link Code.GROUP_TYPE}
     */
    @NotNull(groups = {ValidationGroup.class, ValidationSignUpForGroupAdmin.class})
    private Code.GROUP_TYPE groupTypeCode;




    @NotBlank(groups = {ValidationSignUpForGroupInvite.class})
    private String groupInviteKey;

    /**
     * 사용자 권한 코드
     * {@link Code.ROLE}
     */
    private Code.ROLE userRoleCode;

    /**
     * 기존 비밀번호
     */
    @NotBlank(groups = {ValidationManagement.class, ValidationOldPassword.class})
    @Size(min = 5, max = 20, groups = {ValidationManagement.class, ValidationOldPassword.class})
    private String oldPassword;

    /**
     * 새 비밀번호
     */
    @NotBlank(groups = {ValidationPassword.class})
    @Size(min = 5, max = 20, groups = {ValidationPassword.class})
    private String password;

    /**
     * 새 비밀번호 확인
     */
    @NotBlank(groups = {ValidationPassword.class})
    @Size(min = 5, max = 20, groups = {ValidationPassword.class})
    private String confirmPassword;

    /**
     * 이름
     */
    @NotBlank(groups = {ValidationSignUp.class, ValidationManagement.class, ValidationAdmin.class})
    @Size(max = 50, groups = {ValidationSignUp.class, ValidationManagement.class, ValidationAdmin.class})
    private String name;

    /**
     * 전화 번호
     */
    @NotBlank(groups = {ValidationSignUp.class, ValidationManagement.class, ValidationAdmin.class})
    private String phoneNo;

    /**
     * 이메일
     */
    @NotBlank(groups = {ValidationSignUp.class, ValidationManagement.class, ValidationAdmin.class})
    @Email(groups = {ValidationSignUp.class, ValidationManagement.class, ValidationAdmin.class})
    private String email;

    /**
     * 슬랙 훅 URL
     */
    private String slackHookUrl;

    /**
     * 슬랙 채널L
     */
    private String slackChannel;

    /**
     * 단체 명
     */
    private String organizationName;
    /**
     * 팀 명
     */
    private String teamName;


    private List<UserGpuForm> userGpuFormList = new ArrayList<>();


    /**
     * 카드 번호
     */
    @NotBlank(groups = {ValidationCard.class})
    @Size(max = 19, groups = {ValidationCard.class})
    private String cardNo;

    /**
     * CUSTOMER_UID
     */
    @NotBlank(groups = {ValidationCard.class}, message = "카드정보가 인증되지 않았습니다.")
    private String customerUid;

    /**
     * 카드 유효기간
     */
    @NotBlank(groups = {ValidationCard.class})
    @Size(max = 4, groups = {ValidationCard.class})
    private String cardExpiry;

    @NotBlank(groups = {ValidationCard.class})
    @Size(max = 6, groups = {ValidationCard.class})
    private String cardBirth;

    @NotBlank(groups = {ValidationCard.class})
    @Size(max = 2, groups = {ValidationCard.class})
    private String cardPassword;

    @AssertTrue(groups = {ValidationSignUp.class}, message = "Valid.agreeTerms")
    private Boolean agreeTerms;

    @AssertTrue(groups = {ValidationSignUp.class}, message = "Valid.agreePrivacy")
    private Boolean agreePrivacy;

    private Boolean confirmYn = false;

    private MultipartFile groupImageFile;

    private Long groupImageFileSn;

    private MultipartFile userImageFile;

    private Long userImageFileSn;


    /**
     * Password validation group
     */
    public interface ValidationOldPassword {
    }

    /**
     * Password validation group
     */
    public interface ValidationPassword {
    }

    /**
     * SignUp validation group
     */
    public interface ValidationSignUp {
    }

    public interface ValidationSignUpForGroupInvite {}

    public interface ValidationManagement {
    }
    public interface ValidationAdmin {
    }

    public interface ValidationGroup {}
    public interface ValidationGroupForGpuQuantity {}

    public interface ValidationCard {}

    public interface ValidationSignUpForGroupAdmin {}

    public interface ValidationSignUpForGroupUser {}

}
