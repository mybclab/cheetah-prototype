package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 로그인 이력
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"user"})
public class LoginHistory {

    /**
     * 로그인 이력 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long loginHistorySn;

    /**
     * 사용자아이디
     */
    private String username;


    /**
     * 로그인 아이피
     */
    private String loginIp;

    /**
     * 로그인 이력 코드
     */
    @Enumerated(EnumType.STRING)
    private Code.LOGIN_HISTORY loginHistoryCode;

    /**
     * 로그인 일시
     */
    @Column(nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime loginDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private User user;

}
