package com.mybclab.cheetah.admin.user.dto;

import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 사용자 크레딧 Dto
 */
@Getter
@Setter
@ToString
@Builder
public class UserCreditDto {

    /**
     * 사용자 크레딧 개수
     */
    private int userCreditCount;

    /**
     * 총 크레딧 금액
     */
    @CurrencyFormat
    private Double totalCreditAmount;

    /**
     * 잔여 크레딧 금액
     */
    @CurrencyFormat
    private Double remainCreditAmount;

}
