package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.PodFormatOption;
import com.mybclab.cheetah.admin.pod.domain.PodFormatOptionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PodFormatOptionRepository extends JpaRepository<PodFormatOption, PodFormatOptionPK> {

    List<PodFormatOption> findAllByPodFormatOptionPKPodFormatSn(long podFormatSn);
}
