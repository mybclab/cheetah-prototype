package com.mybclab.cheetah.admin.gpu.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * GPU Form
 */
@Getter
@Setter
@ToString
public class GpuForm implements Serializable {

    /**
     * GPU 일련번호
     */
    private Long gpuSn;
    /**
     * GPU 명
     */
    @NotBlank
    private String gpuName;

    /**
     * GPU 라벨
     */
    @NotBlank
    private String gpuLabel;
    /**
     * 제조사 명
     */
    private String manufacturerName;

    /**
     * 사양
     */
    private String spec;
    /**
     * 분단위 금액
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double minutelyAmount = 0D;
    /**
     * 시간단위 금액
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double hourlyAmount = 0D;
    /**
     * 일단위 금액
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double dailyAmount = 0D;
    /**
     * 월별 금액
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyAmount = 0D;
    /**
     * 월단위 약정 금액
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyContractAmount = 0D;
    /**
     * 월단위 약정 금액 3
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyContractAmount3 = 0D;
    /**
     * 월단위 약정 금액 6
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyContractAmount6 = 0D;
    /**
     * 월단위 약정 금액 12
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyContractAmount12 = 0D;
    /**
     * 월단위 약정 금액 24
     */
    @NotNull
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double monthlyContractAmount24 = 0D;
    /**
     * 사용 여부
     */
    @NotNull
    private Boolean useYn;

    @NotNull
    private Boolean sharedYn;

    /**
     * 클라우드 구분
     */
    @NotEmpty
    private Code.CLOUD_TYPE cloudType;

    /**
     * 자원 구분
     */
    @NotEmpty
    private Code.RESOURCE_TYPE resourceType;

    /**
     * 수량 구성
     */
    @NotEmpty
    private String quantityFormat;

    private int gpuMemory;
    private int cpuCore;
    private int systemMemory;
}
