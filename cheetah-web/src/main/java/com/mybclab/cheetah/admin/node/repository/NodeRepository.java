package com.mybclab.cheetah.admin.node.repository;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 노드 Repository
 */
@Repository
public interface NodeRepository extends JpaRepository<Node, Long>, JpaSpecificationExecutor<Node> {

    List<Node> findAllByStatusCode(Code.NODE_STATUS nodeStatus);

    Optional<Node> findOneByNodeName(String nodeName);

    List<Node> findAllByGpuLabelAndStatusCode(String gpuLabel, Code.NODE_STATUS nodeStatus);
}
