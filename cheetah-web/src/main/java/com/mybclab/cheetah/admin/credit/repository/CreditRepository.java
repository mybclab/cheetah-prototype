package com.mybclab.cheetah.admin.credit.repository;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 크레딧 Repository
 */
@Repository
public interface CreditRepository extends JpaRepository<Credit, String> {

    /**
     * 크레딧 TYPE 으로 조회
     * @param creditType {@link Code.CREDIT_TYPE}
     * @return 크레딧 목록
     */
    List<Credit> findAllByCreditTypeCode(Code.CREDIT_TYPE creditType);
}
