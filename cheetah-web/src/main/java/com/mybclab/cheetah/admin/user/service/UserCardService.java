package com.mybclab.cheetah.admin.user.service;

import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import com.mybclab.cheetah.admin.user.repository.UserCardRepository;
import com.mybclab.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * 사용자 카드 Service
 */
@Service
public class UserCardService implements Serializable {

    /**
     * {@link UserCardRepository}
     */
    private UserCardRepository userCardRepository;
    /**
     * default constructor
     */
    public UserCardService() {}

    /**
     * constructor
     * @param userCardRepository {@link UserCardRepository}
     */
    @Autowired
    public UserCardService(UserCardRepository userCardRepository) {
        this.userCardRepository = userCardRepository;
    }

    /**
     * 사용자카드 목록조회
     * @param username 사용자아이디
     * @return 사용자카드 목록
     */
    public List<UserCard> findAllByUsername(String username) {
        return userCardRepository.findAllByUserCardPKUsername(username);
    }

    /**
     * 사용자아이디로 사용중인 카드 단건조회
     * @param username 사용자아이디
     * @return {@link UserCard}
     */
    public UserCard findValidCardByUsername(String username) {
        return userCardRepository.findByUserCardPKUsernameAndUseYn(username, true);
    }

    /**
     * 저장
     * @param userCard {@link UserCard}
     * @return {@link UserCard}
     */
    @Transactional
    public UserCard save(UserCard userCard) {
        int userCardNo = userCardRepository.findMaxUserCardNo(userCard.getUserCardPK().getUsername()) + 1;
        userCard.getUserCardPK().setUserCardNo(userCardNo);
        return userCardRepository.save(userCard);
    }

    /**
     * 사용자 카드 추가
     * @param userCard {@link UserCard}
     * @return {@link UserCard}
     */
    @Transactional
    public UserCard addCard(UserCard userCard) {
        if (userCard.getUseYn()) {
            List<UserCard> userCardList = this.findAllByUsername(userCard.getUserCardPK().getUsername());
            userCardList.forEach(c -> c.setUseYn(false));
            userCardRepository.saveAll(userCardList);
        }
        return this.save(userCard);
    }

    /**
     * 사용자 카드 삭제
     * @param userCardPK {@link UserCardPK}
     */
    @Transactional
    public void deleteById(UserCardPK userCardPK) {
        userCardRepository.deleteById(userCardPK);
    }


    public String generateCustomerUid(String cardNo) {
        while (true) {
            String randomKey = StringUtils.randomAlphanumeric(8).toUpperCase();
            String customerId = randomKey + "-" + cardNo.substring(cardNo.length() - 4);
            if (!userCardRepository.existsByCustomerUid(customerId)) {
                return customerId;
            }
        }
    }

    public boolean isValidCard(UserCardPK userCardPK) {
        return userCardRepository.existsByUserCardPKAndUseYn(userCardPK, true);
    }
}
