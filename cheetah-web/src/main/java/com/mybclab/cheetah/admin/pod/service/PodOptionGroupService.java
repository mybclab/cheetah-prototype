package com.mybclab.cheetah.admin.pod.service;

import com.mybclab.cheetah.admin.pod.domain.PodOptionGroup;
import com.mybclab.cheetah.admin.pod.repository.PodOptionGroupRepository;
import com.mybclab.cheetah.common.constant.Code;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Slf4j
@NoArgsConstructor
@Service
public class PodOptionGroupService {

    private PodOptionGroupRepository podOptionGroupRepository;

    @Autowired
    public PodOptionGroupService(PodOptionGroupRepository podOptionGroupRepository) {
        this.podOptionGroupRepository = podOptionGroupRepository;
    }

    public List<PodOptionGroup> findAll() {
        return podOptionGroupRepository.findAll(Sort.by(Sort.Direction.ASC, "odr"));
    }

    @Cacheable(cacheNames = "podOptionGroup", key="#podOptionGroupTypeCode")
    public List<PodOptionGroup> findAllByType(Code.POD_OPTION_TYPE podOptionGroupTypeCode) {
        return podOptionGroupRepository.findAllByPodOptionGroupTypeCodeAndUseYnOrderByOdrAsc(podOptionGroupTypeCode, true);
    }

    @Cacheable(cacheNames = "podOptionGroup", key="#podOptionGroupId")
    public PodOptionGroup findById(String podOptionGroupId) {
        return podOptionGroupRepository.findById(podOptionGroupId).orElseThrow(EntityNotFoundException::new);
    }

    @CacheEvict(cacheNames = "podOptionGroup", allEntries = true)
    public PodOptionGroup save(PodOptionGroup podOptionGroup) {
        return podOptionGroupRepository.saveAndFlush(podOptionGroup);
    }

    @CacheEvict(cacheNames = "podOptionGroup", allEntries = true)
    public void delete(String podOptionGroupId) {
        podOptionGroupRepository.deleteById(podOptionGroupId);
        podOptionGroupRepository.flush();
    }
}
