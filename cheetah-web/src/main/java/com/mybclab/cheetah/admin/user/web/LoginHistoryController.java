package com.mybclab.cheetah.admin.user.web;

import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.LoginHistoryCriteria;
import com.mybclab.cheetah.admin.user.service.LoginHistoryService;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.view.LoginHistoryXlsView;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequestMapping("/admin/login-history")
@Controller
public class LoginHistoryController {

    private LoginHistoryService loginHistoryService;

    private UserService userService;

    private CodeGroupService codeGroupService;

    private CheetahMessageSource messageSource;

    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;


    @Autowired
    public LoginHistoryController(LoginHistoryService loginHistoryService, UserService userService, CodeGroupService codeGroupService, CheetahMessageSource messageSource, SessionRegistry sessionRegistry) {
        this.loginHistoryService = loginHistoryService;
        this.userService = userService;
        this.codeGroupService = codeGroupService;
        this.messageSource = messageSource;
        this.sessionRegistry = sessionRegistry;
    }

    @RequestMapping({"", "/list"})
    public String list(LoginHistoryCriteria loginHistoryCriteria, @PageableDefault(sort = { "loginHistorySn" }, direction = Sort.Direction.DESC, size = 50) Pageable pageable, Model model) {


        List<Object> allPrincipals = sessionRegistry.getAllPrincipals();
        List<User> realLoginUserList = allPrincipals.stream().filter(o -> !(sessionRegistry.getAllSessions(o, false)).isEmpty()).map(User.class::cast).collect(Collectors.toList());
        model.addAttribute("realLoginUserList", realLoginUserList);



        model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
        List<User> userList = new ArrayList<>();
        if ("".equals(loginHistoryCriteria.getParentUsername())) {
            userList = userService.findAllByUserRole(Code.ROLE.GENERAL_USER);
        } else if (!"ALL".equals(loginHistoryCriteria.getParentUsername())) {
            userList = userService.findAllByGroupId(loginHistoryCriteria.getParentUsername());
        }
        model.addAttribute("userList", userList);

        model.addAttribute(loginHistoryCriteria);
        model.addAttribute("loginHistoryPage", loginHistoryService.findAll(loginHistoryCriteria, pageable));
        return "admin/user/login-history-list";
    }

    @RequestMapping(path={"", "/list"}, params="xls")
    public View listXls(LoginHistoryCriteria loginHistoryCriteria, Model model) {

        Sort sort = new Sort(Sort.Direction.DESC, "loginHistorySn");
        List<LoginHistory> loginHistoryList = loginHistoryService.findAll(loginHistoryCriteria, sort);
        model.addAttribute("loginHistoryList", loginHistoryList);

        return new LoginHistoryXlsView(messageSource);
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("loginHistoryCodeList", codeGroupService.getCodeList(CodeGroupID.LOGIN_HISTORY));

        return map;
    }

}
