package com.mybclab.cheetah.admin.gpu.web;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.dto.GpuForm;
import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * GPU Controller
 */
@Controller
@RequestMapping("/admin/gpu")
@Slf4j
public class GpuController {

    /**
     * {@link GpuService}
     */
    private GpuService gpuService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    private ContainerService containerService;

    private NodeService nodeService;

    private CodeGroupService codeGroupService;

    /**
     * default constructor
     */
    public GpuController() {}

    /**
     * constructor
     * @param gpuService {@link GpuService}
     * @param containerService {@link ContainerService}
     * @param nodeService {@link NodeService}
     * @param modelMapper {@link ModelMapper}
     */
    @Autowired
    public GpuController(GpuService gpuService, ContainerService containerService, NodeService nodeService, ModelMapper modelMapper, CodeGroupService codeGroupService) {
        this.gpuService = gpuService;
        this.containerService = containerService;
        this.modelMapper = modelMapper;
        this.nodeService = nodeService;
        this.codeGroupService = codeGroupService;
    }


    /**
     * GPU 이미지 목록조회
     * @param model {@link Model}
     * @return GPU 이미지 목록조회
     */
    @RequestMapping({"", "/list"})
    public String list(Model model) {
        List<Gpu> gpuList = gpuService.findAll();
        for (Gpu gpu : gpuList) {
            List<Container> useGpuContainerList = containerService.findUseContainerByGpu(gpu.getGpuSn());
            gpu.setUseContainerCount(useGpuContainerList.size());
            gpu.setUseGpuQuantity(useGpuContainerList.stream().mapToInt(Container::getGpuQuantity).sum());
            List<Node> nodeList = nodeService.findAllByGpuLabel(gpu.getGpuLabel());
            gpu.setTotalNodeCount(nodeList.size());
            gpu.setTotalGpuQuantity(nodeList.stream().mapToInt(Node::getGpuQuantity).sum());
        }
        model.addAttribute(gpuList);
        return "admin/gpu/gpu-list";
    }

    @GetMapping("/save")
    public String save(Model model) {

        model.addAttribute("gpuForm", new GpuForm());

        return "admin/gpu/gpu-form";
    }

    @GetMapping("/{gpuSn}/modify")
    public String modify(@PathVariable Long gpuSn, Model model) {

        Gpu gpu = gpuService.findById(gpuSn);
        GpuForm gpuForm = modelMapper.map(gpu, GpuForm.class);

        model.addAttribute("gpuForm", gpuForm);

        return "admin/gpu/gpu-form";
    }

    @PostMapping({"/save", "/{gpuSn}/modify"})
    public String saveOrModify(@ModelAttribute GpuForm gpuForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/gpu/gpu-form";
        }

        Gpu gpu = modelMapper.map(gpuForm, Gpu.class);
        gpuService.save(gpu);

        return "redirect:/admin/gpu";
    }

    @PostMapping("/{gpuSn}/delete")
    public String delete(@PathVariable Long gpuSn) {
        gpuService.deleteById(gpuSn);
        return "redirect:/admin/gpu";
    }

    /**
     * 캐쉬 gpu 전체 삭제
     * @return gpu 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        gpuService.cacheReloadAll();
        return "redirect:/admin/gpu/";
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("cloudTypeList", codeGroupService.getCodeList(CodeGroupID.CLOUD_TYPE));
        map.put("resourceTypeList", codeGroupService.getCodeList(CodeGroupID.RESOURCE_TYPE));

        return map;
    }
}
