package com.mybclab.cheetah.admin.user.specification;

import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.LoginHistoryCriteria;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

public class LoginHistorySpecification {

    /**
     * equal 사용자아이디
     * @param username 사용자아이디
     * @return {@link Specification < LoginHistory >}
     */
    public static Specification<LoginHistory> equalsUsername(final String username) {
        if (null == username || "".equals(username)) {
            return null;
        }
        return Optional.ofNullable(username)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<String>get("username"), o))
                .orElse(null);
    }

    /**
     * equal 상위 사용자아이디
     * @param parentUsername 상위 사용자아이디(그룹아이디)
     * @return {@link Specification<LoginHistory>}
     */
    public static Specification<LoginHistory> equalsParentUsername(final String parentUsername) {
        if ("ALL".equals(parentUsername)) {
            return null;
        }
        if ("".equals(parentUsername)) {
            return Optional.ofNullable(parentUsername)
                    .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.and(
                            criteriaBuilder.isNull(root.<User>get("user").<String>get("parentUsername")), criteriaBuilder.isNull(root.<User>get("user").<String>get("groupName"))))
                    .orElse(null);
        }
        return Optional.ofNullable(parentUsername)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.or(
                        criteriaBuilder.equal(root.<User>get("user").<String>get("parentUsername"), o), criteriaBuilder.equal(root.<User>get("user").<String>get("username"), o)))
                .orElse(null);
    }

    /**
     * like 이름
     * @param name 이름
     * @return {@link Specification< LoginHistory >}
     */
    public static Specification<LoginHistory> likeName(final String name) {
        return Optional.ofNullable(name)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<User>get("user").<String>get("name"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * like 로그인 IP
     * @param loginIp 로그인 IP
     * @return {@link Specification< LoginHistory >}
     */
    public static Specification<LoginHistory> likeLoginIp(final String loginIp) {
        return Optional.ofNullable(loginIp)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("loginIp"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * equal 로그인 이력 코드
     * @param loginHistoryCode 로그인 이력 코드
     * @return {@link Specification < Container >}
     */
    public static Specification<LoginHistory> equalsLoginHistoryCode(final Code.LOGIN_HISTORY loginHistoryCode) {
        if (Code.LOGIN_HISTORY.FAIL_ALL.equals(loginHistoryCode)) {
            return Optional.ofNullable(loginHistoryCode)
                    .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.notEqual(root.<Code.LOGIN_HISTORY>get("loginHistoryCode"), Code.LOGIN_HISTORY.SUCCESS))
                    .orElse(null);
        }

        return Optional.ofNullable(loginHistoryCode)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Code.LOGIN_HISTORY>get("loginHistoryCode"), o))
                .orElse(null);
    }

    public static Specification<LoginHistory> rangeLoginDate(final LoginHistoryCriteria loginHistoryCriteria) {
        if (loginHistoryCriteria.getLoginDateFrom() == null && loginHistoryCriteria.getLoginDateTo() == null) {
            return null;
        }
        if (loginHistoryCriteria.getLoginDateFrom() != null && loginHistoryCriteria.getLoginDateTo() == null) {
            return Optional.ofNullable(loginHistoryCriteria.getLoginDateFrom())
                    .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.<LocalDateTime>get("loginDate"), toStartDateWithTimeZone(o)))
                    .orElse(null);
        }
        if (loginHistoryCriteria.getLoginDateFrom() == null && loginHistoryCriteria.getLoginDateTo() != null) {
            return Optional.ofNullable(loginHistoryCriteria.getLoginDateTo())
                    .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.<LocalDateTime>get("loginDate"), toEndDateWithTimeZone(o)))
                    .orElse(null);
        }
        return Optional.ofNullable(loginHistoryCriteria)
                .map(o -> (Specification<LoginHistory>) (root, query, criteriaBuilder) -> criteriaBuilder.between(root.<LocalDateTime>get("loginDate"), toStartDateWithTimeZone(o.getLoginDateFrom()), toEndDateWithTimeZone(o.getLoginDateTo())))
                .orElse(null);
    }

    private static LocalDateTime toStartDateWithTimeZone(LocalDate localdate) {
        System.out.println(">>>>>>>>>> from : " + localdate.atStartOfDay(ZoneId.of("UTC")).toLocalDateTime());
        return localdate.atStartOfDay(ZoneId.of("UTC")).toLocalDateTime();
    }

    private static LocalDateTime toEndDateWithTimeZone(LocalDate localDate) {
        System.out.println(">>>>>>>>>> to : " + localDate.atTime(23, 59, 59).atZone(ZoneId.of("UTC")).toLocalDateTime());
        return localDate.atTime(23, 59, 59).atZone(ZoneId.of("UTC")).toLocalDateTime();
    }
}
