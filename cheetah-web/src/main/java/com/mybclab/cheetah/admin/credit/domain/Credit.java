package com.mybclab.cheetah.admin.credit.domain;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.common.domain.HistoryBase;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 크레딧
 */
@Entity
@Setter
@Getter
@ToString(exclude = {"userCreditList"})
public class Credit extends HistoryBase implements Serializable {

    /**
     * 크레딧 아이디
     */
    @Id
    private String creditId;

    /**
     * 크레딧 명
     */
    private String creditName;

    /**
     * 크레딧 구분 코드
     * {@link Code.CREDIT_TYPE}
     */
    @Enumerated(EnumType.STRING)
    private Code.CREDIT_TYPE creditTypeCode;

    /**
     * 크레딧 금액
     */
    @CurrencyFormat
    private Double creditAmount;

    /**
     * 크레딧 유효기간 코드
     * {@link Code.CREDIT_EXPIRY_TYPE}
     */
    @Enumerated(EnumType.STRING)
    private Code.CREDIT_EXPIRY_TYPE creditExpiryTypeCode;

    /**
     * 크레딧 유효기간 값
     */
    private int creditExpiryValue;


    /**
     * 사용자 크레딧 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "creditId", referencedColumnName = "creditId", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<UserCredit> userCreditList = new ArrayList<>();

}
