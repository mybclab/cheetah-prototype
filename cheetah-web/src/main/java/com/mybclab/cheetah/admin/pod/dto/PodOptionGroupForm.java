package com.mybclab.cheetah.admin.pod.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class PodOptionGroupForm {

    @NotEmpty
    private String podOptionGroupId;

    @NotEmpty
    private String podOptionGroupName;


    @Enumerated(EnumType.STRING)
    private Code.POD_OPTION_TYPE podOptionGroupTypeCode;


    /**
     * 순서
     */
    private int odr;
    /**
     * 사용 여부
     */
    private Boolean useYn;


}
