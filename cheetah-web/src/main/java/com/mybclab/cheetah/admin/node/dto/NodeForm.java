package com.mybclab.cheetah.admin.node.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 노드 Form
 */
@Getter
@Setter
public class NodeForm implements Serializable {

    /**
     * 노드 일련번호
     */
    private Long nodeSn;
    /**
     * 노드 명
     */
    @NotBlank
    private String nodeName;

    /**
     * 내부 도메인
     */
    private String internalDomain;

    /**
     * 내부 아이피
     */
    private String internalIp;

    /**
     * 외부 아이피
     */
    private String externalIp;

    /**
     * 상태 코드
     */
    private String statusCode;

    /**
     * 마스터 여부
     */
    private Boolean masterYn;

    /**
     * GPU 라벨
     */
    private String gpuLabel;
    /**
     * GPU 수량
     */
    private int gpuQuantity;

}
