package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.YearMonth;

/**
 * 사용자 카드
 */
@Entity
@Getter
@Setter
@ToString
public class UserCard extends HistoryBase implements Serializable {

    /**
     * {@link UserCardPK}
     */
    @EmbeddedId
    private UserCardPK userCardPK;

    /**
     * 카드 번호
     */
    private String cardNo;
    /**
     * 카드 유효기간
     */
    private String cardExpiry;
    /**
     * CUSTOMER_UID string(80)
     */
    private String customerUid;


    /**
     * 사용 여부
     */
    private Boolean useYn;

    /**
     * default constructor
     */
    public UserCard() {}

    /**
     * constructor
     * @param userCardPK {@link UserCardPK}
     */
    public UserCard(UserCardPK userCardPK) {
        this.userCardPK = userCardPK;
    }

    /**
     * {@link User}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private User user;

    @Transient
    public String getCardExpiryWithFormat() {
        if (this.cardExpiry != null && !"".equals(this.cardExpiry)) {
            return CheetahFormatter.FOR_YEAR_MONTH_WITH_DASH.format(YearMonth.parse(this.cardExpiry, CheetahFormatter.FOR_CARD_EXPIRY));
        }
        return null;
    }

    @Transient
    public String getCardNoWithStar() {
        if (this.cardNo != null && !"".equals(this.cardNo)) {
            return this.cardNo.substring(0, 6) + "**********";
        }
        return null;
    }


}
