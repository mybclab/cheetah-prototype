package com.mybclab.cheetah.admin.pod.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.domain.HistoryBase;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@ToString(exclude = "podOptionList")
public class PodOptionGroup extends HistoryBase implements Serializable {

    /**
     * 파드 옵션 그룹 아이디
     */
    @Id
    private String podOptionGroupId;

    /**
     * 파드 옵션 그룹 명
     */
    private String podOptionGroupName;

    /**
     * 파드 옵션 그룹 구분 코드
     */
    @Enumerated(EnumType.STRING)
    private Code.POD_OPTION_TYPE podOptionGroupTypeCode;

    /**
     * 순서
     */
    private int odr;
    /**
     * 사용 여부
     */
    private Boolean useYn;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "podOptionGroupId", referencedColumnName = "podOptionGroupId", insertable = false, updatable = false)
    @OrderBy("odr ASC")
    private List<PodOption> podOptionList = new ArrayList<>();

    @JsonIgnore
    @Transient
    public String getHtmlIcon() {
        switch (getPodOptionGroupId()) {
            case "OS" : return "md-home";
            case "JUPYTER" : return "md-bookmark";
            case "TENSORFLOW" : return "md-layers";
            case "KERAS" : return "md-flower";
            case "PYTORCH" : return "md-fire";
            case "R" : return "md-receipt";
            case "CAFFE2" : return "md-coffee";
            default: return "md-plus";
        }
    }

}
