package com.mybclab.cheetah.admin.user.dto;

import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.beans.Transient;
import java.io.Serializable;
import java.time.YearMonth;

/**
 * 사용자 카드 Form
 */
@Getter
@Setter
@ToString
public class UserCardForm implements Serializable {
    /**
     * 카드 번호(XXXX-XXXX-XXXX-XXXX)
     */
    @NotBlank
    private String cardNo;
    /**
     * CUSTOMER_UID
     */
    private String customerUid;

    /**
     * 카드 유효기간(MMYY)
     */
    @NotBlank
    @Pattern(regexp = "[0-9]{4}")
    private String cardExpiry;
    /**
     * 사용 여부
     */
    private Boolean useYn;

    /**
     * 카드 생년월일(YYMMDD)
     * 법인카드인 경우 사업자등록번호 10자리
     */
    @NotBlank
    @Pattern(regexp = "[0-9]{6}")
    private String cardBirth;
    /**
     * 카드 비밀번호(앞 두자리)
     */
    @NotBlank
    @Size(min = 2, max = 2)
    @Pattern(regexp = "[0-9]{2}")
    private String cardPassword;

    @Transient
    public String getCardExpiryYYYYMM() {
        if (this.cardExpiry != null && !"".equals(this.cardExpiry)) {
            return CheetahFormatter.FOR_YEAR_MONTH.format(YearMonth.parse(this.cardExpiry, CheetahFormatter.FOR_CARD_EXPIRY));
        }
        return null;
    }
}
