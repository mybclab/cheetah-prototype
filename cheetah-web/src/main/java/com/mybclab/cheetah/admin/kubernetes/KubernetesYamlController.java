package com.mybclab.cheetah.admin.kubernetes;

import com.mybclab.cheetah.kubernetes.constant.Kubernetes;
import com.mybclab.cheetah.kubernetes.domain.KubernetesYaml;
import com.mybclab.cheetah.kubernetes.dto.KubernetesYamlForm;
import com.mybclab.cheetah.kubernetes.service.KubernetesYamlService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin/kubernetes-yaml")
public class KubernetesYamlController {

    private KubernetesYamlService kubernetesYamlService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    public KubernetesYamlController() {}

    @Autowired
    public KubernetesYamlController(KubernetesYamlService kubernetesYamlService, ModelMapper modelMapper) {
        this.kubernetesYamlService = kubernetesYamlService;
        this.modelMapper = modelMapper;
    }


    @RequestMapping({"", "/list"})
    public String list(Model model) {

        List<KubernetesYaml> kubernetesYamlList = kubernetesYamlService.findAll();

        model.addAttribute("kubernetesYamlList", kubernetesYamlList);
        return "admin/kubernetes/kubernetes-yaml-list";
    }

    @GetMapping("/{kind}/modify")
    public String modify(@PathVariable String kind, Model model) {

        KubernetesYaml kubernetesYaml = kubernetesYamlService.findById(Kubernetes.KIND.valueOf(kind));
        KubernetesYamlForm kubernetesYamlForm = modelMapper.map(kubernetesYaml, KubernetesYamlForm.class);
        model.addAttribute("kubernetesYamlForm", kubernetesYamlForm);
        return "admin/kubernetes/kubernetes-yaml-form";
    }

    @PostMapping("/{kind}/modify")
    public String save(KubernetesYamlForm kubernetesYamlForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "admin/kubernetes/kubernetes-yaml-form";
        }

        KubernetesYaml kubernetesYaml = modelMapper.map(kubernetesYamlForm, KubernetesYaml.class);
        kubernetesYamlService.save(kubernetesYaml);

        return "redirect:/admin/kubernetes-yaml";
    }

    /**
     * 캐쉬 yaml 전체 삭제
     * @return yaml 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        kubernetesYamlService.cacheReloadAll();
        return "redirect:/admin/kubernetes-yaml/";
    }
}
