package com.mybclab.cheetah.admin.gpu.repository;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * GPU Repository
 */
@Repository
public interface GpuRepository extends JpaRepository<Gpu, Long> {

    /**
     * GPU 목록조회
     * @param useYn 사용 여부
     * @return GPU 목록
     */
    List<Gpu> findAllByUseYn(Boolean useYn);
}
