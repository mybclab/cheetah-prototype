package com.mybclab.cheetah.admin.pod.web;

import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.pod.domain.*;
import com.mybclab.cheetah.admin.pod.dto.PodFormatCriteria;
import com.mybclab.cheetah.admin.pod.dto.PodFormatForm;
import com.mybclab.cheetah.admin.pod.service.PodFormatService;
import com.mybclab.cheetah.admin.pod.service.PodOptionGroupService;
import com.mybclab.cheetah.admin.pod.service.PodOptionService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.utils.MapUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 파드 구성 Controller
 */
@Slf4j
@Controller
@RequestMapping("/admin/pod-format")
public class PodFormatController {

    /**
     * {@link PodFormatService}
     */
    private PodFormatService podFormatService;

    private PodOptionGroupService podOptionGroupService;

    private PodOptionService podOptionService;

    private GpuService gpuService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    /**
     * default constructor
     */
    public PodFormatController() {}

    /**
     * constructor
     * @param podFormatService {@link PodFormatService}
     * @param podOptionGroupService {@link PodOptionGroupService}
     * @param modelMapper {@link ModelMapper}
     */
    @Autowired
    public PodFormatController(PodFormatService podFormatService, PodOptionGroupService podOptionGroupService, PodOptionService podOptionService, GpuService gpuService, ModelMapper modelMapper) {
        this.podFormatService = podFormatService;
        this.podOptionGroupService = podOptionGroupService;
        this.podOptionService = podOptionService;
        this.gpuService = gpuService;
        this.modelMapper = modelMapper;
    }

    /**
     * 파드 구성 목록조회
     * @param model {@link Model}
     * @return 파드 구성 목록조회
     */
    @RequestMapping({"", "/list"})
    public String list(PodFormatCriteria podFormatCriteria, Model model) {

        List<PodFormat> podFormatList = podFormatService.findAll(podFormatCriteria);
        model.addAttribute("podFormatCriteria", podFormatCriteria);
        model.addAttribute("podFormatList", podFormatList);
        return "admin/pod-format/pod-format-list";
    }

    @GetMapping("/save")
    public String save(Model model) {

        model.addAttribute("podFormatForm", new PodFormatForm());

        return "admin/pod-format/pod-format-form";
    }

    @GetMapping("/{podFormatSn}/modify")
    public String modify(@PathVariable Long podFormatSn, Model model) {

        PodFormat podFormat = podFormatService.findById(podFormatSn);
        PodFormatForm podFormatForm = modelMapper.map(podFormat, PodFormatForm.class);

        Map<String, Long> podOptionSnMap = podFormat.getPodFormatOptionList().stream().collect(Collectors.toMap(PodFormatOption::getPodOptionGroupId, o->o.getPodFormatOptionPK().getPodOptionSn()));
        podFormatForm.setPodOptionSnMap(podOptionSnMap);
        podFormatForm.setGpuSns(podFormat.getPodFormatGpuList().stream().map(g -> g.getPodFormatGpuPK().getGpuSn()).toArray(Long[]::new)); // FIXME

        model.addAttribute("podFormatForm", podFormatForm);

        return "admin/pod-format/pod-format-form";
    }

    @PostMapping({"/save", "/{podFormatSn}/modify"})
    public String saveOrModify(@ModelAttribute PodFormatForm podFormatForm, BindingResult bindingResult) {
        try {
            PodFormat podFormatByHint = podFormatService.findByPodOptionHint(podFormatForm.getPodOptionSnMap());
            if (!podFormatByHint.getPodFormatSn().equals(podFormatForm.getPodFormatSn())) {
                bindingResult.rejectValue("podFormatSn", "Valid.existPodFormat");
//                log.debug(">>>>>>>>>>> podFormatHint : " + MapUtils.toStringSortByKey(podFormatForm.getPodOptionSnMap()));
            }
        } catch (EntityNotFoundException e) {
        }

        if (bindingResult.hasErrors()) {
            return "admin/pod-format/pod-format-form";
        }

        PodFormat podFormat = modelMapper.map(podFormatForm, PodFormat.class);
        podFormat.setPodOptionHint(MapUtils.toStringSortByKey(podFormatForm.getPodOptionSnMap()));
        podFormat.setPodFormatOptionList(setPodFormatOptionList(podFormatForm.getPodOptionSnMap(), podFormat.getPodFormatSn()));
        podFormat.setPodFormatGpuList(setPodFormatGpuList(podFormatForm));

        podFormatService.save(podFormat);

        return "redirect:/admin/pod-format";
    }
    private List<PodFormatGpu> setPodFormatGpuList(PodFormatForm podFormatForm) {
        List<PodFormatGpu> podFormatGpuList = new ArrayList<>();
        for (Long gpuSn : podFormatForm.getGpuSns()) {
            PodFormatGpuPK podFormatGpuPK = modelMapper.map(podFormatForm, PodFormatGpuPK.class);
            podFormatGpuPK.setGpuSn(gpuSn);
            podFormatGpuList.add(new PodFormatGpu(podFormatGpuPK, LocalDateTime.now()));
        }
        return podFormatGpuList;
    }
    private List<PodFormatGpu> setPodFormatGpuList(Long[] gpuSns, Long podFormatSn) {
        List<PodFormatGpu> podFormatGpuList = new ArrayList<>();
        for (Long gpuSn : gpuSns) {
            podFormatGpuList.add(new PodFormatGpu(new PodFormatGpuPK(podFormatSn, gpuSn), LocalDateTime.now()));
        }
        return podFormatGpuList;
    }

    private List<PodFormatOption> setPodFormatOptionList(Map<String, Long> podOptionSnMap, Long podFormatSn) {
        List<PodFormatOption> podFormatOptionList = new ArrayList<>();
        for (String key : podOptionSnMap.keySet()) {
            PodFormatOptionPK podFormatOptionPK = new PodFormatOptionPK(podFormatSn, podOptionSnMap.get(key));
            PodFormatOption podFormatOption = new PodFormatOption();
            podFormatOption.setPodFormatOptionPK(podFormatOptionPK);
            podFormatOption.setPodOptionGroupId(key);
            podFormatOption.setPodOptionId(podOptionService.findById(podFormatOptionPK.getPodOptionSn()).getPodOptionId());
            podFormatOption.setCreatedAt(LocalDateTime.now());
            podFormatOptionList.add(podFormatOption);
        }
        return podFormatOptionList;
    }

    @PostMapping("/{podFormatSn}/delete")
    public String delete(@PathVariable Long podFormatSn) {
        podFormatService.deleteById(podFormatSn);
        return "redirect:/admin/pod-format";
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();


        map.put("gpuList", gpuService.findAll());
        map.put("requiredList", podOptionGroupService.findAllByType(Code.POD_OPTION_TYPE.REQUIRED));
        map.put("libraryList", podOptionGroupService.findAllByType(Code.POD_OPTION_TYPE.LIBRARY));

        return map;
    }

    /**
     * 캐쉬 podFormat 전체 삭제
     * @return podFormat 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        podFormatService.cacheReloadAll();
        return "redirect:/admin/pod-format/";
    }
}
