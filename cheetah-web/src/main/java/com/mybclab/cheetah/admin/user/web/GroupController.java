package com.mybclab.cheetah.admin.user.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.UserCriteria;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.validator.UserFormValidator;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/admin/group")
public class GroupController {

    /**
     * {@link UserService}
     */
    private UserService userService;
    /**
     * {@link UserFormValidator}
     */
    private UserFormValidator userFormValidator;

    public GroupController() {}

    @Autowired
    public GroupController(UserService userService, UserFormValidator userFormValidator) {
        this.userService = userService;
        this.userFormValidator = userFormValidator;
    }

    @RequestMapping({"", "/list"})
    public String list(UserCriteria groupCriteria, @PageableDefault(sort = { "createdAt" }, direction = Sort.Direction.DESC) Pageable pageable, Model model) {

        groupCriteria.setIsGroup(true);
        Page<User> groupPage = userService.findAll(groupCriteria, pageable);

        model.addAttribute("groupCriteria", groupCriteria);
        model.addAttribute("groupPage", groupPage);
        return "admin/group/group-list";
    }

    @PostMapping("/{username}/update")
    public String updateGroup(UserForm userForm, BindingResult bindingResult) {
        User user = userService.findById(userForm.getUsername());

        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationGroupForGpuQuantity.class);
        if (bindingResult.hasErrors()) {
            return "admin/user/user";
        }

        userService.save(user);

        return "redirect:/admin/user/" + user.getUsername();
    }



}
