package com.mybclab.cheetah.admin.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 사용자
 */
@Slf4j
@Getter
@Setter
@Entity
@Table(name = "user")
@DynamicUpdate
@ToString(exclude = {"group", "groupUsers", "userRoles", "containerList", "userCreditList", "userCardList", "userGpuList", "loginHistoryList"})
public class User extends HistoryBase implements Serializable, UserDetails {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6546969738784134421L;
    /**
     * 사용자아이디
     */
    @Id
    private String username;

    /**
     * 상위 사용자아이디
     */
    @Column
    private String parentUsername;

    /**
     * 그룹 명
     */
    @Column
    private String groupName;

    /**
     * 그룹 구분 코드
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Code.GROUP_TYPE groupTypeCode;


    @Column
    private Long userImageFileSn;

    @Column
    private Long groupImageFileSn;

    /**
     * 그룹 초대 키
     */
    @Column
    private String groupInviteKey;

    /**
     * 사용자 이름
     */
    @Column
    private String name;

    /**
     * 비밀번호
     */
    @Column
    private String password;

    /**
     * 전화 번호
     */
    @Column
    private String phoneNo;

    /**
     * 이메일
     */
    @Column
    private String email;

    /**
     * 승인 여부
     */
    @Column
    private Boolean confirmYn = false;


    /**
     * 슬랙 훅 URL
     */
    @Column
    private String slackHookUrl;

    /**
     * 슬랙 채널
     */
    @Column
    private String slackChannel;

    /**
     * 단체 명
     */
    @Column
    private String organizationName;
    /**
     * 팀 명
     */
    @Column
    private String teamName;

    /**
     * 탈퇴 일시
     */
    @Column
    private LocalDateTime withdrawalDate;

    /**
     * 그룹
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentUsername", insertable = false, updatable = false)
    @JsonIgnore
    private User group;

    /**
     * 그룹 사용자
     */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "parentUsername", referencedColumnName = "username", updatable = false)
    @OrderBy("createdBy DESC")
    @JsonIgnore
    private List<User> groupUsers = new ArrayList<>();

    /**
     * User의 역할
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", updatable = false)
    private List<UserRole> userRoles = new ArrayList<>();


    /**
     * 컨테이너 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<Container> containerList = new ArrayList<>();

    /**
     * 사용자 크레딧 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OrderBy("expiryEndDate ASC")
    private List<UserCredit> userCreditList = new ArrayList<>();

    /**
     * 사용자 카드 목록
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<UserCard> userCardList = new ArrayList<>();

    /**
     * 사용자 GPU 목록
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<UserGpu> userGpuList = new ArrayList<>();


    /**
     * 사용자 로그인 이력 목록
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @OrderBy("loginHistorySn DESC")
    private List<LoginHistory> loginHistoryList = new ArrayList<>();

    /**
     * default constructor
     */
    public User() {
    }

    /**
     * constructor
     * @param username 사용자아아디
     */
    public User(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() { return this.userRoles; }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    @Transient
    public boolean isSystemAdmin() {
        return this.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Code.ROLE.SYSTEM_ADMIN.name()));
    }

    @Transient
    public boolean isGroupAdmin() {
        return this.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Code.ROLE.GROUP_ADMIN.name()));
    }

    @Transient
    public boolean isGroupUser() {
        return this.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Code.ROLE.GROUP_USER.name()));
    }

    @Transient
    public boolean isGeneralUser() {
        return this.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Code.ROLE.GENERAL_USER.name()));
    }

    @Transient
    public int getUseContainerCount() {
        return (int) containerList.stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).count();
    }
    @Transient
    public Long getGroupImage() {
        if (isGroupAdmin()) {
            return this.groupImageFileSn;
        } else if (isGroupUser()) {
            return getGroup().getGroupImageFileSn();
        }
        return null;
    }


    /**
     * 상태가 START 인 그룹 컨테이너의 수
     * @return 사용중인 그룹 컨테이너의 수
     */
    @Transient
    public int getGroupContainerCount() {
        List<User> groupUsersIncludeAdmin = getGroupUsersIncludeAdmin();
        if (groupUsersIncludeAdmin.size() > 0) {
            return groupUsersIncludeAdmin.stream().mapToInt(u -> u.getContainerList().stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).collect(Collectors.toList()).size()).sum();
        }
        return 0;
    }



    // 20190905
    public List<User> getGroupUsersIncludeAdmin() {
        List<User> groupUsersIncludeAdmin = new ArrayList<>(groupUsers);
        groupUsersIncludeAdmin.add(this);
        return groupUsersIncludeAdmin;
    }

    public int getTotalUseQuantity() {
        if (userGpuList != null && userGpuList.size() > 0) {
            return userGpuList.stream().mapToInt(UserGpu::getUseQuantity).sum();
        }
        return 0;
    }
    public int getTotalQuotaQuantity() {
        if (userGpuList != null && userGpuList.size() > 0) {
            return userGpuList.stream().mapToInt(UserGpu::getQuotaQuantity).sum();
        }
        return 0;
    }
    public int getTotalGroupUseQuantity() {
        if (userGpuList != null && userGpuList.size() > 0) {
            return userGpuList.stream().mapToInt(UserGpu::getGroupUseQuantity).sum();
        }
        return 0;
    }
    public int getTotalGroupQuotaQuantity() {
        if (userGpuList != null && userGpuList.size() > 0) {
            return userGpuList.stream().mapToInt(UserGpu::getGroupQuotaQuantity).sum();
        }
        return 0;
    }

    public String getUserImagePath() {
        if (this.getUserImageFileSn() != null) {
            return "/download/" + getUserImageFileSn();
        } else {
            String userRoleName = "SYSTEM_ADMIN";
            if (isGroupAdmin()) userRoleName = "GROUP_ADMIN";
            if (isGroupUser()) userRoleName = "GROUP_USER";
            if (isGeneralUser()) userRoleName = "GENERAL_USER";
            return "/assets/images/cheetah-" + userRoleName + ".jpg";
        }
    }
}
