package com.mybclab.cheetah.admin.user.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 사용자 카드 PK
 */
@Getter
@Setter
@ToString
@Embeddable
public class UserCardPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 사용자 카드 번호
     */
    private int userCardNo;

    /**
     * default constuctor
     */
    public UserCardPK() {}

    /**
     * constuctor
     * @param username 사용자아이디
     * @param userCardNo 사용자 카드 번호
     */
    public UserCardPK(String username, int userCardNo) {
        this.username = username;
        this.userCardNo = userCardNo;
    }
}
