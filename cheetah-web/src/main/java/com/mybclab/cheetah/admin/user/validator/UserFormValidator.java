package com.mybclab.cheetah.admin.user.validator;

import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Component
@Slf4j
public class UserFormValidator extends LocalValidatorFactoryBean {

    /**
     * {@link UserRepository}
     */
    private final UserRepository userRepository;

    /**
     * {@link BCryptPasswordEncoder}
     */
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * constructor
     *
     * @param userRepository {@link UserRepository}
     * @param bCryptPasswordEncoder {@link BCryptPasswordEncoder}
     */
    public UserFormValidator(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHints) {
        UserForm userForm = (UserForm) target;

        super.validate(target, errors, validationHints);

        if (validationHints != null) {

            for (Object hint : validationHints) {
                if (hint.equals(UserForm.ValidationSignUp.class)) {
                    if (userRepository.existsById(userForm.getUsername())) {
                        errors.rejectValue("username", "Valid.existUser", "사용 중인 ID입니다.");
                    }
                }

                if (hint.equals(UserForm.ValidationSignUpForGroupAdmin.class)) {
                    if (!userForm.getGroupName().equals(userForm.getOrigGroupName()) && userRepository.existsByGroupName(userForm.getGroupName())) {
                        errors.rejectValue("groupName", "Valid.existGroupName", "사용 중인 그룹명입니다.");
                    }
                }

                if (hint.equals(UserForm.ValidationPassword.class)) {
                    if (!userForm.getPassword().equals(userForm.getConfirmPassword())) {
                        errors.rejectValue("confirmPassword", "Valid.differentPassword", "비밀번호가 다릅니다.");
                    }
                }

//                if (hint.equals(UserForm.ValidationOldPassword.class)) {
//                    User user = userRepository.findById(userForm.getUsername()).orElseThrow(EntityNotFoundException::new);
//
//                    log.error("{}", bCryptPasswordEncoder.matches(userForm.getOldPassword(), user.getPassword()));
//                    log.error("current password {}", user.getPassword());
//                    log.error("input password {}", userForm.getOldPassword());
//
//                    if (!bCryptPasswordEncoder.matches(userForm.getOldPassword(), user.getPassword())) {
//                        errors.rejectValue("oldPassword", "Valid.incorrectOldPassword", "기존 비밀번호가 다릅니다.");
//                    }
//                }
            }
        }
    }
}
