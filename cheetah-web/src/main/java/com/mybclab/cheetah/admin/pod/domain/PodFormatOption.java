package com.mybclab.cheetah.admin.pod.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString(exclude = "podOption")
@NoArgsConstructor
@AllArgsConstructor
public class PodFormatOption implements Serializable {


    @EmbeddedId
    private PodFormatOptionPK podFormatOptionPK;

    /**
     * 파드 옵션 그룹 아이디
     */
    private String podOptionGroupId;

    /**
     * 파드 옵션 아이디
     */
    private String podOptionId;

    private LocalDateTime createdAt;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "podOptionSn", referencedColumnName = "podOptionSn", insertable = false, updatable = false)
    private PodOption podOption;
}
