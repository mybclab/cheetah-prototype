package com.mybclab.cheetah.admin.pod.service;

import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.pod.dto.PodFormatCriteria;
import com.mybclab.cheetah.admin.pod.repository.PodFormatRepository;
import com.mybclab.common.utils.MapUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

import static com.mybclab.cheetah.admin.pod.specification.PodFormatSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

@Service
@Slf4j
public class PodFormatService {

    private PodFormatRepository podFormatRepository;


    public PodFormatService() {}

    @Autowired
    public PodFormatService(PodFormatRepository podFormatRepository) {
        this.podFormatRepository = podFormatRepository;
    }

    public List<PodFormat> findAll() {
        return podFormatRepository.findAll();
    }

    /**
     * 파드 구성 목록 조회
     * @return 파드 구성 목록
     */
    public List<PodFormat> findAll(PodFormatCriteria podFormatCriteria) {
        return podFormatRepository.findAll(getPodFormatSpecification(podFormatCriteria), Sort.by(Sort.Direction.DESC, "createdAt"));
    }
    /**
     * 파드 구성 목록 조회
     * @param useYn 사용 여부
     * @return 파드 구성 목록
     */
    @Cacheable(cacheNames = "podFormat")
    public List<PodFormat> findAll(boolean useYn) {
        return podFormatRepository.findAllByUseYn(useYn);
    }

    /**
     * 파드 구성 저장
     * @param podFormat {@link PodFormat}
     * @return {@link PodFormat}
     */
    @Transactional
    @CacheEvict(cacheNames = "podFormat", allEntries = true)
    public PodFormat save(PodFormat podFormat) {
        podFormat = podFormatRepository.save(podFormat);
        final Long podFormatSn = podFormat.getPodFormatSn();
        // TODO .... 왜 안되는지 잘 모르겠음
        podFormat.getPodFormatGpuList().forEach(g -> g.getPodFormatGpuPK().setPodFormatSn(podFormatSn));
        podFormat.getPodFormatOptionList().forEach(o -> o.getPodFormatOptionPK().setPodFormatSn(podFormatSn));
        podFormatRepository.flush();
        return podFormat;
    }

    @Cacheable(cacheNames = "podFormat", key="#podFormatSn")
    public PodFormat findById(Long podFormatSn) {
        return podFormatRepository.findById(podFormatSn).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @CacheEvict(cacheNames = "podFormat", allEntries = true)
    public void deleteById(Long podFormatSn) {
        podFormatRepository.deleteById(podFormatSn);
    }

    public PodFormat findByPodOptionHint(Map<String, Long> podOptionSnMap) {
        return podFormatRepository.findByPodOptionHint(MapUtils.toStringSortByKey(podOptionSnMap)).orElseThrow(EntityNotFoundException::new);
    }


    /**
     * getPodFormatSpecification
     * @param podFormatCriteria {@link PodFormatCriteria}
     * @return {@link Specification < PodFormat >}
     */
    private Specification<PodFormat> getPodFormatSpecification(PodFormatCriteria podFormatCriteria) {
        Specification<PodFormat> where = where(likePodFormatName(podFormatCriteria.getPodFormatName()))
                .and(equalsGpuSn(podFormatCriteria.getGpuSn()));

        Map<String, Long> podOptionSnMap = podFormatCriteria.getPodOptionSnMap();
        if (podOptionSnMap != null) {
            for (String key : podOptionSnMap.keySet()) {
                where = where.and(inPodOptionSn(key, podOptionSnMap.get(key)));
            }
        }
        return where;
    }
    /**
     * 캐쉬 podFormat 전체 삭제
     */
    @CacheEvict(cacheNames = "podFormat", allEntries = true)
    public void cacheReloadAll() {
    }
}
