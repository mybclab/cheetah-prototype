package com.mybclab.cheetah.admin.credit.web;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.dto.CreditForm;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.container.dto.ContainerForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 크레딧 Controller
 */
@Slf4j
@Controller
@RequestMapping("/admin/credit")
public class CreditController {

    /**
     * {@link CreditService}
     */
    private CreditService creditService;

    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    /**
     * default constructor
     */
    public CreditController() {}

    /**
     * constructor
     * @param creditService {@link CreditService}
     * @param codeGroupService {@link CodeGroupService}
     * @param modelMapper {@link ModelMapper}
     */
    @Autowired
    public CreditController(CreditService creditService, CodeGroupService codeGroupService, ModelMapper modelMapper) {
        this.creditService = creditService;
        this.codeGroupService = codeGroupService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping({"", "/list"})
    public String list(Model model) {

        List<Credit> creditList = creditService.findAll();
        model.addAttribute("creditList", creditList);

        return "admin/credit/credit-list";
    }


    @GetMapping("/save")
    public String save(Model model) {
        model.addAttribute("creditForm", new CreditForm());
        return "admin/credit/credit-form";
    }

    @GetMapping("/{creditId}/modify")
    public String modify(@PathVariable String creditId, Model model) {
        Credit credit = creditService.findById(creditId);
        CreditForm creditForm = modelMapper.map(credit, CreditForm.class);
        model.addAttribute("creditForm", creditForm);
        return "admin/credit/credit-form";
    }

    @PostMapping({"/save", "/{creditId}/modify"})
    public String saveOrModify(CreditForm creditForm, BindingResult bindingResult) {

        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String requestedValue = builder.buildAndExpand().getPath();
        boolean isNew = requestedValue.contains("save");

        Credit credit = modelMapper.map(creditForm, Credit.class);

        if (isNew && isDuplicatedCreditId(creditForm)) {
            bindingResult.rejectValue("creditId", "Valid.existCreditId", "사용중인 크레딧 아이디 입니다.");
        }
        if (bindingResult.hasErrors()) {
            return "admin/credit/credit-form";
        }

        creditService.save(credit);



        return "redirect:/admin/credit";
    }

    @PostMapping("/{creditId}/delete")
    public String delete(@PathVariable String creditId) {
        creditService.deleteById(creditId);
        return "redirect:/admin/credit";
    }

    /**
     * 크레딧 아이디 중복확인
     *
     * @param creditForm {@link ContainerForm}
     * @return 크레딧 아이디 중복여부
     */
    private boolean isDuplicatedCreditId(@ModelAttribute CreditForm creditForm) {
        if (StringUtils.isNotBlank(creditForm.getCreditId())) {
            try {
                creditService.findById(creditForm.getCreditId());
            } catch (EntityNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("creditExpiryTypeList", codeGroupService.getCodeList(CodeGroupID.CREDIT_EXPIRY_TYPE));
        map.put("creditTypeList", codeGroupService.getCodeList(CodeGroupID.CREDIT_TYPE));


        return map;
    }

    /**
     * 캐쉬 credit 전체 삭제
     * @return credit 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        creditService.cacheReloadAll();
        return "redirect:/admin/credit/";
    }
}
