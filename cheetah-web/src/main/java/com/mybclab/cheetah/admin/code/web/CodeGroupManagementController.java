package com.mybclab.cheetah.admin.code.web;

import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.code.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 코드 그룹 Controller
 */
@Controller
@RequestMapping("/admin/code")
@SessionAttributes
public class CodeGroupManagementController {


    /**
     * {@link CodeGroupService}
     */
    private final CodeGroupService codeGroupService;

    /**
     * {@link CodeService}
     */
    private final CodeService codeService;

    /**
     * constructor
     * @param codeGroupService  {@link CodeGroupService}
     */
    @Autowired
    public CodeGroupManagementController(CodeGroupService codeGroupService, CodeService codeService) {
        this.codeGroupService = codeGroupService;
        this.codeService = codeService;
    }

    /**
     * 코드그룹 목록조회
     *
     * @param model         {@link Model}
     * @return 코드그룹 목록조회
     */
    @RequestMapping("")
    public String findAll(Model model) {
        model.addAttribute("codeGroupList", codeGroupService.findAll());

        return "admin/code/code-group-list";
    }

    /**
     * 코드그룹 조회
     *
     * @param codeGroupId   코드그룹아이디
     * @param model         {@link Model}
     * @return 코드그룹 조회
     */
    @GetMapping("/{codeGroupId}")
    public String findByCodeGroupId(@PathVariable String codeGroupId, Model model) {
        model.addAttribute("codeGroup", codeGroupService.findById(codeGroupId));
        //model.addAttribute("codeList", codeGroupService.getCodeList(CodeGroupId.ALERT_LEVEL));
        return "admin/code/code-group";
    }

    /**
     * code의 활성황 여부 변경
     * @param codeGroupId codeGroupId
     * @param codeId codeId
     * @return 코드그룹 조회
     */
    @PostMapping("/{codeGroupId}/{codeId}/activeYn")
    public String modifyCodeActiveYn(@PathVariable String codeGroupId, @PathVariable String codeId) {
        codeService.modifyActiveYn(codeGroupId, codeId);
        return "redirect:/admin/code/" + codeGroupId;
    }


    /**
     * 캐쉬 codeGroup 전체 삭제
     * @return 코드그룹 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        codeGroupService.cacheReloadAll();
        return "redirect:/admin/code/";
    }
    /**
     * 캐쉬 CodeGroup wnd codeGroupId만 삭제
     * @param codeGroupId 코드 그룹 ID
     * @return 코드그룹 조회
     */
    @GetMapping("/reloadCache/{codeGroupId}")
    public String cacheReloadAll(@PathVariable String codeGroupId) {
        codeGroupService.cacheReloadByCodeGroupId(codeGroupId);
        return "redirect:/admin/code/" + codeGroupId;
    }

}
