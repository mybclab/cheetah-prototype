package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

/**
 * 사용자 권한 PK
 */
@Data
@Embeddable
public class UserRoleId implements Serializable {
    /**
     * 사용자아이디
     */
    @Column
    private String username;

    /**
     * 권한 코드
     * {@link Code.ROLE}
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Code.ROLE roleCode;

    /**
     * default constructor
     */
    public UserRoleId() {
    }

    /**
     * constuctor
     * @param username 사용자아이디
     * @param roleCode 권한 코드
     */
    public UserRoleId(String username, Code.ROLE roleCode) {
        this.username = username;
        this.roleCode = roleCode;
    }
}