package com.mybclab.cheetah.admin.user.repository;

import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.admin.user.domain.UserCreditPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 사용자 크레딧 Repository
 */
@Repository
public interface UserCreditRepository extends JpaRepository<UserCredit, UserCreditPK> {
    /**
     * 사용자아이디로 사용자 크레딧 목록조회
     * @param username 사용자아이디
     * @return 크레딧 목록
     */
    List<UserCredit> findAllByUserCreditPKUsernameOrderByCreatedAtDesc(String username);

    /**
     * 사용자아이디로 유효한 크레딧 목록조회
     * (남은 금액 > 0 and (유효기간 is null or 현재날짜 between 유효기간 시작일 and 유효기간 종료일))
     * @param username 사용자아이디
     * @return 사용자 유효 크레딧 목록
     */
    @Query(value = "select c " +
            "From UserCredit c " +
            "where c.userCreditPK.username = :username" +
            "  and c.remainCreditAmount > 0 " +
            "  and (c.expiryEndDate is null" +
            "   or current_date() between c.expiryStartDate and c.expiryEndDate)" +
            "order by isNull(c.expiryEndDate) ASC, c.expiryEndDate ASC")
    List<UserCredit> findAllByValidCredit(@Param("username") String username);

    @Query(" select coalesce(max(c.userCreditPK.userCreditNo), 0) " +
            "  from UserCredit c" +
            " where c.userCreditPK.username = :username")
    int findMaxUserCreditNo(@Param("username") String username);
}
