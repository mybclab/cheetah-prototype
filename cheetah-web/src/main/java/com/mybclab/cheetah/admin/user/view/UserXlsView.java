package com.mybclab.cheetah.admin.user.view;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Component("userXlsView")
public class UserXlsView extends AbstractXlsView {

    private CellStyle cellStyle;


    private CheetahMessageSource messageSource;

    public UserXlsView(CheetahMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        String date = localDateTime.format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH);
        response.setHeader("Content-Disposition", "attachment; filename=\"user_list_" + date + ".xls\"");



        List<User> userList = (List<User>) model.get("userList");
        boolean isAdmin = (boolean) model.get("isAdmin");



        CellStyle numberCellStyle = workbook.createCellStyle();
        DataFormat numberDataFormat = workbook.createDataFormat();
        numberCellStyle.setDataFormat(numberDataFormat.getFormat("#,##0"));


        cellStyle = workbook.createCellStyle();

        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);


        Sheet sheet = workbook.createSheet("사용자목록");


        List<String> headerList = new ArrayList<String>() {{
            add("번호");
            if (isAdmin) add("권한");
            if (isAdmin) add("그룹");
            if (isAdmin) add("그룹명");
            if (isAdmin) add("그룹구분");
            if (isAdmin) add("그룹GPU(사용)");
            if (isAdmin) add("그룹GPU(할당)");
            add("사용자아이디");
            add("이름");
            add("단체(회사/학교)명");
            add("팀(부서/학과)명");
            add("이메일");
            add("전화번호");

            add("컨테이너(사용)");
            add("컨테이너(전체)");
            add("GPU(사용)");
            add("GPU(할당)");

            add("승인여부");
            add("생성일시");
            add("수정일시");
            add("탈퇴일시");
        }};
        createHead(sheet, headerList);

        for (int i = 0 ; i < userList.size() ; i++) {
            createRow(sheet, headerList, userList.get(i),i + 1,userList.size() - i);
        }


    }

    private void createHead(Sheet sheet, List<String> headList) {
        createRow(sheet, headList,0);
    }

    private void createRow(Sheet sheet, List<String> cellList, int rowNum) {
        Row row = sheet.createRow(rowNum);
        for (int i = 0; i < cellList.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(cellList.get(i));
            cell.setCellStyle(cellStyle);
        }
    }

    private void createRow(Sheet sheet, List<String> headerList, User user, int rowNum, int no) {
        Row row = sheet.createRow(rowNum);

        for (int i = 0; i < headerList.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(cellStyle);
            switch (headerList.get(i)) {
                case "번호" :
                    cell.setCellValue(no); break;
                case "그룹" :
                    String groupId = "-";
                    if (user.isGroupAdmin()) groupId = user.getUsername();
                    else if (user.isGroupUser()) groupId = user.getParentUsername();
                    cell.setCellValue(groupId); break;
                case "그룹명" :
                    String groupName = "-";
                    if (user.isGroupAdmin()) groupName = user.getGroupName();
                    else if (user.isGroupUser()) groupName = user.getGroup().getGroupName();
                    cell.setCellValue(groupName); break;
                case "그룹구분" :
                    Code.GROUP_TYPE groupType = null;
                    if (user.isGroupAdmin()) groupType = user.getGroupTypeCode();
                    else if (user.isGroupUser()) groupType = user.getGroup().getGroupTypeCode();
                    String groupTypeCodeName = "-";
                    if (groupType != null) {
                        groupTypeCodeName = messageSource.getMessage("code.GT." + groupType);
                    }
                    cell.setCellValue(groupTypeCodeName); break;
                case "그룹GPU(사용)" :
                    cell.setCellValue(user.isGroupAdmin() ? Integer.toString(user.getTotalGroupUseQuantity()) : "-"); break;
                case "그룹GPU(할당)" :
                    cell.setCellValue(user.isGroupAdmin() ? Integer.toString(user.getTotalGroupQuotaQuantity()) : "-"); break;
                case "권한" :
                    cell.setCellValue(user.getUserRoles().get(0).getRole().getCodeName()); break;
                case "사용자아이디" :
                    cell.setCellValue(user.getUsername()); break;
                case "이름" :
                    cell.setCellValue(user.getName()); break;
                case "단체(회사/학교)명" :
                    cell.setCellValue(user.getOrganizationName()); break;
                case "팀(부서/학과)명" :
                    cell.setCellValue(user.getTeamName()); break;
                case "이메일" :
                    cell.setCellValue(user.getEmail()); break;
                case "전화번호" :
                    cell.setCellValue(user.getPhoneNo()); break;
                case "승인여부" :
                    cell.setCellValue(user.getConfirmYn() ? "승인" : "미승인"); break;
                case "컨테이너(사용)" :
                    cell.setCellValue(user.getUseContainerCount()); break;
                case "컨테이너(전체)" :
                    cell.setCellValue(user.getContainerList().size()); break;
                case "GPU(사용)" :
                    cell.setCellValue(user.getTotalUseQuantity()); break;
                case "GPU(할당)" :
                    cell.setCellValue(user.getTotalQuotaQuantity()); break;
                case "생성일시" :
                    cell.setCellValue(user.getCreatedAt().atZone(ZoneId.of("UTC")).withZoneSameInstant(TimeZone.getTimeZone("Asia/Seoul").toZoneId()).toLocalDateTime()
                            .format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH)); break;
                case "수정일시" :
                    cell.setCellValue(user.getUpdatedAt().atZone(ZoneId.of("UTC")).withZoneSameInstant(TimeZone.getTimeZone("Asia/Seoul").toZoneId()).toLocalDateTime()
                            .format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH)); break;
                case "탈퇴일시" :
                    cell.setCellValue(user.getWithdrawalDate() == null ? "-" : user.getWithdrawalDate().atZone(ZoneId.of("UTC")).withZoneSameInstant(TimeZone.getTimeZone("Asia/Seoul").toZoneId()).toLocalDateTime()
                            .format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH)); break;
            }

            sheet.autoSizeColumn(i);
            sheet.setColumnWidth(i,(sheet.getColumnWidth(i)) + 512);
        }

    }




}
