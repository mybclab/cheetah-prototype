package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.code.domain.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 사용자 권한
 */
@Entity
@Table(name = "user_role")
@Getter
@Setter
@ToString(exclude = {"user"})
public class UserRole implements Serializable, GrantedAuthority {

    /**
     * {@link UserRoleId}
     */
    @EmbeddedId
    private UserRoleId userRoleId;

    /**
     * {@link Code}
     */
    @ManyToOne
    @JoinColumnsOrFormulas({
            @JoinColumnOrFormula(formula = @JoinFormula(value = "'UR'", referencedColumnName = "codeGroupId")),
            @JoinColumnOrFormula(column = @JoinColumn(name = "roleCode", referencedColumnName = "code", insertable = false, updatable = false))
    })
    private Code role;

    /**
     * {@link User}
     */
    @ManyToOne
    @JoinColumn(name = "username", insertable = false, updatable = false)
    private User user;

    /**
     * default constructor
     */
    public UserRole() {
    }

    /**
     * constructor
     * @param username 사용자아이디
     * @param roleCode 권한 코드
     */
    public UserRole(String username, com.mybclab.cheetah.common.constant.Code.ROLE roleCode) {
        this.userRoleId = new UserRoleId(username, roleCode);
    }

    /**
     * 권한 조회
     * @return 권한 코드 명
     */
    @Override
    public String getAuthority() {
        return this.userRoleId.getRoleCode().name();
    }
}
