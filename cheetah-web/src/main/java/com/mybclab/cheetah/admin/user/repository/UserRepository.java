package com.mybclab.cheetah.admin.user.repository;

import com.mybclab.cheetah.admin.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 사용자 Repository
 */
@Repository
public interface UserRepository extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

    /**
     * 사용자 목록조회 페이징
     * @param specification {@link Specification<User>}
     * @param pageable {@link Pageable}
     * @return 사용자 목록
     */
    Page<User> findAll(Specification<User> specification, Pageable pageable);

    /**
     * 그룹 사용자 목록조회
     * @param parentUsername 상위 사용자아이디
     * @return 그룹 사용자 목록
     */
    List<User> findAllByParentUsernameOrUsername(String parentUsername, String username);


    @Query(" select u " +
            " from User u, UserRole ur " +
            " where u.username = ur.user.username" +
            "   and ur.role.codePK.code = :roleCode")
    List<User> findAllByUserRole(@Param("roleCode") String roleCode);


    /**
     * 그룹명 중복 확인
     * @param groupName 그룹 명
     * @return 그룹명 중복 여부
     */
    boolean existsByGroupName(String groupName);

    boolean existsByGroupInviteKey(String groupInviteKey);

    Optional<User> findByUsernameAndConfirmYn(String username, boolean confirmYn);

    Optional<User> findByUsernameAndEmail(String username, String email);

    Optional<User> findByGroupInviteKey(String groupInviteKey);

    List<User> findAllByUsernameIn(String[] usernames);
}
