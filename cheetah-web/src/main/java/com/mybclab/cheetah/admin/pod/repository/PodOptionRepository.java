package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.PodOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PodOptionRepository extends JpaRepository<PodOption, Long> {

    List<PodOption> findAllByPodOptionGroupIdOrderByOdrAsc(String podOptionGroupId);
}
