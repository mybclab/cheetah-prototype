package com.mybclab.cheetah.admin.credit.service;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.repository.CreditRepository;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * 크레딧 Service
 */
@Service
public class CreditService implements Serializable {

    /**
     * {@link CreditRepository}
     */
    private CreditRepository creditRepository;

    /**
     * default constructor
     */
    public CreditService() {}

    /**
     * constructor
     * @param creditRepository {@link CreditRepository}
     */
    @Autowired
    public CreditService(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    /**
     * 크레딧 아이디 로 단건 조회
     * @param creditId 크레딧 아이디
     * @return {@link Credit}
     */
    public Credit findById(String creditId) {
        return creditRepository.findById(creditId).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * 저장
     * @param credit {@link Credit}
     * @return {@link Credit}
     */
    @Transactional
    @CacheEvict(cacheNames = "credit", allEntries = true)
    public Credit save(Credit credit) {
        return creditRepository.save(credit);
    }

    /**
     * existsById
     * @param creditId 크레딧 아이디
     * @return boolean
     */
    public boolean existsById(String creditId) {
        return creditRepository.existsById(creditId);
    }

    /**
     * 크레딧 목록조회
     * @return 크레딧 목록
     */
    public List<Credit> findAll() {
        return creditRepository.findAll();
    }

    /**
     * 크레딧 삭제
     * @param creditId 크레딧 아이디
     */
    @Transactional
    @CacheEvict(cacheNames = "credit", allEntries = true)
    public void deleteById(String creditId) {
        creditRepository.deleteById(creditId);
    }

    /**
     * 크레딧 TYPE 으로 조회
     * @param creditType {@link Code.CREDIT_TYPE}
     * @return 크레딧 목록
     */
    @Cacheable(cacheNames = "credit", key="#creditType")
    public List<Credit> findAllByCreditTypeCode(Code.CREDIT_TYPE creditType) {
        return creditRepository.findAllByCreditTypeCode(creditType);
    }
    /**
     * 캐쉬 credit 전체 삭제
     */
    @CacheEvict(cacheNames = "credit", allEntries = true)
    public void cacheReloadAll() {
    }
}
