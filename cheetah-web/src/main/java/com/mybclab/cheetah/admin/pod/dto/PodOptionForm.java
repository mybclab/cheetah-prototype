package com.mybclab.cheetah.admin.pod.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class PodOptionForm {


    @NotEmpty
    private String podOptionGroupId;

    private long podOptionSn;

    @NotEmpty
    private String podOptionId;

    @NotEmpty
    private String podOptionName;


    /**
     * 순서
     */
    private int odr;
    /**
     * 사용 여부
     */
    private Boolean useYn;


}
