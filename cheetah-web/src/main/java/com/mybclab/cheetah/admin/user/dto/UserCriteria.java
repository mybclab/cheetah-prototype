package com.mybclab.cheetah.admin.user.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import java.io.Serializable;

/**
 * 사용자 Criteria
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class UserCriteria implements Serializable {
    /**
     * 상위 사용자아이디
     */
    private String parentUsername;

    /**
     * 이름
     */
    private String name;

    /**
     * {@link Code.ROLE}
     */
    private Code.ROLE userRole;


    private Boolean isGroup;

    /**
     * 승인 여부
     */
    @Builder.Default
    private Boolean confirmYn = false;

    /**
     * 탈퇴 여부
     */
    @Builder.Default
    private Boolean withdrawalYn = false;

}
