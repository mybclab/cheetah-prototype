package com.mybclab.cheetah.admin.pod.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class PodFormatCriteria {

    private String podFormatName;

    private Long podOptionOsSn;
    private Long podOptionJupyterSn;
    private Long podOptionTensorflowSn;

    private Long gpuSn;

    private Map<String, Long> podOptionSnMap;

}
