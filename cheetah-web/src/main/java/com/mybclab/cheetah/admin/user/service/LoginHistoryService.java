package com.mybclab.cheetah.admin.user.service;

import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.admin.user.dto.LoginHistoryCriteria;
import com.mybclab.cheetah.admin.user.repository.LoginHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.mybclab.cheetah.admin.user.specification.LoginHistorySpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

@Service
@Slf4j
public class LoginHistoryService {

    private LoginHistoryRepository loginHistoryRepository;

    @Autowired
    public LoginHistoryService(LoginHistoryRepository loginHistoryRepository) {
        this.loginHistoryRepository = loginHistoryRepository;
    }

    public List<LoginHistory> findAll() {
        return loginHistoryRepository.findAll();
    }


    public Page<LoginHistory> findAll(LoginHistoryCriteria loginHistoryCriteria, Pageable pageable) {
        return loginHistoryRepository.findAll(getLoginHistorySpecification(loginHistoryCriteria), pageable);
    }

    public List<LoginHistory> findAll(LoginHistoryCriteria loginHistoryCriteria, Sort sort) {
        return loginHistoryRepository.findAll(getLoginHistorySpecification(loginHistoryCriteria), sort);
    }

    private Specification<LoginHistory> getLoginHistorySpecification(LoginHistoryCriteria loginHistoryCriteria) {
        return where(equalsUsername(loginHistoryCriteria.getUsername()))
                .and(equalsParentUsername(loginHistoryCriteria.getParentUsername()))
                .and(likeName(loginHistoryCriteria.getName()))
                .and(equalsLoginHistoryCode(loginHistoryCriteria.getLoginHistoryCode()))
                .and(likeLoginIp(loginHistoryCriteria.getLoginIp()))
                .and(rangeLoginDate(loginHistoryCriteria));
    }
}
