package com.mybclab.cheetah.admin.gpu.service;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.repository.GpuRepository;
import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * GPU Service
 */
@Service
@Slf4j
public class GpuService implements Serializable {
    /**
     * {@link GpuRepository}
     */
    private GpuRepository gpuRepository;

    private ContainerRepository containerRepository;

    private NodeService nodeService;

    /**
     * default constructor
     */
    public GpuService() {}

    /**
     * constructor
     * @param gpuRepository {@link GpuRepository}
     * @param containerRepository {@link ContainerRepository}
     * @param nodeService {@link NodeService}
     */
    @Autowired
    public GpuService(GpuRepository gpuRepository, ContainerRepository containerRepository, NodeService nodeService) {
        this.gpuRepository = gpuRepository;
        this.containerRepository = containerRepository;
        this.nodeService = nodeService;
    }


    /**
     * GPU 목록 조회
     * @return GPU 목록
     */
    public List<Gpu> findAll() {
        List<Gpu> gpuList = gpuRepository.findAll();
        for (Gpu gpu : gpuList) {
            setExtraInfo(gpu);
        }
        return gpuList;
    }
    /**
     * GPU 목록 조회
     * @param useYn 사용 여부
     * @return GPU 목록
     */
    @Cacheable(cacheNames = "gpu")
    public List<Gpu> findAll(boolean useYn) {
        List<Gpu> gpuList = gpuRepository.findAllByUseYn(useYn);
        for (Gpu gpu : gpuList) {
            setExtraInfo(gpu);
        }
        return gpuList;
    }

    /**
     * 저장
     * @param gpu {@link Gpu}
     * @return {@link Gpu}
     */
    @Transactional
    @CacheEvict(cacheNames = "gpu", allEntries = true)
    public Gpu save(Gpu gpu) {
        return gpuRepository.save(gpu);
    }

    @Cacheable(cacheNames = "gpu", key = "#gpuSn")
    public Gpu findById(Long gpuSn) {
        Gpu gpu = gpuRepository.findById(gpuSn).orElseThrow(EntityNotFoundException::new);
        setExtraInfo(gpu);
        return gpu;
    }

    private void setExtraInfo(Gpu gpu) {
        List<Container> useGpuContainerList = containerRepository.findAllByGpuSnAndStatusCode(gpu.getGpuSn(), Code.CONTAINER_STATUS.START);
        gpu.setUseGpuQuantity(useGpuContainerList.stream().mapToInt(Container::getGpuQuantity).sum());
        List<Node> nodeList = nodeService.findAllByGpuLabel(gpu.getGpuLabel());
        gpu.setTotalGpuQuantity(nodeList.stream().mapToInt(Node::getGpuQuantity).sum());
        gpu.setRemainMaxGpuQuantityPerNode(nodeList.stream().mapToInt(n -> n.getGpuQuantity() - n.getUseGpuQuantity()).max().orElse(0));
    }

    @CacheEvict(cacheNames = "gpu", allEntries = true)
    public void deleteById(Long gpuSn) {
        gpuRepository.deleteById(gpuSn);
    }

    /**
     * 캐쉬 gpu 전체 삭제
     */
    @CacheEvict(cacheNames = "gpu", allEntries = true)
    public void cacheReloadAll() {
    }
}
