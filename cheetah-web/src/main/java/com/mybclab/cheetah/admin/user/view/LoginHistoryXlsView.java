package com.mybclab.cheetah.admin.user.view;

import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Component("loginHistoryXlsView")
public class LoginHistoryXlsView extends AbstractXlsView {

    private CellStyle cellStyle;


    private CheetahMessageSource messageSource;

    public LoginHistoryXlsView(CheetahMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        String date = localDateTime.format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH);
        response.setHeader("Content-Disposition", "attachment; filename=\"login_history_list_" + date + ".xls\"");



        List<LoginHistory> loginHistoryList = (List<LoginHistory>) model.get("loginHistoryList");


        CellStyle numberCellStyle = workbook.createCellStyle();
        DataFormat numberDataFormat = workbook.createDataFormat();
        numberCellStyle.setDataFormat(numberDataFormat.getFormat("#,##0"));


        cellStyle = workbook.createCellStyle();

        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);


        Sheet sheet = workbook.createSheet("로그인이력");


        List<String> headerList = new ArrayList<String>() {{
            add("번호");
            add("그룹");
            add("사용자아이디");
            add("이름");

            add("로그인 코드");
            add("로그인 아이피");
            add("로그인 일시");
        }};
        createHead(sheet, headerList);

        for (int i = 0 ; i < loginHistoryList.size() ; i++) {
            createRow(sheet, headerList, loginHistoryList.get(i), i + 1, loginHistoryList.size() - i);
        }


    }

    private void createHead(Sheet sheet, List<String> headList) {
        createRow(sheet, headList,0);
    }

    private void createRow(Sheet sheet, List<String> cellList, int rowNum) {
        Row row = sheet.createRow(rowNum);
        for (int i = 0; i < cellList.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(cellList.get(i));
            cell.setCellStyle(cellStyle);
        }
    }

    private void createRow(Sheet sheet, List<String> headerList, LoginHistory loginHistory, int rowNum, int no) {
        Row row = sheet.createRow(rowNum);

        for (int i = 0; i < headerList.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(cellStyle);
            int valueLength = 0;
            switch (headerList.get(i)) {
                case "번호" :
                    cell.setCellValue(no);
                    valueLength = Double.toString(cell.getNumericCellValue()).length(); break;
                case "그룹" :
                    String groupId = "-";
                    if (loginHistory.getUser().isGroupAdmin()) groupId = loginHistory.getUser().getUsername();
                    else if (loginHistory.getUser().isGroupUser()) groupId = loginHistory.getUser().getParentUsername();
                    cell.setCellValue(groupId);
                    valueLength = cell.getStringCellValue().length(); break;
                case "사용자아이디" :
                    cell.setCellValue(loginHistory.getUsername());
                    valueLength = cell.getStringCellValue().length(); break;
                case "이름" :
                    cell.setCellValue(loginHistory.getUser().getName());
                    valueLength = cell.getStringCellValue().length(); break;
                case "로그인 코드" :
                    Code.LOGIN_HISTORY loginHistoryCode = loginHistory.getLoginHistoryCode();
                    String loginHistoryCodeName = "-";
                    if (loginHistoryCode != null) {
                        loginHistoryCodeName = messageSource.getMessage("code.LH." + loginHistoryCode);
                    }
                    cell.setCellValue(loginHistoryCodeName);
                    valueLength = cell.getStringCellValue().length(); break;
                case "로그인 아이피" :
                    cell.setCellValue(loginHistory.getLoginIp());
                    valueLength = cell.getStringCellValue().length(); break;
                case "로그인 일시" :
                    cell.setCellValue(loginHistory.getLoginDate().atZone(ZoneId.of("UTC")).withZoneSameInstant(TimeZone.getTimeZone("Asia/Seoul").toZoneId()).toLocalDateTime()
                            .format(CheetahFormatter.FOR_FULL_DATE_WITH_DASH));
                    valueLength = cell.getStringCellValue().length(); break;
            }
            try {
                sheet.autoSizeColumn(i);
            } catch (NullPointerException e) {
                sheet.setColumnWidth(i, valueLength * 1000);
            }
            sheet.setColumnWidth(i,(sheet.getColumnWidth(i)) + 512);
        }

    }




}
