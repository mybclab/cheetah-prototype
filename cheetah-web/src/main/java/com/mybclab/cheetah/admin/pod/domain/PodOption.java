package com.mybclab.cheetah.admin.pod.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.common.domain.HistoryBase;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@ToString(exclude = "podOptionGroup")
public class PodOption extends HistoryBase implements Serializable {

    /**
     * 파드 옵션 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long podOptionSn;

    /**
     * 파드 옵션 그룹 아이디
     */
    private String podOptionGroupId;

    /**
     * 파드 옵션 아이디
     */
    private String podOptionId;

    /**
     * 파드 옵션 명
     */
    private String podOptionName;

    /**
     * 순서
     */
    private int odr;
    /**
     * 사용 여부
     */
    private Boolean useYn;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "podOptionGroupId", referencedColumnName = "podOptionGroupId", insertable = false, updatable = false)
    private PodOptionGroup podOptionGroup;
}
