package com.mybclab.cheetah.admin.user.web;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.user.domain.*;
import com.mybclab.cheetah.admin.user.dto.UserCriteria;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.dto.UserGpuForm;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.validator.UserFormValidator;
import com.mybclab.cheetah.admin.user.view.UserXlsView;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.common.dto.AlertMessage;
import com.mybclab.cheetah.common.exception.StorageException;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.ArrayUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 사용자 Controller
 */
@Slf4j
@RequestMapping("/{path:admin|management}/user")
@Controller
public class UserController {

    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link CreditService}
     */
    private CreditService creditService;

    /**
     * {@link UserCreditService}
     */
    private UserCreditService userCreditService;
    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    private GpuService gpuService;
    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;
    /**
     * {@link UserFormValidator}
     */
    private UserFormValidator userFormValidator;


    private CheetahMessageSource messageSource;


    /**
     * default constructor
     */
    public UserController() {}

    /**
     * constructor
     */
    @Autowired
    public UserController(UserService userService, CreditService creditService, UserCreditService userCreditService
            , CodeGroupService codeGroupService, GpuService gpuService, ModelMapper modelMapper, UserFormValidator userFormValidator, CheetahMessageSource messageSource) {
        this.userService = userService;
        this.creditService = creditService;
        this.userCreditService = userCreditService;
        this.codeGroupService = codeGroupService;
        this.gpuService = gpuService;
        this.modelMapper = modelMapper;
        this.userFormValidator = userFormValidator;
        this.messageSource = messageSource;
    }

    /**
     * 사용자 목록
     * @param path path
     * @param userCriteria {@link UserCriteria}
     * @param pageable {@link Pageable}
     * @param model {@link Model}
     * @return 사용자 목록
     */
    @RequestMapping({"", "/list"})
    public String list(@PathVariable String path, UserCriteria userCriteria, @PageableDefault(sort = { "createdAt" }, direction = Sort.Direction.DESC) Pageable pageable, Model model) {
        User user = SecurityUtils.getPrincipal();

        if ("management".equals(path)) {
            userCriteria.setParentUsername(user.getUsername());
        }
        model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
        Page<User> all = userService.findAll(userCriteria, pageable);
        model.addAttribute("userPage", all);
        model.addAttribute("path", path);

        return "admin/user/user-list";
    }

    @RequestMapping(path={"", "/list"}, params="xls")
    public View listXls(@PathVariable String path, UserCriteria userCriteria, Model model) {
        User user = SecurityUtils.getPrincipal();
        if ("management".equals(path)) {
            userCriteria.setParentUsername(user.getUsername());
        }

        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        List<User> userList = userService.findAll(userCriteria, sort);
        model.addAttribute("userList", userList);
        model.addAttribute("isAdmin", "admin".equals(path));

        return new UserXlsView(messageSource);
    }

    @RequestMapping("/{username}")
    public String user(@PathVariable String path, @ModelAttribute User user, Model model) {

        user = userService.findById(user.getUsername());

        List<Credit> creditList = creditService.findAll();

        model.addAttribute("path", path);
        model.addAttribute("user", user);
        model.addAttribute("creditList", creditList);
        return "admin/user/user";
    }

    @GetMapping("/{username}/modify")
    public String modify(@PathVariable String path, @ModelAttribute User user, Model model) {
        user = userService.findById(user.getUsername());

        UserForm userForm = modelMapper.map(user, UserForm.class);

        userForm.setUserGpuFormList(userGpuFormList(path, user));

        model.addAttribute("user", user);
        model.addAttribute("userForm", userForm);
        model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));

        return "admin/user/user-form";
    }

    private List<UserGpuForm> userGpuFormList(String path, User user) {
        List<UserGpu> userGpuList = user.getUserGpuList();

        List<Gpu> gpuList = gpuService.findAll(true);
        List<UserGpu> groupUserGpuList = user.isGroupUser() ? user.getGroup().getUserGpuList() : user.getUserGpuList();
        if ("management".equals(path)) {
            List<Long> groupGpuSns = groupUserGpuList.stream().filter(ug -> ug.getGroupQuotaQuantity() > 0).map(ug -> ug.getUserGpuPK().getGpuSn()).collect(Collectors.toList());
            gpuList = gpuList.stream().filter(g -> ArrayUtils.contains(groupGpuSns.toArray(), g.getGpuSn())).collect(Collectors.toList());
        }

        List<UserGpuForm> userGpuFormList = new ArrayList<>();
        for (Gpu gpu : gpuList) {
            UserGpuForm userGpuForm = new UserGpuForm();
            userGpuForm.setGpuSn(gpu.getGpuSn());
            userGpuForm.setGpuName(gpu.getGpuName());
            userGpuForm.setGroupQuotaQuantity(userGpuList.stream().filter(ug -> ug.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn())).mapToInt(UserGpu::getGroupQuotaQuantity).sum());
            userGpuForm.setQuotaQuantity(userGpuList.stream().filter(ug -> ug.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn())).mapToInt(UserGpu::getQuotaQuantity).sum());
            if (user.isGroupAdmin() || user.isGroupUser()) {
                userGpuForm.setTotalGroupQuotaQuantity(groupUserGpuList.stream().filter(ug -> ug.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn())).mapToInt(UserGpu::getGroupQuotaQuantity).sum());

                List<User> groupUserList = user.isGroupAdmin() ? user.getGroupUsersIncludeAdmin() : user.getGroup().getGroupUsersIncludeAdmin();
                int useQuotaQuantity = 0;
                for (User groupUser : groupUserList) {
                    useQuotaQuantity += groupUser.getUserGpuList().stream().filter(ug -> ug.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn())).mapToLong(UserGpu::getQuotaQuantity).sum();
                }
                userGpuForm.setRemainGroupQuotaQuantity(userGpuForm.getTotalGroupQuotaQuantity() - useQuotaQuantity);
            }

            userGpuFormList.add(userGpuForm);
        }
        return userGpuFormList;
    }


    @PostMapping("/{username}/modify")
    public String modifyUser(@PathVariable String path, @PathVariable String username, @ModelAttribute @Valid UserForm userForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        User loginUser = SecurityUtils.getPrincipal();
        User user = userService.findById(username);

        userForm.setOrigGroupName(user.getGroupName());

        if (user.isGroupAdmin()) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationSignUpForGroupAdmin.class, UserForm.ValidationGroupForGpuQuantity.class);
        }

        checkGroupQuotaQuantity(user, userForm, (index, remainGroupQuotaQuantity) -> {
            log.error("reject : userGpuFormList[" + index + "].quotaQuantity : " + remainGroupQuotaQuantity);
            bindingResult.rejectValue("userGpuFormList[" + index + "].quotaQuantity", "Valid.overGroupMaxQuantity", new Integer[]{remainGroupQuotaQuantity}, "그룹의 잔여 할당량({0})을 초과하였습니다.");
        });

        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationAdmin.class);
        if (bindingResult.hasErrors()) {
            log.debug(">bindingResult : {}", bindingResult.getAllErrors());
            model.addAttribute("user", user);
            userForm.setUserGpuFormList(userGpuFormList(path, user));
            model.addAttribute("userForm", userForm);
            model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));
            return "admin/user/user-form";
        }

        if (user.isGroupAdmin()) {
            user.setGroupTypeCode(userForm.getGroupTypeCode());
            user.setGroupName(userForm.getGroupName());
            user.setGroupImageFileSn(userForm.getGroupImageFileSn());
        }

        user.setName(userForm.getName());
        user.setPhoneNo(userForm.getPhoneNo());
        user.setEmail(userForm.getEmail());
        user.setSlackHookUrl(userForm.getSlackHookUrl());
        user.setSlackChannel(userForm.getSlackChannel());
        user.setOrganizationName(userForm.getOrganizationName());
        user.setTeamName(userForm.getTeamName());
        user.setUserImageFileSn(userForm.getUserImageFileSn());

        user.setConfirmYn(userForm.getConfirmYn());

        user.setUpdatedBy(loginUser.getUsername());
        user.setUpdatedAt(LocalDateTime.now());

        user.getUserGpuList().clear();
        user.getUserGpuList().addAll(userGpuList(userForm, loginUser.getUsername()));

        try {
            userService.saveWithFile(user, userForm.getGroupImageFile(), userForm.getUserImageFile());
        } catch (StorageException e) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.ERROR).messageCode(e.getMessage()).build());
        }

        return "redirect:/" + path + "/user/" + username;
    }

    private boolean checkGroupQuotaQuantity(User user, UserForm userForm, WhenBindingFailed doWhenFail) {
        if (user.isGroupUser() || user.isGroupAdmin()) {
            List<UserGpuForm> userGpuFormList = userForm.getUserGpuFormList();
            for (int i = 0; i < userGpuFormList.size(); i++) {
                UserGpuForm userGpuForm = userGpuFormList.get(i);
                int alreadyQuotaQuantity = user.getUserGpuList().stream().filter(ug -> ug.getUserGpuPK().getGpuSn().equals(userGpuForm.getGpuSn())).mapToInt(UserGpu::getQuotaQuantity).sum();
                log.info(userGpuForm.getQuotaQuantity() + " > alreadyQuotaQuantity : " + alreadyQuotaQuantity + " + " + userGpuForm.getRemainGroupQuotaQuantity());
                if (userGpuForm.getQuotaQuantity() > userGpuForm.getRemainGroupQuotaQuantity() + alreadyQuotaQuantity) {
                    doWhenFail.doSomething(i, userGpuForm.getRemainGroupQuotaQuantity());
                    return false;
                }
            }
        }
        return true;
    }


    private List<UserGpu> userGpuList(UserForm userForm, String createdBy) {
        List<UserGpu> userGpuList = new ArrayList<>();
        for (UserGpuForm userGpuForm : userForm.getUserGpuFormList()) {
            if (userGpuForm.getGpuSn() != null && (userGpuForm.getGroupQuotaQuantity() > 0 || userGpuForm.getQuotaQuantity() > 0)) {
                UserGpu userGpu = new UserGpu(new UserGpuPK(userForm.getUsername(), userGpuForm.getGpuSn()));
                userGpu.setGroupQuotaQuantity(userGpuForm.getGroupQuotaQuantity());
                userGpu.setQuotaQuantity(userGpuForm.getQuotaQuantity());
                userGpu.setCreatedBy(createdBy);
                userGpu.setCreatedAt(LocalDateTime.now());
                userGpuList.add(userGpu);
            }
        }
        return userGpuList;
    }


    public interface WhenBindingFailed {
        void doSomething(int index, int groupGpuMaxQuotaQuantity);
    }

    /**
     * 사용자 승인(승인/할당량 변경)
     * @param path path
     * @param user {@link User}
     * @return redirect 그룹 사용자 관리
     */
    @PostMapping("/{username}/update")
    public String confirmOrUpdateUser(@PathVariable String path, @ModelAttribute User user, RedirectAttributes redirectAttributes) {
        user = userService.findById(user.getUsername());

        String returnPath = "redirect:/" + path + "/user";

        user.setConfirmYn(true);
        if (user.isGroupAdmin()) {
            returnPath = "redirect:/admin/group";
        }
        userService.confirm(user);

        return returnPath;
    }


    /**
     * 사용자에 크레딧 추가
     * @param path path
     * @param userCredit {@link UserCredit}
     * @return 사용자 상세조회
     */
    @PostMapping("/{username}/credit/add")
    public String addCredit(@PathVariable String path, @PathVariable String username, @ModelAttribute UserCredit userCredit) {
        userCreditService.addCreditToUser(username, userCredit.getCreditId());

        return "redirect:/" + path + "/user/" + username;
    }

    @PostMapping("/{username}/credit/{userCreditNo}/delete")
    public String deleteCredit(@PathVariable String path, @PathVariable String username, @PathVariable int userCreditNo) {

        userCreditService.deleteById(new UserCreditPK(username, userCreditNo));
        return "redirect:/" + path + "/user/" + username;
    }

    @PostMapping("/{username}/role/{role}")
    public String changeRoleToGroupAdmin(@PathVariable String path, @PathVariable String username, @PathVariable Code.ROLE role) {
        User loginUser = SecurityUtils.getPrincipal();

        User user = userService.findById(username);
        user.getUserRoles().clear();
        user.getUserRoles().addAll(new ArrayList<UserRole>() {{
            add(new UserRole(user.getUsername(), role));
        }});
        if (Code.ROLE.GROUP_ADMIN.equals(role)) {
            user.setGroupName(user.getUsername() + "-group");
            user.setGroupTypeCode(Code.GROUP_TYPE.SCHOOL);
            user.setGroupInviteKey(userService.generateGroupInviteKey());
        }
        user.setUpdatedBy(loginUser.getUsername());
        user.setUpdatedAt(LocalDateTime.now());

        userService.save(user);

        return "redirect:/" + path + "/user/" + username;
    }

    @PostMapping("/{username}/delete")
    public String deleteUser(@PathVariable String path, @PathVariable String username, RedirectAttributes redirectAttributes) {
        User user = userService.findById(username);
        if (user.getContainerList().size() > 0) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.ERROR).messageCode("Valid.delete.user.existContainer").build());
            return "redirect:/" + path + "/user/" + username;
        } else {
            userService.delete(user);
        }

        return "redirect:/" + path + "/user/";
    }

    @PostMapping("/{username}/withdrawal")
    public String withdrawal(@PathVariable String username, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        User user = userService.findById(username);

        // 실행중인 컨테이너 체크
        int runningContainerCount = user.isGroupAdmin() ? user.getGroupContainerCount() : user.getUseContainerCount();
        if (runningContainerCount > 0) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.ERROR).messageCode("Valid.withdrawal.user.existContainer").build());
            return "redirect:/management/mypage";
        }

        userService.withdrawal(user);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "redirect:/login";
    }

}
