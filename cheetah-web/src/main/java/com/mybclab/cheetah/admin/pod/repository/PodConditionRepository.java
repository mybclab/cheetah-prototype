package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.PodCondition;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PodConditionRepository extends JpaRepository<PodCondition, Long> {

    boolean existsByPodNameAndTypeCodeAndLastTransitionTime(String podName, Code.POD_CONDITION_TYPE typeCode, LocalDateTime lastTransitionTime);
    void deleteByUsernameAndContainerId(String username, String containerId);

    List<PodCondition> findAllByUsernameAndContainerId(String username, String containerId);
}
