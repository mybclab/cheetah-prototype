package com.mybclab.cheetah.admin.node.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import java.io.Serializable;

/**
 * 노드 Criteria
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class NodeCriteria implements Serializable {

    /**
     * 노드 명
     */
    private String nodeName;

    /**
     * 내부 도메인
     */
    private String internalDomain;

    /**
     * 내부 아이피
     */
    private String internalIp;

    /**
     * 외부 아이피
     */
    private String externalIp;

    /**
     * 상태 코드
     * {@link Code.NODE_STATUS}
     */
    private Code.NODE_STATUS statusCode;
}
