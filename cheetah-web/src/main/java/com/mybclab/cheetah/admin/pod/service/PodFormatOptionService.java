package com.mybclab.cheetah.admin.pod.service;

import com.mybclab.cheetah.admin.pod.domain.PodFormatOption;
import com.mybclab.cheetah.admin.pod.repository.PodFormatOptionRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class PodFormatOptionService {
    private PodFormatOptionRepository podFormatOptionRepository;

    @Autowired
    public PodFormatOptionService(PodFormatOptionRepository podFormatOptionRepository) {
        this.podFormatOptionRepository = podFormatOptionRepository;
    }

    public List<PodFormatOption> findAllByPodFormatSn(long podFormatSn) {
        return podFormatOptionRepository.findAllByPodFormatOptionPKPodFormatSn(podFormatSn);
    }
}
