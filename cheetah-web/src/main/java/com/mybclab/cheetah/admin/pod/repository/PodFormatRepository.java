package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import net.bytebuddy.implementation.auxiliary.AuxiliaryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PodFormatRepository extends JpaRepository<PodFormat, Long>, JpaSpecificationExecutor<PodFormat> {

    /**
     * 파드 구성 목록조회
     * @param useYn 사용 여부
     * @return 파드 구성 목록
     */
    List<PodFormat> findAllByUseYn(Boolean useYn);


    Optional<PodFormat> findByPodOptionHint(String podOptionHint);


}