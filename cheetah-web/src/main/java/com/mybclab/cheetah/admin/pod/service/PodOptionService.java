package com.mybclab.cheetah.admin.pod.service;

import com.mybclab.cheetah.admin.pod.domain.PodOption;
import com.mybclab.cheetah.admin.pod.repository.PodOptionRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Slf4j
@Service
@NoArgsConstructor
public class PodOptionService {
    private PodOptionRepository podOptionRepository;

    @Autowired
    public PodOptionService(PodOptionRepository podOptionRepository) {
        this.podOptionRepository = podOptionRepository;
    }

    @Cacheable(cacheNames = "podOption", key="#podOptionSn")
    public PodOption findById(long podOptionSn) {
        return podOptionRepository.findById(podOptionSn).orElseThrow(EntityNotFoundException::new);
    }

    public List<PodOption> findAllByPodOptionGroupId(String podOptionGroupId) {
        return podOptionRepository.findAllByPodOptionGroupIdOrderByOdrAsc(podOptionGroupId);
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = "podOptionGroup", allEntries = true),
            @CacheEvict(cacheNames = "podOption", allEntries = true)
    })
    public PodOption save(PodOption podOption) {
        return podOptionRepository.saveAndFlush(podOption);
    }
    @Caching(evict = {
            @CacheEvict(cacheNames = "podOptionGroup", allEntries = true),
            @CacheEvict(cacheNames = "podOption", allEntries = true)
    })
    public void delete(long podOptionSn) {
        podOptionRepository.deleteById(podOptionSn);
        podOptionRepository.flush();
    }
    /**
     * 캐쉬 podOption. podOptionGroup 전체 삭제
     */
    @Caching(evict = {
            @CacheEvict(cacheNames = "podOptionGroup", allEntries = true),
            @CacheEvict(cacheNames = "podOption", allEntries = true)
    })
    public void cacheReloadAll() {
    }
}
