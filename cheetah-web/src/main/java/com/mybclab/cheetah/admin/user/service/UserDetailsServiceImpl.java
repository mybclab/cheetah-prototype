package com.mybclab.cheetah.admin.user.service;

import com.mybclab.cheetah.admin.user.domain.LoginHistory;
import com.mybclab.cheetah.admin.user.repository.LoginHistoryRepository;
import com.mybclab.cheetah.admin.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    /**
     * {@link UserRepository}
     */
    private final UserRepository userRepository;

    private final LoginHistoryRepository loginHistoryRepository;

    public UserDetailsServiceImpl(UserRepository userRepository, LoginHistoryRepository loginHistoryRepository) {
        this.userRepository = userRepository;
        this.loginHistoryRepository = loginHistoryRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findById(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    public LoginHistory addLoginHistory(LoginHistory loginHistory) {
        return loginHistoryRepository.save(loginHistory);
    }
}
