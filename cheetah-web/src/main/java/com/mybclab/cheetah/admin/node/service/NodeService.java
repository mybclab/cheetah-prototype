package com.mybclab.cheetah.admin.node.service;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.dto.NodeCriteria;
import com.mybclab.cheetah.admin.node.repository.NodeRepository;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

import static com.mybclab.cheetah.admin.node.specification.NodeSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

/**
 * 노드 Service
 */
@Slf4j
@Service
public class NodeService {

    /**
     * {@link NodeRepository}
     */
    private NodeRepository nodeRepository;

    private ContainerRepository containerRepository;

    /**
     * default constructor
     */
    public NodeService() {}

    /**
     * constructor
     * @param nodeRepository {@link NodeRepository}
     */
    @Autowired
    public NodeService(NodeRepository nodeRepository, ContainerRepository containerRepository) {
        this.nodeRepository = nodeRepository;
        this.containerRepository = containerRepository;
    }

    /**
     * 저장
     * @param node {@link Node}
     * @return {@link Node}
     */
    @Transactional
    @CacheEvict(cacheNames = "node", allEntries = true)
    public Node save(Node node) {
        return nodeRepository.saveAndFlush(node);
    }

    /**
     * 노드 일련번호로 단건조회
     * @param nodeSn 노드 일련번호
     * @return {@link Node}
     */
    public Node findById(Long nodeSn) {
        Node node = nodeRepository.findById(nodeSn).orElseThrow(EntityNotFoundException::new);
        setExtraInfo(node);
        return node;
    }

    /**
     * 노드 목록조회
     * @param nodeCriteria {@link NodeCriteria}
     * @param pageable {@link Pageable}
     * @return {@link Page<Node>}
     */
    public Page<Node> findAll(NodeCriteria nodeCriteria, Pageable pageable) {
        Page<Node> nodePage = nodeRepository.findAll(getNodeSpecification(nodeCriteria), pageable);
        for (Node node : nodePage.getContent()) {
            setExtraInfo(node);
        }
        return nodePage;
    }

    private void setExtraInfo(Node node) {
        List<Container> useGpuContainerList = containerRepository.findAllByNodeSnAndStatusCode(node.getNodeSn(), Code.CONTAINER_STATUS.START);
        node.setUseContainerQuantity(useGpuContainerList.size());
        node.setUseGpuQuantity(useGpuContainerList.stream().mapToInt(Container::getGpuQuantity).sum());
    }

    public List<Node> findAll() {
        List<Node> nodeList = nodeRepository.findAll();
        for (Node node : nodeList) {
            setExtraInfo(node);
        }
        return nodeList;
    }

    /**
     * getNodeSpecification
     * @param nodeCriteria {@link NodeCriteria}
     * @return {@link Specification<Node>}
     */
    private Specification<Node> getNodeSpecification(NodeCriteria nodeCriteria) {
        return where(likeNodeName(nodeCriteria.getNodeName()))
                .and(likeInternalDomain(nodeCriteria.getInternalDomain()))
                .and(likeExternalIp(nodeCriteria.getInternalIp()))
                .and(equalsStatusCode(nodeCriteria.getStatusCode()));
    }

    @Cacheable(cacheNames = "node")
    public List<Node> findAllByStatusCode(Code.NODE_STATUS nodeStatus) {
        List<Node> nodeList = nodeRepository.findAllByStatusCode(nodeStatus);
        for (Node node : nodeList) {
            setExtraInfo(node);
        }
        return nodeList;
    }

    /**
     * 노드 삭제
     * @param nodeSn 노드 일련번호
     */
    @Transactional
    @CacheEvict(cacheNames = "node", allEntries = true)
    public void delete(Long nodeSn) {
        nodeRepository.deleteById(nodeSn);
        nodeRepository.flush();
    }

    public Node findOneByNodeName(String nodeName) {
        Node node = nodeRepository.findOneByNodeName(nodeName).orElseThrow(EntityNotFoundException::new);
        setExtraInfo(node);
        return node;
    }

    public List<Node> findAllByGpuLabel(String gpuLabel) {
        List<Node> nodeList = nodeRepository.findAllByGpuLabelAndStatusCode(gpuLabel, Code.NODE_STATUS.READY);
        for (Node node : nodeList) {
            setExtraInfo(node);
        }
        return nodeList;
    }
    /**
     * 캐쉬 node 전체 삭제
     */
    @CacheEvict(cacheNames = "node", allEntries = true)
    public void cacheReloadAll() {
    }
}
