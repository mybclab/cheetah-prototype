package com.mybclab.cheetah.admin.node.web;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.dto.NodeCriteria;
import com.mybclab.cheetah.admin.node.dto.NodeForm;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.dto.ContainerForm;
import com.mybclab.cheetah.container.service.ContainerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 노드 Controller
 */
@Slf4j
@RequestMapping("/admin/node")
@Controller
public class NodeController {

    /**
     * {@link NodeService}
     */
    private NodeService nodeService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    private ContainerService containerService;

    /**
     * default constructor
     */
    public NodeController() {}

    /**
     * constructor
     * @param nodeService {@link NodeService}
     * @param modelMapper {@link ModelMapper}
     * @param codeGroupService {@link CodeGroupService}
     * @param containerService {@link ContainerService}
     */
    @Autowired
    public NodeController(NodeService nodeService, ModelMapper modelMapper, CodeGroupService codeGroupService, ContainerService containerService) {
        this.nodeService = nodeService;
        this.modelMapper = modelMapper;
        this.codeGroupService = codeGroupService;
        this.containerService = containerService;
    }


    /**
     * 노드 목록조회
     * @param nodeCriteria {@link NodeCriteria}
     * @param pageable {@link Pageable}
     * @param model {@link Model}
     * @return 노드목록조회
     */
    @RequestMapping({"", "/list"})
    public String list(NodeCriteria nodeCriteria, @PageableDefault(sort = { "createdAt" }, direction = Sort.Direction.DESC) Pageable pageable, Model model) {

        Page<Node> nodePage = nodeService.findAll(nodeCriteria, pageable);

        for (Node node : nodePage.getContent()) {
            List<Container> useGpuContainerList = containerService.findUseContainerByNode(node.getNodeSn());
            node.setUseContainerQuantity(useGpuContainerList.size());
            node.setUseGpuQuantity(useGpuContainerList.stream().mapToInt(Container::getGpuQuantity).sum());
        }

        model.addAttribute("nodePage", nodePage);

        return "admin/node/node-list";
    }

    /**
     * 노드 등록화면
     * @param model {@link Model}
     * @return 노드 등록화면
     */
    @GetMapping("/save")
    public String save(Model model) {

        model.addAttribute("nodeForm", new NodeForm());

        return "admin/node/node-form";
    }

    /**
     * 노드 저장
     * @param nodeForm {@link NodeForm}
     * @return 노드목록조회
     */
    @PostMapping({"/save", "/{nodeSn}/modify"})
    public String savOrModify(NodeForm nodeForm, BindingResult bindingResult) {
        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String requestedValue = builder.buildAndExpand().getPath();
        boolean isNew = requestedValue.contains("save");

        if (isNew && isDuplicatedNodeName(nodeForm)) {
            bindingResult.rejectValue("nodeName", "Valid.existNodeName", "사용중인 노드명 입니다.");
        }
        if (bindingResult.hasErrors()) {
            return "admin/node/node-form";
        }

        Node node = modelMapper.map(nodeForm, Node.class);
        if (isNew) {
            node.setStatusCode(Code.NODE_STATUS.READY);
        }
        nodeService.save(node);

        return "redirect:/admin/node";
    }

    @PostMapping("/{nodeSn}/apply")
    public String updateExternalIp(@PathVariable Long nodeSn, RedirectAttributes redirectAttributes) {

        Node node = nodeService.findById(nodeSn);
        List<Container> containerList = containerService.findAllByNode(nodeSn);

        for (Container container : containerList) {
            container.setExternalIp(node.getExternalIp());
            containerService.save(container);
        }

        return "redirect:/admin/node";
    }

    /**
     * 노드 수정화면
     * @param nodeSn 노드 일련번호
     * @param model {@link Model}
     * @return 노드 수정화면
     */
    @GetMapping("/{nodeSn}/modify")
    public String modify(@PathVariable Long nodeSn, Model model) {

        Node node = nodeService.findById(nodeSn);
        NodeForm nodeForm = modelMapper.map(node, NodeForm.class);

        model.addAttribute("nodeForm", nodeForm);

        return "admin/node/node-form";
    }

    /**
     * 노드 명 중복확인
     *
     * @param nodeForm {@link ContainerForm}
     * @return 노드 명 중복여부
     */
    private boolean isDuplicatedNodeName(@ModelAttribute NodeForm nodeForm) {
        if (StringUtils.isNotBlank(nodeForm.getNodeName())) {
            try {
                nodeService.findOneByNodeName(nodeForm.getNodeName());
            } catch (EntityNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * 노드 삭제
     * @param nodeSn 노드 일련번호
     * @return 노드목록조회
     */
    @PostMapping("/{nodeSn}/delete")
    public String delete(@PathVariable Long nodeSn) {
        nodeService.delete(nodeSn);
        return "redirect:/admin/node";
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("nodeStatusList", codeGroupService.getCodeList(CodeGroupID.NODE_STATUS));

        return map;
    }

    /**
     * 캐쉬 node 전체 삭제
     * @return node 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        nodeService.cacheReloadAll();
        return "redirect:/admin/node/";
    }
}
