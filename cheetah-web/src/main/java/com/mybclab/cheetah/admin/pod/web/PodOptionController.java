package com.mybclab.cheetah.admin.pod.web;

import com.mybclab.cheetah.admin.pod.domain.PodOption;
import com.mybclab.cheetah.admin.pod.domain.PodOptionGroup;
import com.mybclab.cheetah.admin.pod.dto.PodOptionForm;
import com.mybclab.cheetah.admin.pod.dto.PodOptionGroupForm;
import com.mybclab.cheetah.admin.pod.service.PodOptionGroupService;
import com.mybclab.cheetah.admin.pod.service.PodOptionService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@NoArgsConstructor
@RequestMapping("/admin/pod-option")
public class PodOptionController {

    private PodOptionGroupService podOptionGroupService;
    private PodOptionService podOptionService;

    private CodeGroupService codeGroupService;
    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    @Autowired
    public PodOptionController(PodOptionGroupService podOptionGroupService, PodOptionService podOptionService, CodeGroupService codeGroupService, ModelMapper modelMapper) {
        this.podOptionGroupService = podOptionGroupService;
        this.podOptionService = podOptionService;
        this.codeGroupService = codeGroupService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping({"", "/list"})
    public String list(Model model) {

        model.addAttribute("podOptionGroupList", podOptionGroupService.findAll());

        return "admin/pod-option/pod-option-group-list";
    }

    @GetMapping("/save")
    public String saveGroup(Model model) {

        model.addAttribute("podOptionGroupForm", new PodOptionGroupForm());

        return "admin/pod-option/pod-option-group-form";
    }

    @GetMapping("/{podOptionGroupId}/modify")
    public String modifyGroup(@PathVariable("podOptionGroupId") String podOptionGroupId, Model model) {

        PodOptionGroup podOptionGroup = podOptionGroupService.findById(podOptionGroupId);

        PodOptionGroupForm podOptionGroupForm = modelMapper.map(podOptionGroup, PodOptionGroupForm.class);
        model.addAttribute("podOptionGroupForm", podOptionGroupForm);

        return "admin/pod-option/pod-option-group-form";
    }

    @PostMapping({"/save", "/{podOptionGroupId}/modify"})
    public String saveOrModifyGroup(PodOptionGroupForm podOptionGroupForm) {

        PodOptionGroup podOptionGroup = modelMapper.map(podOptionGroupForm, PodOptionGroup.class);

        podOptionGroupService.save(podOptionGroup);

        return "redirect:/admin/pod-option";
    }

    @PostMapping("/{podOptionGroupId}/delete")
    public String deleteGroup(@PathVariable String podOptionGroupId) {
        podOptionGroupService.delete(podOptionGroupId);

        return "redirect:/admin/pod-option";
    }





    @GetMapping("/{podOptionGroupId}/add")
    public String add(PodOptionForm podOptionForm, Model model) {

        model.addAttribute("podOptionForm", podOptionForm);

        return "admin/pod-option/pod-option-form";
    }

    @GetMapping("/{podOptionGroupId}/{podOptionSn}/modify")
    public String modify(@PathVariable("podOptionSn") long podOptionSn, Model model) {

        PodOption podOption = podOptionService.findById(podOptionSn);

        PodOptionForm podOptionForm = modelMapper.map(podOption, PodOptionForm.class);
        model.addAttribute("podOptionForm", podOptionForm);

        return "admin/pod-option/pod-option-form";
    }

    @PostMapping({"/{podOptionGroupId}/add", "/{podOptionGroupId}/{podOptionSn}/modify"})
    public String saveOrModify(PodOptionForm podOptionForm) {
        PodOption podOption = modelMapper.map(podOptionForm, PodOption.class);

        podOptionService.save(podOption);

        return "redirect:/admin/pod-option";
    }

    @PostMapping("/{podOptionGroupId}/{podOptionSn}/delete")
    public String delete(@PathVariable long podOptionSn) {
        podOptionService.delete(podOptionSn);

        return "redirect:/admin/pod-option";
    }



    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("podOptionGroupTypeList", codeGroupService.getCodeList(CodeGroupID.POD_OPTION_GROUP_TYPE));
        return map;
    }

    /**
     * 캐쉬 podOption, podOptionGroup 전체 삭제
     * @return podOptionGroup 목록조회
     */
    @GetMapping("/reloadCache/all")
    public String cacheReloadAll() {
        podOptionService.cacheReloadAll();
        return "redirect:/admin/pod-option/";
    }

}
