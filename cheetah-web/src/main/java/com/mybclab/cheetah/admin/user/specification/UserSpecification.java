package com.mybclab.cheetah.admin.user.specification;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * 사용자 Specification
 */
public class UserSpecification {

    /**
     * like 이름
     * @param name 이름
     * @return {@link Specification<User>}
     */
    public static Specification<User> likeName(final String name) {
        return Optional.ofNullable(name)
                .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("name"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * equal 상위 사용자아이디
     * @param parentUsername 상위 사용자아이디(그룹아이디)
     * @return {@link Specification<User>}
     */
    public static Specification<User> equalsParentUsername(final String parentUsername) {
        if ("ALL".equals(parentUsername)) {
            return null;
        }
        if ("".equals(parentUsername)) {
            return Optional.ofNullable(parentUsername)
                    .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.and(
                            criteriaBuilder.isNull(root.<String>get("parentUsername")), criteriaBuilder.isNull(root.<String>get("groupName"))))
                    .orElse(null);
        }
        return Optional.ofNullable(parentUsername)
                .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.or(
                        criteriaBuilder.equal(root.<String>get("parentUsername"), o), criteriaBuilder.equal(root.<String>get("username"), o)))
                .orElse(null);
    }
    /**
     * equal 승인 여부
     * @param confirmYn 승인 여부
     * @return {@link Specification<User>}
     */
    public static Specification<User> equalsConfirmYn(final Boolean confirmYn) {
        return Optional.ofNullable(confirmYn)
                .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Boolean>get("confirmYn"), o))
                .orElse(null);
    }

    /**
     * equal 탈퇴 여부
     * @param withdrawalYn 탈퇴 여부
     * @return {@link Specification<User>}
     */
    public static Specification<User> equalsWithdrawalYn(final Boolean withdrawalYn) {
        return Optional.ofNullable(withdrawalYn)
                .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> o ? criteriaBuilder.isNotNull(root.<LocalDateTime>get("withdrawalDate")) : criteriaBuilder.isNull(root.<LocalDateTime>get("withdrawalDate")))
                .orElse(null);
    }

    public static Specification<User> isGroup(final Boolean isGroup) {
        if (null == isGroup) {
            return null;
        }
        if (isGroup) {
            return Optional.ofNullable(isGroup)
                    .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.<String>get("groupName")))
                    .orElse(null);
        } else {
            return Optional.ofNullable(isGroup)
                    .map(o -> (Specification<User>) (root, query, criteriaBuilder) -> criteriaBuilder.isNull(root.<String>get("groupName")))
                    .orElse(null);
        }
    }
}
