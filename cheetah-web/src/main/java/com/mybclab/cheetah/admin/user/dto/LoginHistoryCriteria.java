package com.mybclab.cheetah.admin.user.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class LoginHistoryCriteria {

    private String parentUsername;

    private String username;

    private String name;

    private Code.LOGIN_HISTORY loginHistoryCode;

    private String loginIp;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate loginDateFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate loginDateTo;
}
