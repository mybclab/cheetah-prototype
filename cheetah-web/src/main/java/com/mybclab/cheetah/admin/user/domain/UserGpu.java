package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"user", "gpu"})
public class UserGpu implements Serializable {

    @EmbeddedId
    private UserGpuPK userGpuPK;

    private int groupQuotaQuantity;

    private int quotaQuantity;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdAt;

    @Column(nullable = false, updatable = false)
    @CreatedBy
    private String createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private User user;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gpuSn", referencedColumnName = "gpuSn", insertable = false, updatable = false)
    private Gpu gpu;

    public UserGpu(UserGpuPK userGpuPK) {
        this.userGpuPK = userGpuPK;
    }

    @Transient
    public int getUseQuantity() {
        List<Container> containerList = user.getContainerList();
        if (containerList != null && containerList.size() > 0) {
            return containerList.stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode()) && c.getGpuSn().equals(userGpuPK.getGpuSn()) && c.getParentContainerId() == null).mapToInt(Container::getGpuQuantity).sum();
        }
        return 0;
    }

    @Transient
    public int getGroupUseQuantity() {
        if (user.isGroupAdmin()) {
            List<User> groupUsers = user.getGroupUsers();
            int groupUseQuantity = getUseQuantity();
            for (User groupUser : groupUsers) {
                List<Container> containerList = groupUser.getContainerList();
                groupUseQuantity += containerList.stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode()) && c.getGpuSn().equals(userGpuPK.getGpuSn()) && c.getParentContainerId() == null).mapToInt(Container::getGpuQuantity).sum();
            }
            return groupUseQuantity;
        }
        return 0;
    }

}
