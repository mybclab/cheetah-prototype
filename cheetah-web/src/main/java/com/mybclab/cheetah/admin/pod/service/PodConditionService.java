package com.mybclab.cheetah.admin.pod.service;

import com.mybclab.cheetah.admin.pod.domain.PodCondition;
import com.mybclab.cheetah.admin.pod.repository.PodConditionRepository;
import com.mybclab.cheetah.container.domain.ContainerPK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * 파드 컨디션 Service
 * 와 왜 그러지
 */
@Service
public class PodConditionService {

    private PodConditionRepository podConditionRepository;

    public PodConditionService() {

    }

    /**
     * @param podConditionRepository {@link PodConditionRepository}
     */
    @Autowired
    public PodConditionService(PodConditionRepository podConditionRepository) {
        this.podConditionRepository = podConditionRepository;
    }

    @Transactional
    public PodCondition save(PodCondition podCondition) {
        if (!podConditionRepository.existsByPodNameAndTypeCodeAndLastTransitionTime(podCondition.getPodName(), podCondition.getTypeCode(), podCondition.getLastTransitionTime())) {
            return podConditionRepository.saveAndFlush(podCondition);
        }
        return null;
    }

    public List<PodCondition> findAllByContainerPK(ContainerPK containerPK) {
        return podConditionRepository.findAllByUsernameAndContainerId(containerPK.getUsername(), containerPK.getContainerId());
    }
}
