package com.mybclab.cheetah.admin.user.service;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.admin.credit.service.CreditService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.admin.user.domain.UserCreditPK;
import com.mybclab.cheetah.admin.user.dto.UserCreditDto;
import com.mybclab.cheetah.admin.user.repository.UserCreditRepository;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * 사용자 크레딧 Service
 */
@Slf4j
@Service
public class UserCreditService implements Serializable {
    /**
     * {@link CreditService}
     */
    private CreditService creditService;
    /**
     * {@link UserCreditRepository}
     */
    private UserCreditRepository userCreditRepository;

    /**
     * default constructor
     */
    public UserCreditService() {}
    /**
     * constructor
     * @param userCreditRepository {@link UserCreditRepository}
     * @param creditService {@link CreditService}
     */
    @Autowired
    public UserCreditService(UserCreditRepository userCreditRepository, CreditService creditService) {
        this.userCreditRepository = userCreditRepository;
        this.creditService = creditService;
    }

    /**
     * 저장
     * @param userCredit {@link UserCredit}
     * @return {@link UserCredit}
     */
    @Transactional
    public UserCredit save(UserCredit userCredit) {
        int userCreditNo = userCreditRepository.findMaxUserCreditNo(userCredit.getUserCreditPK().getUsername()) + 1;
        userCredit.getUserCreditPK().setUserCreditNo(userCreditNo);
        return userCreditRepository.saveAndFlush(userCredit);
    }

    /**
     * 사용자아이디에 크레딧을 추가
     * @param username 사용자아이디
     * @param creditId 크레딧 아이디
     * @return {@link UserCredit}
     */
    @Transactional
    public UserCredit addCreditToUser(String username, String creditId) {
        Credit credit = creditService.findById(creditId);

        UserCreditPK userCreditPK = new UserCreditPK();
        userCreditPK.setUsername(username);
        UserCredit userCredit = new UserCredit();
        userCredit.setUserCreditPK(userCreditPK);

        userCredit.setCreditId(creditId);
        userCredit.setCreditAmount(credit.getCreditAmount());
        userCredit.setRemainCreditAmount(credit.getCreditAmount());

        if (!Code.CREDIT_EXPIRY_TYPE.NONE.equals(credit.getCreditExpiryTypeCode())) {
            userCredit.setExpiryStartDate(LocalDate.now().format(CheetahFormatter.FOR_DATE));
            String expiryEndDate = "";
            switch (credit.getCreditExpiryTypeCode()) {
                case YEAR: expiryEndDate = LocalDate.now().plusYears(credit.getCreditExpiryValue()).format(CheetahFormatter.FOR_DATE); break;
                case MONTH: expiryEndDate = LocalDate.now().plusMonths(credit.getCreditExpiryValue()).format(CheetahFormatter.FOR_DATE); break;
                case WEEK: expiryEndDate = LocalDate.now().plusWeeks(credit.getCreditExpiryValue()).format(CheetahFormatter.FOR_DATE); break;
                case DAY: expiryEndDate = LocalDate.now().plusDays(credit.getCreditExpiryValue()).format(CheetahFormatter.FOR_DATE); break;
            }
            userCredit.setExpiryEndDate(expiryEndDate);
        }
        return this.save(userCredit);
    }

    /**
     * 사용자의 크레딧 전체 목록 조회
     * @param username 사용자아이디
     * @return 사용자 크레딧 목록
     */
    public List<UserCredit> findAllByUsername(String username) {
        return userCreditRepository.findAllByUserCreditPKUsernameOrderByCreatedAtDesc(username);
    }
    /**
     * 사용자아이디로 유효한 크레딧 목록조회
     * (남은 금액 > 0 and (유효기간 is null or 현재날짜 between 유효기간 시작일 and 유효기간 종료일))
     * @param username 사용자아이디
     * @return 사용자 유효 크레딧 목록
     */
    public List<UserCredit> findAllByValidCredit(String username) {
        return userCreditRepository.findAllByValidCredit(username);
    }

    /**
     * 사용자 크레딧 현황 조회
     * @param username 사용자아이디
     * @return {@link UserCreditDto}
     */
    public UserCreditDto userCredit(String username) {
        List<UserCredit> userCreditList = this.findAllByValidCredit(username);
        if (userCreditList != null && userCreditList.size() > 0) {
            return UserCreditDto.builder()
                    .userCreditCount(userCreditList.size())
                    .totalCreditAmount(userCreditList.stream().mapToDouble(UserCredit::getCreditAmount).sum())
                    .remainCreditAmount(userCreditList.stream().mapToDouble(UserCredit::getRemainCreditAmount).sum())
                    .build();
        }
        return null;
    }

    @Transactional
    public void deleteById(UserCreditPK userCreditPK) {
        userCreditRepository.deleteById(userCreditPK);
    }

    @Transactional
    public void deleteByUser(User user) {
        for (UserCredit userCredit : user.getUserCreditList()) {
            deleteById(userCredit.getUserCreditPK());
        }
    }
}
