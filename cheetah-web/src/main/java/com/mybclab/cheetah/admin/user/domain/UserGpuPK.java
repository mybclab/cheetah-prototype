package com.mybclab.cheetah.admin.user.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Embeddable
public class UserGpuPK implements Serializable {

    private String username;

    private Long gpuSn;

    public UserGpuPK(String username, Long gpuSn) {
        this.username = username;
        this.gpuSn = gpuSn;
    }
}
