package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.PodOptionGroup;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PodOptionGroupRepository extends JpaRepository<PodOptionGroup, String> {


    List<PodOptionGroup> findAllByPodOptionGroupTypeCodeAndUseYnOrderByOdrAsc(Code.POD_OPTION_TYPE podOptionGroupTypeCode, boolean useYn);
}
