package com.mybclab.cheetah.admin.node.domain;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 노드
 */
@Entity
@Getter
@Setter
@ToString(exclude = "containerList")
public class Node extends HistoryBase implements Serializable {

    /**
     * 노드 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long nodeSn;

    /**
     * 노드 명
     */
    private String nodeName;

    /**
     * 내부 도메인
     */
    private String internalDomain;

    /**
     * 내부 아이피
     */
    private String internalIp;

    /**
     * 외부 아이피
     */
    private String externalIp;

    /**
     * GPU 라벨
     */
    private String gpuLabel;
    /**
     * GPU 수량
     */
    private int gpuQuantity;

    /**
     * 상태 코드
     * {@link Code.NODE_STATUS}
     */
    @Enumerated(EnumType.STRING)
    private Code.NODE_STATUS statusCode;

    /**
     * 마스터 여부
     */
    private Boolean masterYn;

    /**
     * 컨테이너 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "nodeSn", referencedColumnName = "nodeSn", insertable = false, updatable = false)
    @OrderBy("createdAt DESC")
    private List<Container> containerList = new ArrayList<>();

    @Transient
    private int useGpuQuantity;

    @Transient
    private int useContainerQuantity;

}
