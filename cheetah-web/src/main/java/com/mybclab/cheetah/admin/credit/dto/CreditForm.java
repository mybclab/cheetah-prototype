package com.mybclab.cheetah.admin.credit.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 크레딧 Form
 */
@Getter
@Setter
@ToString
public class CreditForm implements Serializable {

    /**
     * 크레딧 아이디
     */
    @NotBlank
    private String creditId;

    /**
     * 크레딧 명
     */
    @NotBlank
    private String creditName;

    /**
     * 크레딧 구분 코드
     * {@link Code.CREDIT_TYPE}
     */
    @NotEmpty
    private Code.CREDIT_TYPE creditTypeCode;

    /**
     * 크레딧 금액
     */
    @NotEmpty
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double CreditAmount;

    /**
     * 크레딧 유효기간 코드
     * {@link Code.CREDIT_EXPIRY_TYPE}
     */
    @NotEmpty
    private Code.CREDIT_EXPIRY_TYPE creditExpiryTypeCode;

    /**
     * 크레딧 유효기간 값
     */
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private int creditExpiryValue;
}
