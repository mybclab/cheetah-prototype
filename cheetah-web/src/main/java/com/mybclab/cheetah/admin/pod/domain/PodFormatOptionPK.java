package com.mybclab.cheetah.admin.pod.domain;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class PodFormatOptionPK implements Serializable {

    /**
     * 파드 포맷 일련번호
     */
    private Long podFormatSn;

    private Long podOptionSn;

}
