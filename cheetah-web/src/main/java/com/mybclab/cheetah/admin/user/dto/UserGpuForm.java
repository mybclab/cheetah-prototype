package com.mybclab.cheetah.admin.user.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UserGpuForm implements Serializable {

    private Long gpuSn;

    private String gpuName;

    private int groupQuotaQuantity;

    private int quotaQuantity;

    private int totalGroupQuotaQuantity;

    private int remainGroupQuotaQuantity;

    public int getMaxRemainGroupQuotaquantity() {
        return remainGroupQuotaQuantity > quotaQuantity ? remainGroupQuotaQuantity : quotaQuantity;
    }
}
