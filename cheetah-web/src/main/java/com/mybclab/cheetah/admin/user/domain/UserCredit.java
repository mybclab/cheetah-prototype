package com.mybclab.cheetah.admin.user.domain;

import com.mybclab.cheetah.admin.credit.domain.Credit;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.payment.domain.UserCreditPayment;
import com.mybclab.common.domain.HistoryBase;
import com.mybclab.common.formatter.annotation.CurrencyFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 사용자 크레딧
 */
@Entity
@Setter
@Getter
@ToString(exclude = {"user", "userCreditPaymentList"})
public class UserCredit extends HistoryBase implements Serializable {


    /**
     * {@link UserCreditPK}
     */
    @EmbeddedId
    private UserCreditPK userCreditPK;

    /**
     * 크레딧 아이디
     */
    private String creditId;

    /**
     * 크레딧 금액
     */
    @CurrencyFormat
    private Double creditAmount;

    /**
     * 잔여 크레딧 금액
     */
    @CurrencyFormat
    private Double remainCreditAmount;

    /**
     * 유효기간 시작 일시
     */
    private String expiryStartDate;

    /**
     * 유효기간 종료 일시
     */
    private String expiryEndDate;

    /**
     * {@link User}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private User user;

    /**
     * {@link Credit}
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creditId", referencedColumnName = "creditId", insertable = false, updatable = false)
    private Credit credit;

    /**
     * 사용자 크레딧 결제 목록
     */
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false),
            @JoinColumn(name = "userCreditNo", referencedColumnName = "userCreditNo", insertable = false, updatable = false)
    })
    private List<UserCreditPayment> userCreditPaymentList = new ArrayList<>();

    /**
     * 사용자 크레딧이 유효한지 확인
     * @return 유효여부
     */
    @Transient
    public boolean isValidCredit() {
        if (getExpiryEndDate() == null) {
            return true;
        }
        String now = LocalDate.now().format(CheetahFormatter.FOR_DATE);
        return now.compareTo(getExpiryStartDate()) >= 0 && now.compareTo(getExpiryEndDate()) <= 0;
    }

    @Transient
    public String getExpiryStartDateFormat() {
        if (this.expiryStartDate != null && !"".equals(this.expiryStartDate)) {
            return CheetahFormatter.FOR_DATE_WITH_DASH.format(LocalDate.parse(this.expiryStartDate, CheetahFormatter.FOR_DATE));
        }
        return "-";
    }

    @Transient
    public String getExpiryEndDateFormat() {
        if (this.expiryEndDate != null && !"".equals(this.expiryEndDate)) {
            return CheetahFormatter.FOR_DATE_WITH_DASH.format(LocalDate.parse(this.expiryEndDate, CheetahFormatter.FOR_DATE));
        }
        return "-";
    }

}
