package com.mybclab.cheetah.admin.user.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 로그인 Form
 */
@Getter
@Setter
@ToString
public class LoginForm implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 비밀번
     */
    private String password;

    /**
     * 이메일
     */
    private String email;

    /**
     * error
     */
    private boolean error = false;
}
