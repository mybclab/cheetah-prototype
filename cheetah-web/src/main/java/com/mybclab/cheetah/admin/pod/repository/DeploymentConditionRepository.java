package com.mybclab.cheetah.admin.pod.repository;

import com.mybclab.cheetah.admin.pod.domain.DeploymentCondition;
import com.mybclab.cheetah.admin.pod.domain.PodCondition;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DeploymentConditionRepository extends JpaRepository<DeploymentCondition, Long> {

    boolean existsByDeploymentNameAndTypeCodeAndLastTransitionTime(String deploymentName, Code.POD_CONDITION_TYPE typeCode, LocalDateTime lastTransitionTime);
    void deleteByUsernameAndContainerId(String username, String containerId);

    List<PodCondition> findAllByUsernameAndContainerId(String username, String containerId);
}
