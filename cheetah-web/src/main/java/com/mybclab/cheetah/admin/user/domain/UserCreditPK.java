package com.mybclab.cheetah.admin.user.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 사용자 크레딧 PK
 */
@Getter
@Setter
@ToString
@Embeddable
public class UserCreditPK implements Serializable {

    /**
     * 사용자아이디
     */
    private String username;

    /**
     * 사용자 크레딧 번호
     */
    private int userCreditNo;

    /**
     * default constructor
     */
    public UserCreditPK() {}

    /**
     * constructor
     * @param username 사용자아이디
     * @param userCreditNo 사용자 크레딧 번호
     */
    public UserCreditPK(String username, int userCreditNo) {
        this.username = username;
        this.userCreditNo = userCreditNo;
    }

}
