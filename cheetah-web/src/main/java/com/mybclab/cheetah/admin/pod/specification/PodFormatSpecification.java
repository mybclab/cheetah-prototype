package com.mybclab.cheetah.admin.pod.specification;

import com.mybclab.cheetah.admin.pod.domain.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.Optional;

public class PodFormatSpecification {

    /**
     * like podFormatName
     * @param podFormatName podFormatName
     * @return {@link Specification < PodFormat >}
     */
    public static Specification<PodFormat> likePodFormatName(final String podFormatName) {
        return Optional.ofNullable(podFormatName)
                .map(o -> (Specification<PodFormat>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("podFormatName"), "%" + o + "%"))
                .orElse(null);
    }

    public static Specification<PodFormat> equalsGpuSn(final Long gpuSn) {
        return Optional.ofNullable(gpuSn)
                .map(o -> (Specification<PodFormat>) (root, query, criteriaBuilder) -> {
                    Subquery<PodFormatGpu> subquery = query.subquery(PodFormatGpu.class);
                    Root<PodFormatGpu> fromPodFormatGpu = subquery.from(PodFormatGpu.class);
                    subquery.select(fromPodFormatGpu.<PodFormatGpuPK>get("podFormatGpuPK").get("podFormatSn"));
                    subquery.where(criteriaBuilder.equal(fromPodFormatGpu.<PodFormatGpuPK>get("podFormatGpuPK").<Long>get("gpuSn"), gpuSn));
                    return criteriaBuilder.in(root.get("podFormatSn")).value(subquery);
                })
                .orElse(null);
    }

    public static Specification<PodFormat> inPodOptionSn(final String podOptionGroupId, final Long podOptionOsSn) {
        return Optional.ofNullable(podOptionOsSn)
                .map(o -> (Specification<PodFormat>) (root, query, criteriaBuilder) -> {
                    Subquery<PodFormatOption> subquery = query.subquery(PodFormatOption.class);
                    Root<PodFormatOption> fromPodFormatOption = subquery.from(PodFormatOption.class);
                    subquery.select(fromPodFormatOption.<PodFormatOptionPK>get("podFormatOptionPK").get("podFormatSn"));
                    subquery.where(criteriaBuilder.and(criteriaBuilder.equal(fromPodFormatOption.<String>get("podOptionGroupId"), podOptionGroupId)
                            , criteriaBuilder.equal(fromPodFormatOption.<PodFormatOptionPK>get("podFormatOptionPK").<Long>get("podOptionSn"), podOptionOsSn)));
                    return criteriaBuilder.in(root.get("podFormatSn")).value(subquery);
                })
                .orElse(null);
    }

}
