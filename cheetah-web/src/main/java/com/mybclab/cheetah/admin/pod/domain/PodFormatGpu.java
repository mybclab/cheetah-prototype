package com.mybclab.cheetah.admin.pod.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"podFormat", "gpu"})
public class PodFormatGpu implements Serializable {


    @EmbeddedId
    private PodFormatGpuPK podFormatGpuPK;

    private LocalDateTime createdAt;

    public PodFormatGpu(PodFormatGpuPK podFormatGpuPK, LocalDateTime createdAt) {
        this.podFormatGpuPK = podFormatGpuPK;
        this.createdAt = createdAt;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "podFormatSn", referencedColumnName = "podFormatSn", insertable = false, updatable = false)
    private PodFormat podFormat;



    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gpuSn", referencedColumnName = "gpuSn", insertable = false, updatable = false)
    private Gpu gpu;
}
