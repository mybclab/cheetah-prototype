package com.mybclab.cheetah.admin.node.specification;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.common.constant.Code;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;
/**
 * 노드 Specification
 */
public class NodeSpecification {

    /**
     * like 노드 명
     * @param nodeName 노드 명
     * @return {@link Specification<Node>}
     */
    public static Specification<Node> likeNodeName(final String nodeName) {
        return Optional.ofNullable(nodeName)
                .map(o -> (Specification<Node>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("nodeName"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * like 내부 도메인
     * @param internalDomain 내부 도메인
     * @return {@link Specification<Node>}
     */
    public static Specification<Node> likeInternalDomain(final String internalDomain) {
        return Optional.ofNullable(internalDomain)
                .map(o -> (Specification<Node>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("internalDomain"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * like 내부 아이피
     * @param internalIp 내부 아이피
     * @return {@link Specification<Node>}
     */
    public static Specification<Node> likeInternaIp(final String internalIp) {
        return Optional.ofNullable(internalIp)
                .map(o -> (Specification<Node>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("internalIp"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * like 내부 아이피
     * @param externalIp 내부 아이피
     * @return {@link Specification<Node>}
     */
    public static Specification<Node> likeExternalIp(final String externalIp) {
        return Optional.ofNullable(externalIp)
                .map(o -> (Specification<Node>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("externalIp"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * equal 상태 코드
     * @param statusCode 상태 코드
     * @return {@link Specification< Node >}
     */
    public static Specification<Node> equalsStatusCode(final Code.NODE_STATUS statusCode) {
        return Optional.ofNullable(statusCode)
                .map(o -> (Specification<Node>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Code.NODE_STATUS>get("statusCode"), o))
                .orElse(null);
    }
}
