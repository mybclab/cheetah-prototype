package com.mybclab.cheetah.main.web;

import com.mybclab.cheetah.common.mvc.AbstractController;
import com.mybclab.cheetah.config.component.CheetahMessageSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hkwon on 2018. 6. 7..
 */
@ControllerAdvice
@Controller
@Slf4j
public class ErrorController extends AbstractController {
    /**
     * {@link CheetahMessageSource}
     */
    private final CheetahMessageSource messageSource;

    /**
     * constructor
     *
     * @param messageSource {@link CheetahMessageSource}
     */
    public ErrorController(CheetahMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * HTTP 상태 error 화면
     *
     * @param errorCode errorCode
     * @param model {@link Model}
     * @return error 화면
     */
    @RequestMapping("/error/{errorCode:400|403|404|405|500|504|999}")
    public String error(@PathVariable String errorCode, Model model) {
        HttpStatus httpStatus = HttpStatus.valueOf(Integer.parseInt(errorCode));
        model.addAttribute("errorCode", errorCode);
        model.addAttribute("errorMsg", httpStatus.getReasonPhrase());

        return "error/error";
    }

}
