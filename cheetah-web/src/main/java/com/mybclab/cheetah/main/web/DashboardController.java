package com.mybclab.cheetah.main.web;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.UserCriteria;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.dto.ContainerCriteria;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.management.post.domain.Post;
import com.mybclab.cheetah.management.post.dto.PostCriteria;
import com.mybclab.cheetah.management.post.service.PostService;
import com.mybclab.cheetah.payment.service.PaymentService;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 대쉬보드 Controller
 */
@Slf4j
@RequestMapping("/dashboard")
@Controller
public class DashboardController {

    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link ContainerService}
     */
    private ContainerService containerService;
    /**
     * {@link ContainerHistoryService}
     */
    private ContainerHistoryService containerHistoryService;

    /**
     * {@link PaymentService}
     */
    private PaymentService paymentService;
    /**
     * {@link PostService}
     */
    private PostService postService;

    /**
     * {@link UserCreditService}
     */
    private UserCreditService userCreditService;

    /**
     * {@link UserCardService}
     */
    private UserCardService userCardService;

    /**
     * {@link GpuService}
     */
    private GpuService gpuService;


    /**
     * default constructor
     */
    public DashboardController() {}

    /**
     * constructor
     * @param userService {@link UserService}
     * @param containerService {@link ContainerService}
     * @param paymentService {@link PaymentService}
     * @param postService {@link PostService}
     * @param userCreditService {@link UserCreditService}
     * @param userCardService {@link UserCardService}
     * @param containerHistoryService {@link ContainerHistoryService}
     */
    @Autowired
    public DashboardController(UserService userService, ContainerService containerService, PaymentService paymentService, PostService postService,
                               UserCreditService userCreditService, UserCardService userCardService, GpuService gpuService, ContainerHistoryService containerHistoryService) {
        this.userService = userService;
        this.containerService = containerService;
        this.paymentService = paymentService;
        this.postService = postService;
        this.userCreditService = userCreditService;
        this.userCardService = userCardService;
        this.gpuService = gpuService;
        this.containerHistoryService = containerHistoryService;
    }

    @RequestMapping("")
    public String dashboard(Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        List<User> userListForNotice = new ArrayList<>(userService.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN));

        List<Code.CONTAINER_STATUS> statusCodeList = new ArrayList<Code.CONTAINER_STATUS>() {{
            add(Code.CONTAINER_STATUS.REQUEST);
            add(Code.CONTAINER_STATUS.CONFIRM);
            add(Code.CONTAINER_STATUS.CREATE);
            add(Code.CONTAINER_STATUS.START);
            add(Code.CONTAINER_STATUS.FAIL);
        }};
        if (user.isGroupAdmin()) { // GROUP_ADMIN
            userListForNotice.add(user);
            model.addAttribute("userRole", Code.ROLE.GROUP_ADMIN);
            List<User> groupUserList = user.getGroupUsers();
            model.addAttribute("userCount", groupUserList.size());
            model.addAttribute("confirmUserCount", groupUserList.stream().filter(User::getConfirmYn).count());

            model.addAttribute("containerPage", containerPage(null, user.getUsername(), null, statusCodeList, 20));
            model.addAttribute("confirmUserPage", userPage(user.getUsername(), 3));
            model.addAttribute("confirmContainerPage", containerPage(null, user.getUsername(), Code.CONTAINER_STATUS.REQUEST, null, 3));

            model.addAttribute("nextPayment", paymentService.nextPayment(user.getUsername(), true));
            model.addAttribute("lastPayment", paymentService.lastPayment(user.getUsername()));
            model.addAttribute("userCard", userCardService.findValidCardByUsername(user.getUsername()));
            model.addAttribute("userCredit", userCreditService.userCredit(user.getUsername()));

            model.addAttribute("containerCount", user.getGroupContainerCount());
            model.addAttribute("useQuantity", user.getTotalGroupUseQuantity());
            model.addAttribute("quotaQuantity", user.getTotalGroupQuotaQuantity());


        } else if (user.isGroupUser()) { // GROUP_USER
            userListForNotice.add(user.getGroup());
            model.addAttribute("userRole", Code.ROLE.GROUP_USER);

            model.addAttribute("containerPage", containerPage(user.getUsername(), null, null, statusCodeList, 10));

            model.addAttribute("containerCount", user.getContainerList().stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).count());
            model.addAttribute("useQuantity", user.getTotalUseQuantity());
            model.addAttribute("quotaQuantity", user.getTotalQuotaQuantity());
            model.addAttribute("remainQuantity", user.getTotalQuotaQuantity() - user.getTotalUseQuantity());

        } else if (user.isGeneralUser()) { // GENERAL_USER
            model.addAttribute("userRole", Code.ROLE.GENERAL_USER);

            model.addAttribute("containerPage", containerPage(user.getUsername(), null, null, statusCodeList, 10));

            model.addAttribute("containerCount", user.getContainerList().stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).count());
            model.addAttribute("lastPayment", paymentService.lastPayment(user.getUsername()));
            model.addAttribute("nextPayment", paymentService.nextPayment(user.getUsername(), false));
            model.addAttribute("userCard", userCardService.findValidCardByUsername(user.getUsername()));
            model.addAttribute("userCredit", userCreditService.userCredit(user.getUsername()));

            model.addAttribute("useQuantity", user.getTotalUseQuantity());
            model.addAttribute("quotaQuantity", user.getTotalQuotaQuantity());
            model.addAttribute("remainQuantity", user.getTotalQuotaQuantity() - user.getTotalUseQuantity());

        } else if (user.isSystemAdmin()) { // SYSTEM_ADMIN
            model.addAttribute("userRole", Code.ROLE.SYSTEM_ADMIN);
            List<User> userList = userService.findAll();
            model.addAttribute("userCount", userList.size());
            model.addAttribute("confirmUserCount", userList.stream().filter(User::getConfirmYn).count());

            model.addAttribute("containerPage", containerPage(null, null, null, statusCodeList, 20));
            model.addAttribute("confirmUserPage", userPage(null, 10));
            model.addAttribute("confirmContainerPage", containerPage(null, null, Code.CONTAINER_STATUS.REQUEST, null, 10));

            List<Container> containerList = containerService.findAll();
            List<Container> useContainerList = containerList.stream().filter(c -> Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).collect(Collectors.toList());
            model.addAttribute("containerCount", useContainerList.size());
            model.addAttribute("useQuantity", useContainerList.stream().filter(c -> c.getParentContainerId() == null).collect(Collectors.toList()).stream().mapToInt(Container::getGpuQuantity).sum());
            model.addAttribute("quotaQuantity", gpuService.findAll(true).stream().mapToInt(Gpu::getTotalGpuQuantity).sum());
            model.addAttribute("remainQuantity", "-");
        }

        // notice
        model.addAttribute("noticeList", noticeList(userListForNotice, 3));
        model.addAttribute("user", user);
        return "dashboard/dashboard";
    }

    @ResponseBody
    @GetMapping("/monthly/gpu/{username}")
    public ResponseEntity<JsonResult> montlyGpuChart(@PathVariable String username) {
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);

        List<Gpu> gpuList = gpuService.findAll();
        int gpuCnt = gpuList.size();
        int size = 6;

        String[] labels = new String[size];
        int[][] series = new int[gpuCnt][size];

        YearMonth yearMonth = YearMonth.now();
        yearMonth = yearMonth.minusMonths(size - 1);
        for (int i = 0; i < size ; i++) {
            List<ContainerHistory> containerHistoryList = containerHistoryService.findAllByMonthly(username, yearMonth.format(CheetahFormatter.FOR_YEAR_MONTH), true);

            for (int j = 0; j < gpuList.size(); j++) {
                Gpu gpu = gpuList.get(j);
                int sum = containerHistoryList.stream().filter(c -> c.getGpuSn().equals(gpu.getGpuSn())).mapToInt(ContainerHistory::getGpuQuantity).sum();
                series[j][i] = sum;
            }

            labels[i] = yearMonth.format(CheetahFormatter.FOR_YEAR_MONTH_WITH_DASH);
            yearMonth = yearMonth.plusMonths(1);
        }
        result.setData("gpuList", gpuList);
        result.setData("labels", labels);
        result.setData("series", series);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 사용자 목록
     * @param parentUsername 상위사용자아이디(그룹아이디)
     * @param size 개수
     * @return 사용자 목록
     */
    private Page<User> userPage(String parentUsername, int size) {
        return userService.findAll(
                UserCriteria.builder()
                        .parentUsername(parentUsername)
                        .confirmYn(false)
                        .build()
                , PageRequest.of(0, size, Sort.Direction.DESC, "createdAt"));
    }

    /**
     * 컨테이너 목록
     * @param username 사용자아이디
     * @param parentUsername 상위사용자아이디(그룹아이디)
     * @param size 개수
     * @return 컨테이너 목록
     */
    private Page<Container> containerPage(String username, String parentUsername, Code.CONTAINER_STATUS containerStatusCode, List<Code.CONTAINER_STATUS> statusCodeList, int size) {
        return containerService.findAll(
                ContainerCriteria.builder()
                        .username(username)
                        .parentUsername(parentUsername)
                        .statusCode(containerStatusCode)
                        .statusCodeList(statusCodeList)
                        .build()
                , PageRequest.of(0, size, Sort.Direction.DESC, "createdAt"));
    }

    /**
     * 공지사항 목록
     * @param userListForNotice 작성사용자목록
     * @param size 개수
     * @return 공지사항 목록
     */
    private List<Post> noticeList(List<User> userListForNotice, int size) {
        Page<Post> noticePage = postService.findAll(PostCriteria.builder()
                .boardTypeCode(Code.BOARD_TYPE.NOTICE)
                .groupIdList(userListForNotice.stream().map(User::getUsername).collect(Collectors.toList()))
                .useYn(true)
                        .build()
                , PageRequest.of(0, size, Sort.Direction.DESC, "createdAt"));

        return noticePage != null && noticePage.getContent() != null ? noticePage.getContent() : null;
    }
}
