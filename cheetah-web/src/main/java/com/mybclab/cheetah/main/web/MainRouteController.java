package com.mybclab.cheetah.main.web;

import com.mybclab.cheetah.common.mvc.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hkwon on 2018. 6. 7..
 */
@Controller
public class MainRouteController extends AbstractController {
    @RequestMapping("/")
    public String index() {
        return REDIRECT_PREFIX + "/dashboard";
    }
}
