package com.mybclab.cheetah.main.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {

    @RequestMapping("/main")
    public String main() {
        return "main/main";
    }

    @RequestMapping("/help/price")
    public String price() {
        return "main/price";
    }
}
