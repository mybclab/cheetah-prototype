package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Offering implements Serializable {

    private String id;

    private String name;

    private int numberOfCpu;

    private int sizeOfMemory;


}
