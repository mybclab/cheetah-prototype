package com.mybclab.cheetah.management.cloud.rabbitmq.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.CloudDriverResponse;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Resource;
import com.mybclab.cheetah.management.cloud.rabbitmq.kubernetes.KubernetesTokenManager;
import com.mybclab.cheetah.management.cloud.rabbitmq.properties.CloudPropertiesService;
import com.mybclab.common.utils.SSHClientUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Profile("cloud")
public class CloudVolumeService {

    ContainerService containerService;
    
    NodeService nodeService;
    
    CloudPropertiesService cloudProperties;
    
    AutowireCapableBeanFactory autowireCapableBeanFactory;
    
    @Autowired    
    public CloudVolumeService(    	    
    		AutowireCapableBeanFactory autowireCapableBeanFactory,
    		KubernetesTokenManager kubernetesTokenService, 
    		ContainerService containerService, 
    		NodeService nodeService) {
    	this.containerService = containerService;
    	this.nodeService = nodeService;
    	this.autowireCapableBeanFactory = autowireCapableBeanFactory;
    }
    
	public void setProperties(CLOUD_VENDOR cloud) {
		try {
			cloudProperties = (CloudPropertiesService) Class.forName(cloud.getClassName()).newInstance();
			autowireCapableBeanFactory.autowireBean(cloudProperties);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}		
	}

	public void inUse(CloudDriverResponse volume, Container container) {
		try {

	        Resource resource = volume.getMessage().getAction().getResource();
			
	        log.debug(">>>>>>>>>>>>>>>>>>>>>>> volume mount start");
	        log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance ip : {}", resource.getAssociatedInstance().getPublicIp());  
	        log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance username : {}",  cloudProperties.getUserName());
	        log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance private key path : {}",  resource.getAssociatedInstance().getVolumes()[0].getAttachedMountName());
	        log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance file upload path : {}", cloudProperties.getMountShellFile());      
	        
			SSHClientUtil sshClientUtil = new SSHClientUtil();	        
    		for(int i=1; i<=10; i++) {
    			int result = sshClientUtil.init(resource.getAssociatedInstance().getPublicIp(), cloudProperties.getUserName(), cloudProperties.getPrivateKey());
        		if(result==0) {
        			break;
        		}
    			log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance connection retry : {}",  i);
        		Thread.sleep(5000);
    		}
    		
    		sshClientUtil.upload(cloudProperties.getFileUploadPath(), cloudProperties.getMountShellFile());  	

    		String command = getCommand(resource);
        	log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance run shell : {}", command);
		    sshClientUtil.command(command);

			boolean result = containerService.createContainerWithCloud(container);
			log.debug(">>>>>>>>>>> CloudVolumeService createContainer result : " + result);
		   
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String getCommand(Resource resource) {
		
		String command = null;
		switch(cloudProperties.getCloud()) {
		case AMAZONEFS:
     		
			String dnsAddressName = String.format("%s.efs.%s.amazonaws.com", resource.getNativeId(), resource.getPool().getRegion().getNativeId());
 			command = "chmod +x ./aws-efs.sh && sudo ./aws-efs.sh " + dnsAddressName + " " + resource.getAssociatedInstance().getVolumes()[0].getAttachedMountName();
			break;
		case TENCENT:
			break;
		case GOOGLE:
			break;
		}
		
		return command;	
	}
    
}
