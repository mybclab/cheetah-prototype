package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.management.cloud.dto.Pool;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
public class PoolService extends CloudCommonService {

    @Autowired
    protected WebClient client;


    public List<Pool> list() {
        Flux<Pool> response = client.get()
                .uri(apiUrl + resource + "/pool/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(Pool.class);
        return response.collectList().block();
    }

    public Pool get(String poolId) {

        Mono<Pool> response = client.get()
                .uri(apiUrl + resource + "/pool/" + poolId + "/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToMono(Pool.class);
        return response.block();

    }
}
