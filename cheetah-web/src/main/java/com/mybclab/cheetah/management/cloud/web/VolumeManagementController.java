package com.mybclab.cheetah.management.cloud.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.common.dto.AlertMessage;
import com.mybclab.cheetah.management.cloud.dto.VolumeForm;
import com.mybclab.cheetah.management.cloud.dto.VolumePage;
import com.mybclab.cheetah.management.cloud.exception.CloudException;
import com.mybclab.cheetah.management.cloud.service.CloudCodeService;
import com.mybclab.cheetah.management.cloud.service.PoolService;
import com.mybclab.cheetah.management.cloud.service.VolumeService;
import com.mybclab.common.utils.SecurityUtils;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@NoArgsConstructor
@RequestMapping("/management/volume")
public class VolumeManagementController {

    private VolumeService volumeService;

    private CloudCodeService cloudCodeService;

    private CodeGroupService codeGroupService;


    private PoolService poolService;

    @Autowired
    public VolumeManagementController(VolumeService volumeService, CloudCodeService cloudCodeService, PoolService poolService, CodeGroupService codeGroupService) {
        this.volumeService = volumeService;
        this.cloudCodeService = cloudCodeService;
        this.poolService = poolService;
        this.codeGroupService = codeGroupService;
    }

    @RequestMapping({"", "/list"})
    public String list(Model model) {
        User loginedUser = SecurityUtils.getPrincipal();

        VolumePage volumePage = volumeService.page("", loginedUser.getUsername());

        model.addAttribute("volumePage", volumePage);

        return "management/cloud/volume-list";
    }

    @GetMapping("/save")
    public String saveForm(Model model) {
        User loginedUser = SecurityUtils.getPrincipal();

        // TODO loginedUser 권한 체크

        VolumeForm volumeForm = new VolumeForm();
        volumeForm.setUsername(loginedUser.getUsername());
        model.addAttribute("volumeForm", volumeForm);
        return "management/cloud/volume-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute @Valid VolumeForm volumeForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        // FIXME instance 생성 호출 하드코딩
        if (Code.CLOUD_TYPE.AWS.equals(volumeForm.getCloudType())) {
            volumeForm.setPoolId("167a2fa3-0416-445e-bdca-c7c31b3d6ffa");
            volumeForm.setZoneId("a79b8101-a883-4358-a58e-ead28315b344");
        } else if (Code.CLOUD_TYPE.TENCENT.equals(volumeForm.getCloudType())) {
            volumeForm.setPoolId("1d1cd1b9-cd8c-470a-9615-5336a727f61e");
            volumeForm.setZoneId("635f0247-38e2-42d9-9e17-fb67d652b89a");
        }

        log.debug(">>>>>>> bindingResult {}", bindingResult);
        if (bindingResult.hasErrors()) {
            return "management/cloud/volume-form";
        }


        try {
            volumeService.save(volumeForm);
        } catch (CloudException e) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.ERROR).messageCode("cloud.instance.save.fail").build());
        }

        return "redirect:/management/volume";
    }

    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("cloudTypeList", codeGroupService.getCodeList(CodeGroupID.CLOUD_TYPE));
        map.put("poolList", poolService.list());
        String[] volumeTypes = {"HDD", "SSD"};
        map.put("volumeTypes", volumeTypes);
        map.put("diskAlloc ceNames", cloudCodeService.list("AttachDeviceName"));
        map.put("diskAllocationUnits", cloudCodeService.list("DiskAllocationUnit"));
        map.put("attachDeviceNames", cloudCodeService.list("AttachDeviceName"));

        return map;
    }
}
