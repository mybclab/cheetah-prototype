package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InstancePort {

    private String id;

    private String componentType;

    private String state;

    private String type;

    private int value;

    private String resourceId;
}
