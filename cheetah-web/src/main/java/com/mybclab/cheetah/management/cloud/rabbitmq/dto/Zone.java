package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Zone {

	private Region region;
	   
    private String nativeName;
    private String createUserId;
    private String createdDate;
    private String updateUserId;
    private String usedStatus;
    private String name;
    private String description;
    private String updatedDate;
    private String id;
    private String ownerId;
    private String nativeId;
    
}
