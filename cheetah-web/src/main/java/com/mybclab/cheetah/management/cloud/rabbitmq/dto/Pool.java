package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Pool {
    
	private Region region;
    private Owner owner;
    private Zones[] zones;
    
    private String authKey;
    private String createUserId;
    private String proxyIp;
    private String updateUserId;
    private String usedStatus;
    private String description;
    private String authKeyData;
    private String updatedDate;
    private String useProxy;
    private String ownerId;
    private String proxyPort;
    private String createdDate;
    private String authUserId;
    private String name;
    private String id;
    private String authType;
    
}
