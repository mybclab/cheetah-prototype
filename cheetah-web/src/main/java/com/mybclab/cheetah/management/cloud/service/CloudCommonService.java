package com.mybclab.cheetah.management.cloud.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CloudCommonService {

    @Value("${cloud.cloud-driver.api.url}")
    protected String apiUrl;

    protected String resource = "/api.resource/resource/v1/handle";

    protected String config = "/api.config/config/v1/handle";


    @Value("${cloud.cloud-driver.api.accessKey}")
    protected String accessKey;

    @Value("${cloud.cloud-driver.api.loginId}")
    protected String loginId;

    @Value("${cloud.cloud-driver.api.secretKey}")
    protected String secretKey;


    protected void setHttpHeaders(HttpHeaders h) {
        h.set("accessKey", accessKey);
        h.set("loginId", loginId);
        h.set("secretKey", secretKey);
        h.setContentType(MediaType.APPLICATION_JSON);
    }

    protected void setHttpHeaders(HttpHeaders h, String loginId) {
        h.set("accessKey", accessKey);
        h.set("loginId", loginId);
        h.set("secretKey", secretKey);
        h.setContentType(MediaType.APPLICATION_JSON);
    }
}
