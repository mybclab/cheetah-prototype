package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.management.cloud.dto.Image;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
@Slf4j
public class ImageService extends CloudCommonService {

    @Autowired
    protected WebClient client;


    public List<Image> list(String poolId) {
        Flux<Image> response = client.get()
                .uri(apiUrl + resource + "/image/" + poolId + "/list/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(Image.class);

        return response.collectList().block();
    }
}
