package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Zones {

    private Zone zone;

    private String createUserId;
    private String createdDate;
    private String updateUserId;
    private String poolId;
    private String zoneId;
    private String updatedDate;
    private String zoneName;
    private String ownerId;
    
}
