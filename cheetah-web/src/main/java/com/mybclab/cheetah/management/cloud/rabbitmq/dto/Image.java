package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Image {
    
	private Pool pool;
    private OsInfo osInfo;
    
    private String nativeName;
    private String createUserId;
    private String updateUserId;
    private String description;
    private String updatedDate;
    private String ownerId;
    private String type;
    private String nativeState;
    private String createdDate;
    private String size;
    private String removable;
    private String name;
    private String progressStatus;
    private String id;
    private String nativeId;
    private String originatedInstance;
    private String zone;
    private String associatedSnapshot;

}
