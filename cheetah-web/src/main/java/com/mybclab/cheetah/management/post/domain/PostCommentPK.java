package com.mybclab.cheetah.management.post.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Embeddable
public class PostCommentPK implements Serializable {


    private static final long serialVersionUID = -4859453109382789173L;

    private Long postSn;

    private int postCommentNo;

    public PostCommentPK() {}

    public PostCommentPK(Long postSn, int postCommentNo) {
        this.postSn = postSn;
        this.postCommentNo = postCommentNo;
    }
}
