package com.mybclab.cheetah.management.mypage.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.validator.UserFormValidator;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.utils.SecurityUtils;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

@Slf4j
@Controller
@RequestMapping("/management/mypage")
public class MyPageController {


    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;
    /**
     * {@link BCryptPasswordEncoder}
     */
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * {@link UserFormValidator}
     */
    private UserFormValidator userFormValidator;

    /**
     * default constructor
     */
    public MyPageController() {}

    /**
     * constructor
     * @param userService {@link UserService}
     * @param modelMapper {@link ModelMapper}
     * @param bCryptPasswordEncoder {@link BCryptPasswordEncoder}
     * @param userFormValidator {@link UserFormValidator}
     */
    @Autowired
    public MyPageController(UserService userService, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder, UserFormValidator userFormValidator) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userFormValidator = userFormValidator;
    }

    @RequestMapping("")
    public String mypage(Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        if (user.isGroupAdmin()) { // GROUP_ADMIN
            model.addAttribute("userRole", Code.ROLE.GROUP_ADMIN);
        } else if (user.isGroupUser()) { // GROUP_USER
            model.addAttribute("userRole", Code.ROLE.GROUP_USER);
        } else if (user.isGeneralUser()) { // GENERAL_USER
            model.addAttribute("userRole", Code.ROLE.GENERAL_USER);
        } else { // SYSTEM_ADMIN
            model.addAttribute("userRole", Code.ROLE.SYSTEM_ADMIN);

        }

        model.addAttribute(user);

        return "management/mypage/mypage";
    }

    @GetMapping("/modify")
    public String modifyView(Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        UserForm userForm = modelMapper.map(user, UserForm.class);
        model.addAttribute("userForm", userForm);
        return "management/mypage/mypage-form";
    }


    @PostMapping("/modify")
    public String modify(UserForm userForm, BindingResult bindingResult) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationManagement.class, UserForm.ValidationOldPassword.class);

        if (!bCryptPasswordEncoder.matches(userForm.getOldPassword(), user.getPassword())) {
            bindingResult.rejectValue("oldPassword", "Valid.differentPassword");
        }

        if (StringUtils.isNotEmpty(userForm.getPassword())) {
            userFormValidator.validate(userForm, bindingResult, UserForm.ValidationPassword.class);
        }
        if (bindingResult.hasErrors()) {
            return "management/mypage/mypage-form";
        }

        user.setName(userForm.getName());
        user.setPhoneNo(userForm.getPhoneNo());
        user.setEmail(userForm.getEmail());
        user.setSlackHookUrl(userForm.getSlackHookUrl());
        user.setSlackChannel(userForm.getSlackChannel());
        user.setOrganizationName(userForm.getOrganizationName());
        user.setTeamName(userForm.getTeamName());

        user.setUpdatedBy(user.getUsername());
        user.setUpdatedAt(LocalDateTime.now());
        user.setUserImageFileSn(userForm.getUserImageFileSn());

        if (StringUtils.isNotEmpty(userForm.getPassword())) {
            user.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        }
        userService.saveWithFile(user, null, userForm.getUserImageFile());

        return "redirect:/management/mypage";
    }



}
