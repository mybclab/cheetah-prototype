package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class Pool implements Serializable {

    private String name;

    private String id;

    private String usedStatus;

    private String description;

    private List<Zone> zones;


    private Region region;


}
