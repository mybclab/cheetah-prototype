package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Offering {

    private Region region;
    
    private String createUserId;
    private String sizeOfMemory;
    private String updateUserId;
    private String sizeOfRootVolume;
    private String description;
    private String updatedDate;
    private String ownerId;
    private String createdDate;
    private String regionId;
    private String name;
    private String numberOfCpu;
    private String id;

}
