package com.mybclab.cheetah.management.post.service;

import com.mybclab.cheetah.management.post.domain.PostComment;
import com.mybclab.cheetah.management.post.repository.PostCommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostCommentService {

    private PostCommentRepository postCommentRepository;

    public PostCommentService() {}

    @Autowired
    public PostCommentService(PostCommentRepository postCommentRepository) {
        this.postCommentRepository = postCommentRepository;
    }


    public List<PostComment> findAllByPostSn(Long postSn) {
        return postCommentRepository.findAllByPostCommentPKPostSn(postSn);
    }

    public PostComment save(PostComment postComment) {
        int postCommentNo = postCommentRepository.findMaxPostCommentNo(postComment.getPostCommentPK().getPostSn()) + 1;
        postComment.getPostCommentPK().setPostCommentNo(postCommentNo);
        return postCommentRepository.save(postComment);
    }

}
