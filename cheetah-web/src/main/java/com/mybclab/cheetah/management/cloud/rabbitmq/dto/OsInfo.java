package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OsInfo {

	private String createUserId;
    private String createdDate;
    private String updateUserId;
    private String name;
    private String osType;
    private String updatedDate;
    private String id;
    private String version;
    private String architecture;
	    
}
