package com.mybclab.cheetah.management.cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CloudCode implements Serializable {

    private String groupCode;

    private String systemCode;

    private String systemName;

    private int displayOrder;

    private String systemInfo;

    private String detailInfo;

    private String description;

    private String codeId;

    private String codeName;

}
