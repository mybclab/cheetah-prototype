package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SecurityGroup {

	private String nativeName;
    private String name;
    private String description;
    private String id;
    private String ownerId;
    private String nativeId;
    
}
