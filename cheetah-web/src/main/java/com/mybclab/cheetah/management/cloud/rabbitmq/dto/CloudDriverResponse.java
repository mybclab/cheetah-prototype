package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CloudDriverResponse {

    private String result;
    private String state;

    private Message message;
    
}
