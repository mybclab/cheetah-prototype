package com.mybclab.cheetah.management.cloud.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CloudException extends RuntimeException {

    private HttpStatus httpStatus;

    public CloudException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }


}
