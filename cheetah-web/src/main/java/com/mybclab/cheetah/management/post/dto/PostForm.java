package com.mybclab.cheetah.management.post.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
public class PostForm implements Serializable {

    /**
     * 게시물 일련번호
     */
    private Long postSn;
    /**
     * 제목
     */
    @NotBlank
    private String title;

    /**
     * 내용
     */
    @NotBlank
    private String contents;

    /**
     * 사용 여부
     */
    @NotNull
    private Boolean useYn = true;

    private String groupId;
}
