package com.mybclab.cheetah.management.card.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCard;
import com.mybclab.cheetah.admin.user.domain.UserCardPK;
import com.mybclab.cheetah.admin.user.dto.UserCardForm;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.common.dto.AlertMessage;
import com.mybclab.common.utils.SecurityUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * 사용자 카드 관리 Controller
 */
@RequestMapping("/management/card")
@Controller
public class UserCardManagementController {

    /**
     * {@link UserCardService}
     */
    private UserCardService userCardService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;
    /**
     * default contstructor
     */
    public UserCardManagementController() {}

    /**
     * constructor
     * @param userCardService {@link UserCardService}
     * @param modelMapper {@link ModelMapper}
     */
    @Autowired
    public UserCardManagementController(UserCardService userCardService, ModelMapper modelMapper) {
        this.userCardService = userCardService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping("")
    public String card(Model model) {
        User user = SecurityUtils.getPrincipal();

        List<UserCard> userCardList = userCardService.findAllByUsername(user.getUsername());

        model.addAttribute(userCardList);

        return "management/card/card-list";
    }

    /**
     * 사용자 카드 등록
     * @param model {@link Model}
     * @return 사용자 카드 등록 화면
     */
    @GetMapping("/save")
    public String save(Model model) {
        model.addAttribute("userCardForm", new UserCardForm());
        return "management/card/card-form";
    }
    /**
     * 사용자 카드 등록
     * @param userCardForm {@link UserCardForm}
     * @return 사용자 카드 등록 화면
     */
    @PostMapping("/save")
    public String saveUserCardForm(@Valid UserCardForm userCardForm, BindingResult bindingResult) {
        User user = SecurityUtils.getPrincipal();

        if (bindingResult.hasErrors()) {
            return "management/card/card-form";
        }

        UserCardPK userCardPK = new UserCardPK();
        userCardPK.setUsername(user.getUsername());
        UserCard userCard = modelMapper.map(userCardForm, UserCard.class);
        userCard.setUserCardPK(userCardPK);
        userCard.setCreatedBy(user.getUsername());
        userCard.setUseYn(true);
        userCardService.addCard(userCard);

        return "redirect:/management/card";
    }


    @PostMapping("/{userCardNo}/delete")
    public String deleteCard(@ModelAttribute UserCardPK userCardPK, RedirectAttributes redirectAttributes) {
        User user = SecurityUtils.getPrincipal();
        userCardPK.setUsername(user.getUsername());

        if (userCardService.isValidCard(userCardPK)) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.WARNING).messageCode("Error.deleteUseCard").build());
            return "redirect:/management/card";
        }

        userCardPK.setUsername(user.getUsername());
        userCardService.deleteById(userCardPK);
        return "redirect:/management/card";
    }
}
