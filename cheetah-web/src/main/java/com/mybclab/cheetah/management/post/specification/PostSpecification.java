package com.mybclab.cheetah.management.post.specification;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.management.post.domain.Post;
import com.mybclab.cheetah.management.post.dto.PostCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import java.util.List;
import java.util.Optional;

public class PostSpecification {

    public static Specification<Post> inGroupId(final List<String> groupIdList) {
        if (null == groupIdList || groupIdList.isEmpty()) {
            return null;
        }

        return Optional.ofNullable(groupIdList)
                .map(o -> (Specification<Post>) (root, query, criteriaBuilder) -> {
                    final Path<String> groupIdPath = root.<String>get("groupId");
                    return groupIdPath.in(groupIdList);
                })
                .orElse(null);
    }
    /**
     * equal 게시판 구분 코드
     * @param boardTypeCode 게시판 구분 코드
     * @return {@link Specification < Post >}
     */
    public static Specification<Post> equalsBoardTypeCode(final Code.BOARD_TYPE boardTypeCode) {
        return Optional.ofNullable(boardTypeCode)
                .map(o -> (Specification<Post>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Code.BOARD_TYPE>get("boardTypeCode"), o))
                .orElse(null);
    }

    /**
     * like 제목
     * @param title 제목
     * @return {@link Specification <  Post  >}
     */
    public static Specification<Post> likeTitle(final String title) {
        return Optional.ofNullable(title)
                .map(o -> (Specification<Post>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("title"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * like 내용
     * @param contents 내용
     * @return {@link Specification< Post >}
     */
    public static Specification<Post> likeContents(final String contents) {
        return Optional.ofNullable(contents)
                .map(o -> (Specification<Post>) (root, query, criteriaBuilder) -> criteriaBuilder.like(root.<String>get("contents"), "%" + o + "%"))
                .orElse(null);
    }

    /**
     * equal 사용 여부
     * @param postCriteria 사용 여부, createdBy
     * @return {@link Specification< Post >}
     */
    public static Specification<Post> equalsUseYn(final PostCriteria postCriteria) {

        if (null == postCriteria || null == postCriteria.getUseYn() || !postCriteria.getUseYn()) {
            return null;
        }

        if (postCriteria.getCreatedBy() == null) {
            return Optional.ofNullable(postCriteria)
                    .map(p -> (Specification<Post>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.<Boolean>get("useYn"), p.getUseYn()))
                    .orElse(null);
        }

        return Optional.ofNullable(postCriteria)
                .map(p -> (Specification<Post>) (root, query, criteriaBuilder) -> criteriaBuilder.or(criteriaBuilder.equal(root.<Boolean>get("useYn"), p.getUseYn()), criteriaBuilder.equal(root.<String>get("createdBy"), p.getCreatedBy())))
                .orElse(null);
    }
}
