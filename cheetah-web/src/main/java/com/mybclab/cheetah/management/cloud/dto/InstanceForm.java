package com.mybclab.cheetah.management.cloud.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class InstanceForm implements Serializable {


    /**
     * 인스턴스 이름
     */
    @NotEmpty
    private String name;

    @JsonIgnore
    private String[] portType;

    private List<Port> ports;

    @JsonIgnore
    @NotNull
    private String poolId; // ?

    private String authUserId;

    private String authPassword;

    private String clusterOption = "CreateCluster";


    private int numberOfInstances = 1;

    private String offeringId;

    private String imageId;

    private String[] securityGroups;

    private String[] zoneList;

    private String keyName;

    private String description;

    private List<Volume> volumes;

    /*
    {
 "authUserId": "n3ncloud", // required
 "authPassword": "n3ncloud", // required
 "name": "n3ncloud-test5",
 "clusterOption": "CreateCluster",
 "numberOfInstances": 1,
 "ports": [
   "SSH"
 ],
 "offeringId": "t2.micro",
 "imageId": "86aee8f2-102a-4866-83ba-05b8e03aaf13",
 "securityGroups": [
   "7d341db8-3f12-4579-8667-f532fa287f43"
 ],
 "zoneList": [
   "43dbffd3-9180-43ed-a316-8737e810d5cd"
 ],
 "keyName": "n3ncloud",
 "description": "인스턴스 설명" // required
}
     */

}
