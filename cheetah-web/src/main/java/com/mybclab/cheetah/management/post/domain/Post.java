package com.mybclab.cheetah.management.post.domain;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@ToString(exclude = {"postCommentList"})
public class Post extends HistoryBase implements Serializable {


    private static final long serialVersionUID = -1545811883096928303L;

    /**
     * 공지사항 일련번호
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long postSn;

    private String groupId;
    /**
     * 게시판 구분
     */
    @Enumerated(EnumType.STRING)
    private Code.BOARD_TYPE boardTypeCode;
    /**
     * 제목
     */
    private String title;

    /**
     * 내용
     */
    private String contents;

    /**
     * 조회 수
     */
    private int readCount;

    /**
     * 사용 여부
     */
    private Boolean useYn;

    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "postSn", referencedColumnName = "postSn", insertable = false, updatable = false)
    @OrderBy("postCommentPK.postCommentNo ASC")
    List<PostComment> postCommentList;
}
