package com.mybclab.cheetah.management.cloud.rabbitmq.properties;

import org.springframework.beans.factory.annotation.Value;

import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;

public class AmazonEfs extends CloudProperties implements CloudPropertiesService {

    @Value("${cloud.vendor.amazon.instance.username}")
	private String USERNAME;

	@Value("${cloud.vendor.amazon.instance.node-install-file-path}")
	private String NODE_INSTALL_FILE_PATH;
   
	@Value("${cloud.vendor.amazon.instance.node-install-file-name}")
	private String NODE_INSTALL_FILE_NAME;
	
    @Value("${cloud.vendor.amazon.instance.private-key-file-path}")
	private String PRIVATE_KEY_FILE_PATH;
    
    @Value("${cloud.vendor.amazon.file.upload-path}")
	private String FILE_UPLOAD_PATH;
    
    @Value("${cloud.vendor.amazon.volume.name}")
	private String VOLUME_NAME;
    
    @Value("${cloud.vendor.amazon.volume.mount-file-path}")
	private String VOLUME_MOUNT_FILE_PATH;

    @Value("${cloud.vendor.amazon.volume.mount-file-name}")
	private String VOLUME_MOUNT_FILE_NAME;
    
	@Override
	public String getUserName() {
		return this.USERNAME;
	}

	@Override
	public String getNodeInstallFilePath() {
		return this.NODE_INSTALL_FILE_PATH;
	}

	@Override
	public String getNodeInstallFileName() {
		return this.NODE_INSTALL_FILE_NAME;
	}

	@Override
	public String getPrivateKeyFilePath() {
		return this.PRIVATE_KEY_FILE_PATH;
	}

	@Override
	public String getFileUploadPath() {
		return this.FILE_UPLOAD_PATH;
	}

	@Override
	public String getVolumeName() {
		return this.VOLUME_NAME;
	}

	@Override
	public String getVolumeMountFilePath() {
		return this.VOLUME_MOUNT_FILE_PATH;
	}

	@Override
	public String getVolumeMountFileName() {
		return this.VOLUME_MOUNT_FILE_NAME;
	}

	@Override
	public CLOUD_VENDOR getCloud() {
		return CLOUD_VENDOR.AMAZONEFS;
	}
	
	
}
