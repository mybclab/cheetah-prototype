package com.mybclab.cheetah.management.cloud.rabbitmq.constant;

public class CloudCode {

    public enum INSTANCE_STATUS {

        None("none"),
        Pending("pending"),
        Building("building"),
        ShuttingDown("shuutting-down"),
        Starting("starting"),
        StartingFailed("starting-failed"),
        Rebooting("rebooting"),
        RebootingFailed("rebooting-failed"),
        Stopping("stopping"),
        StoppingFailed("stopping-failed"),
        Running("running"),
        Terminating("terminating"),
        TerminatingFailed("terminating-failed"),
        Terminated("terminated"),
        Destroyed("destroyed"),
        Stopped("stopped"),
        BackingUp("backing"),
        BackedUp("backed"),
        Restoring("restoring"),
        Restored("restored"),
        Error("error");

        private String status;

        INSTANCE_STATUS(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }

        public static INSTANCE_STATUS byValue(String value) {
            for (INSTANCE_STATUS status: INSTANCE_STATUS.values()) {
                if (status.status.equalsIgnoreCase(value)) {
                    return status;
                }
            }
            return null;
        }

    }

    public enum VOLUME_STATUS {
        Available,
        Creating,
        Deleted,
        Deleting,
        Updating,
        Attaching,
        Detaching,
        Resizing,
        Rollbacking,
        Resetting,
        Dumping,
        Error,
        InUse,
        None
    }

    public enum CLOUD_VENDOR {
        AMAZONEFS("AmazonEfs", "com.mybclab.cheetah.management.cloud.rabbitmq.properties.AmazonEfs"),
        GOOGLE("Google", "com.mybclab.cheetah.management.cloud.rabbitmq.properties.Google"),
        TENCENT("TencentCloud", "com.mybclab.cheetah.management.cloud.rabbitmq.properties.TencentCloud");

        private String name;
        private String className;

        CLOUD_VENDOR(String name, String className) {
            this.name = name;
            this.className = className;
        }

        public String getName() {
            return this.name;
        }

        public String getClassName() {
            return this.className;
        }

        public static CLOUD_VENDOR byValue(String value) {
            for (CLOUD_VENDOR cloud: CLOUD_VENDOR.values()) {
                if (cloud.name.equalsIgnoreCase(value)) {
                    return cloud;
                }
            }
            return null;
        }

    }

}