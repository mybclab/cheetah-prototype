package com.mybclab.cheetah.management.container.web;

import com.mybclab.cheetah.admin.gpu.domain.Gpu;
import com.mybclab.cheetah.admin.gpu.service.GpuService;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.admin.pod.domain.PodFormat;
import com.mybclab.cheetah.admin.pod.domain.PodFormatOption;
import com.mybclab.cheetah.admin.pod.service.PodFormatService;
import com.mybclab.cheetah.admin.pod.service.PodOptionGroupService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserGpu;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.common.dto.AlertMessage;
import com.mybclab.cheetah.common.service.MailService;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerPK;
import com.mybclab.cheetah.container.domain.ContainerVolume;
import com.mybclab.cheetah.container.domain.ContainerVolumePK;
import com.mybclab.cheetah.container.dto.ContainerCriteria;
import com.mybclab.cheetah.container.dto.ContainerForm;
import com.mybclab.cheetah.container.dto.ContainerVolumeForm;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.kubernetes.exception.KubernetesException;
import com.mybclab.cheetah.management.cloud.service.VolumeService;
import com.mybclab.common.notifier.SlackNotifier;
import com.mybclab.common.utils.SecurityUtils;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 컨테이너 Controller
 */
@Slf4j
@Controller
@NoArgsConstructor
@RequestMapping("/management/container")
public class ContainerManagementController {

    /**
     * {@link ContainerService}
     */
    private ContainerService containerService;

    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link GpuService}
     */
    private GpuService gpuService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    /**
     * {@link NodeService}
     */
    private NodeService nodeService;
    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    /**
     * {@link PodFormatService}
     */
    private PodFormatService podFormatService;
    private PodOptionGroupService podOptionGroupService;

    private MailService mailService;
    private SlackNotifier slackNotifier;

    private VolumeService volumeService;

    @Value("${cheetah.kubernetes.cloud-basepath}")
    private String CLOUD_BASE_PATH;


    /**
     * constructor
     * @param containerService {@link ContainerService}
     * @param userService {@link UserService}
     * @param gpuService {@link GpuService}
     * @param modelMapper {@link ModelMapper}
     * @param nodeService {@link NodeService}
     * @param codeGroupService {@link CodeGroupService}
     * @param podFormatService {@link PodFormatService}
     */
    @Autowired
    public ContainerManagementController(ContainerService containerService, UserService userService, GpuService gpuService
            , ModelMapper modelMapper, NodeService nodeService, CodeGroupService codeGroupService, PodFormatService podFormatService
                                         , PodOptionGroupService podOptionGroupService, VolumeService volumeService
            , MailService mailService, SlackNotifier slackNotifier) {
        this.containerService = containerService;
        this.userService = userService;
        this.gpuService = gpuService;
        this.modelMapper = modelMapper;
        this.nodeService = nodeService;
        this.codeGroupService = codeGroupService;
        this.podFormatService = podFormatService;
        this.podOptionGroupService = podOptionGroupService;
        this.volumeService = volumeService;
        this.mailService = mailService;
        this.slackNotifier = slackNotifier;
    }

    @RequestMapping({"", "/list", "/group"})
    public String list(ContainerCriteria containerCriteria, @PageableDefault(sort = { "createdAt" }, direction = Sort.Direction.DESC) Pageable pageable, Model model) {
        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String path = builder.buildAndExpand().getPath();

        User user = SecurityUtils.getPrincipal();
        if (user.isSystemAdmin()) {
            model.addAttribute("groupList", userService.findAllByUserRole(Code.ROLE.GROUP_ADMIN));
            List<User> userList = new ArrayList<>();
            if ("".equals(containerCriteria.getParentUsername())) {
                userList = userService.findAllByUserRole(Code.ROLE.GENERAL_USER);
            } else if (!"ALL".equals(containerCriteria.getParentUsername())) {
                userList = userService.findAllByGroupId(containerCriteria.getParentUsername());
            }
            model.addAttribute("userList", userList);
        } else if (user.isGroupAdmin() && path.contains("group")) {
            model.addAttribute("userList", userService.findAllByGroupId(user.getUsername()));
            containerCriteria.setParentUsername(user.getUsername());
            containerCriteria.setOnlyGroupUser(true);
        } else {
            containerCriteria.setUsername(user.getUsername());
        }

        model.addAttribute("containerPage", containerService.findAll(containerCriteria, pageable));
        model.addAttribute(containerCriteria);
        model.addAttribute("path", path);

        return "management/container/container-list";
    }

    @GetMapping("/{username}/{containerId}")
    public String container(@ModelAttribute ContainerPK containerPK, Model model) {
        User user = SecurityUtils.getPrincipal();
        Container container = containerService.findById(containerPK);


        if (StringUtils.isNotEmpty(container.getParentContainerId())) {
            Container parentContainer = containerService.findByContainerId(container.getParentContainerId());
            container.setParentContainer(parentContainer);
        }

        model.addAttribute("container", container);
        if (container.getGroupSharedYn() && container.getParentContainerId() == null) {
            if (container.getRemainContainerSize() > 0) {
                model.addAttribute("remainUserList", this.getRemainGroupUserList(container));
            }
        }
        boolean showPasswordAndToken = false;
        if (user.isSystemAdmin()
                || user.getUsername().equals(container.getContainerPK().getUsername())
                || (user.isGroupAdmin() && user.getUsername().equals(container.getUser().getParentUsername()))) {
            showPasswordAndToken = true;
        }
        model.addAttribute("showPasswordAndToken", showPasswordAndToken);
        model.addAttribute("listPath", user.isGroupAdmin() && !user.getUsername().equals(container.getContainerPK().getUsername()) ? "group" : "");

        return "management/container/container";
    }

    @GetMapping("/save")
    public String save(Model model) {
        User loginedUser = SecurityUtils.getPrincipal();

        User user = userService.findById(loginedUser.getUsername());
        ContainerForm containerForm = new ContainerForm();
        containerForm.setUsername(user.getUsername());
        containerForm.setGpuQuantity(1);
        containerForm.setChargingMethodCode(Code.CHARGING_METHOD.HOURLY);
        containerForm.setContainerVolumes(new ArrayList<ContainerVolumeForm>() {{
            add(new ContainerVolumeForm());
        }});

        model.addAttribute("userGpuSns", getUserGpuSns(user));
        model.addAttribute(containerForm);
        model.addAttribute("groupUsers", getGroupUsers(user));
        return "management/container/container-form";
    }

    private long[] getUserGpuSns(User user) {
        return user.getUserGpuList().stream().mapToLong(ug -> ug.getUserGpuPK().getGpuSn()).toArray();
    }

    private List<User> getGroupUsers(User user) {
        return user.getGroupUsers() != null ? user.getGroupUsers().stream().filter(u -> u.getConfirmYn() && u.getWithdrawalDate() == null).collect(Collectors.toList()) : null;
    }

    @GetMapping("/{username}/{containerId}/modify")
    public String modify(@ModelAttribute ContainerPK containerPK, Model model) {
        User loginedUser = SecurityUtils.getPrincipal();

        User user = userService.findById(loginedUser.getUsername());
        Container container = containerService.findById(containerPK);
        ContainerForm containerForm = modelMapper.map(container, ContainerForm.class);
        containerForm.setContainerId(container.getContainerPK().getContainerId());
        containerForm.setUsername(container.getContainerPK().getUsername());

        PodFormat podFormat = container.getPodFormat();

        Map<String, Long> podOptionSnMap = podFormat.getPodFormatOptionList().stream().collect(Collectors.toMap(PodFormatOption::getPodOptionGroupId, o->o.getPodFormatOptionPK().getPodOptionSn()));
        containerForm.setPodOptionSnMap(podOptionSnMap);

        if (container.getContainerVolumeList().size() > 0) {
            List<ContainerVolumeForm> containerVolumeForms = new ArrayList<>();
            container.getContainerVolumeList().forEach(cvf -> {
                ContainerVolumeForm containerVolumeForm = new ContainerVolumeForm();
                containerVolumeForm.setVolumeId(cvf.getVolumeId());
                containerVolumeForm.setVolumeMountPath(cvf.getVolumeMountPath());
                containerVolumeForm.setVolumeSize(cvf.getVolumeSize());
                containerVolumeForms.add(containerVolumeForm);
            });
            containerForm.setContainerVolumes(containerVolumeForms);
        } else {
            containerForm.setContainerVolumes(new ArrayList<ContainerVolumeForm>() {{
                add(new ContainerVolumeForm());
            }});
        }


        model.addAttribute(containerForm);
        model.addAttribute("userGpuSns", getUserGpuSns(user));
        model.addAttribute("groupUsers", getGroupUsers(user));
        model.addAttribute("isModify", true);

        return "management/container/container-form";
    }


    @PostMapping(value = {"/save", "/{username}/{containerId}/modify"})
    public String saveOrModify(@ModelAttribute @Valid ContainerForm containerForm, BindingResult bindingResult, int poOptionSize, HttpServletRequest request, Model model) {
        User user = userService.findById(containerForm.getUsername());


        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String requestedValue = builder.buildAndExpand().getPath();
        boolean isNew = requestedValue.contains("save");

        Container container = new Container();

        ContainerPK containerPK = modelMapper.map(containerForm, ContainerPK.class);
        containerPK.setUsername(user.getUsername());
        containerPK.setContainerId(isNew ? containerService.generateContainerId(user.getUsername()) : containerPK.getContainerId());

        if (!isNew) {
            container = containerService.findById(containerPK);
        }

        Gpu gpu = gpuService.findById(containerForm.getGpuSn());
        List<UserGpu> userGpuList = user.getUserGpuList();
        boolean hasQuota = userGpuList.stream().anyMatch(ug -> ug.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn()));
        if (!hasQuota) {
            bindingResult.rejectValue("gpuSn", "Valid.notAllowedResource");
        } else {
            for (UserGpu userGpu : userGpuList) {
                if (userGpu.getUserGpuPK().getGpuSn().equals(gpu.getGpuSn())) {
                    log.info(">> check quota quantity getUseGpuQuantity : " + userGpu.getUseQuantity() + " - " + container.getGpuQuantity() + " + " + containerForm.getGpuQuantity() + " > " + userGpu.getQuotaQuantity());
                    if (userGpu.getUseQuantity() + containerForm.getGpuQuantity() > userGpu.getQuotaQuantity()) {
                        bindingResult.rejectValue("gpuQuantity", "Valid.overMaxQuantity");
                        break;
                    }
                }
            }
            if (Code.CLOUD_TYPE.ON_PREMISE.equals(gpu.getCloudType())) {
                if (Code.RESOURCE_TYPE.GPU.equals(gpu.getResourceType())) {
                    log.info(">> check system gpu quantity : " + gpu.getRemainMaxGpuQuantityPerNode() + "," + containerForm.getGpuQuantity());
                    if (gpu.getRemainMaxGpuQuantityPerNode() < containerForm.getGpuQuantity()) {
                        bindingResult.rejectValue("gpuQuantity", "Valid.overRemainQuantity", new Object[]{gpu.getRemainMaxGpuQuantityPerNode()}, "현재 시스템에 남은 GPU 수량({0})을 초과하였습니다. 시스템관리자에게 문의하세요.");
                    }
                }
            }
        }
        if (containerForm.getPodOptionSnMap() == null || containerForm.getPodOptionSnMap().keySet().size() != poOptionSize) {
            bindingResult.rejectValue("podFormatSn", "Valid.noSelectedOptions");
        } else {
            try {
                PodFormat podFormat = podFormatService.findByPodOptionHint(containerForm.getPodOptionSnMap());
                containerForm.setPodFormatSn(podFormat.getPodFormatSn());
            } catch (EntityNotFoundException e) {
                bindingResult.rejectValue("podFormatSn", "Valid.notFoundPodFormat");
            }
        }

        if (bindingResult.hasErrors()) {
            log.error("bindingResult : " + bindingResult.toString());
            model.addAttribute("userGpuSns", getUserGpuSns(user));
            model.addAttribute("groupUsers", getGroupUsers(user));
            return "management/container/container-form";
        }


        if (isNew) {
            container = modelMapper.map(containerForm, Container.class);
            container.setContainerPK(containerPK);
        } else {
            container.setContainerName(containerForm.getContainerName());
            container.setPodFormatSn(containerForm.getPodFormatSn());
            container.setGpuSn(containerForm.getGpuSn());
            container.setGpuQuantity(containerForm.getGpuQuantity());
            container.setDescription(containerForm.getDescription());
            container.setChargingMethodCode(containerForm.getChargingMethodCode());
            container.setGroupSharedYn(containerForm.getGroupSharedYn());
            container.setGroupSharedSize(containerForm.getGroupSharedSize());
            container.setGroupTargetUser(containerForm.getGroupTargetUser());
            container.setTokenYn(containerForm.getTokenYn());
        }

        container.setStatusCode(isNew ? Code.CONTAINER_STATUS.REQUEST : container.getStatusCode());

        // volume
        List<ContainerVolume> containerVolumeList = new ArrayList<>();
        List<ContainerVolumeForm> containerVolumeForms = containerForm.getContainerVolumes();
        for (int i = 0; i < containerVolumeForms.size(); i++) {
            ContainerVolumeForm containerVolumeForm = containerVolumeForms.get(i);
            if (StringUtils.isNotEmpty(containerVolumeForm.getVolumeId())) {
                ContainerVolumePK containerVolumePK = modelMapper.map(containerForm, ContainerVolumePK.class);
                containerVolumePK.setContainerId(container.getContainerPK().getContainerId());
                containerVolumePK.setContainerVolumeNo(i);
                ContainerVolume containerVolume = modelMapper.map(containerVolumeForm, ContainerVolume.class);
                containerVolume.setContainerVolumePK(containerVolumePK);
                containerVolume.setCloudType(gpu.getCloudType());
                containerVolumeList.add(containerVolume);
            }
        }
        // FIXME tencent volume 하드코딩, mountpath 하드코딩
        container.setGpu(gpu);
        if (container.isTargetToMakeTencentVolume()) {
            ContainerVolumePK containerVolumePK = modelMapper.map(containerForm, ContainerVolumePK.class);
            containerVolumePK.setContainerId(container.getContainerPK().getContainerId());
            containerVolumePK.setContainerVolumeNo(containerVolumeList.size());
            ContainerVolume containerVolume = new ContainerVolume();
            // FIXME - start
            containerVolume.setVolumeId(container.getContainerPK().getUsername() + "-volume-" + com.mybclab.common.utils.StringUtils.randomLowerAlphanumeric(5));
            containerVolume.setVolumeMountPath(CLOUD_BASE_PATH + "/" + container.getContainerPK().getUsername());
            containerVolume.setVolumeSize(100);
            // FIXME - end
            containerVolume.setContainerVolumePK(containerVolumePK);
            containerVolume.setCloudType(Code.CLOUD_TYPE.TENCENT);
            containerVolumeList.add(containerVolume);
        }

        container.getContainerVolumeList().clear();
        container.getContainerVolumeList().addAll(containerVolumeList);

        if (isNew) {
            containerService.register(container);

            slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                    SlackNotifier.SlackMessageAttachement.builder()
                        .title("컨테이너 신청")
                        .title_link((request.getServerName() + ":" + request.getServerPort() + "/management/container/" + container.getContainerPK().getUsername() + "/" + container.getContainerPK().getContainerId()))
                        .text(String.format("%s(%s) 님이 '%s' 컨테이너를 신청하였습니다.", user.getName(), user.getUsername(), container.getContainerName()))
                        .build(), userService.findSlackTargetList(user));
        } else {
            containerService.save(container);
        }


        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }



    @PostMapping("/{username}/{containerId}/delete")
    public String delete(@ModelAttribute ContainerPK containerPK) {
        containerService.delete(containerPK);
        return "redirect:/management/container";
    }

    @PostMapping("/{username}/{containerId}/token")
    public String sendTokenAgain(@ModelAttribute ContainerPK containerPK, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);

        mailService.sendJupyterToken(container);
        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("mail.send.token.success").build());

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @PostMapping("/{username}/{containerId}/password")
    public String sendSshPassword(@ModelAttribute ContainerPK containerPK, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);

        mailService.sendSshPassword(container);
        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("mail.send.password.success").build());

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @PostMapping("/{username}/{containerId}/init")
    public String initContainer(@ModelAttribute ContainerPK containerPK, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);

        boolean result = containerService.initContainer(container);
        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("Kubernetes.pod.init.success").build());

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @PostMapping("/{username}/{containerId}/CONFIRM")
    public String startOrReturn(@ModelAttribute ContainerPK containerPK, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);
        container.setStatusCode(Code.CONTAINER_STATUS.CONFIRM);
        containerService.save(container);
        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("Kubernetes.pod.confirm.success").build());

        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @PostMapping("/{username}/{containerId}/{statusCode:CREATE|RECREATE|RESTART|RETURN|CREATE_INSTANCE|RETURN_INSTANCE}")
    public String controlContainer(@PathVariable Code.CONTAINER_STATUS statusCode, @ModelAttribute ContainerPK containerPK, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);
        String statusCodeLowerCase = statusCode.name().toLowerCase();

        boolean result = false;
        if (statusCode == Code.CONTAINER_STATUS.CREATE) {
            result = containerService.createContainerWithoutCloud(container);
        } else if (statusCode == Code.CONTAINER_STATUS.CREATE_INSTANCE) {
            result = containerService.createInstance(container); // cloud
        } else if (statusCode == Code.CONTAINER_STATUS.RETURN_INSTANCE) {
            result = containerService.deleteInstance(container); // cloud
        } else if (statusCode == Code.CONTAINER_STATUS.RECREATE) {
            try {
                result = containerService.createContainer(container, true);
            } catch (KubernetesException e) {
                redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode(e.getMessage()).build());
                return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
            }
        } else if (statusCode == Code.CONTAINER_STATUS.RESTART) {
            result = containerService.restartContainer(container);
        } else if (statusCode == Code.CONTAINER_STATUS.RETURN) {
            result = containerService.returnContainerWithoutCloud(container);

            User containerUser = container.getUser();
            slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                    SlackNotifier.SlackMessageAttachement.builder()
                            .title("컨테이너 반납")
                            .text(String.format("%s(%s) 님이 '%s' 컨테이너를 반납하였습니다. (%s)", containerUser.getName(), containerUser.getUsername(), container.getContainerName(), result ? "성공" : "실패"))
                            .build(), userService.findSlackTargetList(containerUser));
        }

        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(result ? AlertMessage.ALERT_MESSAGE_LEVEL.SUCCESS : AlertMessage.ALERT_MESSAGE_LEVEL.ERROR)
                .messageCode("Kubernetes.pod." + statusCodeLowerCase + "." + (result ? "success" : "fail")).build());
        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    @PostMapping("/{username}/{containerId}/copy")
    public String copyContainer(@ModelAttribute ContainerPK containerPK, String targetUsername, RedirectAttributes redirectAttributes) {
        Container container = containerService.findById(containerPK);
        User user = userService.findById(targetUsername);

        containerService.copyAndCreateGroupContainer(container, user);


        return "redirect:/management/container/" + containerPK.getUsername() + "/" + containerPK.getContainerId();
    }

    private List<User> getRemainGroupUserList(Container container) {
        List<User> userList = container.getUser().getGroupUsers();
        container.getGroupTargetUserList().forEach(c -> {
            userList.removeIf(u -> u.getUsername().equals(c.getUsername()));
        });
        return userList;
    }

    @PostMapping("/{username}/delete/all")
    public String deleteAllContainer(@PathVariable String username, Code.CONTAINER_STATUS excludeStatusCode) {

        List<Container> containerList = containerService.findAllByUsername(username);
        containerList = containerList.stream().filter(c -> !Code.CONTAINER_STATUS.START.equals(c.getStatusCode())).collect(Collectors.toList());
        if (excludeStatusCode != null) {
            containerList = containerList.stream().filter(c -> !excludeStatusCode.equals(c.getStatusCode())).collect(Collectors.toList());
        }

        containerService.deleteAll(containerList);

        return "redirect:/admin/user/" + username;
    }


    /**
     * ReferenceData
     *
     * @return reference data
     */
    @ModelAttribute("referenceData")
    public Map<String, Object> referenceData() {
        Map<String, Object> map = new HashMap<>();

        map.put("gpuList", gpuService.findAll(true));
        map.put("podFormatList", podFormatService.findAll(true));
        map.put("nodeList", nodeService.findAllByStatusCode(Code.NODE_STATUS.READY));
        map.put("statusList", codeGroupService.getCodeList(CodeGroupID.CONTAINER_STATUS));
        map.put("chargingMethodList", codeGroupService.getCodeList(CodeGroupID.CHARGING_METHOD));

        map.put("requiredList", podOptionGroupService.findAllByType(Code.POD_OPTION_TYPE.REQUIRED));
        map.put("libraryList", podOptionGroupService.findAllByType(Code.POD_OPTION_TYPE.LIBRARY));

        return map;
    }
}
