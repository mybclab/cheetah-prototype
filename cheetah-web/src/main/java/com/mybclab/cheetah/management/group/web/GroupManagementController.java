package com.mybclab.cheetah.management.group.web;

import com.mybclab.cheetah.code.service.CodeGroupService;
import com.mybclab.cheetah.common.constant.CodeGroupID;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.dto.UserForm;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.admin.user.validator.UserFormValidator;
import com.mybclab.cheetah.common.dto.AlertMessage;
import com.mybclab.cheetah.common.exception.StorageException;
import com.mybclab.cheetah.management.group.dto.GroupInviteForm;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDateTime;


/**
 * 그룹 관리 Controller
 */
@Slf4j
@RequestMapping("/management/group")
@Controller
public class GroupManagementController {

    /**
     * {@link UserService}
     */
    private UserService userService;

    /**
     * {@link CodeGroupService}
     */
    private CodeGroupService codeGroupService;

    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;
    /**
     * {@link UserFormValidator}
     */
    private UserFormValidator userFormValidator;

    /**
     * default constructor
     */
    public GroupManagementController() {}

    /**
     * constructor
     * @param userService {@link UserService}
     * @param codeGroupService {@link CodeGroupService}
     * @param modelMapper {@link ModelMapper}
     * @param userFormValidator {@link UserFormValidator}
     */
    @Autowired
    public GroupManagementController(UserService userService, CodeGroupService codeGroupService, ModelMapper modelMapper, UserFormValidator userFormValidator) {
        this.userService = userService;
        this.codeGroupService = codeGroupService;
        this.modelMapper = modelMapper;
        this.userFormValidator = userFormValidator;
    }


    /**
     * 그룹 관리
     * @return 그룹 관리 화면
     */
    @RequestMapping("")
    public String group(Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());
        model.addAttribute("group", user);
        model.addAttribute("groupInviteForm", new GroupInviteForm());
        return "management/group/group";
    }


    @GetMapping("/{username}/modify")
    public String groupModifyView(@ModelAttribute User group, Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        UserForm groupForm = modelMapper.map(user, UserForm.class);

        model.addAttribute("groupTypeList", codeGroupService.getCodeList(CodeGroupID.GROUP_TYPE));
        model.addAttribute("groupForm", groupForm);
        return "management/group/group-form";
    }

    @PostMapping("/{username}/modify")
    public String groupModifty(UserForm userForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        userFormValidator.validate(userForm, bindingResult, UserForm.ValidationGroup.class);
        if (bindingResult.hasErrors()) {
            return "management/mypage/mypage-form";
        }

        user.setGroupName(userForm.getGroupName());
        user.setGroupTypeCode(userForm.getGroupTypeCode());
        user.setUpdatedAt(LocalDateTime.now());
        user.setUpdatedBy(user.getUsername());
        user.setGroupImageFileSn(userForm.getGroupImageFileSn());

        try {
            userService.saveWithFile(user, userForm.getGroupImageFile(), null);
        } catch (StorageException e) {
            redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.ERROR).messageCode(e.getMessage()).build());
        }

        return "redirect:/management/group";
    }


    @PostMapping("/{username}/invite")
    public String invite(@Valid GroupInviteForm groupInviteForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        User user = SecurityUtils.getPrincipal();
        user = userService.findById(user.getUsername());

        if (bindingResult.hasErrors()) {
            model.addAttribute("groupInviteForm", groupInviteForm);
            model.addAttribute("group", user);
            model.addAttribute("viewModal", "groupInviteModal");
            return "management/group/group";
        }

        userService.sendGroupInviteMail(user, groupInviteForm);


        redirectAttributes.addFlashAttribute(AlertMessage.builder().level(AlertMessage.ALERT_MESSAGE_LEVEL.INFO).messageCode("group.invite.mail.send").build());


        return "redirect:/management/group";
    }


}
