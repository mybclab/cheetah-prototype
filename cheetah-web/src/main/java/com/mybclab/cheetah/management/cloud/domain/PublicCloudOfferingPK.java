package com.mybclab.cheetah.management.cloud.domain;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

/**
 * 퍼블릭 클라우드 OFFERING PK
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class PublicCloudOfferingPK implements Serializable {
    /**
     * 클라우드 구분
     */
    @Enumerated(EnumType.STRING)
    private Code.CLOUD_TYPE cloudType;

    /**
     * offering id
     */
    private String offeringId;
}
