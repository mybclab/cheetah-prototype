package com.mybclab.cheetah.management.cloud.domain;

import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * 퍼블릭 클라우드 OFFERING
 */
@Entity
@Setter
@Getter
@ToString
public class PublicCloudOffering extends HistoryBase implements Serializable {

    @EmbeddedId
    private PublicCloudOfferingPK publicCloudOfferingPK;

    private String offeringName;

    private Long gpuSn;

    private int gpuQuantity;

    private int cpuCore;

    private int systemMemory;
    
    private int gpuMemory;

    private int gpuTotalMemory;
}
