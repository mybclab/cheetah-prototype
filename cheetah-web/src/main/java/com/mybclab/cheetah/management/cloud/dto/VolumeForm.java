package com.mybclab.cheetah.management.cloud.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VolumeForm implements Serializable {

    /**
     * 클라우드 구분
     */
    @JsonIgnore
    @NotNull
    private Code.CLOUD_TYPE cloudType;

    @JsonIgnore
    private String poolId; // ?

    @JsonIgnore
    private String zoneId;

    @JsonIgnore
    private String username;

    private String name;

    private String description;

    private String deviceName;

    private int size;



}
