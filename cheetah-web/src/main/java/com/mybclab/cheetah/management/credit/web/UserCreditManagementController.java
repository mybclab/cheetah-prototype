package com.mybclab.cheetah.management.credit.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.domain.UserCredit;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 사용자 크레딧 관리 Controller
 */
@Controller
@RequestMapping("/management/credit")
public class UserCreditManagementController {

    /**
     * {@link UserCreditService}
     */
    private UserCreditService userCreditService;
    /**
     * default constructor
     */
    public UserCreditManagementController() {}

    /**
     * constructor
     * @param userCreditService {@link UserCreditService}
     */
    @Autowired
    public UserCreditManagementController(UserCreditService userCreditService) {
        this.userCreditService = userCreditService;
    }

    @RequestMapping({"", "/list"})
    public String list(Model model) {
        User user = SecurityUtils.getPrincipal();
        List<UserCredit> userCreditList = userCreditService.findAllByUsername(user.getUsername());

        model.addAttribute("userCreditList", userCreditList);

        return "management/credit/credit-list";
    }
}
