package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.management.cloud.dto.Volume;
import com.mybclab.cheetah.management.cloud.dto.VolumeForm;
import com.mybclab.cheetah.management.cloud.dto.VolumePage;
import com.mybclab.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@Slf4j
public class VolumeService extends CloudCommonService {

    @Autowired
    protected WebClient client;

    public VolumePage page(String poolId, String username) {
        String url = apiUrl + resource + "/volume/?size=100";
        if (StringUtils.isNotEmpty(poolId)) {
            url += "&poolId=" + poolId;
        }
        Mono<VolumePage> response = client.get()
                .uri(url)
                .headers(h -> setHttpHeaders(h, username))
                .retrieve()
                .bodyToMono(VolumePage.class);
        return response.block();
    }

    public List<Volume> list(Code.CLOUD_TYPE cloudType, String username) {
        // FIXME : public cloud pool ID 하드코딩됨
        String poolId = "";
        switch (cloudType) {
            case AWS : poolId = "167a2fa3-0416-445e-bdca-c7c31b3d6ffa"; break;
            case TENCENT : poolId = "932b5312-9654-4ad3-8ad0-d696f65ba6d4"; break;
        }

        VolumePage volumePage = this.page(poolId, username);
        return volumePage.getContent();
    }

    public List<Volume> list(String poolId, String username) {
        VolumePage volumePage = this.page(poolId, username);
        return volumePage.getContent();
    }

    public Volume save(VolumeForm volumeForm) {
        log.debug(">>>>>>>>> url : " + apiUrl + resource + "/volume/" + volumeForm.getPoolId() + "/zone/" + volumeForm.getZoneId() + "/");
        Mono<Volume> response = client.post()
                .uri(apiUrl + resource + "/volume/" + volumeForm.getPoolId() + "/zone/" + volumeForm.getZoneId() + "/")
                .headers(h -> setHttpHeaders(h, volumeForm.getUsername()))
                .body(BodyInserters.fromObject(volumeForm))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
//                .onStatus(HttpStatus::is2xxSuccessful, res -> )
//                .onStatus(HttpStatus::is4xxClientError, res -> Mono.error(new CloudException(res.statusCode(), res.toString())))
//                .onStatus(HttpStatus::is5xxServerError, res -> Mono.error(new CloudException(res.statusCode(), res.toString())))
                .bodyToMono(Volume.class);
        return response.block();
    }
}
