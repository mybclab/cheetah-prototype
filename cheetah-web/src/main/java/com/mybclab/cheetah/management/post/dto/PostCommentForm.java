package com.mybclab.cheetah.management.post.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PostCommentForm {

    private Long postSn;
    /**
     * 내용
     */
    @NotBlank
    private String contents;


    /**
     * 사용 여부
     */
    @NotNull
    private Boolean useYn;
}
