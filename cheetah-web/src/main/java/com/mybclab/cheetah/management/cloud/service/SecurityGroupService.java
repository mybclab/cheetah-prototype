package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.management.cloud.dto.SecurityGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
@Slf4j
public class SecurityGroupService extends CloudCommonService {

    @Autowired
    protected WebClient client;


    public List<SecurityGroup> list(String poolId) {
        Flux<SecurityGroup> response = client.get()
                .uri(apiUrl + resource + "/securityGroup/" + poolId + "/list/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(SecurityGroup.class);

        return response.collectList().block();
    }

}
