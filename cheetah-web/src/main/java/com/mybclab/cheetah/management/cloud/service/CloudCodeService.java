package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.management.cloud.dto.CloudCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

@Slf4j
@Service
public class CloudCodeService extends CloudCommonService {

    @Autowired
    protected WebClient client;



    public List<CloudCode> list(String groupCode) {
        Flux<CloudCode> response = client.get()
                .uri(apiUrl + config + "/groupCode/" + groupCode + "/code/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(CloudCode.class);
        return response.collectList().block();
    }
}
