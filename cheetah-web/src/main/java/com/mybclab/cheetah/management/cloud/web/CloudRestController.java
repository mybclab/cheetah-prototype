package com.mybclab.cheetah.management.cloud.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.common.constant.Code.CLOUD_TYPE;
import com.mybclab.cheetah.management.cloud.dto.*;
import com.mybclab.cheetah.management.cloud.service.*;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.mybclab.common.utils.SecurityUtils;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@NoArgsConstructor
@RestController
@Slf4j
public class CloudRestController {

    private PoolService poolService;

    private CloudService cloudService;

    private ImageService imageService;

    private SecurityGroupService securityGroupService;

    private VolumeService volumeService;

    @Autowired
    public CloudRestController(PoolService poolService, CloudService cloudService, ImageService imageService, SecurityGroupService securityGroupService, VolumeService volumeService) {
        this.poolService = poolService;
        this.cloudService = cloudService;
        this.imageService = imageService;
        this.securityGroupService = securityGroupService;
        this.volumeService = volumeService;
    }

    @GetMapping("/cloud/pool/{poolId}")
    public ResponseEntity<JsonResult> pool(@PathVariable String poolId) {
        JsonResult result = new JsonResult();

        result.setStatus(ResultStatus.SUCCESS);
        Pool pool = poolService.get(poolId);
        String cloudId = pool.getRegion().getCloud().getId();
        String regionId = pool.getRegion().getId();
        log.debug(">>>>>> cloudId : " + cloudId + ", regionId : " + regionId);


        Cloud cloud = cloudService.cloud(cloudId, regionId);
        result.setData("zoneList", cloud.getZones());
        result.setData("offeringList", cloud.getOfferings());
        result.setData("keyPairList", cloud.getKeyPairs());

        List<Image> imageList = imageService.list(poolId);
        result.setData("imageList", imageList);

        List<SecurityGroup> securityGroupList = securityGroupService.list(poolId);
        result.setData("securityGroupList", securityGroupList);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping("/cloud/volumes/{cloudType}")
    public ResponseEntity<JsonResult> volumesByCloudType(@PathVariable CLOUD_TYPE cloudType) {
        User loginedUser = SecurityUtils.getPrincipal();
        JsonResult result = new JsonResult();
        result.setStatus(ResultStatus.SUCCESS);
        List<Volume> volumeList = volumeService.list(cloudType, loginedUser.getUsername());
        result.setData("volumeList", volumeList);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }




}
