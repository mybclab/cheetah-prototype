package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Action {

    private Resource resource;
    
    private String action;
    private String ownerId;
    private String resourceType;
    private String resourceClass;
    
}
