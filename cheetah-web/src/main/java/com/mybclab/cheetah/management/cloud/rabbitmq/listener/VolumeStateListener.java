package com.mybclab.cheetah.management.cloud.rabbitmq.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Cloud;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.CloudDriverResponse;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Resource;
import com.mybclab.cheetah.management.cloud.rabbitmq.service.CloudVolumeService;
import com.mybclab.common.utils.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Profile("cloud")
public class VolumeStateListener {

    ContainerService containerService;
    CloudVolumeService cloudVolumeService;

    @Autowired
    public VolumeStateListener(ContainerService containerService, CloudVolumeService cloudVolumeService) {
        this.containerService = containerService;
        this.cloudVolumeService = cloudVolumeService;
    }

    @RabbitListener(queues = "${cloud.rabbitmq.queue.volume}")
    public void listener(final Message message) {
        
    	log.debug(">>>>>>>>>>>>>>>>>>>>>>> volume message : {}", new String(message.getBody()));
	    
    	try { 
    		CloudDriverResponse volume = ObjectMapperUtils.stringToObject(new String(message.getBody()), CloudDriverResponse.class);

            Resource resource = volume.getMessage().getAction().getResource();
            Cloud cloud = resource.getPool().getRegion().getCloud();
            
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud : {}", cloud.getVendor());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance id : {}", resource.getAssociatedInstance().getId());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> volume state : {}", volume.getState());

            Container container = containerService.findByInstanceId(resource.getAssociatedInstance().getId());
            switch (CloudCode.VOLUME_STATUS.valueOf(volume.getState())) {
                case InUse:
                    cloudVolumeService.setProperties(CLOUD_VENDOR.byValue(cloud.getVendor()));
                    cloudVolumeService.inUse(volume, container);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
        }

    }


}