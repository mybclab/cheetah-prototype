package com.mybclab.cheetah.management.cloud.rabbitmq.properties;

import java.io.File;

import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;


public interface CloudPropertiesService {

	public String getUserName();
	public String getNodeInstallFilePath();
	public String getNodeInstallFileName();
	public String getPrivateKeyFilePath();
	public String getFileUploadPath();
	public String getVolumeName();	
	public String getVolumeMountFilePath();
	public String getVolumeMountFileName();
	public CLOUD_VENDOR getCloud();
	
	public byte[] getPrivateKey();
	public File getNodeInstallShellFile();
	public File getMountShellFile();

}
