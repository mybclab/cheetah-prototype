package com.mybclab.cheetah.management.cloud.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Port implements Serializable {

    private String type;

    private int value = 0;

    public Port(String portType) {
        this.type = portType;
    }

}
