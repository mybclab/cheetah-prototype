package com.mybclab.cheetah.management.cloud.repository;

import com.mybclab.cheetah.management.cloud.domain.PublicCloudOffering;
import com.mybclab.cheetah.management.cloud.domain.PublicCloudOfferingPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicCloudOfferingRepository extends JpaRepository<PublicCloudOffering, PublicCloudOfferingPK> {

    PublicCloudOffering findByGpuSnAndGpuQuantity(Long gpuSn, int gpuQuantity);
}
