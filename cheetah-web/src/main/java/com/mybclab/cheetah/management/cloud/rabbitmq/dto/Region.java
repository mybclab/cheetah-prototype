package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Region {

    private Cloud cloud;

    private String nativeName;
    private String createdDate;
    private String name;
    private String description;
    private String id;
    private String updatedDate;
    private String ownerId;
    private String nativeId;
    
}
