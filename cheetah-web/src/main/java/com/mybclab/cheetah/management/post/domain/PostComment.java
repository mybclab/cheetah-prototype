package com.mybclab.cheetah.management.post.domain;

import com.mybclab.common.domain.HistoryBase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table
@Getter
@Setter
@ToString
public class PostComment extends HistoryBase implements Serializable {


    private static final long serialVersionUID = 6446460551366709417L;

    @EmbeddedId
    private PostCommentPK postCommentPK;

    private String contents;

    /**
     * 사용 여부
     */
    private Boolean useYn;
}
