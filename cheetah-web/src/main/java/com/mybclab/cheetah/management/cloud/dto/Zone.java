package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Zone implements Serializable {
    private String id;

    private String name;

    private String zoneId;

    private String zoneName;
}
