package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AssociatedInstance {

    private Zone zone;
    private Ports[] ports;
    private String[] loadBalancerRules;
    private String[] nics;
    private Image image;
    private Pool pool;
    private Volumes[] volumes;
    private Offering offering;
    private SecurityGroups[] securityGroups;
    private String[] publicIps;
    
    private String nativeName;
    private String createUserId;
    private String privateIp;
    private String autoDeleteRootVolumeYn;
    private String description;
    private String updatedDate;
    private String clusterId;
    private String ownerId;
    private String type;
    private String nativeState;
    private String authKeyName;
    private String sizeOfVolume;
    private String agentState;
    private String progressStatus;
    private String numberOfCpu;
    private String id;
    private String sizeOfMemory;
    private String updateUserId;
    private String sizeOfRootVolume;
    private String publicIp;
    private String authPassword;
    private String createdDate;
    private String authUserId;
    private String name;
    private String nativeId;

}
