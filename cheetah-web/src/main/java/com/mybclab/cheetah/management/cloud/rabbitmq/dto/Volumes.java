package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Volumes {

    private Instance instance;
    private Volume volume;
    
    private String attachedDeviceName;
    private String componentType;
    private String createUserId;
    private String resourceId;
    private String updateUserId;
    private String updatedDate;
    private String ownerId;
    private String rootYn;
    private String createdDate;
    private String attachedMountName;
    private String id;
    private String state;
}
