package com.mybclab.cheetah.management.post.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.common.exception.UnauthorizedException;
import com.mybclab.cheetah.management.post.domain.Post;
import com.mybclab.cheetah.management.post.domain.PostComment;
import com.mybclab.cheetah.management.post.domain.PostCommentPK;
import com.mybclab.cheetah.management.post.dto.PostCommentForm;
import com.mybclab.cheetah.management.post.dto.PostCriteria;
import com.mybclab.cheetah.management.post.dto.PostForm;
import com.mybclab.cheetah.management.post.service.PostCommentService;
import com.mybclab.cheetah.management.post.service.PostService;
import com.mybclab.common.notifier.SlackNotifier;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/help/{path:qna|notice}")
@Slf4j
public class PostController {

    private PostService postService;

    private PostCommentService postCommentService;

    /**
     * {@link UserService}
     */
    private UserService userService;
    /**
     * {@link ModelMapper}
     */
    private ModelMapper modelMapper;

    private SlackNotifier slackNotifier;

    public PostController() {
    }

    @Autowired
    public PostController(PostService postService, PostCommentService postCommentService, UserService userService, ModelMapper modelMapper, SlackNotifier slackNotifier) {
        this.postService = postService;
        this.postCommentService = postCommentService;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.slackNotifier = slackNotifier;
    }
    /**
     * 게시물 목록
     * @param model {@link Model[}
     * @return 게시물 목록
     */
    @RequestMapping({"", "/list"})
    public String list(@PathVariable String path, @ModelAttribute PostCriteria postCriteria, @PageableDefault(sort = { "createdAt" }, direction = Sort.Direction.DESC) Pageable pageable, Model model) {
        User user = null;
        try {
            user = SecurityUtils.getPrincipal();
        } catch (UnauthorizedException e) {
            log.info(">>>>>>>> post-list UnauthorizedException : {}", e);
        }
        postCriteria.setBoardTypeCode(Code.BOARD_TYPE.valueOf(path.toUpperCase()));
        List<User> userListForNotice = new ArrayList<>(userService.findAllByUserRole(Code.ROLE.SYSTEM_ADMIN));
        if (user != null && !user.isSystemAdmin()) { // 시스템관리자가 아닌 사용자
            if (Code.BOARD_TYPE.NOTICE.equals(postCriteria.getBoardTypeCode())) {
                if (user.isGroupAdmin()) {
                    userListForNotice.add(user);
                } else if (user.isGroupUser()) {
                    userListForNotice.add(user.getGroup());
                }
                postCriteria.setGroupIdList(userListForNotice.stream().map(User::getUsername).collect(Collectors.toList()));
            }
            postCriteria.setUseYn(true);
            postCriteria.setCreatedBy(user.getUsername());
        } else if (user == null) { // 로그인되지 않은 사용자
            if (Code.BOARD_TYPE.NOTICE.equals(postCriteria.getBoardTypeCode())) {
                postCriteria.setGroupIdList(userListForNotice.stream().map(User::getUsername).collect(Collectors.toList()));
            }
            postCriteria.setUseYn(true);
        }
        Page<Post> postPage = postService.findAll(postCriteria, pageable);

        model.addAttribute("user", user);
        model.addAttribute("path", path);
        model.addAttribute("postPage", postPage);
        return "help/post/post-list";
    }

    @GetMapping("/{postSn}")
    public String view(@PathVariable String path, @PathVariable Long postSn, Model model) {
        Post post = postService.findById(postSn);
        post.setReadCount(post.getReadCount() + 1);
        postService.save(post);

        post.setContents(renderToHtml(post.getContents()));

        model.addAttribute("path", path);
        model.addAttribute("post", post);

        PostCommentForm postCommentForm = new PostCommentForm();
        postCommentForm.setPostSn(postSn);
        model.addAttribute("postCommentForm", postCommentForm);
        return "help/post/post";
    }

    private String renderToHtml(String md) {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(md);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        return renderer.render(document);
    }

    @GetMapping("/save")
    public String save(@PathVariable String path, Model model) {
        User user = SecurityUtils.getPrincipal();
        model.addAttribute("path", path);
        PostForm postForm = new PostForm();
        if (Code.BOARD_TYPE.NOTICE.name().toLowerCase().equals(path)) {
            postForm.setGroupId(user.getUsername());
        }
        model.addAttribute("postForm", postForm);
        return "help/post/post-form";
    }

    @GetMapping("/{postSn}/modify")
    public String modify(@PathVariable String path, @PathVariable Long postSn, Model model) {
        Post post = postService.findById(postSn);
        PostForm postForm = modelMapper.map(post, PostForm.class);

        model.addAttribute("path", path);
        model.addAttribute("postForm", postForm);

        return "help/post/post-form";
    }

    @PostMapping({"/save", "/{postSn}/modify"})
    public String saveOrModify(@PathVariable String path, @ModelAttribute @Valid PostForm postForm, BindingResult bindingResult, HttpServletRequest request) {
        User user = SecurityUtils.getPrincipal();

        if (bindingResult.hasErrors()) {
            return "help/post/post-form";
        }

        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String requestedValue = builder.buildAndExpand().getPath();
        boolean isNew = requestedValue.contains("save");

        Post post = modelMapper.map(postForm, Post.class);
        post.setBoardTypeCode(Code.BOARD_TYPE.valueOf(path.toUpperCase()));
        postService.save(post);

        if (isNew) {
            if (Code.BOARD_TYPE.QNA.equals(post.getBoardTypeCode())) {
                slackNotifier.notify(SlackNotifier.SlackTarget.REGISTER_QNA, SlackNotifier.SlackMessageAttachement.builder()
                        .title("Q&A 등록")
                        .title_link(request.getServerName() + ":" + request.getServerPort() + "/help/qna/" + post.getPostSn())
                        .text(String.format("%s(%s) 님이 '%s' 질문을 등록하였습니다.", user.getName(), user.getUsername(), post.getTitle()))
                        .build(), userService.findSlackTargetList(user));
            }
        }

        return "redirect:/help/" + path + "/" + post.getPostSn();
    }

    @PostMapping("/{postSn}/comment/save")
    public String saveComments(@PathVariable String path, @ModelAttribute @Valid PostCommentForm postCommentForm, BindingResult bindingResult, Model model) {
        User user = SecurityUtils.getPrincipal();
        if (bindingResult.hasErrors()) {
            Post post = postService.findById(postCommentForm.getPostSn());
            post.setContents(renderToHtml(post.getContents()));

            model.addAttribute("path", path);
            model.addAttribute("post", post);
            return "help/post/post";
        }

        PostCommentPK postCommentPK = modelMapper.map(postCommentForm, PostCommentPK.class);
        PostComment postComment = modelMapper.map(postCommentForm, PostComment.class);
        postComment.setPostCommentPK(postCommentPK);
        postComment.setCreatedBy(user.getUsername());
        postCommentService.save(postComment);

        return "redirect:/help/" + path + "/" + postComment.getPostCommentPK().getPostSn();
    }

    @PostMapping("/{postSn}/delete")
    public String delete(@PathVariable String path, @PathVariable Long postSn) {
        postService.delete(postSn);
        return "redirect:/help/" + path;


    }

}
