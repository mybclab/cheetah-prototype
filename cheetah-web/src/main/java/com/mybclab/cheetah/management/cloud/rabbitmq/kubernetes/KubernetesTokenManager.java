package com.mybclab.cheetah.management.cloud.rabbitmq.kubernetes;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Slf4j
@Component
@Profile("cloud")
public class KubernetesTokenManager {

    @Value("${cheetah.kubernetes.master-node-ip}")
	private String KUBERNETES_MASTER_NODE_IP;
    


	@Autowired
	protected WebClient client;

    
	@Autowired 
	public KubernetesTokenManager(
			@Value("${cheetah.kubernetes.master-node-ip}") String KUBERNETES_MASTER_NODE_IP) {
		this.KUBERNETES_MASTER_NODE_IP = KUBERNETES_MASTER_NODE_IP;
	}

	private String getMasterTokenInfo(String key) {
		Mono<String> response = client.get()
				.uri("http://" + KUBERNETES_MASTER_NODE_IP + ":8080/" + key)
				.headers(h -> h.setContentType(MediaType.APPLICATION_JSON))
				.retrieve()
				.bodyToMono(String.class);
		String value = response.block();
		value = value.replaceAll("\"", "");
		return value;
	}

    
    public String getSha256() {


		String openssl = getMasterTokenInfo("openssl");
		log.debug(">>>>>>> openssl: " + openssl);

		return openssl;
    }
    

    
    public String getToken() {

		String token = getMasterTokenInfo("token");
		log.debug(">>>>>>> token: " + token);

		return token;
    }
    
	public String getKUBERNETES_MASTER_NODE_IP() {
		return KUBERNETES_MASTER_NODE_IP;
	}

}

