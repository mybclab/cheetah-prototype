package com.mybclab.cheetah.management.cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VolumePage implements Serializable {

    private List<Volume> content;

    private Boolean last;
    private Boolean first;
    private int totalPages;
    private int totalElements;
    private int numberOfElements;
    private int size;
    private int number;

}
