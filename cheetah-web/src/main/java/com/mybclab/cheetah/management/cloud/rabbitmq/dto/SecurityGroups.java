package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SecurityGroups {

	private String[] securities;
	private Instance instance;
    
	private String componentType;
    private String createUserId;
    private String resourceId;
    private String createdDate;
    private String updateUserId;
    private String updatedDate;
    private String id;
    private String state;
    private SecurityGroup securityGroup;
    private String ownerId;
	    
}
