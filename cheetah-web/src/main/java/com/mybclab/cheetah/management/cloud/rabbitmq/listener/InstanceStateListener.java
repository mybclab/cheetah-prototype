package com.mybclab.cheetah.management.cloud.rabbitmq.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Cloud;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.CloudDriverResponse;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Resource;
import com.mybclab.cheetah.management.cloud.rabbitmq.service.CloudInstanceService;
import com.mybclab.common.utils.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Profile("cloud")
public class InstanceStateListener {

    ContainerService containerService;
    CloudInstanceService cloudInstanceService;

    @Autowired
    public InstanceStateListener(ContainerService containerService, CloudInstanceService cloudInstanceService) {
        this.containerService = containerService;
        this.cloudInstanceService = cloudInstanceService;
    }

    @RabbitListener(queues = "${cloud.rabbitmq.queue.instance}")
    public void listener(final Message message) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance message : {}", new String(message.getBody()));
        
        try {
            CloudDriverResponse instance =
                ObjectMapperUtils.stringToObject(new String(message.getBody()), CloudDriverResponse.class);

            Resource resource = instance.getMessage().getAction().getResource();
            Cloud cloud = resource.getPool().getRegion().getCloud();

            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud : {}", cloud.getVendor());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance id : {}", resource.getId());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance state : {}", resource.getNativeState());

            Container container = containerService.findByInstanceId(resource.getId());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> container : {}", container);
            if (container != null) {
                log.debug(">>>>>>>>>>>>>>>>>>>>>>> before setProperties ");
                log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud.getVendor() : {}", cloud.getVendor());
                cloudInstanceService.setProperties(CLOUD_VENDOR.byValue(cloud.getVendor()));
                log.debug(">>>>>>>>>>>>>>>>>>>>>>> before switch ");
                switch (CloudCode.INSTANCE_STATUS.byValue(resource.getNativeState())) {
                    case Running:
                        log.debug(">>>>>>>>>>>>>>>>>>>>>>> case Running ");
                        cloudInstanceService.created(instance, container);
                        break;
                    case Terminated:
                        log.debug(">>>>>>>>>>>>>>>>>>>>>>> case Terminated ");
                        cloudInstanceService.deleted(container);
                        break;
                    case Error:
                        log.debug(">>>>>>>>>>>>>>>>>>>>>>> case Error ");
                    	cloudInstanceService.error(instance, container);
                    default:
                        break;
                }
            }
        } catch (Exception e) {

        }

    }

}