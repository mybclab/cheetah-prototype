package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class Cloud implements Serializable {

    private String id;

    private String name;

    private String nativeId;

    private String nativeName;

    private List<Offering> offerings;

    private List<Zone> zones;

    private List<KeyPair> keyPairs;
}
