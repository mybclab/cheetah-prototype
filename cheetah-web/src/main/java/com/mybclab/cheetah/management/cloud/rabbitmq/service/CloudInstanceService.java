package com.mybclab.cheetah.management.cloud.rabbitmq.service;

import com.mybclab.cheetah.admin.node.domain.Node;
import com.mybclab.cheetah.admin.node.service.NodeService;
import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserService;
import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.service.ContainerService;
import com.mybclab.cheetah.kubernetes.service.KubernetesAPIService;
import com.mybclab.cheetah.management.cloud.rabbitmq.constant.CloudCode.CLOUD_VENDOR;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.CloudDriverResponse;
import com.mybclab.cheetah.management.cloud.rabbitmq.dto.Resource;
import com.mybclab.cheetah.management.cloud.rabbitmq.kubernetes.KubernetesTokenManager;
import com.mybclab.cheetah.management.cloud.rabbitmq.properties.CloudPropertiesService;
import com.mybclab.cheetah.management.cloud.service.InstanceService;
import com.mybclab.common.notifier.SlackNotifier;
import com.mybclab.common.utils.SSHClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@Profile("cloud")
public class CloudInstanceService {

    @Value("${cheetah.kubernetes.master-node-ip}")
    private String KUBERNETES_MASTER_NODE_IP;

    @Value("${cheetah.kubernetes.bearer-token}")
    private String KUBERNETES_BEARER_TOKEN;

    @Value("${cloud.cheetah.domain}")
    private String CHEETAH_DOMAIN;

    private AutowireCapableBeanFactory autowireCapableBeanFactory;
    private ContainerService containerService;
    private NodeService nodeService;
    private KubernetesTokenManager kubernetesTokenManager;

    private CloudPropertiesService cloudProperties;

    private SSHClientUtil sshClientUtil = new SSHClientUtil();
    private SlackNotifier slackNotifier;

    private UserService userService;

    private InstanceService instanceService;

    private KubernetesAPIService kubernetesAPIService;

    @Autowired
    public CloudInstanceService(
        AutowireCapableBeanFactory autowireCapableBeanFactory,
        ContainerService containerService,
        NodeService nodeService,
        InstanceService instanceService,
        KubernetesTokenManager kubernetesTokenManager,
        SlackNotifier slackNotifier,
        UserService userService,
        KubernetesAPIService kubernetesAPIService) {
        this.containerService = containerService;
        this.nodeService = nodeService;
        this.instanceService = instanceService;
        this.autowireCapableBeanFactory = autowireCapableBeanFactory;
        this.kubernetesTokenManager = kubernetesTokenManager;
        this.slackNotifier = slackNotifier;
        this.userService = userService;
        this.kubernetesAPIService = kubernetesAPIService;
    }

    public void setProperties(CLOUD_VENDOR cloud) {
        try {
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties class: {}", cloud.getClassName());

            cloudProperties = (CloudPropertiesService) Class.forName(cloud.getClassName()).newInstance();
            autowireCapableBeanFactory.autowireBean(cloudProperties);
            
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties : {}", cloudProperties.getFileUploadPath());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties node install file : {}", cloudProperties.getNodeInstallFileName());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties file path : {}", cloudProperties.getNodeInstallFilePath());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties shell file : {}", cloudProperties.getNodeInstallShellFile());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties private key : {}", cloudProperties.getPrivateKey());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties keyfile path : {}", cloudProperties.getPrivateKeyFilePath());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties username : {}", cloudProperties.getUserName());        
        } catch (Exception e) {
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> cloud properties error : {}", e);
        }
    }

    public void created(CloudDriverResponse instance, Container container) {
        try {

            Resource resource = instance.getMessage().getAction().getResource();

            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance connection");
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance ip : {}", resource.getPublicIp());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance id : {}", resource.getId());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance username : {}", cloudProperties.getUserName());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance private key path : {}", cloudProperties.getPrivateKeyFilePath());
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance file upload path : {}", cloudProperties.getFileUploadPath());
            
            for (int i = 1; i <= 10; i++) {
                int result = sshClientUtil.init(resource.getPublicIp(), cloudProperties.getUserName(), cloudProperties.getPrivateKey());
                if (result == 0) {
                    break;
                }
                log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance connection retry : {}", i);
                Thread.sleep(5000);
            }

            sshClientUtil.upload(cloudProperties.getFileUploadPath(), cloudProperties.getNodeInstallShellFile());

            String command = getCommand(resource, container);

            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance run script : {}", command);
            String returnStrFromSH = sshClientUtil.command(command);
            log.debug(">>>>>>>>>>>>>>>>>>>>>>> instance run script : returnStrFromSH : {}", returnStrFromSH);

            if (returnStrFromSH.contains("FAILED")) {
                log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> cheetah-node-gpu-install.sh is failed.");
                container.setStatusCode(Code.CONTAINER_STATUS.FAIL);
                containerService.save(container);
//                // 인스턴스 삭제
                instanceService.delete(container.getInstanceId());
            } else {
                // 볼륨 정보 없을 때
                // FIXME tencent volume 하드코딩
                if (container.getContainerVolumeList().size() == 0 || container.isTargetToMakeTencentVolume()) {
                    boolean result = containerService.createContainerWithCloud(container);
                    log.debug(">>>>>>>>>>> CloudInstanceService createContainer result : " + result);
                }
            }
        } catch (Exception e) {
        	 log.error(">>>>>>>>>>>>>>>>>>>>>>> instance created connection error  : {}", e);
        }

    }
    
    public void error(CloudDriverResponse instance, Container container) {
    	try {
            container.setStatusCode(Code.CONTAINER_STATUS.FAIL);
            containerService.save(container);
    	} catch(Exception e) {
       	 log.error(">>>>>>>>>>>>>>>>>>>>>>> instance created connection error  : {}", e);
    	}
    }

    public void deleted(Container container) {
        try {
            log.debug(">>>>>>>>>>> listener terminated get Node start ");
            Node node = container.getNode();
            log.debug(">>>>>>>>>>> listener terminated get Node end {}", node);


            kubernetesAPIService.deleteNode(node.getNodeName());

            log.debug(">>>>>>>>>>> listener terminated kubectl end ");

            if (!Code.CONTAINER_STATUS.FAIL.equals(container.getStatusCode())) {
                containerService.returnContainerStatus(container);
                User containerUser = container.getUser();
                slackNotifier.notify(SlackNotifier.SlackTarget.CONTAINER_REQUEST,
                        SlackNotifier.SlackMessageAttachement.builder()
                                .title("컨테이너 반납")
                                .text(String.format("%s(%s) 님이 '%s' 컨테이너를 반납하였습니다. (%s)", containerUser.getName(), containerUser.getUsername(), container.getContainerName(), "성공"))
                                .build(), userService.findSlackTargetList(containerUser));
                log.debug(">>>>>>>>>>> listener terminated return container end ");
            }

            nodeService.delete(node.getNodeSn());
            log.debug(">>>>>>>>>>> listener terminated delete node end");
        } catch (Exception e) {
       	 log.error(">>>>>>>>>>>>>>>>>>>>>>> instance deleted error  : {}", e);
        }

    }

    private String getCommand(Resource resource, Container container) {

        String mountPath = container.isTargetToMakeTencentVolume() ? container.getContainerVolumeList().get(0).getVolumeMountPath() : "";
        mountPath = mountPath.replace("/" + container.getContainerPK().getUsername(), "");
        String token = kubernetesTokenManager.getToken();
        String sha256 = kubernetesTokenManager.getSha256();
        log.debug(">>>>>>>> all parameters : (1) " + container.getGroupSharedYn() + ", (2)" + KUBERNETES_BEARER_TOKEN + ", (3)" + KUBERNETES_MASTER_NODE_IP + ", (4)" + token
                + ", (5)" + sha256 + ", (6)" + resource.getPublicIp() + ", (7)" + resource.getId() + ", (8)" + CHEETAH_DOMAIN + ", (9)" + mountPath);
        String command = String.format("%s %s %s %s %s %s %s %s %s %s",
            "chmod +x ./cheetah-node-gpu-install.sh && sudo ./cheetah-node-gpu-install.sh ",
            container.getGroupSharedYn(), // not null
            KUBERNETES_BEARER_TOKEN, // cheetah-admin Bearer Token, not null
            KUBERNETES_MASTER_NODE_IP, // not null
            token, // not null
            sha256, // not null
            resource.getPublicIp(), // not null
            resource.getId(), // instanceId, not null
            CHEETAH_DOMAIN, // not null
            mountPath); // null

        return command;

    }

}