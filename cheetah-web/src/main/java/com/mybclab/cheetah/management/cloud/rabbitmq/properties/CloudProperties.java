package com.mybclab.cheetah.management.cloud.rabbitmq.properties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

public abstract class CloudProperties implements CloudPropertiesService {

	public byte[] getPrivateKey() {
    	
		InputStream is = CloudProperties.class.getResourceAsStream(getPrivateKeyFilePath());
    	try {
			return IOUtils.toByteArray(is);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	return null;
	}

	public File getNodeInstallShellFile() {
		
		File file = new File(getNodeInstallFileName());
		try {
			InputStream is = CloudProperties.class.getResourceAsStream(getNodeInstallFilePath());
			OutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(is, outputStream);
			outputStream.close();
			return file;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public File getMountShellFile() {
	
		File file = new File(getVolumeMountFileName());
		try {
			InputStream is = CloudProperties.class.getResourceAsStream(getVolumeMountFilePath());
			OutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(is, outputStream);
			outputStream.close();
			return file;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
