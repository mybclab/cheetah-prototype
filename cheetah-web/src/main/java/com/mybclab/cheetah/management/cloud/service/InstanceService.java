package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.common.constant.Code;
import com.mybclab.cheetah.container.domain.Container;
import com.mybclab.cheetah.container.domain.ContainerVolume;
import com.mybclab.cheetah.container.repository.ContainerRepository;
import com.mybclab.cheetah.management.cloud.domain.PublicCloudOffering;
import com.mybclab.cheetah.management.cloud.dto.Instance;
import com.mybclab.cheetah.management.cloud.dto.InstanceForm;
import com.mybclab.cheetah.management.cloud.dto.Port;
import com.mybclab.cheetah.management.cloud.dto.Volume;
import com.mybclab.cheetah.management.cloud.repository.PublicCloudOfferingRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@NoArgsConstructor
public class InstanceService extends CloudCommonService {

    @Autowired
    protected WebClient client;

    private PublicCloudOfferingRepository publicCloudOfferingRepository;

    private ContainerRepository containerRepository;


    @Value("${cloud.vendor.amazon.image.gpu}")
    private String AWS_IMAGE_GPU;
    @Value("${cloud.vendor.google.image.gpu}")
    private String GCP_IMAGE_GPU;
    @Value("${cloud.vendor.tencent.image.gpu}")
    private String TENCENT_IMAGE_GPU;
    @Value("${cloud.vendor.tencent.image.cpu}")
    private String TENCENT_IMAGE_CPU;

    @Autowired
    public InstanceService(PublicCloudOfferingRepository publicCloudOfferingRepository, ContainerRepository containerRepository) {
        this.publicCloudOfferingRepository = publicCloudOfferingRepository;
        this.containerRepository = containerRepository;
    }


    public List<Instance> list() {
        Flux<Instance> response = client.get()
                .uri(apiUrl + resource + "/instance/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(Instance.class);
        return response.collectList().block();
    }

    public Instance get(String instanceId) {

        Mono<Instance> response = client.get()
                .uri(apiUrl + resource + "/instance/" + instanceId + "/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToMono(Instance.class);
        return response.block();

    }

    public List<Instance> save(InstanceForm instanceForm) {
        log.debug(">>>>>>>>> url : " + apiUrl + resource + "/instance/" + instanceForm.getPoolId());
        Flux<Instance> response = client.post()
                .uri(apiUrl + resource + "/instance/" + instanceForm.getPoolId() + "/")
                .headers(this::setHttpHeaders)
                .body(BodyInserters.fromObject(instanceForm))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
//                .onStatus(HttpStatus::is2xxSuccessful, res -> )
//                .onStatus(HttpStatus::is4xxClientError, res -> Mono.error(new CloudException(res.statusCode(), res.toString())))
//                .onStatus(HttpStatus::is5xxServerError, res -> Mono.error(new CloudException(res.statusCode(), res.toString())))
                .bodyToFlux(Instance.class);
        return response.collectList().block();
    }

    public void delete(String instanceId) {
        client.delete()
                .uri(apiUrl + resource + "/instance/" + instanceId + "/")
                .headers(this::setHttpHeaders)
                .exchange()
                .doOnError(res-> {
                    log.debug(">>>>>>> doOnError delete error ", res.getMessage());
                })
                .doOnSuccess(res -> {
                    List<String> header = res.headers().header("Date");
                    log.debug(">>>>>>>>> doOnSuccess delete header {}", header);
                })
                .block();
    }

    public Instance stop(String instanceId) {
        client.patch()
                .uri(apiUrl + resource + "/instance/" + instanceId + "/stop")
                .headers(this::setHttpHeaders)
                .exchange()
                .doOnError(res-> {
                    log.debug(">>>>>>> doOnError delete error ", res.getMessage());
                })
                .doOnSuccess(res -> {
                    List<String> header = res.headers().header("Date");
                    log.debug(">>>>>>>>> doOnSuccess delete header {}", header);
                })
                .block();
        return null;
    }









    // FIXME instance 생성 호출 하드코딩
    public String createInstance(Container container) {



        InstanceForm instanceForm = new InstanceForm();
        instanceForm.setName("cheetah-" + container.getContainerPK().getUsername() + "-" + container.getContainerPK().getContainerId());
        instanceForm.setNumberOfInstances(1);

        List<Port> ports = new ArrayList<Port>() {{
            add(new Port("SSH", 22));
        }};

        instanceForm.setPorts(ports);
        // gpu의 오퍼링아이디 찾아서 넣기
        PublicCloudOffering publicCloudOffering = publicCloudOfferingRepository.findByGpuSnAndGpuQuantity(container.getGpuSn(), container.getGpuQuantity());
        instanceForm.setOfferingId(publicCloudOffering.getPublicCloudOfferingPK().getOfferingId());
        instanceForm.setDescription(container.getDescription());

        int countOfGroupContainers = container.getGroupContainerListSize() + 1;
        int cpuLimit = (int) (container.getGroupSharedSize() * 1000);
        int memoryLimit = (int) Math.floor((((container.getGpu().getSystemMemory() * 1000.0) - 50) / (container.getGpu().getCpuCore() - 2)) * container.getGroupSharedSize());
//        int memoryLimit = (int) Math.floor(((publicCloudOffering.getSystemMemory() * 1000.0) - 50) / countOfGroupContainers);
        log.debug(">>>>>>>> publicCloudOffering : {}", publicCloudOffering);
        log.debug(">>>>>>>> countOfGroupContainers : " + countOfGroupContainers);
        log.debug(">>>>>>>> cpuLimit : " + cpuLimit);
        log.debug(">>>>>>>> memoryLimit : " + memoryLimit);

        container.setCpuLimit(cpuLimit);
        container.setMemoryLimit(memoryLimit);
        containerRepository.saveAndFlush(container);


        if (Code.CLOUD_TYPE.AWS.equals(container.getGpu().getCloudType())) {
            instanceForm.setPoolId("167a2fa3-0416-445e-bdca-c7c31b3d6ffa");
            instanceForm.setZoneList(new String[]{"a79b8101-a883-4358-a58e-ead28315b344"});
            instanceForm.setImageId(AWS_IMAGE_GPU);
            instanceForm.setSecurityGroups(new String[]{"92a3ac54-1015-4e9a-a76f-4f09c7073321"});

            instanceForm.setKeyName("2");
            instanceForm.setAuthUserId("n3ncloud");
            instanceForm.setAuthPassword("n3ncloud");


            if (container.getContainerVolumeList().size() > 0) {
                List<Volume> volumes = new ArrayList<Volume>() {{
                    for (ContainerVolume containerVolume : container.getContainerVolumeList()) {
                        add(new Volume(containerVolume.getVolumeId(), containerVolume.getVolumeMountPath()));
                    }
                }};
                instanceForm.setVolumes(volumes);
            }

        } else if (Code.CLOUD_TYPE.GCP.equals(container.getGpu().getCloudType())) {
            instanceForm.setPoolId("ed0c47e4-d400-49ae-bdfc-a8496abffd97");
            instanceForm.setZoneList(new String[]{"581d6557-75f0-4f7a-ba77-d748197bc270"});
            instanceForm.setImageId(GCP_IMAGE_GPU);
            instanceForm.setSecurityGroups(new String[]{"08a45bbd-09fd-446c-b51c-9b1734cf6c70"});

            instanceForm.setKeyName("");
            instanceForm.setAuthUserId("n3ncloud");
            instanceForm.setAuthPassword("n3ncloud");

        } else if (Code.CLOUD_TYPE.TENCENT.equals(container.getGpu().getCloudType())) {
            instanceForm.setPoolId("1d1cd1b9-cd8c-470a-9615-5336a727f61e");
            instanceForm.setZoneList(new String[]{"635f0247-38e2-42d9-9e17-fb67d652b89a"});
            if (Code.RESOURCE_TYPE.GPU.equals(container.getGpu().getResourceType())) {
                instanceForm.setImageId(TENCENT_IMAGE_GPU); // gpu image
            } else if (Code.RESOURCE_TYPE.CPU.equals(container.getGpu().getResourceType())) {
                instanceForm.setImageId(TENCENT_IMAGE_CPU); // cpu image
            }

            instanceForm.setSecurityGroups(new String[]{"67470814-c6bc-417c-a750-0b3148732a5f"});

            instanceForm.setKeyName("4");
            instanceForm.setAuthUserId("n3ncloud");
            instanceForm.setAuthPassword("n3ncloud");
        } else if (Code.CLOUD_TYPE.AZURE.equals(container.getGpu().getCloudType())) {
            // FIXME Azure
            instanceForm.setPoolId("fixMeToAzurePoolId"); 
            instanceForm.setZoneList(new String[]{"fixMeToAzureZonId"}); 
            instanceForm.setImageId("fixMeToAzureImageId"); 
            instanceForm.setSecurityGroups(new String[]{"fixMeToAzureSecurityGroupId"}); 

            instanceForm.setKeyName("fixMeToAzureKeyName"); 
            instanceForm.setAuthUserId("n3ncloud");
            instanceForm.setAuthPassword("n3ncloud");
        }




        log.debug(">>>>>>>>>> instanceForm : {}", instanceForm);
        List<Instance> savedInstance = this.save(instanceForm);
        log.debug(">>>>>>>>>> savedInstance : {}", savedInstance);
//
        for (Instance instance : savedInstance) {
            if (instance.getName().equals(instanceForm.getName())) {
                String instanceId = savedInstance.get(0).getId();
                log.debug(">>>>>>>>>> saved instance Id : {}", instanceId);
                return instanceId;
            }
        }



        return null;
    }

}
