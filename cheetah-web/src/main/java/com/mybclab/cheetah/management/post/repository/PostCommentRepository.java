package com.mybclab.cheetah.management.post.repository;

import com.mybclab.cheetah.management.post.domain.PostComment;
import com.mybclab.cheetah.management.post.domain.PostCommentPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostCommentRepository extends JpaRepository<PostComment, PostCommentPK> {

    List<PostComment> findAllByPostCommentPKPostSn(Long postSn);

    @Query(" select coalesce(max(c.postCommentPK.postCommentNo), 0) " +
            "  from PostComment c" +
            " where c.postCommentPK.postSn = :postSn")
    int findMaxPostCommentNo(@Param("postSn") Long postSn);
}
