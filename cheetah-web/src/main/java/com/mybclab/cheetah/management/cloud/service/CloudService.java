package com.mybclab.cheetah.management.cloud.service;

import com.mybclab.cheetah.management.cloud.dto.Cloud;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@Slf4j
public class CloudService extends CloudCommonService {

    @Autowired
    protected WebClient client;

    public List<Cloud> list() {
        Flux<Cloud> response = client.get()
                .uri(apiUrl + resource + "/cloud/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(Cloud.class);
        return response.collectList().block();
    }

    public Cloud cloud(String cloudId, String regionId) {
        Mono<Cloud> response = client.get()
                .uri(apiUrl + resource + "/cloud/" + cloudId + "/region/" + regionId + "/")
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToMono(Cloud.class);
        return response.block();
    }
}
