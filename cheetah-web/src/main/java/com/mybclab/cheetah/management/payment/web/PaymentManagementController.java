package com.mybclab.cheetah.management.payment.web;

import com.mybclab.cheetah.admin.user.domain.User;
import com.mybclab.cheetah.admin.user.service.UserCardService;
import com.mybclab.cheetah.admin.user.service.UserCreditService;
import com.mybclab.cheetah.common.formatter.CheetahFormatter;
import com.mybclab.cheetah.container.domain.ContainerHistory;
import com.mybclab.cheetah.container.service.ContainerHistoryService;
import com.mybclab.cheetah.payment.domain.Payment;
import com.mybclab.cheetah.payment.service.PaymentService;
import com.mybclab.common.result.JsonResult;
import com.mybclab.common.result.ResultStatus;
import com.mybclab.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.YearMonth;
import java.util.List;

/**
 * 결제관리 Controller
 */
@RequestMapping("/management/payment")
@Controller
@Slf4j
public class PaymentManagementController {


    /**
     * {@link PaymentService}
     */
    private PaymentService paymentService;

    /**
     * {@link UserCreditService}
     */
    private UserCreditService userCreditService;

    /**
     * {@link ContainerHistoryService}
     */
    private ContainerHistoryService containerHistoryServiceService;

    /**
     * {@link UserCardService}
     */
    private UserCardService userCardService;
    /**
     * default constructor
     */
    public PaymentManagementController() {}

    /**
     * constructor
     * @param userCreditService {@link UserCreditService}
     * @param paymentService {@link PaymentService}
     * @param containerHistoryServiceService {@link ContainerHistoryService}
     * @param userCardService {@link UserCardService}
     */
    @Autowired
    public PaymentManagementController(PaymentService paymentService, UserCreditService userCreditService, ContainerHistoryService containerHistoryServiceService, UserCardService userCardService) {
        this.paymentService = paymentService;
        this.userCreditService = userCreditService;
        this.containerHistoryServiceService = containerHistoryServiceService;
        this.userCardService = userCardService;
    }


    /**
     * 결제관리
     * @param model {@link Model}
     * @return 결제관리 화면
     */
    @RequestMapping({"", "/dashboard"})
    public String payment(Model model) {
        User user = SecurityUtils.getPrincipal();

        model.addAttribute("user", user);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("userCard", userCardService.findValidCardByUsername(user.getUsername()));
        model.addAttribute("userCredit", userCreditService.userCredit(user.getUsername()));
        model.addAttribute("nextPayment", paymentService.nextPayment(user.getUsername(), user.isGroupAdmin()));
        model.addAttribute("lastPayment", paymentService.lastPayment(user.getUsername()));

        model.addAttribute("nextPaymentContainerList", containerHistoryServiceService.findAllByMonthly(user.getUsername(), CheetahFormatter.FOR_YEAR_MONTH.format(YearMonth.now()), user.isGroupAdmin()));

        // 결제내역 목록
        PageRequest pageRequest = PageRequest.of(0, 6, Sort.Direction.DESC, "paymentDate");
        Page<Payment> paymentPage = paymentService.findAll(user.getUsername(), pageRequest);
        model.addAttribute("paymentPage", paymentPage);

        return "management/payment/payment";
    }

    @ResponseBody
    @GetMapping("/monthly/{username}")
    public ResponseEntity<JsonResult> monthlyChart(@PathVariable String username) {
        JsonResult result = new JsonResult();

        int size = 6;
        PageRequest pageRequest = PageRequest.of(0, size, Sort.Direction.DESC, "paymentDate");
        Page<Payment> paymentPage = paymentService.findAll(username, pageRequest);
        List<Payment> paymentList = paymentPage.getContent();

        if (paymentList != null && paymentList.size() > 0) {
            size = paymentList.size();
            String[] labels = new String[size];
            Double[] dataForCredit = new Double[size];
            Double[] dataForCard = new Double[size];

            for (int i = 0; i < size ; i++) {
                dataForCredit[i] = paymentList.get(size - (i + 1)).getCreditPaymentAmount();
                dataForCard[i] = paymentList.get(size - (i + 1)).getCardPaymentAmount();
                labels[i] = paymentList.get(size - (i + 1)).getPaymentDate().format(CheetahFormatter.FOR_YEAR_MONTH_WITH_DASH);
            }

            result.setData("labels", labels);
            result.setData("dataForCredit", dataForCredit);
            result.setData("dataForCard", dataForCard);
            result.setStatus(ResultStatus.SUCCESS);
        } else {
            result.setStatus(ResultStatus.FAIL);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @RequestMapping("/target")
    public String target(Model model) {
        User user = SecurityUtils.getPrincipal();

        model.addAttribute("user", user);
        model.addAttribute("nextPayment", paymentService.nextPayment(user.getUsername(), user.isGroupAdmin()));
        model.addAttribute("nextPaymentContainerList", containerHistoryServiceService.findAllByMonthly(user.getUsername(), CheetahFormatter.FOR_YEAR_MONTH.format(YearMonth.now()), user.isGroupAdmin()));

        return "management/payment/payment-target-list";
    }

    /**
     * 결제 목록
     * @param pageable {@link Pageable}
     * @param model {@link com.sun.org.apache.xpath.internal.operations.Mod}
     * @return 결제 목록 화면
     */
    @RequestMapping("/list")
    public String list(Pageable pageable, Model model) {
        User user = SecurityUtils.getPrincipal();
        Page<Payment> paymentPage = paymentService.findAll(user.getUsername(), pageable);
        for (Payment payment : paymentPage.getContent()) {
            List<ContainerHistory> targetContainerList = containerHistoryServiceService.findAllByMonthly(user.getUsername(), payment.getTargetYearMonth(), user.isGroupAdmin());
            payment.setTargetContainerList(targetContainerList);
        }
        model.addAttribute("paymentPage", paymentPage);
        return "management/payment/payment-list";
    }
}
