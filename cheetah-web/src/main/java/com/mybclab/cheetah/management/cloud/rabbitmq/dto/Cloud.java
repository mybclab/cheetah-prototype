package com.mybclab.cheetah.management.cloud.rabbitmq.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Cloud {

    private String createdDate;
    private String vendor;
    private String name;
    private String description;
    private String id;
    private String updatedDate;
    private String ownerId;
}
