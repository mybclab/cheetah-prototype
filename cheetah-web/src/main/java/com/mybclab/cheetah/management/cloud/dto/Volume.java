package com.mybclab.cheetah.management.cloud.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Volume implements Serializable {

    public Volume(String volumeId, String mountName) {
        this.volumeId = volumeId;
        this.mountName = mountName;
    }
    private String name;

    private String description;

    private String deviceName;

    private String mountName;

    private int size;

    private String volumeId;

    private String volumeType;

    private String volumeUsage;

    private String nativeState;

    private String id;

    private String attachingDeviceName;

    private Pool pool;

    private Zone zone;


    private long createdDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getCreatedDate() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.createdDate),
                TimeZone.getDefault().toZoneId());
    }
}
