package com.mybclab.cheetah.management.cloud.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


/**
 * restClientAdapter
 * @param <T>
 */
@Slf4j
@Component
public abstract class RestClientAdapter<T> {
    @Autowired
    protected WebClient client;

    @Value("${cheetah.cloud-driver.api.scheme}")
    protected String apiScheme;
    @Value("${cheetah.cloud-driver.api.host}")
    protected String apiHost;
    @Value("${cheetah.cloud-driver.api.port}")
    protected String apiPort;

    @Value("${cheetah.cloud-driver.api.accessKey}")
    protected String accessKey;

    @Value("${cheetah.cloud-driver.api.loginId}")
    protected String loginId;

    @Value("${cheetah.cloud-driver.api.secretKey}")
    protected String secretKey;


//
//    /**
//     * generateToken
//     * @param map grantType 과 그에 따른 파라미터 map {@link MultiValueMap}
//     * @return {@link Token}
//     */
//    protected Token generateToken(MultiValueMap<String, String> map) {
//
//        map.add("scope", "openid");
//
//        Mono<Token> response = client.post()
//                .uri(tokenUri)
//                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString((clientId + ":" + clientSecret).getBytes()))
//                .body(BodyInserters.fromFormData(map))
//                .retrieve()
//                .onStatus(HttpStatus::is4xxClientError, res -> Mono.error(new RestClientException(res.statusCode(), res.toString())))
//                .bodyToMono(Token.class);
//
//        return response.block();
//    }
//
//    /**
//     * 만료된 refresh 토큰으로 토큰 재발급
//     * @param expiredToken 만료된 {@link Token}
//     * @return 토큰값
//     */
//    private String refreshToken(Token expiredToken) {
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//        map.add(OAuth2ParameterNames.GRANT_TYPE, GrantType.REFRESH_TOKEN.getValue());
//        map.add("refresh_token", expiredToken.getRefreshToken());
//
//        Token token = generateToken(map);
//        log.debug(">>>>>>>>>>>>>> RestClientAdapter get refresh token : {}", token);
//        SessionUtils.setAttribute(TOKEN, token);
//        return token.getAccessToken();
//    }
//
//    /**
//     * session 에 토큰이 있는지 확인 후 반환
//     * 만료되었다면 refresh 토큰으로 재발급 후 반환
//     * @return 토큰값
//     */
//    protected String getAccessToken() {
//        Token tokenInSession = (Token) SessionUtils.getAttribute(TOKEN);
//        log.debug(">>>>>>>> RestClientAdapter tokenInSession :" + tokenInSession.getJti() +","+ tokenInSession.getAccessToken());
//
//        if (tokenInSession != null && tokenInSession.getAccessToken() != null) {
//            if (tokenInSession.isExpired()) {
//                return refreshToken(tokenInSession);
//            }
//            return tokenInSession.getAccessToken();
//        }
//        return null;
//    }
//
//
    /**
     * getOne
     * @param endpoint endpoint
     * @return T
     */
    @SuppressWarnings("unchecked")
    protected T one(String endpoint) {
        Mono<T> response = client.get()
                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).build())
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToMono(getClassType());
//                .bodyToMono(String.class);
//        log.debug(">>>>>>>> RestClientAdapter getMono toString : {}", response.block());


        return response.block();
    }

    /**
     * list
     * @param endpoint endpoint
     * @return {@link List}
     */
    protected List<T> list(String endpoint) {
        return list(endpoint, null);
    }

    /**
     * list
     * @param endpoint endpoint
     * @param queryParams {@link MultiValueMap}
     * @return {@link List}
     */
    @SuppressWarnings("unchecked")
    protected List<T> list(String endpoint, MultiValueMap<String, String> queryParams) {
        Flux<T> response = client.get()
                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).queryParams(queryParams).build())
                .headers(this::setHttpHeaders)
                .retrieve()
                .bodyToFlux(getClassType());
        return response.collectList().block();
    }
//
//    /**
//     * page
//     * @param endpoint endpoint
//     * @param pageable {@link Pageable}
//     * @return {@link Page}
//     */
//    protected Page<T> page(String endpoint, Pageable pageable) {
//        return page(endpoint, pageable, null);
//    }
//
//    /**
//     * page
//     * @param endpoint endpoint
//     * @param pageable {@link Pageable}
//     * @param queryParams {@link MultiValueMap}
//     * @return {@link Page<T>}
//     */
//    @SuppressWarnings("unchecked")
//    protected Page<T> page(String endpoint, Pageable pageable, MultiValueMap<String, String> queryParams) {
//        final int[] totalCnt = {0}; // TODO need refactoring
//        log.debug(">>>>>>>>> queryParams {}", queryParams);
//        Flux<T> response = client.get()
//                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).queryParams(queryParams).build())
//                .headers(h -> h.setBearerAuth(getAccessToken()))
//                .exchange()
//                .doOnSuccess(res -> {
//                    List<String> header = res.headers().header("X-Total-Count");
////                    log.debug(">>>>>>>>> doOnSuccess res header {}", header);
//                    totalCnt[0] = Integer.parseInt(header.get(0));
//                })
//                .flatMapMany(res -> res.bodyToFlux(getClassType()));
//        List<T> list = response.collectList().block();
//
//        return new PageImpl<>(list, pageable, totalCnt[0]);
//    }
//
//
//    /**
//     * post
//     * @param endpoint endpoint
//     * @param object T
//     * @return T
//     */
//    @SuppressWarnings("unchecked")
//    protected T post(String endpoint, T object) {
//        log.debug(">>>>>>>>> post  : {}", object);
//        Mono<T> response = client.post()
//                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).build())
//                .headers(h -> {
//                    h.setBearerAuth(getAccessToken());
//                    h.setContentType(MediaType.APPLICATION_JSON);
//                })
//                .body(BodyInserters.fromObject(object))
//                .accept(MediaType.APPLICATION_JSON)
//                .retrieve().bodyToMono(getClassType());
//        log.debug(">>>>>>>>>>>> response post : " + response.block());
////        log.debug(">>>>>>>>>>>> response post : " + response.block());
//        return response.block();
//    }
//
//    /**
//     * put (as a whole object)
//     * @param endpoint endpoint
//     * @param object T
//     * @return T
//     */
//    @SuppressWarnings("unchecked")
//    protected T put(String endpoint, T object) {
//        Mono<T> response = client.put()
//                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).build())
//                .headers(h -> {
//                    h.setBearerAuth(getAccessToken());
//                    h.setContentType(MediaType.APPLICATION_JSON);
//                })
//                .body(BodyInserters.fromObject(object))
//                .accept(MediaType.APPLICATION_JSON)
//                .retrieve().bodyToMono(getClassType());
//        log.debug(">>>>>>>>>>>> response put : " + response.block());
////        log.debug(">>>>>>>>>>>> response post : " + response.block());
//        return response.block();
//    }
//
//    /**
//     * patch (with partial information for the object)
//     * @param endpoint endpoint
//     * @param object T
//     * @return T
//     */
//    @SuppressWarnings("unchecked")
//    protected T patch(String endpoint, T object) {
//        Mono<T> response = client.patch()
//                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).build())
//                .headers(h -> {
//                    h.setBearerAuth(getAccessToken());
//                    h.setContentType(MediaType.APPLICATION_JSON);
//                })
//                .body(BodyInserters.fromObject(object))
//                .accept(MediaType.APPLICATION_JSON)
//                .retrieve().bodyToMono(getClassType());
//        return response.block();
//    }
//
//
//    /**
//     * delete
//     * @param endpoint endpoint
//     * @return 삭제 결과
//     */
//    protected boolean delete(String endpoint) {
//        log.debug(">>>>>>>>> delete : " + endpoint);
//        client.delete()
//                .uri(builder -> builder.scheme(apiScheme).host(apiHost).port(apiPort).path(endpoint).build())
//                .headers(h -> h.setBearerAuth(getAccessToken()))
//                .exchange()
//                .doOnError(res-> {
//                    log.debug(">>>>>>> doOnError delete error ", res.getMessage());
//                })
//                .doOnSuccess(res -> {
//                    List<String> header = res.headers().header("Date");
//                    log.debug(">>>>>>>>> doOnSuccess delete header {}", header);
//                })
//                .block();
//        log.debug(">>>>>>>>> delete done" );
//        return true; // TODO
//    }


    /**
     * classType 을 반환
     * @return {@link Class}
     */
    protected abstract Class getClassType();

    protected void setHttpHeaders(HttpHeaders h) {
        h.set("accessKey", accessKey);
        h.set("loginId", loginId);
        h.set("secretKey", secretKey);
        h.setContentType(MediaType.APPLICATION_JSON);
    }

}
