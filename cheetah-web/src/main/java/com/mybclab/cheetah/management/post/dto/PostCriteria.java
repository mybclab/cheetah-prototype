package com.mybclab.cheetah.management.post.dto;

import com.mybclab.cheetah.common.constant.Code;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class PostCriteria implements Serializable {

    private static final long serialVersionUID = -3049516591064525856L;

    private List<String> groupIdList;

    private Code.BOARD_TYPE boardTypeCode;
    /**
     * 제목
     */
    private String title;

    /**
     * 내용
     */
    private String contents;

    /**
     * 사용 여부
     */
    private Boolean useYn;

    private String createdBy;
}
