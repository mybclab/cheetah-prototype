package com.mybclab.cheetah.management.post.service;

import com.mybclab.cheetah.management.post.domain.Post;
import com.mybclab.cheetah.management.post.dto.PostCriteria;
import com.mybclab.cheetah.management.post.repository.PostRepository;
import com.mybclab.cheetah.management.post.specification.PostSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
public class PostService {

    private PostRepository postRepository;

    public PostService() {
    }

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    /**
     * 저장
     * @param post {@link Post}
     * @return {@link Post}
     */
    @Transactional
    public Post save(Post post) {
        return postRepository.saveAndFlush(post);
    }

    /**
     * 게시물 일련번호로 단건 조회
     * @param postSn 게시물 일련번호
     * @return {@link Post}
     */
    public Post findById(Long postSn) {
        return postRepository.findById(postSn).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * 게시물 목록조회(페이징)
     * @param postCriteria {@link PostCriteria}
     * @param pageable {@link Pageable}
     * @return {@link Page <Post>  }
     */
    public Page<Post> findAll(PostCriteria postCriteria, Pageable pageable) {
        return postRepository.findAll(getPostSpecification(postCriteria), pageable);
    }

    /**
     * getPostSpecification
     * @param postCriteria {@link PostCriteria}
     * @return {@link Specification < Post >}
     */
    private Specification<Post> getPostSpecification(PostCriteria postCriteria) {
        return where(PostSpecification.likeTitle(postCriteria.getTitle()))
                .and(PostSpecification.inGroupId(postCriteria.getGroupIdList()))
                .and(PostSpecification.equalsBoardTypeCode(postCriteria.getBoardTypeCode()))
                .and(PostSpecification.likeContents(postCriteria.getContents()))
                .and(PostSpecification.equalsUseYn(postCriteria));
    }

    /**
     * 게시물 삭제
     * @param postSn 게시물 일련번호
     */
    @Transactional
    public void delete(Long postSn) {
        postRepository.deleteById(postSn);
        postRepository.flush();
    }
}
