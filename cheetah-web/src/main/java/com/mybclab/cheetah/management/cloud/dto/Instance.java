package com.mybclab.cheetah.management.cloud.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

@Getter
@Setter
@ToString(exclude = "ports")
@NoArgsConstructor
public class Instance implements Serializable {

    private String id;
    private String nativeId;


    private String clusterId;
    /**
     * 인스턴스 이름
     */
    private String name;

    private String publicIp;

    private String privateIp;
    /**
     * 인스턴스 IP
     */
    private String[] publicIps;

    private String nativeState;
    private String agentState;

    private long createdDate;

    // 포트
    private List<InstancePort> ports;

    // 풀
    private Pool pool;

    // 사양

    // 존
    private Zone zone;

    // 이미지
    private Image image;

    // 보안 그룹 (들)

    // 키 페어

    // 인증 사용자 ID
    private String authUserId;

    // 인증 암호
    private String authPassword;

    private String authKeyName;

    // 인스턴스 설명
    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getCreatedDate() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.createdDate),
                TimeZone.getDefault().toZoneId());
    }

}
