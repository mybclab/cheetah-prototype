import Plugin from 'Plugin'
import moment from '../../vendor/moment/src/moment'
import ko from '../../vendor/moment/src/locale/ko'

const NAME = 'moment'

class Moment extends Plugin {
  getName() {
    return NAME
  }

  static getDefaults() {
    return {}
  }

  render() {
    const $el = this.$el

    console.log(this.options)

    if (this.options.utcDate) {
      if (window.locale === 'ko_KR') {
        console.log('ko_KR')
        moment.locale('ko')
      } else {
        console.log('en')
        moment.locale('en')
      }

      const momentDate = moment.utc(this.options.utcDate, moment.ISO_8601)

      if (this.options.fromNow) {
        $el.html(momentDate.local().fromNow())

        $el.tooltip({
          title: momentDate.local().format('LLLL')
        })
      } else if (this.options.dateFormat) {
        $el.html(momentDate.local().format(this.options.dateFormat))
      } else {
        $el.html(momentDate.local().format('LLLL'))
      }
    }
  }
}

Plugin.register(NAME, Moment)

export default Moment
