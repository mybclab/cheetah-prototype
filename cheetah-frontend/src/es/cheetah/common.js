import * as Site from 'Site'
import $ from 'jquery'

// prototype 추가

// 숫자 타입에서 쓸 수 있도록 format() 함수 추가
Number.prototype.format = function () {
  if (this === 0) {
    return 0
  }

  const reg = /(^[+-]?\d+)(\d{3})/
  let n = String(this)

  while (reg.test(n)) {
    n = n.replace(reg, '$1' + ',' + '$2')
  }

  return n
}

// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
String.prototype.format = function () {
  const num = parseFloat(this)
  if (isNaN(num)) {
    return '0'
  }

  return num.format()
}

$.fn.serializeAll = function () {
  const data = $(this).serializeArray()

  $(':disabled[name]', this).each(function () {
    data.push({
      name: this.name,
      value: $(this).val()
    })
  })

  return data
}

$.fn.serializeFormJSON = function () {
  const o = {}
  const a = this.serializeAll()

  $.each(a, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]]
      }
      o[this.name].push(this.value || '')
    } else {
      o[this.name] = this.value || ''
    }
  })
  return o
}


$(document).ready(($) => {
  Site.run()

  if ($('#alertMessage').length > 0) {
    console.log($('#alertMessage').text())
    // $.toastr($("#alertMessage").text);

    const level = $('#alertLevel').text()

    if (level === 'success') {
      toastr.success($('#alertMessage').text())
    } else if (level === 'info') {
      toastr.info($('#alertMessage').text())
    } else if (level === 'warning') {
      toastr.warning($('#alertMessage').text())
    } else if (level === 'error') {
      toastr.error($('#alertMessage').text())
    }
  }
})

