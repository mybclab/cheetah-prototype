import $ from 'jquery'


$.i18n.messages.ko_KR = {
  lang: 'ko_KR',
  register: {
    card: {
      certification: {
        success: '카드 인증에 성공하였습니다.',
        fail: '카드 인증에 실패하였습니다.'
      }
    }
  },
  currency: {
    symbol : '₩'
  },
  gpu: {
    chargingMethod: {
      unit: {
        MINUTELY : '분',
        HOURLY : '시간',
        DAILY : '일',
        MONTHLY : '월',
        MONTHLY_CONTRACT_1 : '월',
        MONTHLY_CONTRACT_3 : '월',
        MONTHLY_CONTRACT_6 : '월',
        MONTHLY_CONTRACT_12 : '월',
        MONTHLY_CONTRACT_24 : '월'
      }
    }
  },
  container: {
    gpuShare: {
      infoTotal: '총 GPU MEMORY {0}GiB, 각 {1}GiB',
      infoCase: '{0}GiB 선택 시, 현재 컨테이너 포함 약 {1}개 생성 가능',
      targetUserCountOver: '선택가능한 사용자 수를 넘었습니다. 현재 컨테이너를 포함하여 {0}개까지 생성가능합니다.'
    },
    cpuShare: {
      infoTotal: '총 {0}Core, 각 {1}Core',
      infoCase: '{0}Core 선택 시, 현재 컨테이너 포함 약 {1}개 생성 가능',
      targetUserCountOver: '선택가능한 사용자 수를 넘었습니다. 현재 컨테이너를 포함하여 {0}개까지 생성가능합니다.'
    }
  },
  common: {
    select: {
      none: '선택안함'
    }
  }
}

