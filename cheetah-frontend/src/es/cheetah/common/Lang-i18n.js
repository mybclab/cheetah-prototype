import $ from 'jquery'

$.i18n = {}
$.i18n.messages = {}

$.i18n.getMessage = function (messageCode) {
  const message = eval(`$.i18n.messages.${window.locale}.${messageCode}`)
  return message !== undefined ? message : messageCode
}

$.i18n.getMessage = function (messageCode, params) {
  let message = eval(`$.i18n.messages.${window.locale}.${messageCode}`)
  // console.log('params.length : ' + params.length);
  if (params != undefined && params.length > 0) {
    for (var i = 0; i < params.length; i++) {
      // console.log('message replace  : ' + '{'+i+'}  to ' + params[i]);
      message = message.replace('{'+i+'}', params[i])
    }
  }

  return message !== undefined ? message : messageCode
}
