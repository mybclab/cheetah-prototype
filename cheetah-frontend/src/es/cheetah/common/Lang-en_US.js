import $ from 'jquery'


$.i18n.messages['en_US'] = {
  lang: "en_US",
  register: {
    card: {
      certification: {
        success: "Card authentication is succeeded.",
        fail: "Card authentication is failed."
      }
    }
  },
  currency: {
    symbol : '₩'
  },
  gpu: {
    chargingMethod: {
      unit: {
        MINUTELY : 'minutely',
        HOURLY : 'hourly',
        DAILY : 'daily',
        MONTHLY : 'monthly',
        MONTHLY_CONTRACT_1 : 'monthly',
        MONTHLY_CONTRACT_3 : 'monthly',
        MONTHLY_CONTRACT_6 : 'monthly',
        MONTHLY_CONTRACT_12 : 'monthly',
        MONTHLY_CONTRACT_24 : 'monthly'
      }
    }
  },
  container: {
    gpuShare: {
      infoTotal: '(eng)총 GPU MEMORY {0}GiB, 각 {1}GiB',
      infoCase: '(eng){0}GiB 선택 시, 현재 컨테이너 포함 약 {1}개 생성 가능',
      targetUserCountOver: '(eng)선택가능한 사용자 수를 넘었습니다. 현재 컨테이너를 포함하여 {0}개까지 생성가능합니다.'
    },
    cpuShare: {
      infoTotal: '총 {0}Core, 각 {1}Core',
      infoCase: '{0}Core 선택 시, 현재 컨테이너 포함 약 {1}개 생성 가능',
      targetUserCountOver: '선택가능한 사용자 수를 넘었습니다. 현재 컨테이너를 포함하여 {0}개까지 생성가능합니다.'
    }
  },
  common: {
    select: {
      none: 'No Select'
    }
  }
};

