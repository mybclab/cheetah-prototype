import $ from 'jquery'


$.i18n.messages['fr_FR'] = {
  lang: "fr_FR",
  register: {
    card: {
      certification: {
        success: "register.card.certification.success",
        fail: "register.card.certification.fail"
      }
    }
  },
  currency: {
    symbol : 'currency.symbol'
  },
  gpu: {
    chargingMethod: {
      unit: {
        MINUTELY : 'gpu.chargingMethod.unit.MINUTELY',
        HOURLY : 'gpu.chargingMethod.unit.HOURLY',
        DAILY : 'gpu.chargingMethod.unit.DAILY',
        MONTHLY : 'gpu.chargingMethod.unit.MONTHLY',
        MONTHLY_CONTRACT_1 : 'gpu.chargingMethod.unit.MONTHLY_CONTRACT_1',
        MONTHLY_CONTRACT_3 : 'gpu.chargingMethod.unit.MONTHLY_CONTRACT_3',
        MONTHLY_CONTRACT_6 : 'gpu.chargingMethod.unit.MONTHLY_CONTRACT_6',
        MONTHLY_CONTRACT_12 : 'gpu.chargingMethod.unit.MONTHLY_CONTRACT_12',
        MONTHLY_CONTRACT_24 : 'gpu.chargingMethod.unit.MONTHLY_CONTRACT_24'
      }
    }
  },
  container: {
    gpuShare: {
      infoTotal: 'container.gpuShare.infoTotal{0}{1}',
      infoCase: 'container.gpuShare.infoCase{0}{1}',
      targetUserCountOver: 'container.gpuShare.targetUserCountOver{0}'
    },
    cpuShare: {
      infoTotal: 'container.cpuShare.infoTotal{0}{1}',
      infoCase: 'container.cpuShare.infoCase{0}{1}',
      targetUserCountOver: 'container.cpuShare.targetUserCountOver{0}'
    }
  },
  common: {
    select: {
      none: 'common.select.none'
    }
  }
};

