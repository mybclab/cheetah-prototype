# Front-end 프로젝트 설명 

## Overview

## Building Tools 

### Files Structure

We supply building tools for each layouts and global folder. They are independent that you need install the packages separately. The files relating to the building tools you will see below:

```
(layout)/
├── tasks/
├── .babelrc
├── config.js
├── gulpfile.babel.js
└── package.json
```

### Install Tools

#### Node version manager

Node version manager
Install NVM. And use the latest version of NodeJS.

```bash
$ nvm install node
$ nvm use node
```

#### Install Sass

Go to sass-lang.com/install for installation in command line.

```bash
gem install sass
```

Before install sass, you should install Ruby and install Gem.

#### Install Babel

Install Babel globally.

```bash
$ npm install --global babel-cli
```

#### Install Gulp

Install Gulp globally.

```bash
npm install --global gulp-cli
```

#### Install Bower

Install Bower globally.

```bash
npm install --global bower
```

### Build project

#### 의존성 라이브러리 설치 

`gulp`로 빌드하기 전에 의존성 라이브러리를 먼저 설치한다.

```bash
$ npm install
$ bower install
```

#### 프로젝트 빌드

```bash
$ gulp build
$ gulp watch
$ gulp vendor 
$ gulp iamges
$ gulp fonts

```

##### Gulp 태스크




 
