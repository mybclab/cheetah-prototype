import gulp from 'gulp';
import config from '../config';
import eslint from 'gulp-eslint';
import gulpif from 'gulp-if';
import size from 'gulp-size';
import plumber from 'gulp-plumber';
import uglifyjs from 'uglify-js';
import composer from 'gulp-uglify/composer';
import browser from './browser';
import gutil from 'gulp-util';
import notify from 'gulp-notify';
import notifier from 'node-notifier';
import babel from 'gulp-babel';
import handleErrors from './utils/handleErrors';
import rollup from 'gulp-rollup';
import header from 'gulp-header';
import Glob from 'glob-fs';
import path from 'path';

// SCRIPTS
// ------------------
gulp.task('lint:scripts', () => {
  return gulp
    .src(`${config.scripts.source}/**/*.js`, {
      base: './',
      since: gulp.lastRun('lint:scripts'),
    })
    .pipe(eslint({fix: true})) // see http://eslint.org/docs/rules/
    .pipe(eslint.format())
    .pipe(gulp.dest('.'));
});

// compiles / concatenates javascript & minifies it (production)
gulp.task('make:scripts', (done) => {
  const uglify = composer(uglifyjs, console);
  const glob = Glob();
  let files = glob.readdirSync(path.join(config.scripts.source, '**/*.js'));

files = ['src/es/Base.js', 'src/es/BaseApp.js', 'src/es/Component.js', 'src/es/Config.js', 'src/es/Plugin.js', 'src/es/Site.js', 'src/es/State.js'

  , 'src/es/App/Calendar.js', 'src/es/App/Contacts.js', 'src/es/App/Documents.js', 'src/es/App/Forum.js', 'src/es/App/Location.js', 'src/es/App/Mailbox.js'
  , 'src/es/App/Media.js', 'src/es/App/Message.js', 'src/es/App/Notebook.js', 'src/es/App/Projects.js', 'src/es/App/Taskboard.js', 'src/es/App/Travel.js', 'src/es/App/Work.js'

  , 'src/es/cheetah/common.js', 'src/es/cheetah/test.js', 'src/es/cheetah/jquery.scrollbox.js'
  , 'src/es/cheetah/common/Lang-ko_KR.js', 'src/es/cheetah/common/Lang-en_US.js', 'src/es/cheetah/common/Lang-fr_FR.js', 'src/es/cheetah/common/Lang-i18n.js'

  , 'src/es/config/colors.js', 'src/es/config/tour.js'

  , 'src/es/Plugin/ace-editor.js', 'src/es/Plugin/action-btn.js', 'src/es/Plugin/alertify.js', 'src/es/Plugin/animate-list.js', 'src/es/Plugin/animsition.js', 'src/es/Plugin/asbreadcrumbs.js'
  , 'src/es/Plugin/ascolorpicker.js', 'src/es/Plugin/aspaginator.js', 'src/es/Plugin/aspieprogress.js', 'src/es/Plugin/asprogress.js', 'src/es/Plugin/asrange.js', 'src/es/Plugin/asscrollable.js'
  , 'src/es/Plugin/asselectable.js', 'src/es/Plugin/asspinner.js'

  , 'src/es/Plugin/bootbox.js', 'src/es/Plugin/bootstrap-datepicker.js', 'src/es/Plugin/bootstrap-maxlength.js', 'src/es/Plugin/bootstrap-select.js'
  , 'src/es/Plugin/bootstrap-sweetalert.js', 'src/es/Plugin/bootstrap-tagsinput.js', 'src/es/Plugin/bootstrap-tokenfield.js', 'src/es/Plugin/bootstrap-touchspin.js', 'src/es/Plugin/bootstrap-treeview.js'
  , 'src/es/Plugin/card.js', 'src/es/Plugin/clockpicker.js', 'src/es/Plugin/closeable-tabs.js'

  , 'src/es/Plugin/datatables.js', 'src/es/Plugin/datepair.js', 'src/es/Plugin/donut.js', 'src/es/Plugin/dropify.js'

  , 'src/es/Plugin/editable-table.js', 'src/es/Plugin/editlist.js'

  , 'src/es/Plugin/filterable.js', 'src/es/Plugin/floatthead.js', 'src/es/Plugin/formatter.js'

  , 'src/es/Plugin/gauge.js', 'src/es/Plugin/gmaps.js', 'src/es/Plugin/gridstack.js'

  , 'src/es/Plugin/highlight.js', 'src/es/Plugin/html5sortable.js'

  , 'src/es/Plugin/icheck.js', 'src/es/Plugin/input-group-file.js', 'src/es/Plugin/ionrangeslider.js', 'src/es/Plugin/isotope.js'

  , 'src/es/Plugin/jquery-appear.js', 'src/es/Plugin/jquery-knob.js', 'src/es/Plugin/jquery-knob.js', 'src/es/Plugin/jquery-labelauty.js'
  , 'src/es/Plugin/jquery-placeholder.js', 'src/es/Plugin/jquery-strength.js', 'src/es/Plugin/jquery-wizard.js', 'src/es/Plugin/jstree.js', 'src/es/Plugin/jt-timepicker.js', 'src/es/Plugin/jvectormap.js'

  , 'src/es/Plugin/ladda.js', 'src/es/Plugin/loading-button.js'

  , 'src/es/Plugin/magnific-popup.js', 'src/es/Plugin/masonry.js', 'src/es/Plugin/matchheight.js', 'src/es/Plugin/material.js'
  , 'src/es/Plugin/menu.js', 'src/es/Plugin/moment.js', 'src/es/Plugin/more-button.js', 'src/es/Plugin/multi-select.js'

  , 'src/es/Plugin/nestable.js', 'src/es/Plugin/notie-js.js', 'src/es/Plugin/nprogress.js'

  , 'src/es/Plugin/owl-carousel.js'

  , 'src/es/Plugin/panel.js', 'src/es/Plugin/peity.js', 'src/es/Plugin/plyr.js', 'src/es/Plugin/raty.js'

  , 'src/es/Plugin/responsive-tabs.js'

  , 'src/es/Plugin/select2.js', 'src/es/Plugin/selectable.js', 'src/es/Plugin/skintools.js', 'src/es/Plugin/slidepanel.js', 'src/es/Plugin/sticky-header.js', 'src/es/Plugin/summernote.js', 'src/es/Plugin/switchery.js'

  , 'src/es/Plugin/table.js', 'src/es/Plugin/tablesaw.js', 'src/es/Plugin/tabs.js', 'src/es/Plugin/tasklist.js', 'src/es/Plugin/toastr.js', 'src/es/Plugin/toolbar.js'

  , 'src/es/Plugin/webui-popover.js'

  , 'src/es/Section/GridMenu.js', 'src/es/Section/Menubar.js', 'src/es/Section/PageAside.js', 'src/es/Section/Sidebar.js'];
  // console.log(glob);
  // console.log(config.scripts.source);
  console.log(files);

  const globals = {
    jquery: 'jQuery',
    Component: 'Component',
    Plugin: 'Plugin',
    Config: 'Config',
    Base: 'Base',
    BaseApp: 'BaseApp',
    Site: 'Site',
    GridMenu: "SectionGridMenu",
    Menubar: "SectionMenubar",
    PageAside: "SectionPageAside",
    Sidebar: "SectionSidebar"
  };

  const external = Object.keys(globals);

  return gulp
    .src(`${config.scripts.source}/**/*.js`)
    .on('error', handleErrors)
    .pipe(
      plumber({
        errorHandler: notify.onError('Error: <%= error.message %>'),
      })
    )
    .pipe(rollup({
      input: files,
      rollup: require('rollup'),
      allowRealFiles: true,
      output: {
        globals: globals,
        format: 'es'
      },
      external: external,
    }))
    .pipe(babel({
      babelrc: false,
      presets: [
        [
          '@babel/preset-env'
        ]
      ],
      moduleRoot: '',
      moduleIds: true,
      plugins: [
        ["@babel/plugin-transform-modules-umd", {
          "globals": globals
        }],
        "@babel/plugin-proposal-object-rest-spread",
        "@babel/plugin-proposal-class-properties",
        "@babel/plugin-external-helpers"
      ]
    }))
    .pipe(gulpif(config.production, uglify()))
    .pipe(gulpif(config.production, header(config.banner)))
    .pipe(size({gzip: true, showFiles: true}))
    .pipe(gulp.dest(`${config.scripts.build}`))
    .pipe(gulp.dest(`${config.scripts.dist}`));
});

gulp.task(
  'scripts',
  gulp.series('lint:scripts', 'make:scripts', (done) => {
    if (config.enable.notify) {
      notifier.notify({
        title: config.notify.title,
        message: 'Scripts task complete',
      });
    }

    done();
  })
);
