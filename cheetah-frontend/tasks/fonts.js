import gulp from 'gulp';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import size from 'gulp-size';
import config from '../config';
import minify from 'gulp-clean-css';
import rename from 'gulp-rename';
import gulpif from 'gulp-if';
import notify from 'gulp-notify';
import del from 'del';
import notifier from 'node-notifier';

// FONTS
// ------------------
gulp.task('make:styles:fonts', () => {
  return gulp
    .src(`${config.fonts.source}/*/*.scss`)
    .pipe(
      sass({
        precision: 10, // https://github.com/sass/sass/issues/1122
        includePaths: config.styles.include,
      })
    )
    .pipe(postcss())
    .pipe(size({gzip: true, showFiles: true}))
    .pipe(gulp.dest(`${config.fonts.build}`))
    .pipe(gulp.dest(`${config.fonts.dist}`))
    .pipe(minify())
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(size({gzip: true, showFiles: true}))
    .pipe(gulp.dest(`${config.fonts.build}`))
    .pipe(gulp.dest(`${config.fonts.dist}`))
    .pipe(
      gulpif(
        config.enable.notify,
        notify({
          title: config.notify.title,
          message: 'Style Fonts task complete',
          onLast: true,
        })
      )
    );
});

gulp.task('copy:fonts', () => {
  return gulp
    .src(`${config.fonts.source}/**/*.+(eot|svg|woff|ttf|woff2)`)
    .pipe(gulp.dest(`${config.fonts.build}`))
    .pipe(gulp.dest(`${config.fonts.dist}`))
    .pipe(
      gulpif(
        config.enable.notify,
        notify({
          title: config.notify.title,
          message: 'Copy Fonts task complete',
          onLast: true,
        })
      )
    );
});

// Clean fonts files
gulp.task('clean:fonts', (done) => {
  return del([`${config.fonts.build}/**/*.+(eot|svg|woff|ttf|woff2)`]).then(() => {
    if (config.enable.notify) {
      notifier.notify({
        title: config.notify.title,
        message: 'Clean fonts task complete',
      });
    }

    done();
  });
});

gulp.task(
  'fonts',
  gulp.series('clean:fonts', 'copy:fonts', 'make:styles:fonts', (done) => {
    if (config.enable.notify) {
      notifier.notify({
        title: config.notify.title,
        message: 'Fonts task complete',
      });
    }

    done();
  })
);
