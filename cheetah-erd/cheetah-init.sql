-- 시스템 관리자
INSERT INTO USER (USERNAME, PARENT_USERNAME, GROUP_NAME, GROUP_TYPE_CODE, GROUP_INVITE_KEY, NAME, PASSWORD, PHONE_NO, EMAIL, CONFIRM_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, SLACK_HOOK_URL, SLACK_CHANNEL, ORGANIZATION_NAME, TEAM_NAME, GROUP_IMAGE_FILE_SN, USER_IMAGE_FILE_SN)
VALUES ('admin', null, null, null, null, '시스템관리자', '$2a$10$cK1QZgHGjXHujox1iFbRqOXGEjBMB4IlWwWQ/w1z0zNmVEwD8hFSW', '010-1111-2222', 'cheetah@n3ncloud.co.kr', 1, sysdate(), 'admin', sysdate(), 'admin', null, null, 'n3ncloud', 'cloud', null, null);
INSERT INTO USER_ROLE VALUES ('admin', 'SYSTEM_ADMIN');
-- 1231 : $2a$10$Aob0NE5hXhzPBO44uqOHR.UU7DwJpNZTq3yTyfUbCqY8AiCyJuHy6, admin : $2a$10$cK1QZgHGjXHujox1iFbRqOXGEjBMB4IlWwWQ/w1z0zNmVEwD8hFSW


-- 코드
INSERT INTO CODE_GROUP VALUES ('UR', 'USER ROLE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'SYSTEM_ADMIN', '시스템관리자', '시스템관리자', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GROUP_ADMIN', '그룹관리자', '그룹관리자', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GROUP_USER', '그룹사용자', '그룹사용자', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GENERAL_USER', '일반사용자', '일반사용자', 4, 1);


INSERT INTO CODE_GROUP VALUES ('GT', 'GROUP TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'SCHOOL', '학교', '학교', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'COMPANY', '회사', '회사', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'ORGANIZATION', '단체', '단체', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'ETC', '기타', '기타', 4, 1);

INSERT INTO CODE_GROUP VALUES ('NS', 'NODE STATUS');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('NS', 'READY', 'READY', 'READY', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('NS', 'NOT_READY', 'NOT READY', 'NOT READY', 2, 1);


INSERT INTO CODE_GROUP VALUES ('CT', 'CREDIT TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'WELCOME_GROUP_ADMIN', '그룹관리자 환영크레딧', '그룹관리자 환영크레딧', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'WELCOME_GENERAL_USER', '일반사용자 환영크레딧', '일반사용자 환영크레딧', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'BONUS', '보너스 크레딧', '보너스 크레딧', 3, 1);

INSERT INTO CODE_GROUP VALUES ('CE', 'CREDIT EXPIRY TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'NONE', '없음', '없음', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'YEAR', '년단위', '년단위', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'MONTH', '월단위', '월단위', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'WEEK', '주단위', '주단위', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'DAY', '일단위', '일단위', 5, 1);

INSERT INTO CODE_GROUP VALUES ('CS', 'CONTAINER STATUS');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'REQUEST', '요청', '요청', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'CONFIRM', '승인', '승인', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'CREATE', '생성중', '생성중', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'START', '시작', '시작', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'STOP', '중지', '중지', 5, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'FAIL', '실패', '실패', 6, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'RETURN', '반납', '반납', 7, 1);


INSERT INTO CODE_GROUP VALUES ('BT', 'BOARD TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('BT', 'QNA', 'QNA', 'QNA', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('BT', 'NOTICE', 'NOTICE', 'NOTICE', 2, 1);


INSERT INTO CODE_GROUP VALUES ('OG', 'POD OPTION GROUP TYPE');
DELETE FROM CODE WHERE CODE_GROUP_ID = 'OG';
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('OG', 'REQUIRED', 'REQUIRED', 'REQUIRED', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('OG', 'LIBRARY', 'LIBRARY', 'LIBRARY', 2, 1);


INSERT INTO CODE_GROUP VALUES ('CM', 'CHARGING METHOD');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MINUTELY', '분단위', '분단위', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'HOURLY', '시간단위', '시간단위', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'DAILY', '일단위', '일단위', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY', '월단위', '월단위', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_1', '1달 약정', '1달 약정', 5, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_3', '3달 약정', '3달 약정', 6, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_6', '6달 약정', '6달 약정', 7, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_12', '1년 약정', '1년 약정', 8, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_24', '2년 약정', '2년 약정', 9, 1);


INSERT INTO CODE_GROUP VALUES ('CL', 'CLOUD TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'ON_PREMISE', 'ON_PREMISE', 'ON_PREMISE', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'AWS', 'AWS', 'AWS', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'GCP', 'GCP', 'GCP', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'TENCENT', 'TENCENT', 'TENCENT', 4, 1);


INSERT INTO CODE_GROUP VALUES ('RT', 'RESOURCE TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('RT', 'GPU', 'GPU', 'GPU', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('RT', 'CPU', 'CPU', 'CPU', 2, 1);

INSERT INTO CODE_GROUP VALUES ('LH', 'LOGIN HISTORY');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'SUCCESS', 'SUCCESS', 'SUCCESS', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'FAIL_WRONG_PASSWORD', 'FAIL_WRONG_PASSWORD', 'FAIL_WRONG_PASSWORD', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'FAIL_NOT_CONFIRMED', 'FAIL_NOT_CONFIRMED', 'FAIL_NOT_CONFIRMED', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'FAIL_WITHDRAWAL', 'FAIL_WITHDRAWAL', 'FAIL_WITHDRAWAL', 4, 1);


-- 크레딧
INSERT INTO CREDIT VALUES ('WELCOME_CREDIT_FOR_GROUP', '그룹 회원가입 크레딧', 'WELCOME_GROUP_ADMIN', 300000, 'NONE', 1, sysdate(), 'admin', null, null);
INSERT INTO CREDIT VALUES ('WELCOME_CREDIT_FOR_USER', '사용자 회원가입 크레딧', 'WELCOME_GENERAL_USER', 100000, 'YEAR', 1, sysdate(), 'admin', null, null);
INSERT INTO CREDIT VALUES ('BONUS_CREDIT_1_YEAR', '보너스 크레딧(1년)', 'BONUS', 5000, 'YEAR', 1, sysdate(), 'admin', null, null);


-- yaml
INSERT INTO cheetah.kubernetes_yaml (KIND, YAML) VALUES ('DEPLOYMENT', 'apiVersion: apps/v1
kind: Deployment
metadata:
  name: {label}
  namespace: {namespace}
  labels:
    app: {label}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {label}
  template:
    metadata:
      labels:
        app: {label}
      namespace: {namespace}
    spec:
      dnsPolicy: Default
      securityContext:
        fsGroup: 0
        runAsUser: 0
      nodeSelector:
        accelerator: {gpu}
      containers:
      - name: {label}
        image: {image}
        ports:
        - name: jupyter
          containerPort: 8888
        - name: websocket
          containerPort: 8010
        - name: ssh
          containerPort: 22
        - name: tensorboard
          containerPort: 6006
        imagePullPolicy: IfNotPresent
        lifecycle:
         postStart:
           exec:
             command: ["/bin/sh", "-c", "export hostname=hostname && curl -X POST -H ''Content-type: application/json'' --data ''{\\"hostname\\":\\"''\\"$hostname\\"''\\"}'' http://cheetah.n3ncloud.co.kr/kube/post-start/{username}/{containerId}"]
         preStop:
           exec:
             command: ["/bin/sh", "-c", "export hostname=hostname && curl -X POST -H ''Content-type: application/json'' --data ''{\\"hostname\\":\\"''\\"$hostname\\"''\\"}'' http://cheetah.n3ncloud.co.kr/kube/pre-stop/{username}/{containerId}"]
        command: ["/bin/sh", "-c"]
        args: ["{command}"]
        env:
        - name: LD_LIBRARY_PATH
          value: /usr/lib/nvidia:/usr/lib/x86_64-linux-gnu
        resources:
          limits:
            {gpuLabel}: {gpuQuantity}
            memory: {memorySize}
          requests:
            {gpuLabel}: {gpuQuantity}
            memory: {memorySize}
        volumeMounts:
        - mountPath: /usr/local/nvidia/bin
          name: bin
        - mountPath: /usr/lib/nvidia
          name: lib
        - mountPath: /usr/lib/x86_64-linux-gnu/libcuda.so.1
          name: libcuda
        - mountPath: /home/jovyan/
          name: jupyter
      volumes:
      - name: bin
        hostPath:
          path: /usr/lib/nvidia-384/bin
      - name: lib
        hostPath:
          path: /usr/lib/nvidia-384
      - name: libcuda
        hostPath:
          path: /usr/lib/x86_64-linux-gnu/libcuda.so.1
      - name: jupyter
        persistentVolumeClaim:
            claimName: {label}');
INSERT INTO cheetah.kubernetes_yaml (KIND, YAML) VALUES ('SERVICE', 'apiVersion: v1
kind: Service
metadata:
  labels:
    app: {label}
  name: {label}
  namespace: {namespace}
spec:
  ports:
  - name: jupyter
    port: 8888
    targetPort: 8888
  - name: websocket
    port: 8010
    targetPort: 8010
  - name: ssh
    port: 22
    targetPort: 22
  - name: tensorboard
    port: 6006
    targetPort: 6006
  selector:
    app: {label}
  type: NodePort');
INSERT INTO cheetah.kubernetes_yaml (KIND, YAML) VALUES ('VOLUME', 'kind: PersistentVolume
apiVersion: v1
metadata:
 name: {label}
 namespace: {namespace}
spec:
 persistentVolumeReclaimPolicy: Retain
 storageClassName: {label}
 capacity:
  storage: {capacity}
 accessModes:
  - ReadWriteOnce
 hostPath:
  path: {jupyterPath}');
INSERT INTO cheetah.kubernetes_yaml (KIND, YAML) VALUES ('VOLUME_CLAIM', 'kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: {label}
  namespace: {namespace}
spec:
  storageClassName: {label}
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
     storage: {capacity}');

---------------------------------------------------
-- optional
---------------------------------------------------

-- pod option
insert into POD_OPTION_GROUP values ('OS', '운영체제', 'OPTION', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION_GROUP values ('JUPYTER', 'Jupyter', 'OPTION', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION_GROUP values ('TENSORFLOW', 'Tensorflow', 'LIBRARY', 3, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION_GROUP values ('KERAS', 'Keras', 'LIBRARY', 4, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION_GROUP values ('PYTORCH', 'Pytorch', 'LIBRARY', 5, 1, sysdate(), 'admin', sysdate(), 'admin');

select * from POD_OPTION;

insert into POD_OPTION values (1, 'OS', 'UBUNTU16_04_CUDA9_0', 'ubuntu16.04 cuda9.0', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (2, 'OS', 'UBUNTU16_04_CUDA10_0', 'ubuntu16.04 cuda10.0', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (15, 'OS', 'UBUNTU16_04', 'ubuntu16.04', 3, 1, sysdate(), 'admin', sysdate(), 'admin');


insert into POD_OPTION values (3, 'JUPYTER', 'V5_7_5', 'v5.7.5', 1, 1, sysdate(), 'admin', sysdate(), 'admin');

insert into POD_OPTION values (4, 'TENSORFLOW', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (5, 'TENSORFLOW', 'V1_5', 'v1.5', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (6, 'TENSORFLOW', 'V1_12', 'v1.12', 3, 1, sysdate(), 'admin', sysdate(), 'admin');


insert into POD_OPTION values (7, 'KERAS', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (8, 'KERAS', 'V2_0', 'v2.0', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (9, 'KERAS', 'V2_2', 'v2.2', 3, 1, sysdate(), 'admin', sysdate(), 'admin');

insert into POD_OPTION values (10, 'PYTORCH', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (11, 'PYTORCH', 'V0_4', 'v0.4', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
insert into POD_OPTION values (12, 'PYTORCH', 'V1_0', 'v1.0', 3, 1, sysdate(), 'admin', sysdate(), 'admin');

INSERT INTO POD_OPTION VALUES (13, 'R', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (14, 'R', 'V_3_6_1', 'v3.6.1', 2, 1, sysdate(), 'admin', sysdate(), 'admin');


-- GPU
INSERT INTO cheetah.gpu (GPU_SN, GPU_NAME, GPU_LABEL, MANUFACTURER_NAME, SPEC, MONTHLY_AMOUNT, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (1, 'P106-090', 'nvidia-p106-090', 'NVIDIA', 'Pascal / 768 CUDA Cores / 3GB GDDR5 / 1,352 MHz ', 12000, 1, '2019-03-28 17:24:22', 'admin', '2019-03-29 11:59:12', 'admin');
INSERT INTO cheetah.gpu (GPU_SN, GPU_NAME, GPU_LABEL, MANUFACTURER_NAME, SPEC, MONTHLY_AMOUNT, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (2, 'NVIDIA GeForce GTX 1080Ti', 'nvidia-gtx-1080ti', 'NVIDIA', 'Pascal / 3,584 CUDA Cores / 11GB GDDR5X / 1,582 MHz', 36000, 1, '2019-03-28 17:24:22', 'admin', null, null);


INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (1, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12', '주피터랑 텐서랑', 'UBUNTU16_04_CUDA10_0', 'V5_7_5', 'V1_12', 'NONE', 'NONE', 'NONE', 'cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-18 17:46:35', 'admin', '2019-04-02 09:59:23', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (2, 'ubuntu16.04-jupyter5.7.5', 'jupyter', 'UBUNTU16_04_CUDA10_0', 'V5_7_5', 'NONE', 'NONE', 'NONE', 'NONE', 'cheetah-jupyter:ubuntu16.04-jupyter5.7.5', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '1Gi', 1, '2019-03-20 13:46:58', 'admin', '2019-04-01 18:11:52', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (3, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2', 'jupyter/tensorflow/keras', 'UBUNTU16_04_CUDA10_0', 'V5_7_5', 'V1_12', 'NONE', 'V2_2', 'NONE', 'cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-29 11:29:04', 'admin', '2019-04-02 09:59:33', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (4, 'ubuntu16.04-cuda9.0-jupyter', 'ubuntu16.04-cuda9.0-jupyter', 'UBUNTU16_04_CUDA9_0', 'V5_7_5', 'NONE', 'NONE', 'NONE', 'NONE', 'cheetah-jupyter:ubuntu16.04-cuda9.0-jupyter5.7.5', 'python3.6 /usr/src/app/websocketserver.py & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-29 13:27:53', 'admin', '2019-03-29 13:27:53', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (5, 'ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5', 'ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5', 'UBUNTU16_04_CUDA9_0', 'V5_7_5', 'V1_5', 'NONE', 'NONE', 'NONE', 'cheetah-jupyter-tensorflow:ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5', 'python3.6 /usr/src/app/websocketserver.py & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-29 13:42:29', 'admin', '2019-03-29 15:40:43', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (6, 'ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5-keras2.0', 'ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5-keras2.0', 'UBUNTU16_04_CUDA9_0', 'V5_7_5', 'V1_5', 'NONE', 'V2_0', 'NONE', 'cheetah-jupyter-tensorflow-keras:ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5-keras2.0', 'python3.6 /usr/src/app/websocketserver.py & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-29 13:55:20', 'admin', '2019-03-29 15:41:00', 'admin');
INSERT INTO cheetah.pod_format (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, OS_VERSION, JUPYTER_VERSION, TENSORFLOW_VERSION, TENSORBOARD_VERSION, KERAS_VERSION, PYTORCH_VERSION, IMAGE, COMMAND, CAPACITY, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) VALUES (7, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0', 'cheetah-jupyter-tensorflow-pytorch', 'UBUNTU16_04_CUDA10_0', 'V5_7_5', 'V1_12', 'NONE', 'NONE', 'V1_0', 'cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '2Gi', 1, '2019-03-29 18:15:25', 'admin', '2019-04-02 13:09:26', 'admin');


commit;




-----------------------------

/*
{docker}/mysql5.7/conf.d/cheetah-mysql.cnf 파일 생성

docker ps

docker exec -i -t docker_mysql5.7_1 bash

mysql -u root -p

alter database cheetah default character set utf8;

grant all privileges on cheetah.* to cheetah@127.0.0.1 identified by 'cheetah!@#';

flush privileges;
*/

show variables like 'c%';

alter table code convert to charset utf8mb4;

show tables;

select * from credit;


select * from user;
insert into user
values ('admin', null, null, null, '시스템관리자', '$2a$10$Aob0NE5hXhzPBO44uqOHR.UU7DwJpNZTq3yTyfUbCqY8AiCyJuHy6', '010-1111-2222', 'admin@mbl.com', 1, 0, sysdate(), 'admin', null, null, 0);

update user set GPU_MAX_QUOTA_QUANTITY = 6 where username = 'korea_student';
update user set group_name = null where username = 'general_user';
update user set email = 'want813@naver.com' where username = 'general_user';

select * from user_role;

insert into user_role values ('admin', 'SYSTEM_ADMIN');

select * from user_card;
select * from USER_CREDIT
where username = 'korea_university'
order by isNull(EXPIRY_END_DATE) asc, EXPIRY_END_DATE asc;


select *
from gpu;

INSERT INTO USER_CARD values ('korea_university', '1111222233334444', 'billingkkk', '1121', true, sysdate(), 'korea_university', null, null);
commit;
delete from user_role;
delete from user; where PARENT_USERNAME = 'mbladmin';

select *
from CONTAINER;

select c.username, CONTAINER_ID, GPU_QUANTITY, START_DATE, RETURN_DATE, c.gpu_sn, g.MONTHLY_AMOUNT
from User u, CONTAINER c, gpu g
where u.username = c.username
	  and c.gpu_sn = g.GPU_SN
		and u.PARENT_USERNAME = 'korea_university'
		and '2019-02' >= date_format(start_date, '%Y-%m') and (RETURN_DATE is null or '2019-02' <= date_format(RETURN_DATE, '%Y-%m'))
order by START_DATE;

commit;

update CONTAINER set START_DATE = date_add(sysdate(), interval -3 month), RETURN_DATE= date_add(sysdate(), interval -1 day) where CONTAINER_ID = 'gpu-server-1';
update CONTAINER set START_DATE = date_add(sysdate(), interval -3 month), RETURN_DATE= date_add(sysdate(), interval 1 year) where CONTAINER_ID = 'gpu-server-3';
update CONTAINER set START_DATE = date_add(sysdate(), interval -3 month), RETURN_DATE= date_add(sysdate(), interval 1 year) where CONTAINER_ID = 'general_container';



update CONTAINER set STATUS_CODE = 'REQUEST' where container_id = 'gpu-server-pro';

commit;


select *
from payment;


delete
from USER_CREDIT_PAYMENT;

delete
from USER_CARD_PAYMENT;

delete
from payment;

select *
from USER_credit
where REMAIN_CREDIT_AMOUNT >= 0;

update USER_CREDIT
set REMAIN_CREDIT_AMOUNT = CREDIT_AMOUNT;

select usercredit0_.credit_no as credit_n1_15_, usercredit0_.username as username2_15_, usercredit0_.created_at as created_3_15_, usercredit0_.created_by as created_4_15_, usercredit0_.updated_at as updated_5_15_, usercredit0_.updated_by as updated_6_15_, usercredit0_.credit_amount as credit_a7_15_, usercredit0_.credit_id as credit_i8_15_, usercredit0_.expiry_end_date as expiry_e9_15_, usercredit0_.expiry_start_date as expiry_10_15_, usercredit0_.remain_credit_amount as remain_11_15_
from user_credit usercredit0_
where usercredit0_.username='korea_university'
 	and usercredit0_.remain_credit_amount>=0
	and (usercredit0_.expiry_end_date is null or current_date between usercredit0_.expiry_start_date and usercredit0_.expiry_end_date)
order by isNull(usercredit0_.expiry_end_date) ASC, usercredit0_.expiry_end_date ASC


commit;


update user_card set CARD_EXPIRY = '1121' where CARD_EXPIRY = '12';

select *
from user_credit;

select *
from code_group;

select *
from code;


select *
from container;

update container
set GPU_QUANTITY = 1
where container_id = 'gpu-server-1';


select * from payment;

update payment set TARGET_YEARMONTH = '201901';

update payment set PAYMENT_STATUS_CODE = 'PAID' where PAYMENT_Id = '201903-ISIY';

update credit set credit_type_code = 'BONUS' where credit_id = 'BONUS_CREDIT_1_YEAR';

select a.username, a.PAYMENT_id, a.PAYMENT_DATE, a.TARGET_YEAR_MONTH, a.PAYMENT_STATUS_CODE
from payment a,
		 (
select username, TARGET_YEAR_MONTH, max(PAYMENT_DATE) as payment_date
from payment
group by username, TARGET_YEAR_MONTH) b
where a.USERNAME = b.USERNAME
  and a.TARGET_YEAR_MONTH = b.TARGET_YEAR_MONTH
  and a.PAYMENT_DATE = b.payment_date;



select *
from payment p
  where PAYMENT_DATE = (select max(payment_date)
											    from payment m
											    where p.username = m.USERNAME
											     and p.TARGET_YEAR_MONTH = m.TARGET_YEAR_MONTH
											     group by m.username, m.TARGET_YEAR_MONTH);



