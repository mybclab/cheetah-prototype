-- 사용자
CREATE TABLE USER (
	USERNAME               VARCHAR(20)  NOT NULL, -- 사용자아이디
	PARENT_USERNAME        VARCHAR(20)  NULL,     -- 상위 사용자 아이디
	GROUP_NAME             VARCHAR(50)  NULL,     -- 그룹 명
	GROUP_TYPE_CODE        VARCHAR(50)  NULL,     -- 그룹 구분 코드
	GROUP_INVITE_KEY             VARCHAR(100) NULL,     -- 그룹 초대 키
	GROUP_IMAGE_FILE_SN          BIGINT       NULL,     -- 그룹 이미지 파일 일련번호
	NAME                   VARCHAR(50)  NOT NULL, -- 이름
	PASSWORD               VARCHAR(255) NOT NULL, -- 비밀번호
	PHONE_NO               VARCHAR(30)  NOT NULL, -- 전화 번호
	EMAIL                  VARCHAR(40)  NOT NULL, -- 이메일
	CONFIRM_YN             BOOLEAN      NOT NULL DEFAULT 0, -- 승인 여부
	SLACK_HOOK_URL               VARCHAR(500) NULL,     -- 슬랙 훅 URL
	SLACK_CHANNEL                VARCHAR(100) NULL,     -- 슬랙 채널
	USER_IMAGE_FILE_SN          BIGINT       NULL,     -- 사용자 이미지 파일 일련번호
	ORGANIZATION_NAME            VARCHAR(100) NULL,     -- 단체 명
	TEAM_NAME                    VARCHAR(100) NULL,     -- 팀 명
	CREATED_AT             DATETIME         NOT NULL,  -- 등록일시
	CREATED_BY             VARCHAR(20)  NULL, -- 등록사용자
	UPDATED_AT             DATETIME         NULL,     -- 수정일시
	UPDATED_BY             VARCHAR(20)  NULL,      -- 수정사용자
	WITHDRAWAL_DATE        DATETIME NULL      -- 탈퇴 일시
);

-- 사용자
ALTER TABLE USER
	ADD CONSTRAINT PK_USER -- 사용자 기본키
		PRIMARY KEY (
			USERNAME -- 사용자아이디
		);

-- 사용자
ALTER TABLE USER
	ADD CONSTRAINT FK_USER_TO_USER -- 사용자 -> 사용자
		FOREIGN KEY (
			PARENT_USERNAME -- 상위 사용자 아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);



-- 사용자 권한
CREATE TABLE USER_ROLE (
	USERNAME  VARCHAR(20) NOT NULL, -- 사용자아이디
	ROLE_CODE VARCHAR(50) NOT NULL  -- 권한 코드
);

-- 사용자 권한
ALTER TABLE USER_ROLE
	ADD CONSTRAINT PK_USER_ROLE -- 사용자 권한 기본키
		PRIMARY KEY (
			USERNAME,  -- 사용자아이디
			ROLE_CODE  -- 권한 코드
		);

-- 사용자 권한
ALTER TABLE USER_ROLE
	ADD CONSTRAINT FK_USER_TO_USER_ROLE -- 사용자 -> 사용자 권한
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 코드 그룹
CREATE TABLE CODE_GROUP (
	CODE_GROUP_ID   VARCHAR(2)  NOT NULL, -- 코드 그룹 아이디
	CODE_GROUP_NAME VARCHAR(50) NOT NULL  -- 코드 그룹 이름
);

-- 코드 그룹
ALTER TABLE CODE_GROUP
	ADD CONSTRAINT PK_CODE_GROUP -- 코드 그룹 기본키
		PRIMARY KEY (
			CODE_GROUP_ID -- 코드 그룹 아이디
		);


-- 코드
CREATE TABLE CODE (
	CODE_GROUP_ID    VARCHAR(2)    NOT NULL, -- 코드 그룹 아이디
	CODE             VARCHAR(50)   NOT NULL, -- 코드
	CODE_NAME        VARCHAR(50)   NOT NULL, -- 코드 이름
	CODE_DESCRIPTION VARCHAR(2000) NULL,     -- 코드 설명
	ODR              INTEGER(3)    NOT NULL DEFAULT 1, -- 순서
	ACTIVE_YN        BOOLEAN       NOT NULL DEFAULT 0 -- 활성 여부
);

-- 코드
ALTER TABLE CODE
	ADD CONSTRAINT PK_CODE -- 코드 기본키
		PRIMARY KEY (
			CODE_GROUP_ID, -- 코드 그룹 아이디
			CODE           -- 코드
		);

-- 코드
ALTER TABLE CODE
	ADD CONSTRAINT FK_CODE_GROUP_TO_CODE -- 코드 그룹 -> 코드
		FOREIGN KEY (
			CODE_GROUP_ID -- 코드 그룹 아이디
		)
		REFERENCES CODE_GROUP ( -- 코드 그룹
			CODE_GROUP_ID -- 코드 그룹 아이디
		);


-- GPU
CREATE TABLE GPU (
	GPU_SN            BIGINT       NOT NULL, -- GPU 일련번호
	CLOUD_TYPE                 VARCHAR(50)  NOT NULL DEFAULT 'ON_PREMISE', -- 클라우드 구분
	RESOURCE_TYPE              VARCHAR(50)  NOT NULL DEFAULT 'GPU', -- 자원 구분
	GPU_NAME          VARCHAR(100) NOT NULL, -- GPU 명
	GPU_LABEL         VARCHAR(100) NULL,     -- GPU 라벨
	MANUFACTURER_NAME VARCHAR(100) NOT NULL, -- 제조사 명
	SPEC              VARCHAR(500) NOT NULL, -- 사양
	MINUTELY_AMOUNT            DOUBLE       NOT NULL DEFAULT 0, -- 분단위 금액
	HOURLY_AMOUNT             DOUBLE       NOT NULL DEFAULT 0, -- 시간단위 금액
	DAILY_AMOUNT              DOUBLE       NOT NULL DEFAULT 0, -- 일단위 금액
	MONTHLY_AMOUNT            DOUBLE       NOT NULL DEFAULT 0, -- 월단위 금액
	MONTHLY_CONTRACT_AMOUNT   DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액
	MONTHLY_CONTRACT_AMOUNT_3 DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 3
	MONTHLY_CONTRACT_AMOUNT_6 DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 6
	MONTHLY_CONTRACT_AMOUNT_12    DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 12
	MONTHLY_CONTRACT_AMOUNT_24  DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 24
	QUANTITY_FORMAT            VARCHAR(100) NOT NULL, -- 수량 구성
	SHARED_YN                  BOOLEAN      NOT NULL DEFAULT 1, -- 공유 여부
	USE_YN                     BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	GPU_MEMORY                 INTEGER      NULL,     -- GPU 메모리
	CPU_CORE                   INTEGER      NULL,     -- CPU 코어
	SYSTEM_MEMORY              INTEGER      NULL,     -- 시스템 메모리
	CREATED_AT        DATETIME         NULL,     -- 등록일시
	CREATED_BY        VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT        DATETIME         NULL,     -- 수정일시
	UPDATED_BY        VARCHAR(20)  NULL      -- 수정사용자
);

-- GPU
ALTER TABLE GPU
	ADD CONSTRAINT PK_GPU -- GPU 기본키
		PRIMARY KEY (
			GPU_SN -- GPU 일련번호
		);

ALTER TABLE GPU
	MODIFY COLUMN GPU_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 노드
CREATE TABLE NODE (
	NODE_SN         BIGINT       NOT NULL, -- 노드 일련번호
	NODE_NAME       VARCHAR(100) NOT NULL, -- 노드 명
	INTERNAL_DOMAIN VARCHAR(255) NULL,     -- 내부 도메인
	INTERNAL_IP     VARCHAR(15)  NULL,     -- 내부 아이피
	EXTERNAL_IP     VARCHAR(15)  NULL,     -- 외부 아이피
	STATUS_CODE     VARCHAR(50)  NOT NULL, -- 상태 코드
	GPU_LABEL       VARCHAR(100) NULL,     -- GPU 라벨
	GPU_QUANTITY    INTEGER(4)   NULL     DEFAULT 0, -- GPU 수량
	MASTER_YN       BOOLEAN      NULL     DEFAULT 0, -- 마스터 여부
	CREATED_AT      DATETIME         NULL,     -- 등록일시
	CREATED_BY      VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT      DATETIME         NULL,     -- 수정일시
	UPDATED_BY      VARCHAR(20)  NULL      -- 수정사용자
);

-- 노드
ALTER TABLE NODE
	ADD CONSTRAINT PK_NODE -- 노드 기본키
		PRIMARY KEY (
			NODE_SN -- 노드 일련번호
		);

ALTER TABLE NODE
	MODIFY COLUMN NODE_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 파드 구성
CREATE TABLE POD_FORMAT (
	POD_FORMAT_SN          BIGINT        NOT NULL, -- 파드 구성 일련번호
	POD_FORMAT_NAME        VARCHAR(100)  NOT NULL, -- 파드 구성 명
	POD_OPTION_HINT        VARCHAR(500) NULL,     -- 파드 옵션 힌트
	POD_FORMAT_DESCRIPTION VARCHAR(500)  NULL,     -- 파드 구성 설명
	IMAGE                  VARCHAR(500)  NOT NULL, -- 이미지
	COMMAND                VARCHAR(500)  NULL,     -- 명령어
	CAPACITY               VARCHAR(10)   NOT NULL, -- 용량
	USE_YN                 BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT             DATETIME      NULL,     -- 등록일시
	CREATED_BY             VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT             DATETIME      NULL,     -- 수정일시
	UPDATED_BY             VARCHAR(20)   NULL      -- 수정사용자
);

-- 파드 구성
ALTER TABLE POD_FORMAT
	ADD CONSTRAINT PK_POD_FORMAT -- 파드 구성 기본키
		PRIMARY KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		);
ALTER TABLE POD_FORMAT
	MODIFY COLUMN POD_FORMAT_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 컨테이너
CREATE TABLE CONTAINER (
	USERNAME        VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID    VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	CONTAINER_NAME  VARCHAR(100) NOT NULL, -- 컨테이너 명
	GPU_SN          BIGINT       NOT NULL, -- GPU 일련번호
	NODE_SN         BIGINT       NULL,     -- 노드 일련번호
	POD_FORMAT_SN   BIGINT       NOT NULL,     -- 파드 구성 일련번호
	GPU_QUANTITY    INTEGER(4)   NOT NULL DEFAULT 0, -- GPU 수량
	INTERNAL_IP     VARCHAR(15)  NULL,     -- 내부 아이피
	EXTERNAL_IP     VARCHAR(15)  NULL,     -- 외부 아이피
	NOTEBOOK_PORT   INTEGER(5)   NULL,     -- 노트북 포트
	NOTEBOOK_TOKEN  VARCHAR(100) NULL,     -- 노트북 토큰
	SSH_PORT        INTEGER(5)   NULL,     -- SSH 포트
	SSH_PASSWORD    VARCHAR(100) NULL,     -- SSH 비밀번호
	WEBSOCKET_PORT  INTEGER(5)   NULL,     -- 웹소켓 포트
	TENSORBOARD_PORT INTEGER(5)   NULL,     -- 텐서보드 포트
	DESCRIPTION     VARCHAR(100) NOT NULL, -- 설명
	STATUS_CODE     VARCHAR(50)  NULL,     -- 상태 코드
	POD_NAME        VARCHAR(100) NULL,     -- 파드 명
	POD_STATUS_CODE VARCHAR(50)  NULL,     -- 파드 상태 코드
	START_DATE      DATETIME         NULL,     -- 시작 일시
	RETURN_DATE     DATETIME         NULL,     -- 반납 일시
	CHARGING_METHOD_CODE VARCHAR(50)  NULL,     -- 과금 방법 코드
	INSTANCE_ID          VARCHAR(100)  NULL,     -- 인스턴스 아이디
	LISTEN_YN            BOOLEAN       NULL     DEFAULT 1, -- LISTEN_여부
	PARENT_CONTAINER_ID  VARCHAR(20)   NULL,     -- 상위 컨테이너 아이디
	GROUP_SHARED_YN      BOOLEAN       NULL     DEFAULT 0, -- 그룹 공유 여부
	GROUP_SHARED_SIZE       DOUBLE       NULL,     -- 그룹 공유 크기
	GROUP_TARGET_USER    VARCHAR(1000) NULL,     -- 그룹 대상 사용자
	CPU_LIMIT            INTEGER       NULL,     -- CPU 제한
	MEMORY_LIMIT         INTEGER       NULL,     -- 메모리 제한
	TOKEN_YN            BOOLEAN       NULL     DEFAULT 1, -- 토큰 여부
	CREATED_AT      DATETIME         NULL,     -- 등록일시
	CREATED_BY      VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT      DATETIME         NULL,     -- 수정일시
	UPDATED_BY      VARCHAR(20)  NULL      -- 수정사용자
);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT PK_CONTAINER -- 컨테이너 기본키
		PRIMARY KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);


-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_GPU_TO_CONTAINER -- GPU -> 컨테이너
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_NODE_TO_CONTAINER -- 노드 -> 컨테이너
		FOREIGN KEY (
			NODE_SN -- 노드 일련번호
		)
		REFERENCES NODE ( -- 노드
			NODE_SN -- 노드 일련번호
		);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_USER_TO_CONTAINER -- 사용자 -> 컨테이너
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);





-- 파드 컨디션
CREATE TABLE POD_CONDITION (
	POD_CONDITION_SN     BIGINT       NOT NULL, -- 파드 컨디션 일련번호
	USERNAME             VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID         VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	POD_NAME             VARCHAR(100) NOT NULL, -- 파드 명
	LAST_PROBE_TIME      DATETIME     NULL,     -- 마지막 조사 일시
	LAST_TRANSITION_TIME DATETIME     NULL,     -- 마지막 전이 일시
	MESSAGE              VARCHAR(500) NULL,     -- 메세지
	REASON               VARCHAR(100) NULL,     -- 이유
	STATUS               BOOLEAN      NULL, -- 상태 코드
	TYPE_CODE            VARCHAR(50)  NULL,     -- 타입 코드
	CREATED_AT           DATETIME     NOT NULL  -- 등록일시
);

-- 파드 컨디션
ALTER TABLE POD_CONDITION
	ADD CONSTRAINT PK_POD_CONDITION -- 파드 컨디션 기본키
		PRIMARY KEY (
			POD_CONDITION_SN -- 파드 컨디션 일련번호
		);

ALTER TABLE POD_CONDITION
	MODIFY COLUMN POD_CONDITION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 파드 컨디션
ALTER TABLE POD_CONDITION
	ADD CONSTRAINT FK_CONTAINER_TO_POD_CONDITION -- 컨테이너 -> 파드 컨디션
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);



-- 크레딧
CREATE TABLE CREDIT (
	CREDIT_ID               VARCHAR(50) NOT NULL, -- 크레딧 아이디
	CREDIT_NAME             VARCHAR(50) NOT NULL, -- 크레딧 명
	CREDIT_TYPE_CODE        VARCHAR(50) NOT NULL, -- 크레딧 구분 코드
	CREDIT_AMOUNT           DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 금액
	CREDIT_EXPIRY_TYPE_CODE VARCHAR(50) NOT NULL, -- 크레딧 유효기간 구분 코드
	CREDIT_EXPIRY_VALUE     INTEGER     NULL,     -- 크레딧 유효기간 값
	CREATED_AT              DATETIME        NULL,     -- 등록일시
	CREATED_BY              VARCHAR(20) NULL,     -- 등록사용자
	UPDATED_AT              DATETIME        NULL,     -- 수정일시
	UPDATED_BY              VARCHAR(20) NULL      -- 수정사용자
);

-- 크레딧
ALTER TABLE CREDIT
	ADD CONSTRAINT PK_CREDIT -- 크레딧 기본키
		PRIMARY KEY (
			CREDIT_ID -- 크레딧 아이디
		);



-- 사용자 크레딧
CREATE TABLE USER_CREDIT (
	USERNAME             VARCHAR(20) NOT NULL, -- 사용자아이디
	USER_CREDIT_NO       INTEGER(3)  NOT NULL DEFAULT 0, -- 사용자 크레딧 번호
	CREDIT_ID            VARCHAR(50) NOT NULL, -- 크레딧 아이디
	CREDIT_AMOUNT        DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 금액
	REMAIN_CREDIT_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 잔여 크레딧 금액
	EXPIRY_START_DATE    VARCHAR(8)  NULL,     -- 유효기간 시작 일시
	EXPIRY_END_DATE      VARCHAR(8)  NULL,     -- 유효기간 종료 일시
	CREATED_AT           DATETIME    NULL,     -- 등록일시
	CREATED_BY           VARCHAR(20) NULL,     -- 등록사용자
	UPDATED_AT           DATETIME    NULL,     -- 수정일시
	UPDATED_BY           VARCHAR(20) NULL      -- 수정사용자
);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT PK_USER_CREDIT -- 사용자 크레딧 기본키
		PRIMARY KEY (
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT FK_USER_TO_USER_CREDIT -- 사용자 -> 사용자 크레딧
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT FK_CREDIT_TO_USER_CREDIT -- 크레딧 -> 사용자 크레딧
		FOREIGN KEY (
			CREDIT_ID -- 크레딧 아이디
		)
		REFERENCES CREDIT ( -- 크레딧
			CREDIT_ID -- 크레딧 아이디
		);



-- 사용자 카드
CREATE TABLE USER_CARD (
	USERNAME     VARCHAR(20)   NOT NULL, -- 사용자아이디
	USER_CARD_NO INTEGER(3)    NOT NULL DEFAULT 0, -- 사용자 카드 번호
	CARD_NO      VARCHAR(19)   NOT NULL, -- 카드 번호
	CARD_EXPIRY  VARCHAR(4)    NOT NULL, -- 카드 유효기간
	CUSTOMER_UID  VARCHAR(1000) NOT NULL, -- 빌링 키
	USE_YN       BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT   DATETIME      NULL,     -- 등록일시
	CREATED_BY   VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT   DATETIME      NULL,     -- 수정일시
	UPDATED_BY   VARCHAR(20)   NULL      -- 수정사용자
);

-- 사용자 카드
ALTER TABLE USER_CARD
	ADD CONSTRAINT PK_USER_CARD -- 사용자 카드 기본키
		PRIMARY KEY (
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드
ALTER TABLE USER_CARD
	ADD CONSTRAINT FK_USER_TO_USER_CARD -- 사용자 -> 사용자 카드
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);



-- 결제
CREATE TABLE PAYMENT (
	USERNAME              VARCHAR(20) NOT NULL, -- 사용자아이디
	PAYMENT_ID            VARCHAR(50) NOT NULL, -- 결제 아이디
	TARGET_YEAR_MONTH     VARCHAR(6)  NOT NULL, -- 대상년월
	USE_START_DATE        VARCHAR(8)  NOT NULL, -- 이용 시작 일시
	USE_END_DATE          VARCHAR(8)  NOT NULL, -- 이용 종료 일시
	TOTAL_PAYMENT_AMOUNT  DOUBLE      NOT NULL DEFAULT 0, -- 총 결제 금액
	CREDIT_PAYMENT_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 결제 금액
	CARD_PAYMENT_AMOUNT   DOUBLE      NOT NULL DEFAULT 0, -- 카드 결제 금액
	USER_CARD_NO          INTEGER(3)  NULL,     -- 사용자 카드 번호
	USER_CREDIT_NO        INTEGER(3)  NULL     DEFAULT 0, -- 사용자 크레딧 번호
	USE_QUANTITY          INTEGER(4)  NOT NULL DEFAULT 0, -- 이용 수량
	PAYMENT_STATUS_CODE   VARCHAR(50) NOT NULL, -- 결제 상태 코드
	PAYMENT_DATE          DATETIME    NOT NULL  -- 결제 일시
);

-- 결제
ALTER TABLE PAYMENT
	ADD CONSTRAINT PK_PAYMENT -- 결제 기본키
		PRIMARY KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);

-- 결제
ALTER TABLE PAYMENT
	ADD CONSTRAINT FK_USER_TO_PAYMENT -- 사용자 -> 결제
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 사용자 카드 결제
CREATE TABLE USER_CARD_PAYMENT (
	PAYMENT_ID          VARCHAR(50)  NOT NULL, -- 결제 아이디
	USERNAME            VARCHAR(20)  NOT NULL, -- 사용자아이디
	USER_CARD_NO        INTEGER(3)   NOT NULL DEFAULT 0, -- 사용자 카드 번호
	TRANSACTION_PG      VARCHAR(50)  NOT NULL, -- 거래 PG
	TRANSACTION_UID     VARCHAR(200) NOT NULL, -- 거래 고유번호
	CARD_APPLY_NO       VARCHAR(200) NOT NULL, -- 카드 승인 번호
	PAYMENT_AMOUNT      DOUBLE       NOT NULL DEFAULT 0, -- 결제 금액
	PAYMENT_STATUS_CODE VARCHAR(50)  NOT NULL, -- 결제 상태 코드
	PAYMENT_DATE        DATETIME     NOT NULL  -- 결제 일시
);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT PK_USER_CARD_PAYMENT -- 사용자 카드 결제 기본키
		PRIMARY KEY (
			PAYMENT_ID,   -- 결제 아이디
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT FK_USER_CARD_TO_USER_CARD_PAYMENT -- 사용자 카드 -> 사용자 카드 결제
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		)
		REFERENCES USER_CARD ( -- 사용자 카드
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT FK_PAYMENT_TO_USER_CARD_PAYMENT -- 결제 -> 사용자 카드 결제
		FOREIGN KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		)
		REFERENCES PAYMENT ( -- 결제
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);



-- 사용자 크레딧 결제
CREATE TABLE USER_CREDIT_PAYMENT (
	PAYMENT_ID             VARCHAR(50) NOT NULL, -- 결제 아이디
	USERNAME               VARCHAR(20) NOT NULL, -- 사용자아이디
	USER_CREDIT_NO         INTEGER(3)  NOT NULL DEFAULT 0, -- 사용자 크레딧 번호
	CREDIT_PAYMENT_AMOUNT  DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 결제 금액
	CREDIT_PREVIOUS_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 이전 금액
	CREDIT_REMAIN_AMOUNT   DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 잔여 금액
	PAYMENT_DATE           DATETIME    NOT NULL  -- 결제 일시
);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT PK_USER_CREDIT_PAYMENT -- 사용자 크레딧 결제 기본키
		PRIMARY KEY (
			PAYMENT_ID,     -- 결제 아이디
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT FK_USER_CREDIT_TO_USER_CREDIT_PAYMENT -- 사용자 크레딧 -> 사용자 크레딧 결제
		FOREIGN KEY (
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		)
		REFERENCES USER_CREDIT ( -- 사용자 크레딧
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT FK_PAYMENT_TO_USER_CREDIT_PAYMENT -- 결제 -> 사용자 크레딧 결제
		FOREIGN KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		)
		REFERENCES PAYMENT ( -- 결제
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);



-- 쿠버네티스 YAML
CREATE TABLE KUBERNETES_YAML (
	KIND VARCHAR(100)  NOT NULL, -- KIND
	YAML VARCHAR(4000) NULL      -- YAML
);

-- 쿠버네티스 YAML
ALTER TABLE KUBERNETES_YAML
	ADD CONSTRAINT PK_KUBERNETES_YAML -- 쿠버네티스 YAML 기본키
		PRIMARY KEY (
			KIND -- KIND
		);


-- 게시물
CREATE TABLE POST (
	POST_SN    BIGINT        NOT NULL, -- 게시물 일련번호
	BOARD_TYPE_CODE VARCHAR(50)   NOT NULL, -- 게시판 구분
	TITLE      VARCHAR(200)  NOT NULL, -- 제목
	CONTENTS   VARCHAR(2000) NOT NULL, -- 내용
	USE_YN     BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	READ_COUNT INTEGER(5)    NOT NULL DEFAULT 0, -- 조회 수
	CREATED_AT DATETIME      NULL,     -- 등록일시
	CREATED_BY VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT DATETIME      NULL,     -- 수정일시
	UPDATED_BY VARCHAR(20)   NULL,     -- 수정사용자
	GROUP_ID VARCHAR(20)   NULL     -- 그룹 아이디
);

-- 게시물
ALTER TABLE POST
	ADD CONSTRAINT PK_POST -- 게시물 기본키
		PRIMARY KEY (
			POST_SN -- 게시물 일련번호
		);

ALTER TABLE POST
	MODIFY COLUMN POST_SN BIGINT NOT NULL AUTO_INCREMENT;



-- 게시물 댓글
CREATE TABLE POST_COMMENT (
	POST_SN    BIGINT        NOT NULL, -- 게시물 일련번호
	GROUP_ID        VARCHAR(20)   NULL,     -- 그룹 아이디
	POST_COMMENT_NO INTEGER(3)    NOT NULL DEFAULT 0, -- 댓글 번호
	CONTENTS   VARCHAR(2000) NOT NULL, -- 내용
	USE_YN     BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT DATETIME      NULL,     -- 등록일시
	CREATED_BY VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT DATETIME      NULL,     -- 수정일시
	UPDATED_BY VARCHAR(20)   NULL      -- 수정사용자
);

-- 게시물 댓글
ALTER TABLE POST_COMMENT
	ADD CONSTRAINT PK_POST_COMMENT -- 게시물 댓글 기본키
		PRIMARY KEY (
			POST_SN,    -- 게시물 일련번호
			POST_COMMENT_NO  -- 댓글 번호
		);

-- 게시물 댓글
ALTER TABLE POST_COMMENT
	ADD CONSTRAINT FK_POST_TO_POST_COMMENT -- 게시물 -> 게시물 댓글
		FOREIGN KEY (
			POST_SN -- 게시물 일련번호
		)
		REFERENCES POST ( -- 게시물
			POST_SN -- 게시물 일련번호
		);



---- pod option seperate
-- 파드 옵션 그룹
CREATE TABLE POD_OPTION_GROUP (
	POD_OPTION_GROUP_ID        VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 아이디
	POD_OPTION_GROUP_NAME      VARCHAR(100) NOT NULL, -- 파드 옵션 그룹 명
	POD_OPTION_GROUP_TYPE_CODE VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 구분 코드
	ODR                        INTEGER(3)   NULL     DEFAULT 1, -- 순서
	USE_YN                     BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT                 DATETIME     NULL,     -- 등록일시
	CREATED_BY                 VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT                 DATETIME     NULL,     -- 수정일시
	UPDATED_BY                 VARCHAR(20)  NULL      -- 수정사용자
);

-- 파드 옵션 그룹
ALTER TABLE POD_OPTION_GROUP
	ADD CONSTRAINT PK_POD_OPTION_GROUP -- 파드 옵션 그룹 기본키
		PRIMARY KEY (
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		);
-- 파드 옵션
CREATE TABLE POD_OPTION (
	POD_OPTION_SN       BIGINT       NOT NULL, -- 파드 옵션 일련번호
	POD_OPTION_GROUP_ID VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 아이디
	POD_OPTION_ID       VARCHAR(50)  NOT NULL, -- 파드 옵션 아이디
	POD_OPTION_NAME     VARCHAR(100) NOT NULL, -- 파드 옵션 명
	ODR                 INTEGER(3)   NOT NULL DEFAULT 1, -- 순서
	USE_YN              BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT          DATETIME     NULL,     -- 등록일시
	CREATED_BY          VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT          DATETIME     NULL,     -- 수정일시
	UPDATED_BY          VARCHAR(20)  NULL      -- 수정사용자
);

-- 파드 옵션
ALTER TABLE POD_OPTION
	ADD CONSTRAINT PK_POD_OPTION -- 파드 옵션 기본키
		PRIMARY KEY (
			POD_OPTION_SN -- 파드 옵션 일련번호
		);

ALTER TABLE POD_OPTION
	MODIFY COLUMN POD_OPTION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 파드 옵션
ALTER TABLE POD_OPTION
	ADD CONSTRAINT FK_POD_OPTION_GROUP_TO_POD_OPTION -- 파드 옵션 그룹 -> 파드 옵션
		FOREIGN KEY (
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		)
		REFERENCES POD_OPTION_GROUP ( -- 파드 옵션 그룹
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		);

-- 파드 구성 옵션
CREATE TABLE POD_FORMAT_OPTION (
	POD_FORMAT_SN       BIGINT      NOT NULL, -- 파드 구성 일련번호
	POD_OPTION_SN       BIGINT      NOT NULL, -- 파드 옵션 일련번호
	POD_OPTION_GROUP_ID VARCHAR(50) NULL,     -- 파드 옵션 그룹 아이디
	POD_OPTION_ID       VARCHAR(50) NULL,     -- 파드 옵션 아이디
	CREATED_AT          DATETIME    NULL      -- 등록일시
);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT PK_POD_FORMAT_OPTION -- 파드 구성 옵션 기본키
		PRIMARY KEY (
			POD_FORMAT_SN, -- 파드 구성 일련번호
			POD_OPTION_SN  -- 파드 옵션 일련번호
		);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT FK_POD_FORMAT_TO_POD_FORMAT_OPTION -- 파드 구성 -> 파드 구성 옵션
		FOREIGN KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		)
		REFERENCES POD_FORMAT ( -- 파드 구성
			POD_FORMAT_SN -- 파드 구성 일련번호
		);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT FK_POD_OPTION_TO_POD_FORMAT_OPTION -- 파드 옵션 -> 파드 구성 옵션
		FOREIGN KEY (
			POD_OPTION_SN -- 파드 옵션 일련번호
		)
		REFERENCES POD_OPTION ( -- 파드 옵션
			POD_OPTION_SN -- 파드 옵션 일련번호
		);


-- 파드 구성 GPU
CREATE TABLE POD_FORMAT_GPU (
	GPU_SN        BIGINT   NOT NULL, -- GPU 일련번호
	POD_FORMAT_SN BIGINT   NOT NULL, -- 파드 구성 일련번호
	CREATED_AT    DATETIME NULL      -- 등록일시
);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT PK_POD_FORMAT_GPU -- 파드 구성 GPU 기본키
		PRIMARY KEY (
			GPU_SN,        -- GPU 일련번호
			POD_FORMAT_SN  -- 파드 구성 일련번호
		);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT FK_GPU_TO_POD_FORMAT_GPU -- GPU -> 파드 구성 GPU
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT FK_POD_FORMAT_TO_POD_FORMAT_GPU -- 파드 구성 -> 파드 구성 GPU
		FOREIGN KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		)
		REFERENCES POD_FORMAT ( -- 파드 구성
			POD_FORMAT_SN -- 파드 구성 일련번호
		);



-- 컨테이너 이력
CREATE TABLE CONTAINER_HISTORY (
	USERNAME             VARCHAR(20) NOT NULL, -- 사용자아이디
	CONTAINER_ID         VARCHAR(20) NOT NULL, -- 컨테이너 아이디
	CONTAINER_HISTORY_NO INTEGER(3)  NOT NULL DEFAULT 0, -- 컨테이너 이력 번호
	GPU_SN               BIGINT      NOT NULL, -- GPU 일련번호
	GPU_QUANTITY         INTEGER(4)  NOT NULL DEFAULT 0, -- GPU 수량
	GPU_AMOUNT           DOUBLE      NOT NULL DEFAULT 0, -- GPU 금액
	NODE_SN              BIGINT      NOT NULL, -- 노드 일련번호
	CHARGING_METHOD_CODE VARCHAR(50) NOT NULL, -- 과금 방법 코드
	START_DATE           DATETIME    NOT NULL, -- 시작 일시
	RETURN_DATE          DATETIME    NULL      -- 반납 일시
);

-- 컨테이너 이력
ALTER TABLE CONTAINER_HISTORY
	ADD CONSTRAINT PK_CONTAINER_HISTORY -- 컨테이너 이력 기본키
		PRIMARY KEY (
			USERNAME,             -- 사용자아이디
			CONTAINER_ID,         -- 컨테이너 아이디
			CONTAINER_HISTORY_NO  -- 컨테이너 이력 번호
		);


-- 컨테이너 이력
ALTER TABLE CONTAINER_HISTORY
	ADD CONSTRAINT FK_CONTAINER_TO_CONTAINER_HISTORY -- 컨테이너 -> 컨테이너 이력
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);




-- 퍼블릭 클라우드 OFFERING
CREATE TABLE PUBLIC_CLOUD_OFFERING (
	CLOUD_TYPE       VARCHAR(50)  NOT NULL, -- 클라우드 구분
	OFFERING_ID      VARCHAR(50)  NOT NULL, -- 제공 아이디
	OFFERING_NAME    VARCHAR(100) NOT NULL, -- 제공 명
	GPU_SN           BIGINT       NOT NULL, -- GPU 일련번호
	GPU_QUANTITY     INTEGER(4)   NOT NULL DEFAULT 0, -- GPU 수량
	CPU_CORE         INTEGER      NULL,     -- CPU 코어
	SYSTEM_MEMORY    INTEGER      NULL,     -- 시스템 메모리
	GPU_MEMORY       INTEGER      NULL,     -- GPU 메모리
	GPU_TOTAL_MEMORY INTEGER      NULL,     -- GPU 총 메모리
	CREATED_AT       DATETIME     NULL,     -- 등록일시
	CREATED_BY       VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT       DATETIME     NULL,     -- 수정일시
	UPDATED_BY       VARCHAR(20)  NULL      -- 수정사용자
);

-- 퍼블릭 클라우드 OFFERING
ALTER TABLE PUBLIC_CLOUD_OFFERING
	ADD CONSTRAINT PK_PUBLIC_CLOUD_OFFERING -- 퍼블릭 클라우드 OFFERING 기본키
		PRIMARY KEY (
			CLOUD_TYPE,  -- 클라우드 구분
			OFFERING_ID  -- 제공 아이디
		);

-- 퍼블릭 클라우드 OFFERING
ALTER TABLE PUBLIC_CLOUD_OFFERING
	ADD CONSTRAINT FK_GPU_TO_PUBLIC_CLOUD_OFFERING -- GPU -> 퍼블릭 클라우드 OFFERING
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);




-- 배포 컨디션
CREATE TABLE DEPLOYMENT_CONDITION (
	DEPLOYMENT_CONDITION_SN BIGINT       NOT NULL, -- 배포 컨디션 일련번호
	USERNAME                VARCHAR(20)  NULL,     -- 사용자아이디
	CONTAINER_ID            VARCHAR(20)  NULL,     -- 컨테이너 아이디
	DEPLOYMENT_NAME         VARCHAR(100) NULL,     -- 배포 명
	LAST_UPDATE_TIME         DATETIME     NULL,     -- 마지막 수정 일시
	LAST_TRANSITION_TIME    DATETIME     NULL,     -- 마지막 전이 일시
	MESSAGE                 VARCHAR(500) NULL,     -- 메세지
	REASON                  VARCHAR(100) NULL,     -- 이유
	STATUS             BOOLEAN      NULL     DEFAULT 1, -- 상태 코드
	TYPE_CODE               VARCHAR(50)  NULL,     -- 타입 코드
	CREATED_AT              DATETIME     NULL      -- 등록일시
);

-- 배포 컨디션
ALTER TABLE DEPLOYMENT_CONDITION
	ADD CONSTRAINT PK_DEPLOYMENT_CONDITION -- 배포 컨디션 기본키
		PRIMARY KEY (
			DEPLOYMENT_CONDITION_SN -- 배포 컨디션 일련번호
		);

ALTER TABLE DEPLOYMENT_CONDITION
	MODIFY COLUMN DEPLOYMENT_CONDITION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 배포 컨디션
ALTER TABLE DEPLOYMENT_CONDITION
	ADD CONSTRAINT FK_CONTAINER_TO_DEPLOYMENT_CONDITION -- 컨테이너 -> 배포 컨디션
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);




-- 컨테이너 볼륨
CREATE TABLE CONTAINER_VOLUME (
	USERNAME            VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID        VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	CONTAINER_VOLUME_NO INTEGER(3)   NOT NULL DEFAULT 0, -- 컨테이너 볼륨 번호
	CLOUD_TYPE          VARCHAR(50)  NOT NULL, -- 클라우드 구분
	VOLUME_ID           VARCHAR(100) NOT NULL, -- 볼륨 아이디
	VOLUME_SIZE         INTEGER      NOT NULL, -- 볼륨 크기
	VOLUME_MOUNT_PATH   VARCHAR(500) NOT NULL  -- 볼륨 마운트 경로
);

-- 컨테이너 볼륨
ALTER TABLE CONTAINER_VOLUME
	ADD CONSTRAINT PK_CONTAINER_VOLUME -- 컨테이너 볼륨 기본키
		PRIMARY KEY (
			USERNAME,            -- 사용자아이디
			CONTAINER_ID,        -- 컨테이너 아이디
			CONTAINER_VOLUME_NO  -- 컨테이너 볼륨 번호
		);

-- 컨테이너 볼륨
ALTER TABLE CONTAINER_VOLUME
	ADD CONSTRAINT FK_CONTAINER_TO_CONTAINER_VOLUME -- 컨테이너 -> 컨테이너 볼륨
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);



-- 공유 볼륨
CREATE TABLE SHARE_VOLUME (
	USERNAME    VARCHAR(20)  NOT NULL, -- 사용자아이디
	VOLUME_ID   VARCHAR(100) NOT NULL, -- 볼륨 아이디
	VOLUME_NAME VARCHAR(100) NOT NULL, -- 볼륨 명
	VOLUME_SIZE         INTEGER      NOT NULL, -- 볼륨 사이즈
	DESCRIPTION VARCHAR(100) NULL,     -- 설명
	CREATED_AT  DATETIME     NULL,     -- 등록일시
	CREATED_BY  VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT  DATETIME     NULL,     -- 수정일시
	UPDATED_BY  VARCHAR(20)  NULL      -- 수정사용자
);

-- 공유 볼륨
ALTER TABLE SHARE_VOLUME
	ADD CONSTRAINT PK_SHARE_VOLUME -- 공유 볼륨 기본키
		PRIMARY KEY (
			USERNAME,  -- 사용자아이디
			VOLUME_ID  -- 볼륨 아이디
		);

-- 공유 볼륨
ALTER TABLE SHARE_VOLUME
	ADD CONSTRAINT FK_USER_TO_SHARE_VOLUME -- 사용자 -> 공유 볼륨
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);




-- 파일
CREATE TABLE FILE (
	FILE_SN            BIGINT       NOT NULL, -- 파일 일련번호
	LOGICAL_FILE_NAME  VARCHAR(100) NOT NULL, -- 논리 파일 명
	PHYSICAL_FILE_NAME VARCHAR(100) NOT NULL, -- 물리 파일 명
	FILE_EXTENSION     VARCHAR(10)  NOT NULL, -- 파일 확장자
	FILE_PATH          VARCHAR(500) NOT NULL, -- 파일 경로
	FILE_SIZE          BIGINT      NOT NULL, -- 파일 용량
	CREATED_AT         DATETIME     NOT NULL, -- 등록일시
	CREATED_BY         VARCHAR(20)  NOT NULL  -- 등록사용자
);

-- 파일
ALTER TABLE FILE
	ADD CONSTRAINT PK_FILE -- 파일 기본키
		PRIMARY KEY (
			FILE_SN -- 파일 일련번호
		);




-- 사용자 GPU
CREATE TABLE USER_GPU (
	USERNAME       VARCHAR(20) NOT NULL, -- 사용자아이디
	GPU_SN         BIGINT      NOT NULL, -- GPU 일련번호
	GROUP_QUOTA_QUANTITY INTEGER(4)  NULL     DEFAULT 0, -- 그룹 할당 수량
	QUOTA_QUANTITY INTEGER(4)  NOT NULL DEFAULT 0, -- 할당 수량
	CREATED_AT     DATETIME    NULL,     -- 등록일시
	CREATED_BY     VARCHAR(20) NULL      -- 등록사용자
);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT PK_USER_GPU -- 사용자 GPU 기본키
		PRIMARY KEY (
			USERNAME, -- 사용자아이디
			GPU_SN    -- GPU 일련번호
		);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT FK_USER_TO_USER_GPU -- 사용자 -> 사용자 GPU
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT FK_GPU_TO_USER_GPU -- GPU -> 사용자 GPU
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);




-- 로그인 이력
CREATE TABLE `cheetah`.`LOGIN_HISTORY` (
	`LOGIN_HISTORY_SN`   BIGINT      NOT NULL, -- 로그인 이력 일련번호
	`USERNAME`           VARCHAR(20) NOT NULL, -- 사용자아이디
	`LOGIN_HISTORY_CODE` VARCHAR(50) NULL,     -- 로그인 이력 코드
	`LOGIN_IP`           VARCHAR(15) NULL,     -- 로그인 아이피
	`LOGIN_DATE`         DATETIME    NULL      -- 로그인 일시
);

-- 로그인 이력
ALTER TABLE `cheetah`.`LOGIN_HISTORY`
	ADD CONSTRAINT `PK_LOGIN_HISTORY` -- 로그인 이력 기본키
		PRIMARY KEY (
			`LOGIN_HISTORY_SN` -- 로그인 이력 일련번호
		);

ALTER TABLE `cheetah`.`LOGIN_HISTORY`
	MODIFY COLUMN `LOGIN_HISTORY_SN` BIGINT NOT NULL AUTO_INCREMENT;

-- 로그인 이력
ALTER TABLE `cheetah`.`LOGIN_HISTORY`
	ADD CONSTRAINT `FK_USER_TO_LOGIN_HISTORY` -- 사용자 -> 로그인 이력
		FOREIGN KEY (
			`USERNAME` -- 사용자아이디
		)
		REFERENCES `cheetah`.`USER` ( -- 사용자
			`USERNAME` -- 사용자아이디
		);